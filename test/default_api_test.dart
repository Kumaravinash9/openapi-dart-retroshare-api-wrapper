import 'package:openapi/api.dart';
import 'package:test/test.dart';


/// tests for DefaultApi
void main() {
  var instance = DefaultApi();

  group('tests for DefaultApi', () {
    // Export full encrypted PGP identity to file
    //
    //Future<ResExportIdentity> rsAccountsExportIdentity({ ReqExportIdentity reqExportIdentity }) async 
    test('test rsAccountsExportIdentity', () async {
      // TODO
    });

    // Export full encrypted PGP identity to string
    //
    //Future<ResExportIdentityToString> rsAccountsExportIdentityToString({ ReqExportIdentityToString reqExportIdentityToString }) async 
    test('test rsAccountsExportIdentityToString', () async {
      // TODO
    });

    // Get current account id. Beware that an account may be selected without actually logging in.
    //
    //Future<ResGetCurrentAccountId> rsAccountsGetCurrentAccountId() async 
    test('test rsAccountsGetCurrentAccountId', () async {
      // TODO
    });

    // Get available PGP identities id list
    //
    //Future<ResGetPGPLogins> rsAccountsGetPGPLogins() async 
    test('test rsAccountsGetPGPLogins', () async {
      // TODO
    });

    // Import full encrypted PGP identity from file
    //
    //Future<ResImportIdentity> rsAccountsImportIdentity({ ReqImportIdentity reqImportIdentity }) async 
    test('test rsAccountsImportIdentity', () async {
      // TODO
    });

    // Import full encrypted PGP identity from string
    //
    //Future<ResImportIdentityFromString> rsAccountsImportIdentityFromString({ ReqImportIdentityFromString reqImportIdentityFromString }) async 
    test('test rsAccountsImportIdentityFromString', () async {
      // TODO
    });

    // Enable or disable IP filtering service
    //
    //Future rsBanListEnableIPFiltering({ ReqEnableIPFiltering reqEnableIPFiltering }) async 
    test('test rsBanListEnableIPFiltering', () async {
      // TODO
    });

    // Get ip filtering service status
    //
    //Future<ResIpFilteringEnabled> rsBanListIpFilteringEnabled() async 
    test('test rsBanListIpFilteringEnabled', () async {
      // TODO
    });

    // Disable multicast listening
    //
    //Future<ResDisableMulticastListening> rsBroadcastDiscoveryDisableMulticastListening() async 
    test('test rsBroadcastDiscoveryDisableMulticastListening', () async {
      // TODO
    });

    // On platforms that need it enable low level multicast listening
    //
    //Future<ResEnableMulticastListening> rsBroadcastDiscoveryEnableMulticastListening() async 
    test('test rsBroadcastDiscoveryEnableMulticastListening', () async {
      // TODO
    });

    // Get potential peers that have been discovered up until now
    //
    //Future<ResGetDiscoveredPeers> rsBroadcastDiscoveryGetDiscoveredPeers() async 
    test('test rsBroadcastDiscoveryGetDiscoveredPeers', () async {
      // TODO
    });

    // Check if multicast listening is enabled
    //
    //Future<ResIsMulticastListeningEnabled> rsBroadcastDiscoveryIsMulticastListeningEnabled() async 
    test('test rsBroadcastDiscoveryIsMulticastListeningEnabled', () async {
      // TODO
    });

    // getAllBandwidthRates get the bandwidth rates for all peers
    //
    //Future<ResGetAllBandwidthRates> rsConfigGetAllBandwidthRates() async 
    test('test rsConfigGetAllBandwidthRates', () async {
      // TODO
    });

    // getConfigNetStatus return the net status
    //
    //Future<ResGetConfigNetStatus> rsConfigGetConfigNetStatus() async 
    test('test rsConfigGetConfigNetStatus', () async {
      // TODO
    });

    // GetCurrentDataRates get current upload and download rates
    //
    //Future<ResGetCurrentDataRates> rsConfigGetCurrentDataRates() async 
    test('test rsConfigGetCurrentDataRates', () async {
      // TODO
    });

    // GetMaxDataRates get maximum upload and download rates
    //
    //Future<ResGetMaxDataRates> rsConfigGetMaxDataRates() async 
    test('test rsConfigGetMaxDataRates', () async {
      // TODO
    });

    // getOperatingMode get current operating mode
    //
    //Future<ResGetOperatingMode> rsConfigGetOperatingMode() async 
    test('test rsConfigGetOperatingMode', () async {
      // TODO
    });

    // getTotalBandwidthRates returns the current bandwidths rates
    //
    //Future<ResGetTotalBandwidthRates> rsConfigGetTotalBandwidthRates() async 
    test('test rsConfigGetTotalBandwidthRates', () async {
      // TODO
    });

    // getTrafficInfo returns a list of all tracked traffic clues
    //
    //Future<ResGetTrafficInfo> rsConfigGetTrafficInfo() async 
    test('test rsConfigGetTrafficInfo', () async {
      // TODO
    });

    // SetMaxDataRates set maximum upload and download rates
    //
    //Future<ResSetMaxDataRates> rsConfigSetMaxDataRates({ ReqSetMaxDataRates reqSetMaxDataRates }) async 
    test('test rsConfigSetMaxDataRates', () async {
      // TODO
    });

    // setOperatingMode set the current oprating mode
    //
    //Future<ResSetOperatingMode> rsConfigSetOperatingMode({ ReqSetOperatingMode reqSetOperatingMode }) async 
    test('test rsConfigSetOperatingMode', () async {
      // TODO
    });

    // Check if core is fully ready, true only after
    //
    //Future<ResIsReady> rsControlIsReady() async 
    test('test rsControlIsReady', () async {
      // TODO
    });

    // Turn off RetroShare
    //
    //Future rsControlRsGlobalShutDown() async 
    test('test rsControlRsGlobalShutDown', () async {
      // TODO
    });

    // Add shared directory
    //
    //Future<ResAddSharedDirectory> rsFilesAddSharedDirectory({ ReqAddSharedDirectory reqAddSharedDirectory }) async 
    test('test rsFilesAddSharedDirectory', () async {
      // TODO
    });

    // Check if we already have a file
    //
    //Future<ResAlreadyHaveFile> rsFilesAlreadyHaveFile({ ReqAlreadyHaveFile reqAlreadyHaveFile }) async 
    test('test rsFilesAlreadyHaveFile', () async {
      // TODO
    });

    // Ban unwanted file from being, searched and forwarded by this node
    //
    //Future<ResBanFile> rsFilesBanFile({ ReqBanFile reqBanFile }) async 
    test('test rsFilesBanFile', () async {
      // TODO
    });

    // Get default chunk strategy
    //
    //Future<ResDefaultChunkStrategy> rsFilesDefaultChunkStrategy() async 
    test('test rsFilesDefaultChunkStrategy', () async {
      // TODO
    });

    // Export link to a collection of files
    //
    //Future<ResExportCollectionLink> rsFilesExportCollectionLink({ ReqExportCollectionLink reqExportCollectionLink }) async 
    test('test rsFilesExportCollectionLink', () async {
      // TODO
    });

    // Export link to a file
    //
    //Future<ResExportFileLink> rsFilesExportFileLink({ ReqExportFileLink reqExportFileLink }) async 
    test('test rsFilesExportFileLink', () async {
      // TODO
    });

    // Add file to extra shared file list
    //
    //Future<ResExtraFileHash> rsFilesExtraFileHash({ ReqExtraFileHash reqExtraFileHash }) async 
    test('test rsFilesExtraFileHash', () async {
      // TODO
    });

    // Remove file from extra fila shared list
    //
    //Future<ResExtraFileRemove> rsFilesExtraFileRemove({ ReqExtraFileRemove reqExtraFileRemove }) async 
    test('test rsFilesExtraFileRemove', () async {
      // TODO
    });

    // Get extra file information
    //
    //Future<ResExtraFileStatus> rsFilesExtraFileStatus({ ReqExtraFileStatus reqExtraFileStatus }) async 
    test('test rsFilesExtraFileStatus', () async {
      // TODO
    });

    // Cancel file downloading
    //
    //Future<ResFileCancel> rsFilesFileCancel({ ReqFileCancel reqFileCancel }) async 
    test('test rsFilesFileCancel', () async {
      // TODO
    });

    // Clear completed downloaded files list
    //
    //Future<ResFileClearCompleted> rsFilesFileClearCompleted() async 
    test('test rsFilesFileClearCompleted', () async {
      // TODO
    });

    // Controls file transfer
    //
    //Future<ResFileControl> rsFilesFileControl({ ReqFileControl reqFileControl }) async 
    test('test rsFilesFileControl', () async {
      // TODO
    });

    // Get file details
    //
    //Future<ResFileDetails> rsFilesFileDetails({ ReqFileDetails reqFileDetails }) async 
    test('test rsFilesFileDetails', () async {
      // TODO
    });

    // Get chunk details about the downloaded file with given hash.
    //
    //Future<ResFileDownloadChunksDetails> rsFilesFileDownloadChunksDetails({ ReqFileDownloadChunksDetails reqFileDownloadChunksDetails }) async 
    test('test rsFilesFileDownloadChunksDetails', () async {
      // TODO
    });

    // Get incoming files list
    //
    //Future<ResFileDownloads> rsFilesFileDownloads() async 
    test('test rsFilesFileDownloads', () async {
      // TODO
    });

    // Initiate downloading of a file
    //
    //Future<ResFileRequest> rsFilesFileRequest({ ReqFileRequest reqFileRequest }) async 
    test('test rsFilesFileRequest', () async {
      // TODO
    });

    // Get details about the upload with given hash
    //
    //Future<ResFileUploadChunksDetails> rsFilesFileUploadChunksDetails({ ReqFileUploadChunksDetails reqFileUploadChunksDetails }) async 
    test('test rsFilesFileUploadChunksDetails', () async {
      // TODO
    });

    // Get outgoing files list
    //
    //Future<ResFileUploads> rsFilesFileUploads() async 
    test('test rsFilesFileUploads', () async {
      // TODO
    });

    // Force shared directories check.
    //
    //Future rsFilesForceDirectoryCheck({ ReqForceDirectoryCheck reqForceDirectoryCheck }) async 
    test('test rsFilesForceDirectoryCheck', () async {
      // TODO
    });

    // Get free disk space limit
    //
    //Future<ResFreeDiskSpaceLimit> rsFilesFreeDiskSpaceLimit() async 
    test('test rsFilesFreeDiskSpaceLimit', () async {
      // TODO
    });

    // Get default complete downloads directory
    //
    //Future<ResGetDownloadDirectory> rsFilesGetDownloadDirectory() async 
    test('test rsFilesGetDownloadDirectory', () async {
      // TODO
    });

    // Provides file data for the GUI, media streaming or API clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
    //
    //Future<ResGetFileData> rsFilesGetFileData({ ReqGetFileData reqGetFileData }) async 
    test('test rsFilesGetFileData', () async {
      // TODO
    });

    // Get partial downloads directory
    //
    //Future<ResGetPartialsDirectory> rsFilesGetPartialsDirectory() async 
    test('test rsFilesGetPartialsDirectory', () async {
      // TODO
    });

    // Get list of banned files
    //
    //Future<ResGetPrimaryBannedFilesList> rsFilesGetPrimaryBannedFilesList() async 
    test('test rsFilesGetPrimaryBannedFilesList', () async {
      // TODO
    });

    // Get list of current shared directories
    //
    //Future<ResGetSharedDirectories> rsFilesGetSharedDirectories() async 
    test('test rsFilesGetSharedDirectories', () async {
      // TODO
    });

    // Check if a file is on banned list
    //
    //Future<ResIsHashBanned> rsFilesIsHashBanned({ ReqIsHashBanned reqIsHashBanned }) async 
    test('test rsFilesIsHashBanned', () async {
      // TODO
    });

    // Parse RetroShare files link
    //
    //Future<ResParseFilesLink> rsFilesParseFilesLink({ ReqParseFilesLink reqParseFilesLink }) async 
    test('test rsFilesParseFilesLink', () async {
      // TODO
    });

    // Remove directory from shared list
    //
    //Future<ResRemoveSharedDirectory> rsFilesRemoveSharedDirectory({ ReqRemoveSharedDirectory reqRemoveSharedDirectory }) async 
    test('test rsFilesRemoveSharedDirectory', () async {
      // TODO
    });

    // Request directory details, subsequent multiple call may be used to explore a whole directory tree.
    //
    //Future<ResRequestDirDetails> rsFilesRequestDirDetails({ ReqRequestDirDetails reqRequestDirDetails }) async 
    test('test rsFilesRequestDirDetails', () async {
      // TODO
    });

    // Initiate download of a files collection
    //
    //Future<ResRequestFiles> rsFilesRequestFiles({ ReqRequestFiles reqRequestFiles }) async 
    test('test rsFilesRequestFiles', () async {
      // TODO
    });

    // Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
    //
    //Future<ResSetChunkStrategy> rsFilesSetChunkStrategy({ ReqSetChunkStrategy reqSetChunkStrategy }) async 
    test('test rsFilesSetChunkStrategy', () async {
      // TODO
    });

    // Set default chunk strategy
    //
    //Future rsFilesSetDefaultChunkStrategy({ ReqSetDefaultChunkStrategy reqSetDefaultChunkStrategy }) async 
    test('test rsFilesSetDefaultChunkStrategy', () async {
      // TODO
    });

    // Set destination directory for given file
    //
    //Future<ResSetDestinationDirectory> rsFilesSetDestinationDirectory({ ReqSetDestinationDirectory reqSetDestinationDirectory }) async 
    test('test rsFilesSetDestinationDirectory', () async {
      // TODO
    });

    // Set name for dowloaded file
    //
    //Future<ResSetDestinationName> rsFilesSetDestinationName({ ReqSetDestinationName reqSetDestinationName }) async 
    test('test rsFilesSetDestinationName', () async {
      // TODO
    });

    // Set default complete downloads directory
    //
    //Future<ResSetDownloadDirectory> rsFilesSetDownloadDirectory({ ReqSetDownloadDirectory reqSetDownloadDirectory }) async 
    test('test rsFilesSetDownloadDirectory', () async {
      // TODO
    });

    // Set minimum free disk space limit
    //
    //Future rsFilesSetFreeDiskSpaceLimit({ ReqSetFreeDiskSpaceLimit reqSetFreeDiskSpaceLimit }) async 
    test('test rsFilesSetFreeDiskSpaceLimit', () async {
      // TODO
    });

    // Set partial downloads directory
    //
    //Future<ResSetPartialsDirectory> rsFilesSetPartialsDirectory({ ReqSetPartialsDirectory reqSetPartialsDirectory }) async 
    test('test rsFilesSetPartialsDirectory', () async {
      // TODO
    });

    // Set shared directories
    //
    //Future<ResSetSharedDirectories> rsFilesSetSharedDirectories({ ReqSetSharedDirectories reqSetSharedDirectories }) async 
    test('test rsFilesSetSharedDirectories', () async {
      // TODO
    });

    // This method is asynchronous. Request remote files search
    //
    //Future<ResTurtleSearchRequest> rsFilesTurtleSearchRequest({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async 
    test('test rsFilesTurtleSearchRequest', () async {
      // TODO
    });

    // Remove file from unwanted list
    //
    //Future<ResUnbanFile> rsFilesUnbanFile({ ReqUnbanFile reqUnbanFile }) async 
    test('test rsFilesUnbanFile', () async {
      // TODO
    });

    // Updates shared directory sharing flags. The directory should be already shared!
    //
    //Future<ResUpdateShareFlags> rsFilesUpdateShareFlags({ ReqUpdateShareFlags reqUpdateShareFlags }) async 
    test('test rsFilesUpdateShareFlags', () async {
      // TODO
    });

    // getDiscFriends get a list of all friends of a given friend
    //
    //Future<ResGetDiscFriends> rsGossipDiscoveryGetDiscFriends({ ReqGetDiscFriends reqGetDiscFriends }) async 
    test('test rsGossipDiscoveryGetDiscFriends', () async {
      // TODO
    });

    // getDiscPgpFriends get a list of all friends of a given friend
    //
    //Future<ResGetDiscPgpFriends> rsGossipDiscoveryGetDiscPgpFriends({ ReqGetDiscPgpFriends reqGetDiscPgpFriends }) async 
    test('test rsGossipDiscoveryGetDiscPgpFriends', () async {
      // TODO
    });

    // getPeerVersion get the version string of a peer.
    //
    //Future<ResGetPeerVersion> rsGossipDiscoveryGetPeerVersion({ ReqGetPeerVersion reqGetPeerVersion }) async 
    test('test rsGossipDiscoveryGetPeerVersion', () async {
      // TODO
    });

    // getWaitingDiscCount get the number of queued discovery packets.
    //
    //Future<ResGetWaitingDiscCount> rsGossipDiscoveryGetWaitingDiscCount() async 
    test('test rsGossipDiscoveryGetWaitingDiscCount', () async {
      // TODO
    });

    // Deprecated{ substituted by createChannelV2 }
    //
    //Future<ResCreateChannel> rsGxsChannelsCreateChannel({ ReqCreateChannel reqCreateChannel }) async 
    test('test rsGxsChannelsCreateChannel', () async {
      // TODO
    });

    // Create channel. Blocking API.
    //
    //Future<ResCreateChannelV2> rsGxsChannelsCreateChannelV2({ ReqCreateChannelV2 reqCreateChannelV2 }) async 
    test('test rsGxsChannelsCreateChannelV2', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResCreateComment> rsGxsChannelsCreateComment({ ReqCreateComment reqCreateComment }) async 
    test('test rsGxsChannelsCreateComment', () async {
      // TODO
    });

    // Add a comment on a post or on another comment. Blocking API.
    //
    //Future<ResCreateCommentV2> rsGxsChannelsCreateCommentV2({ ReqCreateCommentV2 reqCreateCommentV2 }) async 
    test('test rsGxsChannelsCreateCommentV2', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResCreatePost> rsGxsChannelsCreatePost({ ReqCreatePost reqCreatePost }) async 
    test('test rsGxsChannelsCreatePost', () async {
      // TODO
    });

    // Create channel post. Blocking API.
    //
    //Future<ResCreatePostV2> rsGxsChannelsCreatePostV2({ ReqCreatePostV2 reqCreatePostV2 }) async 
    test('test rsGxsChannelsCreatePostV2', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResCreateVote> rsGxsChannelsCreateVote({ ReqCreateVote reqCreateVote }) async 
    test('test rsGxsChannelsCreateVote', () async {
      // TODO
    });

    // Create a vote
    //
    //Future<ResCreateVoteV2> rsGxsChannelsCreateVoteV2({ ReqCreateVoteV2 reqCreateVoteV2 }) async 
    test('test rsGxsChannelsCreateVoteV2', () async {
      // TODO
    });

    // Edit channel details.
    //
    //Future<ResEditChannel> rsGxsChannelsEditChannel({ ReqEditChannel reqEditChannel }) async 
    test('test rsGxsChannelsEditChannel', () async {
      // TODO
    });

    // Get link to a channel
    //
    //Future<ResExportChannelLink> rsGxsChannelsExportChannelLink({ ReqExportChannelLink reqExportChannelLink }) async 
    test('test rsGxsChannelsExportChannelLink', () async {
      // TODO
    });

    // Share extra file Can be used to share extra file attached to a channel post
    //
    //Future<ResExtraFileHash> rsGxsChannelsExtraFileHash({ ReqExtraFileHash reqExtraFileHash }) async 
    test('test rsGxsChannelsExtraFileHash', () async {
      // TODO
    });

    // Remove extra file from shared files
    //
    //Future<ResExtraFileRemove> rsGxsChannelsExtraFileRemove({ ReqExtraFileRemove reqExtraFileRemove }) async 
    test('test rsGxsChannelsExtraFileRemove', () async {
      // TODO
    });

    // Get all channel messages and comments in a given channel
    //
    //Future<ResGetChannelAllContent> rsGxsChannelsGetChannelAllContent({ ReqGetChannelAllContent reqGetChannelAllContent }) async 
    test('test rsGxsChannelsGetChannelAllContent', () async {
      // TODO
    });

    // DeprecatedThis feature rely on very buggy code, the returned value is not reliable
    //
    //Future<ResGetChannelAutoDownload> rsGxsChannelsGetChannelAutoDownload({ ReqGetChannelAutoDownload reqGetChannelAutoDownload }) async 
    test('test rsGxsChannelsGetChannelAutoDownload', () async {
      // TODO
    });

    // Get channel comments corresponding to the given IDs. If the set is empty, nothing is returned.
    //
    //Future<ResGetChannelComments> rsGxsChannelsGetChannelComments({ ReqGetChannelComments reqGetChannelComments }) async 
    test('test rsGxsChannelsGetChannelComments', () async {
      // TODO
    });

    // Get channel messages and comments corresponding to the given IDs. If the set is empty, nothing is returned.
    //
    //Future<ResGetChannelContent> rsGxsChannelsGetChannelContent({ ReqGetChannelContent reqGetChannelContent }) async 
    test('test rsGxsChannelsGetChannelContent', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResGetChannelDownloadDirectory> rsGxsChannelsGetChannelDownloadDirectory({ ReqGetChannelDownloadDirectory reqGetChannelDownloadDirectory }) async 
    test('test rsGxsChannelsGetChannelDownloadDirectory', () async {
      // TODO
    });

    // Retrieve statistics about the channel service
    //
    //Future<ResGetChannelServiceStatistics> rsGxsChannelsGetChannelServiceStatistics() async 
    test('test rsGxsChannelsGetChannelServiceStatistics', () async {
      // TODO
    });

    // Retrieve statistics about the given channel
    //
    //Future<ResGetChannelStatistics> rsGxsChannelsGetChannelStatistics({ ReqGetChannelStatistics reqGetChannelStatistics }) async 
    test('test rsGxsChannelsGetChannelStatistics', () async {
      // TODO
    });

    // Get channels information (description, thumbnail...). Blocking API.
    //
    //Future<ResGetChannelsInfo> rsGxsChannelsGetChannelsInfo({ ReqGetChannelsInfo reqGetChannelsInfo }) async 
    test('test rsGxsChannelsGetChannelsInfo', () async {
      // TODO
    });

    // Get channels summaries list. Blocking API.
    //
    //Future<ResGetChannelsSummaries> rsGxsChannelsGetChannelsSummaries() async 
    test('test rsGxsChannelsGetChannelsSummaries', () async {
      // TODO
    });

    // Get channel content summaries
    //
    //Future<ResGetContentSummaries> rsGxsChannelsGetContentSummaries({ ReqGetContentSummaries reqGetContentSummaries }) async 
    test('test rsGxsChannelsGetContentSummaries', () async {
      // TODO
    });

    // This method is asynchronous. Search local channels
    //
    //Future<ResLocalSearchRequest> rsGxsChannelsLocalSearchRequest({ ReqLocalSearchRequest reqLocalSearchRequest }) async 
    test('test rsGxsChannelsLocalSearchRequest', () async {
      // TODO
    });

    // Toggle post read status. Blocking API.
    //
    //Future<ResMarkRead> rsGxsChannelsMarkRead({ ReqMarkRead reqMarkRead }) async 
    test('test rsGxsChannelsMarkRead', () async {
      // TODO
    });

    // null
    //
    //Future<ResRequestStatus> rsGxsChannelsRequestStatus({ ReqRequestStatus reqRequestStatus }) async 
    test('test rsGxsChannelsRequestStatus', () async {
      // TODO
    });

    // DeprecatedThis feature rely on very buggy code, when enabled the channel service start flooding erratically log with error messages, apparently without more dangerous consequences. Still those messages hints that something out of control is happening under the hood, use at your own risk. A safe alternative to this method can easly implemented at API client level instead.
    //
    //Future<ResSetChannelAutoDownload> rsGxsChannelsSetChannelAutoDownload({ ReqSetChannelAutoDownload reqSetChannelAutoDownload }) async 
    test('test rsGxsChannelsSetChannelAutoDownload', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResSetChannelDownloadDirectory> rsGxsChannelsSetChannelDownloadDirectory({ ReqSetChannelDownloadDirectory reqSetChannelDownloadDirectory }) async 
    test('test rsGxsChannelsSetChannelDownloadDirectory', () async {
      // TODO
    });

    // Share channel publishing key This can be used to authorize other peers to post on the channel
    //
    //Future<ResShareChannelKeys> rsGxsChannelsShareChannelKeys({ ReqShareChannelKeys reqShareChannelKeys }) async 
    test('test rsGxsChannelsShareChannelKeys', () async {
      // TODO
    });

    // Subscrbe to a channel. Blocking API
    //
    //Future<ResSubscribeToChannel> rsGxsChannelsSubscribeToChannel({ ReqSubscribeToChannel reqSubscribeToChannel }) async 
    test('test rsGxsChannelsSubscribeToChannel', () async {
      // TODO
    });

    // This method is asynchronous. Request remote channel
    //
    //Future<ResTurtleChannelRequest> rsGxsChannelsTurtleChannelRequest({ ReqTurtleChannelRequest reqTurtleChannelRequest }) async 
    test('test rsGxsChannelsTurtleChannelRequest', () async {
      // TODO
    });

    // This method is asynchronous. Request remote channels search
    //
    //Future<ResTurtleSearchRequest> rsGxsChannelsTurtleSearchRequest({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async 
    test('test rsGxsChannelsTurtleSearchRequest', () async {
      // TODO
    });

    // Leave given circle
    //
    //Future<ResCancelCircleMembership> rsGxsCirclesCancelCircleMembership({ ReqCancelCircleMembership reqCancelCircleMembership }) async 
    test('test rsGxsCirclesCancelCircleMembership', () async {
      // TODO
    });

    // Create new circle
    //
    //Future<ResCreateCircle> rsGxsCirclesCreateCircle({ ReqCreateCircle reqCreateCircle }) async 
    test('test rsGxsCirclesCreateCircle', () async {
      // TODO
    });

    // Edit own existing circle
    //
    //Future<ResEditCircle> rsGxsCirclesEditCircle({ ReqEditCircle reqEditCircle }) async 
    test('test rsGxsCirclesEditCircle', () async {
      // TODO
    });

    // Get link to a circle
    //
    //Future<ResExportCircleLink> rsGxsCirclesExportCircleLink({ ReqExportCircleLink reqExportCircleLink }) async 
    test('test rsGxsCirclesExportCircleLink', () async {
      // TODO
    });

    // Get circle details. Memory cached
    //
    //Future<ResGetCircleDetails> rsGxsCirclesGetCircleDetails({ ReqGetCircleDetails reqGetCircleDetails }) async 
    test('test rsGxsCirclesGetCircleDetails', () async {
      // TODO
    });

    // Get list of known external circles ids. Memory cached
    //
    //Future<ResGetCircleExternalIdList> rsGxsCirclesGetCircleExternalIdList({ ReqGetCircleExternalIdList reqGetCircleExternalIdList }) async 
    test('test rsGxsCirclesGetCircleExternalIdList', () async {
      // TODO
    });

    // Get specific circle request
    //
    //Future<ResGetCircleRequest> rsGxsCirclesGetCircleRequest({ ReqGetCircleRequest reqGetCircleRequest }) async 
    test('test rsGxsCirclesGetCircleRequest', () async {
      // TODO
    });

    // Get circle requests
    //
    //Future<ResGetCircleRequests> rsGxsCirclesGetCircleRequests({ ReqGetCircleRequests reqGetCircleRequests }) async 
    test('test rsGxsCirclesGetCircleRequests', () async {
      // TODO
    });

    // Get circles information
    //
    //Future<ResGetCirclesInfo> rsGxsCirclesGetCirclesInfo({ ReqGetCirclesInfo reqGetCirclesInfo }) async 
    test('test rsGxsCirclesGetCirclesInfo', () async {
      // TODO
    });

    // Get circles summaries list.
    //
    //Future<ResGetCirclesSummaries> rsGxsCirclesGetCirclesSummaries() async 
    test('test rsGxsCirclesGetCirclesSummaries', () async {
      // TODO
    });

    // Invite identities to circle (admin key is required)
    //
    //Future<ResInviteIdsToCircle> rsGxsCirclesInviteIdsToCircle({ ReqInviteIdsToCircle reqInviteIdsToCircle }) async 
    test('test rsGxsCirclesInviteIdsToCircle', () async {
      // TODO
    });

    // Request circle membership, or accept circle invitation
    //
    //Future<ResRequestCircleMembership> rsGxsCirclesRequestCircleMembership({ ReqRequestCircleMembership reqRequestCircleMembership }) async 
    test('test rsGxsCirclesRequestCircleMembership', () async {
      // TODO
    });

    // null
    //
    //Future<ResRequestStatus> rsGxsCirclesRequestStatus({ ReqRequestStatus reqRequestStatus }) async 
    test('test rsGxsCirclesRequestStatus', () async {
      // TODO
    });

    // Remove identities from circle (admin key is required)
    //
    //Future<ResRevokeIdsFromCircle> rsGxsCirclesRevokeIdsFromCircle({ ReqRevokeIdsFromCircle reqRevokeIdsFromCircle }) async 
    test('test rsGxsCirclesRevokeIdsFromCircle', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResCreateForum> rsGxsForumsCreateForum({ ReqCreateForum reqCreateForum }) async 
    test('test rsGxsForumsCreateForum', () async {
      // TODO
    });

    // Create forum.
    //
    //Future<ResCreateForumV2> rsGxsForumsCreateForumV2({ ReqCreateForumV2 reqCreateForumV2 }) async 
    test('test rsGxsForumsCreateForumV2', () async {
      // TODO
    });

    // Deprecated
    //
    //Future<ResCreateMessage> rsGxsForumsCreateMessage({ ReqCreateMessage reqCreateMessage }) async 
    test('test rsGxsForumsCreateMessage', () async {
      // TODO
    });

    // Create a post on the given forum.
    //
    //Future<ResCreatePost> rsGxsForumsCreatePost({ ReqCreatePost reqCreatePost }) async 
    test('test rsGxsForumsCreatePost', () async {
      // TODO
    });

    // Edit forum details.
    //
    //Future<ResEditForum> rsGxsForumsEditForum({ ReqEditForum reqEditForum }) async 
    test('test rsGxsForumsEditForum', () async {
      // TODO
    });

    // Get link to a forum
    //
    //Future<ResExportForumLink> rsGxsForumsExportForumLink({ ReqExportForumLink reqExportForumLink }) async 
    test('test rsGxsForumsExportForumLink', () async {
      // TODO
    });

    // Get specific list of messages from a single forum. Blocking API
    //
    //Future<ResGetForumContent> rsGxsForumsGetForumContent({ ReqGetForumContent reqGetForumContent }) async 
    test('test rsGxsForumsGetForumContent', () async {
      // TODO
    });

    // Get message metadatas for a specific forum. Blocking API
    //
    //Future<ResGetForumMsgMetaData> rsGxsForumsGetForumMsgMetaData({ ReqGetForumMsgMetaData reqGetForumMsgMetaData }) async 
    test('test rsGxsForumsGetForumMsgMetaData', () async {
      // TODO
    });

    // returns statistics for the forum service
    //
    //Future<ResGetForumServiceStatistics> rsGxsForumsGetForumServiceStatistics() async 
    test('test rsGxsForumsGetForumServiceStatistics', () async {
      // TODO
    });

    // returns statistics about a particular forum
    //
    //Future<ResGetForumStatistics> rsGxsForumsGetForumStatistics({ ReqGetForumStatistics reqGetForumStatistics }) async 
    test('test rsGxsForumsGetForumStatistics', () async {
      // TODO
    });

    // Get forums information (description, thumbnail...). Blocking API.
    //
    //Future<ResGetForumsInfo> rsGxsForumsGetForumsInfo({ ReqGetForumsInfo reqGetForumsInfo }) async 
    test('test rsGxsForumsGetForumsInfo', () async {
      // TODO
    });

    // Get forums summaries list. Blocking API.
    //
    //Future<ResGetForumsSummaries> rsGxsForumsGetForumsSummaries() async 
    test('test rsGxsForumsGetForumsSummaries', () async {
      // TODO
    });

    // Toggle message read status. Blocking API.
    //
    //Future<ResMarkRead> rsGxsForumsMarkRead({ ReqMarkRead reqMarkRead }) async 
    test('test rsGxsForumsMarkRead', () async {
      // TODO
    });

    // null
    //
    //Future<ResRequestStatus> rsGxsForumsRequestStatus({ ReqRequestStatus reqRequestStatus }) async 
    test('test rsGxsForumsRequestStatus', () async {
      // TODO
    });

    // Subscrbe to a forum. Blocking API
    //
    //Future<ResSubscribeToForum> rsGxsForumsSubscribeToForum({ ReqSubscribeToForum reqSubscribeToForum }) async 
    test('test rsGxsForumsSubscribeToForum', () async {
      // TODO
    });

    // Check if automatic signed by friend identity contact flagging is enabled
    //
    //Future<ResAutoAddFriendIdsAsContact> rsIdentityAutoAddFriendIdsAsContact() async 
    test('test rsIdentityAutoAddFriendIdsAsContact', () async {
      // TODO
    });

    // Create a new identity
    //
    //Future<ResCreateIdentity> rsIdentityCreateIdentity({ ReqCreateIdentity reqCreateIdentity }) async 
    test('test rsIdentityCreateIdentity', () async {
      // TODO
    });

    // Get number of days after which delete a banned identities
    //
    //Future<ResDeleteBannedNodesThreshold> rsIdentityDeleteBannedNodesThreshold() async 
    test('test rsIdentityDeleteBannedNodesThreshold', () async {
      // TODO
    });

    // Locally delete given identity
    //
    //Future<ResDeleteIdentity> rsIdentityDeleteIdentity({ ReqDeleteIdentity reqDeleteIdentity }) async 
    test('test rsIdentityDeleteIdentity', () async {
      // TODO
    });

    // Get link to a identity
    //
    //Future<ResExportIdentityLink> rsIdentityExportIdentityLink({ ReqExportIdentityLink reqExportIdentityLink }) async 
    test('test rsIdentityExportIdentityLink', () async {
      // TODO
    });

    // Get identity details, from the cache
    //
    //Future<ResGetIdDetails> rsIdentityGetIdDetails({ ReqGetIdDetails reqGetIdDetails }) async 
    test('test rsIdentityGetIdDetails', () async {
      // TODO
    });

    // Get identities information (name, avatar...). Blocking API.
    //
    //Future<ResGetIdentitiesInfo> rsIdentityGetIdentitiesInfo({ ReqGetIdentitiesInfo reqGetIdentitiesInfo }) async 
    test('test rsIdentityGetIdentitiesInfo', () async {
      // TODO
    });

    // Get identities summaries list.
    //
    //Future<ResGetIdentitiesSummaries> rsIdentityGetIdentitiesSummaries() async 
    test('test rsIdentityGetIdentitiesSummaries', () async {
      // TODO
    });

    // Get last seen usage time of given identity
    //
    //Future<ResGetLastUsageTS> rsIdentityGetLastUsageTS({ ReqGetLastUsageTS reqGetLastUsageTS }) async 
    test('test rsIdentityGetLastUsageTS', () async {
      // TODO
    });

    // Get own pseudonimous (unsigned) ids
    //
    //Future<ResGetOwnPseudonimousIds> rsIdentityGetOwnPseudonimousIds() async 
    test('test rsIdentityGetOwnPseudonimousIds', () async {
      // TODO
    });

    // Get own signed ids
    //
    //Future<ResGetOwnSignedIds> rsIdentityGetOwnSignedIds() async 
    test('test rsIdentityGetOwnSignedIds', () async {
      // TODO
    });

    // Check if an identity is contact
    //
    //Future<ResIsARegularContact> rsIdentityIsARegularContact({ ReqIsARegularContact reqIsARegularContact }) async 
    test('test rsIdentityIsARegularContact', () async {
      // TODO
    });

    // Check if an id is known
    //
    //Future<ResIsKnownId> rsIdentityIsKnownId({ ReqIsKnownId reqIsKnownId }) async 
    test('test rsIdentityIsKnownId', () async {
      // TODO
    });

    // Check if an id is own
    //
    //Future<ResIsOwnId> rsIdentityIsOwnId({ ReqIsOwnId reqIsOwnId }) async 
    test('test rsIdentityIsOwnId', () async {
      // TODO
    });

    // request details of a not yet known identity to the network
    //
    //Future<ResRequestIdentity> rsIdentityRequestIdentity({ ReqRequestIdentity reqRequestIdentity }) async 
    test('test rsIdentityRequestIdentity', () async {
      // TODO
    });

    // null
    //
    //Future<ResRequestStatus> rsIdentityRequestStatus({ ReqRequestStatus reqRequestStatus }) async 
    test('test rsIdentityRequestStatus', () async {
      // TODO
    });

    // Set/unset identity as contact
    //
    //Future<ResSetAsRegularContact> rsIdentitySetAsRegularContact({ ReqSetAsRegularContact reqSetAsRegularContact }) async 
    test('test rsIdentitySetAsRegularContact', () async {
      // TODO
    });

    // Toggle automatic flagging signed by friends identity as contact
    //
    //Future rsIdentitySetAutoAddFriendIdsAsContact({ ReqSetAutoAddFriendIdsAsContact reqSetAutoAddFriendIdsAsContact }) async 
    test('test rsIdentitySetAutoAddFriendIdsAsContact', () async {
      // TODO
    });

    // Set number of days after which delete a banned identities
    //
    //Future rsIdentitySetDeleteBannedNodesThreshold({ ReqSetDeleteBannedNodesThreshold reqSetDeleteBannedNodesThreshold }) async 
    test('test rsIdentitySetDeleteBannedNodesThreshold', () async {
      // TODO
    });

    // Update identity data (name, avatar...)
    //
    //Future<ResUpdateIdentity> rsIdentityUpdateIdentity({ ReqUpdateIdentity reqUpdateIdentity }) async 
    test('test rsIdentityUpdateIdentity', () async {
      // TODO
    });

    // Request
    //
    //Future rsJsonApiAskForStop() async 
    test('test rsJsonApiAskForStop', () async {
      // TODO
    });

    // null
    //
    //Future<ResAuthorizeUser> rsJsonApiAuthorizeUser({ ReqAuthorizeUser reqAuthorizeUser }) async 
    test('test rsJsonApiAuthorizeUser', () async {
      // TODO
    });

    // Get authorized tokens
    //
    //Future<ResGetAuthorizedTokens> rsJsonApiGetAuthorizedTokens() async 
    test('test rsJsonApiGetAuthorizedTokens', () async {
      // TODO
    });

    // null
    //
    //Future<ResGetBindingAddress> rsJsonApiGetBindingAddress() async 
    test('test rsJsonApiGetBindingAddress', () async {
      // TODO
    });

    // Check if given JSON API auth token is authorized
    //
    //Future<ResIsAuthTokenValid> rsJsonApiIsAuthTokenValid({ ReqIsAuthTokenValid reqIsAuthTokenValid }) async 
    test('test rsJsonApiIsAuthTokenValid', () async {
      // TODO
    });

    // null
    //
    //Future<ResListeningPort> rsJsonApiListeningPort() async 
    test('test rsJsonApiListeningPort', () async {
      // TODO
    });

    // This function should be used by JSON API clients that aren't authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
    //
    //Future<ResRequestNewTokenAutorization> rsJsonApiRequestNewTokenAutorization({ ReqRequestNewTokenAutorization reqRequestNewTokenAutorization }) async 
    test('test rsJsonApiRequestNewTokenAutorization', () async {
      // TODO
    });

    // Restart
    //
    //Future<ResRestart> rsJsonApiRestart() async 
    test('test rsJsonApiRestart', () async {
      // TODO
    });

    // Revoke given auth token
    //
    //Future<ResRevokeAuthToken> rsJsonApiRevokeAuthToken({ ReqRevokeAuthToken reqRevokeAuthToken }) async 
    test('test rsJsonApiRevokeAuthToken', () async {
      // TODO
    });

    // null
    //
    //Future rsJsonApiSetBindingAddress({ ReqSetBindingAddress reqSetBindingAddress }) async 
    test('test rsJsonApiSetBindingAddress', () async {
      // TODO
    });

    // null
    //
    //Future rsJsonApiSetListeningPort({ ReqSetListeningPort reqSetListeningPort }) async 
    test('test rsJsonApiSetListeningPort', () async {
      // TODO
    });

    // Write version information to given paramethers
    //
    //Future<ResVersion> rsJsonApiVersion() async 
    test('test rsJsonApiVersion', () async {
      // TODO
    });

    // Normal way to attempt login
    //
    //Future<ResAttemptLogin> rsLoginHelperAttemptLogin({ ReqAttemptLogin reqAttemptLogin }) async 
    test('test rsLoginHelperAttemptLogin', () async {
      // TODO
    });

    // Feed extra entropy to the crypto libraries
    //
    //Future<ResCollectEntropy> rsLoginHelperCollectEntropy({ ReqCollectEntropy reqCollectEntropy }) async 
    test('test rsLoginHelperCollectEntropy', () async {
      // TODO
    });

    // Creates a new RetroShare location, and log in once is created
    //
    //Future<ResCreateLocation> rsLoginHelperCreateLocation({ ReqCreateLocation reqCreateLocation }) async 
    test('test rsLoginHelperCreateLocation', () async {
      // TODO
    });

    // Get locations and associated information
    //
    //Future<ResGetLocations> rsLoginHelperGetLocations() async 
    test('test rsLoginHelperGetLocations', () async {
      // TODO
    });

    // Check if RetroShare is already logged in, this usually return true after a successfull
    //
    //Future<ResIsLoggedIn> rsLoginHelperIsLoggedIn() async 
    test('test rsLoginHelperIsLoggedIn', () async {
      // TODO
    });

    // acceptLobbyInvite accept a chat invite
    //
    //Future<ResAcceptLobbyInvite> rsMsgsAcceptLobbyInvite({ ReqAcceptLobbyInvite reqAcceptLobbyInvite }) async 
    test('test rsMsgsAcceptLobbyInvite', () async {
      // TODO
    });

    // clearChatLobby clear a chat lobby
    //
    //Future rsMsgsClearChatLobby({ ReqClearChatLobby reqClearChatLobby }) async 
    test('test rsMsgsClearChatLobby', () async {
      // TODO
    });

    // closeDistantChatConnexion
    //
    //Future<ResCloseDistantChatConnexion> rsMsgsCloseDistantChatConnexion({ ReqCloseDistantChatConnexion reqCloseDistantChatConnexion }) async 
    test('test rsMsgsCloseDistantChatConnexion', () async {
      // TODO
    });

    // createChatLobby create a new chat lobby
    //
    //Future<ResCreateChatLobby> rsMsgsCreateChatLobby({ ReqCreateChatLobby reqCreateChatLobby }) async 
    test('test rsMsgsCreateChatLobby', () async {
      // TODO
    });

    // denyLobbyInvite deny a chat lobby invite
    //
    //Future rsMsgsDenyLobbyInvite({ ReqDenyLobbyInvite reqDenyLobbyInvite }) async 
    test('test rsMsgsDenyLobbyInvite', () async {
      // TODO
    });

    // getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
    //
    //Future<ResGetChatLobbyInfo> rsMsgsGetChatLobbyInfo({ ReqGetChatLobbyInfo reqGetChatLobbyInfo }) async 
    test('test rsMsgsGetChatLobbyInfo', () async {
      // TODO
    });

    // getChatLobbyList get ids of subscribed lobbies
    //
    //Future<ResGetChatLobbyList> rsMsgsGetChatLobbyList() async 
    test('test rsMsgsGetChatLobbyList', () async {
      // TODO
    });

    // getCustomStateString get the custom status message from a peer
    //
    //Future<ResGetCustomStateString> rsMsgsGetCustomStateString({ ReqGetCustomStateString reqGetCustomStateString }) async 
    test('test rsMsgsGetCustomStateString', () async {
      // TODO
    });

    // getDefaultIdentityForChatLobby get the default identity used for chat lobbies
    //
    //Future<ResGetDefaultIdentityForChatLobby> rsMsgsGetDefaultIdentityForChatLobby() async 
    test('test rsMsgsGetDefaultIdentityForChatLobby', () async {
      // TODO
    });

    // getDistantChatStatus receives distant chat info to a given distant chat id
    //
    //Future<ResGetDistantChatStatus> rsMsgsGetDistantChatStatus({ ReqGetDistantChatStatus reqGetDistantChatStatus }) async 
    test('test rsMsgsGetDistantChatStatus', () async {
      // TODO
    });

    // getIdentityForChatLobby
    //
    //Future<ResGetIdentityForChatLobby> rsMsgsGetIdentityForChatLobby({ ReqGetIdentityForChatLobby reqGetIdentityForChatLobby }) async 
    test('test rsMsgsGetIdentityForChatLobby', () async {
      // TODO
    });

    // getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
    //
    //Future<ResGetListOfNearbyChatLobbies> rsMsgsGetListOfNearbyChatLobbies() async 
    test('test rsMsgsGetListOfNearbyChatLobbies', () async {
      // TODO
    });

    // getLobbyAutoSubscribe get current value of auto subscribe
    //
    //Future<ResGetLobbyAutoSubscribe> rsMsgsGetLobbyAutoSubscribe({ ReqGetLobbyAutoSubscribe reqGetLobbyAutoSubscribe }) async 
    test('test rsMsgsGetLobbyAutoSubscribe', () async {
      // TODO
    });

    // getMaxMessageSecuritySize get the maximum size of a chta message
    //
    //Future<ResGetMaxMessageSecuritySize> rsMsgsGetMaxMessageSecuritySize({ ReqGetMaxMessageSecuritySize reqGetMaxMessageSecuritySize }) async 
    test('test rsMsgsGetMaxMessageSecuritySize', () async {
      // TODO
    });

    // getMessage
    //
    //Future<ResGetMessage> rsMsgsGetMessage({ ReqGetMessage reqGetMessage }) async 
    test('test rsMsgsGetMessage', () async {
      // TODO
    });

    // getMessageCount
    //
    //Future<ResGetMessageCount> rsMsgsGetMessageCount() async 
    test('test rsMsgsGetMessageCount', () async {
      // TODO
    });

    // getMessageSummaries
    //
    //Future<ResGetMessageSummaries> rsMsgsGetMessageSummaries() async 
    test('test rsMsgsGetMessageSummaries', () async {
      // TODO
    });

    // getMessageTag
    //
    //Future<ResGetMessageTag> rsMsgsGetMessageTag({ ReqGetMessageTag reqGetMessageTag }) async 
    test('test rsMsgsGetMessageTag', () async {
      // TODO
    });

    // getMessageTagTypes
    //
    //Future<ResGetMessageTagTypes> rsMsgsGetMessageTagTypes() async 
    test('test rsMsgsGetMessageTagTypes', () async {
      // TODO
    });

    // getMsgParentId
    //
    //Future<ResGetMsgParentId> rsMsgsGetMsgParentId({ ReqGetMsgParentId reqGetMsgParentId }) async 
    test('test rsMsgsGetMsgParentId', () async {
      // TODO
    });

    // getPendingChatLobbyInvites get a list of all pending chat lobby invites
    //
    //Future<ResGetPendingChatLobbyInvites> rsMsgsGetPendingChatLobbyInvites() async 
    test('test rsMsgsGetPendingChatLobbyInvites', () async {
      // TODO
    });

    // initiateDistantChatConnexion initiate a connexion for a distant chat
    //
    //Future<ResInitiateDistantChatConnexion> rsMsgsInitiateDistantChatConnexion({ ReqInitiateDistantChatConnexion reqInitiateDistantChatConnexion }) async 
    test('test rsMsgsInitiateDistantChatConnexion', () async {
      // TODO
    });

    // invitePeerToLobby invite a peer to join a lobby
    //
    //Future rsMsgsInvitePeerToLobby({ ReqInvitePeerToLobby reqInvitePeerToLobby }) async 
    test('test rsMsgsInvitePeerToLobby', () async {
      // TODO
    });

    // joinVisibleChatLobby join a lobby that is visible
    //
    //Future<ResJoinVisibleChatLobby> rsMsgsJoinVisibleChatLobby({ ReqJoinVisibleChatLobby reqJoinVisibleChatLobby }) async 
    test('test rsMsgsJoinVisibleChatLobby', () async {
      // TODO
    });

    // MessageDelete
    //
    //Future<ResMessageDelete> rsMsgsMessageDelete({ ReqMessageDelete reqMessageDelete }) async 
    test('test rsMsgsMessageDelete', () async {
      // TODO
    });

    // MessageForwarded
    //
    //Future<ResMessageForwarded> rsMsgsMessageForwarded({ ReqMessageForwarded reqMessageForwarded }) async 
    test('test rsMsgsMessageForwarded', () async {
      // TODO
    });

    // MessageJunk
    //
    //Future<ResMessageJunk> rsMsgsMessageJunk({ ReqMessageJunk reqMessageJunk }) async 
    test('test rsMsgsMessageJunk', () async {
      // TODO
    });

    // MessageLoadEmbeddedImages
    //
    //Future<ResMessageLoadEmbeddedImages> rsMsgsMessageLoadEmbeddedImages({ ReqMessageLoadEmbeddedImages reqMessageLoadEmbeddedImages }) async 
    test('test rsMsgsMessageLoadEmbeddedImages', () async {
      // TODO
    });

    // MessageRead
    //
    //Future<ResMessageRead> rsMsgsMessageRead({ ReqMessageRead reqMessageRead }) async 
    test('test rsMsgsMessageRead', () async {
      // TODO
    });

    // MessageReplied
    //
    //Future<ResMessageReplied> rsMsgsMessageReplied({ ReqMessageReplied reqMessageReplied }) async 
    test('test rsMsgsMessageReplied', () async {
      // TODO
    });

    // MessageSend
    //
    //Future<ResMessageSend> rsMsgsMessageSend({ ReqMessageSend reqMessageSend }) async 
    test('test rsMsgsMessageSend', () async {
      // TODO
    });

    // MessageStar
    //
    //Future<ResMessageStar> rsMsgsMessageStar({ ReqMessageStar reqMessageStar }) async 
    test('test rsMsgsMessageStar', () async {
      // TODO
    });

    // MessageToDraft
    //
    //Future<ResMessageToDraft> rsMsgsMessageToDraft({ ReqMessageToDraft reqMessageToDraft }) async 
    test('test rsMsgsMessageToDraft', () async {
      // TODO
    });

    // MessageToTrash
    //
    //Future<ResMessageToTrash> rsMsgsMessageToTrash({ ReqMessageToTrash reqMessageToTrash }) async 
    test('test rsMsgsMessageToTrash', () async {
      // TODO
    });

    // removeMessageTagType
    //
    //Future<ResRemoveMessageTagType> rsMsgsRemoveMessageTagType({ ReqRemoveMessageTagType reqRemoveMessageTagType }) async 
    test('test rsMsgsRemoveMessageTagType', () async {
      // TODO
    });

    // resetMessageStandardTagTypes
    //
    //Future<ResResetMessageStandardTagTypes> rsMsgsResetMessageStandardTagTypes() async 
    test('test rsMsgsResetMessageStandardTagTypes', () async {
      // TODO
    });

    // sendChat send a chat message to a given id
    //
    //Future<ResSendChat> rsMsgsSendChat({ ReqSendChat reqSendChat }) async 
    test('test rsMsgsSendChat', () async {
      // TODO
    });

    // sendLobbyStatusPeerLeaving notify friend nodes that we're leaving a subscribed lobby
    //
    //Future rsMsgsSendLobbyStatusPeerLeaving({ ReqSendLobbyStatusPeerLeaving reqSendLobbyStatusPeerLeaving }) async 
    test('test rsMsgsSendLobbyStatusPeerLeaving', () async {
      // TODO
    });

    // sendMail
    //
    //Future<ResSendMail> rsMsgsSendMail({ ReqSendMail reqSendMail }) async 
    test('test rsMsgsSendMail', () async {
      // TODO
    });

    // sendStatusString send a status string
    //
    //Future rsMsgsSendStatusString({ ReqSendStatusString reqSendStatusString }) async 
    test('test rsMsgsSendStatusString', () async {
      // TODO
    });

    // setCustomStateString set your custom status message
    //
    //Future rsMsgsSetCustomStateString({ ReqSetCustomStateString reqSetCustomStateString }) async 
    test('test rsMsgsSetCustomStateString', () async {
      // TODO
    });

    // setDefaultIdentityForChatLobby set the default identity used for chat lobbies
    //
    //Future<ResSetDefaultIdentityForChatLobby> rsMsgsSetDefaultIdentityForChatLobby({ ReqSetDefaultIdentityForChatLobby reqSetDefaultIdentityForChatLobby }) async 
    test('test rsMsgsSetDefaultIdentityForChatLobby', () async {
      // TODO
    });

    // setIdentityForChatLobby set the chat identit
    //
    //Future<ResSetIdentityForChatLobby> rsMsgsSetIdentityForChatLobby({ ReqSetIdentityForChatLobby reqSetIdentityForChatLobby }) async 
    test('test rsMsgsSetIdentityForChatLobby', () async {
      // TODO
    });

    // setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
    //
    //Future rsMsgsSetLobbyAutoSubscribe({ ReqSetLobbyAutoSubscribe reqSetLobbyAutoSubscribe }) async 
    test('test rsMsgsSetLobbyAutoSubscribe', () async {
      // TODO
    });

    // setMessageTag set == false && tagId == 0
    //
    //Future<ResSetMessageTag> rsMsgsSetMessageTag({ ReqSetMessageTag reqSetMessageTag }) async 
    test('test rsMsgsSetMessageTag', () async {
      // TODO
    });

    // setMessageTagType
    //
    //Future<ResSetMessageTagType> rsMsgsSetMessageTagType({ ReqSetMessageTagType reqSetMessageTagType }) async 
    test('test rsMsgsSetMessageTagType', () async {
      // TODO
    });

    // SystemMessage
    //
    //Future<ResSystemMessage> rsMsgsSystemMessage({ ReqSystemMessage reqSystemMessage }) async 
    test('test rsMsgsSystemMessage', () async {
      // TODO
    });

    // unsubscribeChatLobby leave a chat lobby
    //
    //Future rsMsgsUnsubscribeChatLobby({ ReqUnsubscribeChatLobby reqUnsubscribeChatLobby }) async 
    test('test rsMsgsUnsubscribeChatLobby', () async {
      // TODO
    });

    // Add trusted node from invite
    //
    //Future<ResAcceptInvite> rsPeersAcceptInvite({ ReqAcceptInvite reqAcceptInvite }) async 
    test('test rsPeersAcceptInvite', () async {
      // TODO
    });

    // Add trusted node
    //
    //Future<ResAddFriend> rsPeersAddFriend({ ReqAddFriend reqAddFriend }) async 
    test('test rsPeersAddFriend', () async {
      // TODO
    });

    // addGroup create a new group
    //
    //Future<ResAddGroup> rsPeersAddGroup({ ReqAddGroup reqAddGroup }) async 
    test('test rsPeersAddGroup', () async {
      // TODO
    });

    // Add URL locator for given peer
    //
    //Future<ResAddPeerLocator> rsPeersAddPeerLocator({ ReqAddPeerLocator reqAddPeerLocator }) async 
    test('test rsPeersAddPeerLocator', () async {
      // TODO
    });

    // Add SSL-only trusted node When adding an SSL-only node, it is authorized to connect. Every time a connection is established the user is notified about the need to verify the PGP fingerprint, until she does, at that point the node become a full SSL+PGP friend.
    //
    //Future<ResAddSslOnlyFriend> rsPeersAddSslOnlyFriend({ ReqAddSslOnlyFriend reqAddSslOnlyFriend }) async 
    test('test rsPeersAddSslOnlyFriend', () async {
      // TODO
    });

    // assignPeerToGroup add a peer to a group
    //
    //Future<ResAssignPeerToGroup> rsPeersAssignPeerToGroup({ ReqAssignPeerToGroup reqAssignPeerToGroup }) async 
    test('test rsPeersAssignPeerToGroup', () async {
      // TODO
    });

    // assignPeersToGroup add a list of peers to a group
    //
    //Future<ResAssignPeersToGroup> rsPeersAssignPeersToGroup({ ReqAssignPeersToGroup reqAssignPeersToGroup }) async 
    test('test rsPeersAssignPeersToGroup', () async {
      // TODO
    });

    // Trigger connection attempt to given node
    //
    //Future<ResConnectAttempt> rsPeersConnectAttempt({ ReqConnectAttempt reqConnectAttempt }) async 
    test('test rsPeersConnectAttempt', () async {
      // TODO
    });

    // editGroup edit an existing group
    //
    //Future<ResEditGroup> rsPeersEditGroup({ ReqEditGroup reqEditGroup }) async 
    test('test rsPeersEditGroup', () async {
      // TODO
    });

    // Get trusted peers list
    //
    //Future<ResGetFriendList> rsPeersGetFriendList() async 
    test('test rsPeersGetFriendList', () async {
      // TODO
    });

    // Get PGP id for the given peer
    //
    //Future<ResGetGPGId> rsPeersGetGPGId({ ReqGetGPGId reqGetGPGId }) async 
    test('test rsPeersGetGPGId', () async {
      // TODO
    });

    // getGroupInfo get group information to one group
    //
    //Future<ResGetGroupInfo> rsPeersGetGroupInfo({ ReqGetGroupInfo reqGetGroupInfo }) async 
    test('test rsPeersGetGroupInfo', () async {
      // TODO
    });

    // getGroupInfoByName get group information by group name
    //
    //Future<ResGetGroupInfoByName> rsPeersGetGroupInfoByName({ ReqGetGroupInfoByName reqGetGroupInfoByName }) async 
    test('test rsPeersGetGroupInfoByName', () async {
      // TODO
    });

    // getGroupInfoList get list of all groups
    //
    //Future<ResGetGroupInfoList> rsPeersGetGroupInfoList() async 
    test('test rsPeersGetGroupInfoList', () async {
      // TODO
    });

    // Get connected peers list
    //
    //Future<ResGetOnlineList> rsPeersGetOnlineList() async 
    test('test rsPeersGetOnlineList', () async {
      // TODO
    });

    // Get details details of the given peer
    //
    //Future<ResGetPeerDetails> rsPeersGetPeerDetails({ ReqGetPeerDetails reqGetPeerDetails }) async 
    test('test rsPeersGetPeerDetails', () async {
      // TODO
    });

    // Get peers count
    //
    //Future<ResGetPeersCount> rsPeersGetPeersCount({ ReqGetPeersCount reqGetPeersCount }) async 
    test('test rsPeersGetPeersCount', () async {
      // TODO
    });

    // Get trusted PGP ids list
    //
    //Future<ResGetPgpFriendList> rsPeersGetPgpFriendList() async 
    test('test rsPeersGetPgpFriendList', () async {
      // TODO
    });

    // Get RetroShare invite of the given peer
    //
    //Future<ResGetRetroshareInvite> rsPeersGetRetroshareInvite({ ReqGetRetroshareInvite reqGetRetroshareInvite }) async 
    test('test rsPeersGetRetroshareInvite', () async {
      // TODO
    });

    // Get RetroShare short invite of the given peer
    //
    //Future<ResGetShortInvite> rsPeersGetShortInvite({ ReqGetShortInvite reqGetShortInvite }) async 
    test('test rsPeersGetShortInvite', () async {
      // TODO
    });

    // Check if given peer is a trusted node
    //
    //Future<ResIsFriend> rsPeersIsFriend({ ReqIsFriend reqIsFriend }) async 
    test('test rsPeersIsFriend', () async {
      // TODO
    });

    // Check if there is an established connection to the given peer
    //
    //Future<ResIsOnline> rsPeersIsOnline({ ReqIsOnline reqIsOnline }) async 
    test('test rsPeersIsOnline', () async {
      // TODO
    });

    // Check if given PGP id is trusted
    //
    //Future<ResIsPgpFriend> rsPeersIsPgpFriend({ ReqIsPgpFriend reqIsPgpFriend }) async 
    test('test rsPeersIsPgpFriend', () async {
      // TODO
    });

    // Check if given peer is a trusted SSL node pending PGP approval Peers added through short invite remain in this state as long as their PGP key is not received and verified/approved by the user.
    //
    //Future<ResIsSslOnlyFriend> rsPeersIsSslOnlyFriend({ ReqIsSslOnlyFriend reqIsSslOnlyFriend }) async 
    test('test rsPeersIsSslOnlyFriend', () async {
      // TODO
    });

    // Import certificate into the keyring
    //
    //Future<ResLoadCertificateFromString> rsPeersLoadCertificateFromString({ ReqLoadCertificateFromString reqLoadCertificateFromString }) async 
    test('test rsPeersLoadCertificateFromString', () async {
      // TODO
    });

    // Examine certificate and get details without importing into the keyring
    //
    //Future<ResLoadDetailsFromStringCert> rsPeersLoadDetailsFromStringCert({ ReqLoadDetailsFromStringCert reqLoadDetailsFromStringCert }) async 
    test('test rsPeersLoadDetailsFromStringCert', () async {
      // TODO
    });

    // Parse the give short invite to extract contained information
    //
    //Future<ResParseShortInvite> rsPeersParseShortInvite({ ReqParseShortInvite reqParseShortInvite }) async 
    test('test rsPeersParseShortInvite', () async {
      // TODO
    });

    // Convert PGP fingerprint to PGP id
    //
    //Future<ResPgpIdFromFingerprint> rsPeersPgpIdFromFingerprint({ ReqPgpIdFromFingerprint reqPgpIdFromFingerprint }) async 
    test('test rsPeersPgpIdFromFingerprint', () async {
      // TODO
    });

    // Revoke connection trust from to node
    //
    //Future<ResRemoveFriend> rsPeersRemoveFriend({ ReqRemoveFriend reqRemoveFriend }) async 
    test('test rsPeersRemoveFriend', () async {
      // TODO
    });

    // Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
    //
    //Future<ResRemoveFriendLocation> rsPeersRemoveFriendLocation({ ReqRemoveFriendLocation reqRemoveFriendLocation }) async 
    test('test rsPeersRemoveFriendLocation', () async {
      // TODO
    });

    // removeGroup remove a group
    //
    //Future<ResRemoveGroup> rsPeersRemoveGroup({ ReqRemoveGroup reqRemoveGroup }) async 
    test('test rsPeersRemoveGroup', () async {
      // TODO
    });

    // Set (dynamical) domain name associated to the given peer
    //
    //Future<ResSetDynDNS> rsPeersSetDynDNS({ ReqSetDynDNS reqSetDynDNS }) async 
    test('test rsPeersSetDynDNS', () async {
      // TODO
    });

    // Set external IPv4 address for given peer
    //
    //Future<ResSetExtAddress> rsPeersSetExtAddress({ ReqSetExtAddress reqSetExtAddress }) async 
    test('test rsPeersSetExtAddress', () async {
      // TODO
    });

    // Set local IPv4 address for the given peer
    //
    //Future<ResSetLocalAddress> rsPeersSetLocalAddress({ ReqSetLocalAddress reqSetLocalAddress }) async 
    test('test rsPeersSetLocalAddress', () async {
      // TODO
    });

    // Set network mode of the given peer
    //
    //Future<ResSetNetworkMode> rsPeersSetNetworkMode({ ReqSetNetworkMode reqSetNetworkMode }) async 
    test('test rsPeersSetNetworkMode', () async {
      // TODO
    });

    // set DHT and discovery modes
    //
    //Future<ResSetVisState> rsPeersSetVisState({ ReqSetVisState reqSetVisState }) async 
    test('test rsPeersSetVisState', () async {
      // TODO
    });

    // null
    //
    //Future<ResRequestStatus> rsPostedRequestStatus({ ReqRequestStatus reqRequestStatus }) async 
    test('test rsPostedRequestStatus', () async {
      // TODO
    });

    // check if giving automatic positive opinion when flagging as contact is enbaled
    //
    //Future<ResAutoPositiveOpinionForContacts> rsReputationsAutoPositiveOpinionForContacts() async 
    test('test rsReputationsAutoPositiveOpinionForContacts', () async {
      // TODO
    });

    // Enable automatic banning of all identities signed by the given node
    //
    //Future rsReputationsBanNode({ ReqBanNode reqBanNode }) async 
    test('test rsReputationsBanNode', () async {
      // TODO
    });

    // Get own opition about the given identity
    //
    //Future<ResGetOwnOpinion> rsReputationsGetOwnOpinion({ ReqGetOwnOpinion reqGetOwnOpinion }) async 
    test('test rsReputationsGetOwnOpinion', () async {
      // TODO
    });

    // Get reputation data of given identity
    //
    //Future<ResGetReputationInfo> rsReputationsGetReputationInfo({ ReqGetReputationInfo reqGetReputationInfo }) async 
    test('test rsReputationsGetReputationInfo', () async {
      // TODO
    });

    // This method allow fast checking if a GXS identity is banned.
    //
    //Future<ResIsIdentityBanned> rsReputationsIsIdentityBanned({ ReqIsIdentityBanned reqIsIdentityBanned }) async 
    test('test rsReputationsIsIdentityBanned', () async {
      // TODO
    });

    // Check if automatic banning of all identities signed by the given node is enabled
    //
    //Future<ResIsNodeBanned> rsReputationsIsNodeBanned({ ReqIsNodeBanned reqIsNodeBanned }) async 
    test('test rsReputationsIsNodeBanned', () async {
      // TODO
    });

    // Get overall reputation level of given identity
    //
    //Future<ResOverallReputationLevel> rsReputationsOverallReputationLevel({ ReqOverallReputationLevel reqOverallReputationLevel }) async 
    test('test rsReputationsOverallReputationLevel', () async {
      // TODO
    });

    // Get number of days to wait before deleting a banned identity from local storage
    //
    //Future<ResRememberBannedIdThreshold> rsReputationsRememberBannedIdThreshold() async 
    test('test rsReputationsRememberBannedIdThreshold', () async {
      // TODO
    });

    // Enable giving automatic positive opinion when flagging as contact
    //
    //Future rsReputationsSetAutoPositiveOpinionForContacts({ ReqSetAutoPositiveOpinionForContacts reqSetAutoPositiveOpinionForContacts }) async 
    test('test rsReputationsSetAutoPositiveOpinionForContacts', () async {
      // TODO
    });

    // Set own opinion about the given identity
    //
    //Future<ResSetOwnOpinion> rsReputationsSetOwnOpinion({ ReqSetOwnOpinion reqSetOwnOpinion }) async 
    test('test rsReputationsSetOwnOpinion', () async {
      // TODO
    });

    // Set number of days to wait before deleting a banned identity from local storage
    //
    //Future rsReputationsSetRememberBannedIdThreshold({ ReqSetRememberBannedIdThreshold reqSetRememberBannedIdThreshold }) async 
    test('test rsReputationsSetRememberBannedIdThreshold', () async {
      // TODO
    });

    // Set threshold on remote reputation to consider it remotely negative
    //
    //Future rsReputationsSetThresholdForRemotelyNegativeReputation({ ReqSetThresholdForRemotelyNegativeReputation reqSetThresholdForRemotelyNegativeReputation }) async 
    test('test rsReputationsSetThresholdForRemotelyNegativeReputation', () async {
      // TODO
    });

    // Set threshold on remote reputation to consider it remotely positive
    //
    //Future rsReputationsSetThresholdForRemotelyPositiveReputation({ ReqSetThresholdForRemotelyPositiveReputation reqSetThresholdForRemotelyPositiveReputation }) async 
    test('test rsReputationsSetThresholdForRemotelyPositiveReputation', () async {
      // TODO
    });

    // Get threshold on remote reputation to consider it remotely negative
    //
    //Future<ResThresholdForRemotelyNegativeReputation> rsReputationsThresholdForRemotelyNegativeReputation() async 
    test('test rsReputationsThresholdForRemotelyNegativeReputation', () async {
      // TODO
    });

    // Get threshold on remote reputation to consider it remotely negative
    //
    //Future<ResThresholdForRemotelyPositiveReputation> rsReputationsThresholdForRemotelyPositiveReputation() async 
    test('test rsReputationsThresholdForRemotelyPositiveReputation', () async {
      // TODO
    });

    // get a map off all services.
    //
    //Future<ResGetOwnServices> rsServiceControlGetOwnServices() async 
    test('test rsServiceControlGetOwnServices', () async {
      // TODO
    });

    // getPeersConnected return peers using a service.
    //
    //Future<ResGetPeersConnected> rsServiceControlGetPeersConnected({ ReqGetPeersConnected reqGetPeersConnected }) async 
    test('test rsServiceControlGetPeersConnected', () async {
      // TODO
    });

    // getServiceItemNames return a map of service item names.
    //
    //Future<ResGetServiceItemNames> rsServiceControlGetServiceItemNames({ ReqGetServiceItemNames reqGetServiceItemNames }) async 
    test('test rsServiceControlGetServiceItemNames', () async {
      // TODO
    });

    // getServiceName lookup the name of a service.
    //
    //Future<ResGetServiceName> rsServiceControlGetServiceName({ ReqGetServiceName reqGetServiceName }) async 
    test('test rsServiceControlGetServiceName', () async {
      // TODO
    });

    // getServicePermissions return permissions of one service.
    //
    //Future<ResGetServicePermissions> rsServiceControlGetServicePermissions({ ReqGetServicePermissions reqGetServicePermissions }) async 
    test('test rsServiceControlGetServicePermissions', () async {
      // TODO
    });

    // getServicesAllowed return a mpa with allowed service information.
    //
    //Future<ResGetServicesAllowed> rsServiceControlGetServicesAllowed({ ReqGetServicesAllowed reqGetServicesAllowed }) async 
    test('test rsServiceControlGetServicesAllowed', () async {
      // TODO
    });

    // getServicesProvided return services provided by a peer.
    //
    //Future<ResGetServicesProvided> rsServiceControlGetServicesProvided({ ReqGetServicesProvided reqGetServicesProvided }) async 
    test('test rsServiceControlGetServicesProvided', () async {
      // TODO
    });

    // updateServicePermissions update service permissions of one service.
    //
    //Future<ResUpdateServicePermissions> rsServiceControlUpdateServicePermissions({ ReqUpdateServicePermissions reqUpdateServicePermissions }) async 
    test('test rsServiceControlUpdateServicePermissions', () async {
      // TODO
    });

  });
}

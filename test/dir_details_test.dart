import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for DirDetails
void main() {
    var instance = new DirDetails();

  group('test DirDetails', () {
    // ReqBanFileFileSize parent (default value: null)
    test('to test the property `parent`', () async {
      // TODO
    });

    // int prow (default value: null)
    test('to test the property `prow`', () async {
      // TODO
    });

    // ReqBanFileFileSize ref (default value: null)
    test('to test the property `ref`', () async {
      // TODO
    });

    // int type (default value: null)
    test('to test the property `type`', () async {
      // TODO
    });

    // String id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // String name (default value: null)
    test('to test the property `name`', () async {
      // TODO
    });

    // String hash (default value: null)
    test('to test the property `hash`', () async {
      // TODO
    });

    // String path (default value: null)
    test('to test the property `path`', () async {
      // TODO
    });

    // ReqBanFileFileSize count (default value: null)
    test('to test the property `count`', () async {
      // TODO
    });

    // int mtime (default value: null)
    test('to test the property `mtime`', () async {
      // TODO
    });

    // int flags (default value: null)
    test('to test the property `flags`', () async {
      // TODO
    });

    // int maxMtime (default value: null)
    test('to test the property `maxMtime`', () async {
      // TODO
    });

    // List<DirStub> children (default value: [])
    test('to test the property `children`', () async {
      // TODO
    });

    // List<String> parentGroups (default value: [])
    test('to test the property `parentGroups`', () async {
      // TODO
    });


  });

}

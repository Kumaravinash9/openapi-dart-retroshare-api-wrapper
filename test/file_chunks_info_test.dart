import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for FileChunksInfo
void main() {
    var instance = new FileChunksInfo();

  group('test FileChunksInfo', () {
    // ReqBanFileFileSize fileSize (default value: null)
    test('to test the property `fileSize`', () async {
      // TODO
    });

    // int chunkSize (default value: null)
    test('to test the property `chunkSize`', () async {
      // TODO
    });

    // FileChunksInfoChunkStrategy strategy (default value: null)
    test('to test the property `strategy`', () async {
      // TODO
    });

    // List<ChunkState> chunks (default value: [])
    test('to test the property `chunks`', () async {
      // TODO
    });

    // List<FileChunksInfoCompressedPeerAvailabilityMaps> compressedPeerAvailabilityMaps (default value: [])
    test('to test the property `compressedPeerAvailabilityMaps`', () async {
      // TODO
    });

    // List<FileChunksInfoActiveChunks> activeChunks (default value: [])
    test('to test the property `activeChunks`', () async {
      // TODO
    });

    // List<FileChunksInfoPendingSlices> pendingSlices (default value: [])
    test('to test the property `pendingSlices`', () async {
      // TODO
    });


  });

}

import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for RsPeerDetails
void main() {
    var instance = new RsPeerDetails();

  group('test RsPeerDetails', () {
    // bool isOnlyGPGdetail (default value: null)
    test('to test the property `isOnlyGPGdetail`', () async {
      // TODO
    });

    // String id (default value: null)
    test('to test the property `id`', () async {
      // TODO
    });

    // String gpgId (default value: null)
    test('to test the property `gpgId`', () async {
      // TODO
    });

    // String name (default value: null)
    test('to test the property `name`', () async {
      // TODO
    });

    // String email (default value: null)
    test('to test the property `email`', () async {
      // TODO
    });

    // String location (default value: null)
    test('to test the property `location`', () async {
      // TODO
    });

    // String org (default value: null)
    test('to test the property `org`', () async {
      // TODO
    });

    // String issuer (default value: null)
    test('to test the property `issuer`', () async {
      // TODO
    });

    // String fpr (default value: null)
    test('to test the property `fpr`', () async {
      // TODO
    });

    // String authcode (default value: null)
    test('to test the property `authcode`', () async {
      // TODO
    });

    // List<String> gpgSigners (default value: [])
    test('to test the property `gpgSigners`', () async {
      // TODO
    });

    // int trustLvl (default value: null)
    test('to test the property `trustLvl`', () async {
      // TODO
    });

    // int validLvl (default value: null)
    test('to test the property `validLvl`', () async {
      // TODO
    });

    // bool skipPgpSignatureValidation (default value: null)
    test('to test the property `skipPgpSignatureValidation`', () async {
      // TODO
    });

    // bool ownsign (default value: null)
    test('to test the property `ownsign`', () async {
      // TODO
    });

    // bool hasSignedMe (default value: null)
    test('to test the property `hasSignedMe`', () async {
      // TODO
    });

    // bool acceptConnection (default value: null)
    test('to test the property `acceptConnection`', () async {
      // TODO
    });

    // int servicePermFlags (default value: null)
    test('to test the property `servicePermFlags`', () async {
      // TODO
    });

    // int state (default value: null)
    test('to test the property `state`', () async {
      // TODO
    });

    // bool actAsServer (default value: null)
    test('to test the property `actAsServer`', () async {
      // TODO
    });

    // String connectAddr (default value: null)
    test('to test the property `connectAddr`', () async {
      // TODO
    });

    // int connectPort (default value: null)
    test('to test the property `connectPort`', () async {
      // TODO
    });

    // bool isHiddenNode (default value: null)
    test('to test the property `isHiddenNode`', () async {
      // TODO
    });

    // String hiddenNodeAddress (default value: null)
    test('to test the property `hiddenNodeAddress`', () async {
      // TODO
    });

    // int hiddenNodePort (default value: null)
    test('to test the property `hiddenNodePort`', () async {
      // TODO
    });

    // int hiddenType (default value: null)
    test('to test the property `hiddenType`', () async {
      // TODO
    });

    // String localAddr (default value: null)
    test('to test the property `localAddr`', () async {
      // TODO
    });

    // int localPort (default value: null)
    test('to test the property `localPort`', () async {
      // TODO
    });

    // String extAddr (default value: null)
    test('to test the property `extAddr`', () async {
      // TODO
    });

    // int extPort (default value: null)
    test('to test the property `extPort`', () async {
      // TODO
    });

    // String dyndns (default value: null)
    test('to test the property `dyndns`', () async {
      // TODO
    });

    // List<String> ipAddressList (default value: [])
    test('to test the property `ipAddressList`', () async {
      // TODO
    });

    // int netMode (default value: null)
    test('to test the property `netMode`', () async {
      // TODO
    });

    // int vsDisc (default value: null)
    test('to test the property `vsDisc`', () async {
      // TODO
    });

    // int vsDht (default value: null)
    test('to test the property `vsDht`', () async {
      // TODO
    });

    // int lastConnect (default value: null)
    test('to test the property `lastConnect`', () async {
      // TODO
    });

    // int lastUsed (default value: null)
    test('to test the property `lastUsed`', () async {
      // TODO
    });

    // int connectState (default value: null)
    test('to test the property `connectState`', () async {
      // TODO
    });

    // String connectStateString (default value: null)
    test('to test the property `connectStateString`', () async {
      // TODO
    });

    // int connectPeriod (default value: null)
    test('to test the property `connectPeriod`', () async {
      // TODO
    });

    // bool foundDHT (default value: null)
    test('to test the property `foundDHT`', () async {
      // TODO
    });

    // bool wasDeniedConnection (default value: null)
    test('to test the property `wasDeniedConnection`', () async {
      // TODO
    });

    // RstimeT deniedTS (default value: null)
    test('to test the property `deniedTS`', () async {
      // TODO
    });

    // int linkType (default value: null)
    test('to test the property `linkType`', () async {
      // TODO
    });


  });

}

import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for jsonApiServerAuthorizeToken
void main() {

  group('test jsonApiServerAuthorizeToken', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test jsonApiServerAuthorizeToken', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = '814228577bc0c5da968c79272adcbfce';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'prueba';

        var authorizeuser = ReqAuthorizeUser(); // ReqJsonApiServerRequestNewTokenAutorization | token: >              (string)token to autorize
        authorizeuser.user = "test";
        authorizeuser.password = "test";

        var api_instance = DefaultApi();
        api_instance.rsJsonApiAuthorizeUser(reqAuthorizeUser: authorizeuser)
        .then((onValue){
          print(onValue);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->jsonApiServerAuthorizeToken: $e\n");
      }

    });


  });

}

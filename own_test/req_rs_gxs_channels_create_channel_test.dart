// Deprecated USE CREATE CHANNEL V2

// import 'package:openapi/api.dart';
// import 'package:test/test.dart';

// // tests for rsGxsChannelsCreateChannel
// void main() {

//   group('test rsGxsChannelsCreateChannel', () {
//     // RsInitLoadCertificateStatus retval (default value: null)
//     test('to test rsGxsChannelsCreateChannel', () async {
//       try { 

//         defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
//         defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

//         var channelReq = ReqRsGxsChannelsCreateChannel(); // ReqRsGxsChannelsCreateChannel | channel: >              (RsGxsChannelGroup)Channel data (name, description...)  
//         // First metadata of the channel
//         var channelMeta = RsGroupMetaData();
//         channelMeta.mGroupName = "Dart channel creation";
//         channelMeta.mGroupFlags = 4;
//         channelMeta.mSignFlags = 250;
//         // Crate the channel group
//         var channel = RsGxsChannelGroup();
//         // channel.mMeta = channelMeta;
//         channel.mDescription = "Description of this Dart created channel";
//         // Finally add the channel to the request
//         channelReq.channel = channel;

//         var api_instance = DefaultApi();
//         api_instance.rsGxsChannelsCreateChannel(reqRsGxsChannelsCreateChannel : channelReq)
//           .then((onValue){
//             print(onValue);
//         });
//       } catch (e) {
//         print("Exception when calling DefaultApi->rsGxsChannelsCreateChannel: $e\n");
//       }

//     });


//   });

// }

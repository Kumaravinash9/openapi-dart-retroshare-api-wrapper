import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for ReqRsFilesAddSharedDirectory
void main() {

  group('test ReqRsFilesAddSharedDirectory', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test ReqRsFilesAddSharedDirectory', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

        String filename = "/tmp";
        String virtualname = "tmpTest";
        int shareflags = 128; // For share flags see FileStorageFlags
        
        var reqRsFilesAddSharedDirectory = ReqAddSharedDirectory();

        var sharedDirInfo = SharedDirInfo();
        sharedDirInfo.filename = filename;
        sharedDirInfo.virtualname = virtualname;
        sharedDirInfo.shareflags = shareflags;
        
        reqRsFilesAddSharedDirectory.dir = sharedDirInfo;

        var api_instance = DefaultApi();
        api_instance.rsFilesAddSharedDirectory(reqAddSharedDirectory: reqRsFilesAddSharedDirectory).then((onValue){
          print(onValue);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->ReqRsFilesAddSharedDirectory: $e\n");
      }

    });


  });

}

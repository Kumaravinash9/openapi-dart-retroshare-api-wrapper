import 'package:openapi/api.dart';
import 'package:test/test.dart';

// tests for ReqRsIdentityCreateIdentity
void main() {

  group('test ReqRsIdentityCreateIdentity', () {
    // RsInitLoadCertificateStatus retval (default value: null)
    test('to test ReqRsIdentityCreateIdentity', () async {
      try { 

        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').username = 'test';
        defaultApiClient.getAuthentication<HttpBasicAuth>('BasicAuth').password = 'test';

        var reqRsIdentityCreateIdentity = ReqCreateIdentity();

        reqRsIdentityCreateIdentity.name = "DartIdentityLinked";
        reqRsIdentityCreateIdentity.pseudonimous = false;
        // If pseudonimus is false you will need to provide your pgp password to link it to your profile
        reqRsIdentityCreateIdentity.pgpPassword = "prueba";
        
        var api_instance = DefaultApi();
        api_instance.rsIdentityCreateIdentity(
            reqCreateIdentity: reqRsIdentityCreateIdentity
        ).then((onValue){
          print(onValue);
        });
      } catch (e) {
        print("Exception when calling DefaultApi->ReqRsIdentityCreateIdentity: $e\n");
      }

    });


  });

}

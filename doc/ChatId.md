# openapi.model.ChatId

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**broadcastStatusPeerId** | **String** |  | [optional] [default to null]
**type** | [**ChatIdType**](ChatIdType.md) |  | [optional] [default to null]
**peerId** | **String** |  | [optional] [default to null]
**distantChatId** | **String** |  | [optional] [default to null]
**lobbyId** | [**ChatLobbyId**](ChatLobbyId.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



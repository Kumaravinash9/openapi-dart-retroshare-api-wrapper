# openapi.model.RsGxsVote

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] [default to null]
**mVoteType** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



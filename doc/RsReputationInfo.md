# openapi.model.RsReputationInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mOwnOpinion** | [**RsOpinion**](RsOpinion.md) |  | [optional] [default to null]
**mFriendsPositiveVotes** | **int** |  | [optional] [default to null]
**mFriendsNegativeVotes** | **int** |  | [optional] [default to null]
**mFriendAverageScore** | **num** |  | [optional] [default to null]
**mOverallReputationLevel** | [**RsReputationLevel**](RsReputationLevel.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



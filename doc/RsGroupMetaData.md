# openapi.model.RsGroupMetaData

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] [default to null]
**mGroupName** | **String** |  | [optional] [default to null]
**mGroupFlags** | **int** |  | [optional] [default to null]
**mSignFlags** | **int** |  | [optional] [default to null]
**mPublishTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mAuthorId** | **String** |  | [optional] [default to null]
**mCircleId** | **String** |  | [optional] [default to null]
**mCircleType** | **int** |  | [optional] [default to null]
**mAuthenFlags** | **int** |  | [optional] [default to null]
**mParentGrpId** | **String** |  | [optional] [default to null]
**mSubscribeFlags** | **int** |  | [optional] [default to null]
**mPop** | **int** |  | [optional] [default to null]
**mVisibleMsgCount** | **int** |  | [optional] [default to null]
**mLastPost** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mGroupStatus** | **int** |  | [optional] [default to null]
**mServiceString** | **String** |  | [optional] [default to null]
**mOriginator** | **String** |  | [optional] [default to null]
**mInternalCircle** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



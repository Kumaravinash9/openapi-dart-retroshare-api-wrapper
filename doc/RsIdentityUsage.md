# openapi.model.RsIdentityUsage

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceId** | [**RsServiceType**](RsServiceType.md) |  | [optional] [default to null]
**mUsageCode** | [**UsageCode**](UsageCode.md) |  | [optional] [default to null]
**mGrpId** | **String** |  | [optional] [default to null]
**mMsgId** | **String** |  | [optional] [default to null]
**mAdditionalId** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**mComment** | **String** |  | [optional] [default to null]
**mHash** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



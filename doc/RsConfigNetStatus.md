# openapi.model.RsConfigNetStatus

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownId** | **String** |  | [optional] [default to null]
**ownName** | **String** |  | [optional] [default to null]
**localAddr** | **String** |  | [optional] [default to null]
**localPort** | **int** |  | [optional] [default to null]
**extAddr** | **String** |  | [optional] [default to null]
**extPort** | **int** |  | [optional] [default to null]
**extDynDns** | **String** |  | [optional] [default to null]
**firewalled** | **bool** |  | [optional] [default to null]
**forwardPort** | **bool** |  | [optional] [default to null]
**dHTActive** | **bool** |  | [optional] [default to null]
**uPnPActive** | **bool** |  | [optional] [default to null]
**uPnPState** | **int** |  | [optional] [default to null]
**netLocalOk** | **bool** |  | [optional] [default to null]
**netUpnpOk** | **bool** |  | [optional] [default to null]
**netDhtOk** | **bool** |  | [optional] [default to null]
**netStunOk** | **bool** |  | [optional] [default to null]
**netExtAddressOk** | **bool** |  | [optional] [default to null]
**netDhtNetSize** | **int** |  | [optional] [default to null]
**netDhtRsNetSize** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.RsLoginHelperLocation

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mLocationId** | **String** |  | [optional] [default to null]
**mPgpId** | **String** |  | [optional] [default to null]
**mLocationName** | **String** |  | [optional] [default to null]
**mPgpName** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.ChatLobbyInvite

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyId** | [**ChatLobbyId**](ChatLobbyId.md) |  | [optional] [default to null]
**peerId** | **String** |  | [optional] [default to null]
**lobbyName** | **String** |  | [optional] [default to null]
**lobbyTopic** | **String** |  | [optional] [default to null]
**lobbyFlags** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



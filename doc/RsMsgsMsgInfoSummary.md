# openapi.model.RsMsgsMsgInfoSummary

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**msgId** | **String** |  | [optional] [default to null]
**srcId** | **String** |  | [optional] [default to null]
**msgflags** | **int** |  | [optional] [default to null]
**msgtags** | **List&lt;int&gt;** |  | [optional] [default to []]
**title** | **String** |  | [optional] [default to null]
**count** | **int** |  | [optional] [default to null]
**ts** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



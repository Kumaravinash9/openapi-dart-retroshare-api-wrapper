# openapi.model.ReqBanFile

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**realFileHash** | **String** |  | [optional] [default to null]
**filename** | **String** |  | [optional] [default to null]
**fileSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



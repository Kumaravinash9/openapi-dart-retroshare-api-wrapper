# openapi.model.RsGxsIdGroup

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] [default to null]
**mPgpIdHash** | **String** |  | [optional] [default to null]
**mPgpIdSign** | **String** |  | [optional] [default to null]
**mRecognTags** | **List&lt;String&gt;** |  | [optional] [default to []]
**mImage** | [**RsGxsImage**](RsGxsImage.md) |  | [optional] [default to null]
**mLastUsageTS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mPgpKnown** | **bool** |  | [optional] [default to null]
**mIsAContact** | **bool** |  | [optional] [default to null]
**mPgpId** | **String** |  | [optional] [default to null]
**mReputation** | [**GxsReputation**](GxsReputation.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.ReqCreateVoteV2

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] [default to null]
**postId** | **String** |  | [optional] [default to null]
**commentId** | **String** |  | [optional] [default to null]
**authorId** | **String** |  | [optional] [default to null]
**vote** | [**RsGxsVoteType**](RsGxsVoteType.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



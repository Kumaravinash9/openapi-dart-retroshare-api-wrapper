# openapi.model.BannedFileEntry

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mFilename** | **String** |  | [optional] [default to null]
**mSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**mBanTimeStamp** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



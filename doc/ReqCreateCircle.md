# openapi.model.ReqCreateCircle

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**circleName** | **String** |  | [optional] [default to null]
**circleType** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] [default to null]
**restrictedId** | **String** |  | [optional] [default to null]
**authorId** | **String** |  | [optional] [default to null]
**gxsIdMembers** | **List&lt;String&gt;** |  | [optional] [default to []]
**localMembers** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



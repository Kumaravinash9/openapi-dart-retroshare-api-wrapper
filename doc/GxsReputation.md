# openapi.model.GxsReputation

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mOverallScore** | **int** |  | [optional] [default to null]
**mIdScore** | **int** |  | [optional] [default to null]
**mOwnOpinion** | **int** |  | [optional] [default to null]
**mPeerOpinion** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



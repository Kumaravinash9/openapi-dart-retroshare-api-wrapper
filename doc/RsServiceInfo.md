# openapi.model.RsServiceInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mServiceName** | **String** |  | [optional] [default to null]
**mServiceType** | **int** |  | [optional] [default to null]
**mVersionMajor** | **int** |  | [optional] [default to null]
**mVersionMinor** | **int** |  | [optional] [default to null]
**mMinVersionMajor** | **int** |  | [optional] [default to null]
**mMinVersionMinor** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



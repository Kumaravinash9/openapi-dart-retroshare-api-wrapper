# openapi.model.ReqCreateChatLobby

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lobbyName** | **String** |  | [optional] [default to null]
**lobbyIdentity** | **String** |  | [optional] [default to null]
**lobbyTopic** | **String** |  | [optional] [default to null]
**invitedFriends** | **List&lt;String&gt;** |  | [optional] [default to []]
**lobbyPrivacyType** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



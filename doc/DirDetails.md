# openapi.model.DirDetails

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**prow** | **int** |  | [optional] [default to null]
**ref** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**type** | **int** |  | [optional] [default to null]
**id** | **String** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**hash** | **String** |  | [optional] [default to null]
**path** | **String** |  | [optional] [default to null]
**count** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**mtime** | **int** |  | [optional] [default to null]
**flags** | **int** |  | [optional] [default to null]
**maxMtime** | **int** |  | [optional] [default to null]
**children** | [**List&lt;DirStub&gt;**](DirStub.md) |  | [optional] [default to []]
**parentGroups** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.ReqAddSslOnlyFriend

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sslId** | **String** |  | [optional] [default to null]
**pgpId** | **String** |  | [optional] [default to null]
**details** | [**RsPeerDetails**](RsPeerDetails.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.ReqCreateCommentV2

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**channelId** | **String** |  | [optional] [default to null]
**threadId** | **String** |  | [optional] [default to null]
**comment** | **String** |  | [optional] [default to null]
**authorId** | **String** |  | [optional] [default to null]
**parentId** | **String** |  | [optional] [default to null]
**origCommentId** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



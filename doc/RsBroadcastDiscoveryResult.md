# openapi.model.RsBroadcastDiscoveryResult

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mPgpFingerprint** | **String** |  | [optional] [default to null]
**mSslId** | **String** |  | [optional] [default to null]
**mProfileName** | **String** |  | [optional] [default to null]
**mLocator** | [**RsUrl**](RsUrl.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



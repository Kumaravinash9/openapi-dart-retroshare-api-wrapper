# openapi.model.FileChunksInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fileSize** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]
**chunkSize** | **int** |  | [optional] [default to null]
**strategy** | [**FileChunksInfoChunkStrategy**](FileChunksInfoChunkStrategy.md) |  | [optional] [default to null]
**chunks** | [**List&lt;ChunkState&gt;**](ChunkState.md) |  | [optional] [default to []]
**compressedPeerAvailabilityMaps** | [**List&lt;FileChunksInfoCompressedPeerAvailabilityMaps&gt;**](FileChunksInfoCompressedPeerAvailabilityMaps.md) |  | [optional] [default to []]
**activeChunks** | [**List&lt;FileChunksInfoActiveChunks&gt;**](FileChunksInfoActiveChunks.md) |  | [optional] [default to []]
**pendingSlices** | [**List&lt;FileChunksInfoPendingSlices&gt;**](FileChunksInfoPendingSlices.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



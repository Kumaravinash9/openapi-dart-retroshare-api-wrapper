# openapi.model.RsGxsCircleDetails

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mCircleId** | **String** |  | [optional] [default to null]
**mCircleName** | **String** |  | [optional] [default to null]
**mCircleType** | [**RsGxsCircleType**](RsGxsCircleType.md) |  | [optional] [default to null]
**mRestrictedCircleId** | **String** |  | [optional] [default to null]
**mAmIAllowed** | **bool** |  | [optional] [default to null]
**mAmIAdmin** | **bool** |  | [optional] [default to null]
**mAllowedGxsIds** | **List&lt;String&gt;** |  | [optional] [default to []]
**mAllowedNodes** | **List&lt;String&gt;** |  | [optional] [default to []]
**mSubscriptionFlags** | [**List&lt;RsGxsCircleDetailsMSubscriptionFlags&gt;**](RsGxsCircleDetailsMSubscriptionFlags.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



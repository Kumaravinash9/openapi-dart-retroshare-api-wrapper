# openapi.model.ReqSendMail

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** |  | [optional] [default to null]
**subject** | **String** |  | [optional] [default to null]
**mailBody** | **String** |  | [optional] [default to null]
**to** | **List&lt;String&gt;** |  | [optional] [default to []]
**cc** | **List&lt;String&gt;** |  | [optional] [default to []]
**bcc** | **List&lt;String&gt;** |  | [optional] [default to []]
**attachments** | [**List&lt;FileInfo&gt;**](FileInfo.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.RsPeerDetails

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isOnlyGPGdetail** | **bool** |  | [optional] [default to null]
**id** | **String** |  | [optional] [default to null]
**gpgId** | **String** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**email** | **String** |  | [optional] [default to null]
**location** | **String** |  | [optional] [default to null]
**org** | **String** |  | [optional] [default to null]
**issuer** | **String** |  | [optional] [default to null]
**fpr** | **String** |  | [optional] [default to null]
**authcode** | **String** |  | [optional] [default to null]
**gpgSigners** | **List&lt;String&gt;** |  | [optional] [default to []]
**trustLvl** | **int** |  | [optional] [default to null]
**validLvl** | **int** |  | [optional] [default to null]
**skipPgpSignatureValidation** | **bool** |  | [optional] [default to null]
**ownsign** | **bool** |  | [optional] [default to null]
**hasSignedMe** | **bool** |  | [optional] [default to null]
**acceptConnection** | **bool** |  | [optional] [default to null]
**servicePermFlags** | **int** |  | [optional] [default to null]
**state** | **int** |  | [optional] [default to null]
**actAsServer** | **bool** |  | [optional] [default to null]
**connectAddr** | **String** |  | [optional] [default to null]
**connectPort** | **int** |  | [optional] [default to null]
**isHiddenNode** | **bool** |  | [optional] [default to null]
**hiddenNodeAddress** | **String** |  | [optional] [default to null]
**hiddenNodePort** | **int** |  | [optional] [default to null]
**hiddenType** | **int** |  | [optional] [default to null]
**localAddr** | **String** |  | [optional] [default to null]
**localPort** | **int** |  | [optional] [default to null]
**extAddr** | **String** |  | [optional] [default to null]
**extPort** | **int** |  | [optional] [default to null]
**dyndns** | **String** |  | [optional] [default to null]
**ipAddressList** | **List&lt;String&gt;** |  | [optional] [default to []]
**netMode** | **int** |  | [optional] [default to null]
**vsDisc** | **int** |  | [optional] [default to null]
**vsDht** | **int** |  | [optional] [default to null]
**lastConnect** | **int** |  | [optional] [default to null]
**lastUsed** | **int** |  | [optional] [default to null]
**connectState** | **int** |  | [optional] [default to null]
**connectStateString** | **String** |  | [optional] [default to null]
**connectPeriod** | **int** |  | [optional] [default to null]
**foundDHT** | **bool** |  | [optional] [default to null]
**wasDeniedConnection** | **bool** |  | [optional] [default to null]
**deniedTS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**linkType** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.RsConfigDataRates

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mRateIn** | **num** |  | [optional] [default to null]
**mRateMaxIn** | **num** |  | [optional] [default to null]
**mAllocIn** | **num** |  | [optional] [default to null]
**mAllocTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mRateOut** | **num** |  | [optional] [default to null]
**mRateMaxOut** | **num** |  | [optional] [default to null]
**mAllowedOut** | **num** |  | [optional] [default to null]
**mAllowedTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mQueueIn** | **int** |  | [optional] [default to null]
**mQueueOut** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



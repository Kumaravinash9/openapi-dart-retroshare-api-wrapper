# openapi.model.RsGxsComment

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsMsgMetaData**](RsMsgMetaData.md) |  | [optional] [default to null]
**mComment** | **String** |  | [optional] [default to null]
**mUpVotes** | **int** |  | [optional] [default to null]
**mDownVotes** | **int** |  | [optional] [default to null]
**mScore** | **num** |  | [optional] [default to null]
**mOwnVote** | **int** |  | [optional] [default to null]
**mVotes** | [**List&lt;RsGxsVote&gt;**](RsGxsVote.md) |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



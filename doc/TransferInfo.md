# openapi.model.TransferInfo

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**peerId** | **String** |  | [optional] [default to null]
**name** | **String** |  | [optional] [default to null]
**tfRate** | **num** |  | [optional] [default to null]
**status** | **int** |  | [optional] [default to null]
**transfered** | [**ReqBanFileFileSize**](ReqBanFileFileSize.md) |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



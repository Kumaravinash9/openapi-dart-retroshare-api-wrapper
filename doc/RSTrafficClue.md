# openapi.model.RSTrafficClue

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TS** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**size** | **int** |  | [optional] [default to null]
**priority** | **int** |  | [optional] [default to null]
**serviceId** | **int** |  | [optional] [default to null]
**serviceSubId** | **int** |  | [optional] [default to null]
**peerId** | **String** |  | [optional] [default to null]
**count** | **int** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



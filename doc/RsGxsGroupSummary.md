# openapi.model.RsGxsGroupSummary

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mGroupId** | **String** |  | [optional] [default to null]
**mGroupName** | **String** |  | [optional] [default to null]
**mAuthorId** | **String** |  | [optional] [default to null]
**mPublishTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mNumberOfMessages** | **int** |  | [optional] [default to null]
**mLastMessageTs** | [**RstimeT**](RstimeT.md) |  | [optional] [default to null]
**mSignFlags** | **int** |  | [optional] [default to null]
**mPopularity** | **int** |  | [optional] [default to null]
**mSearchContext** | **String** |  | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# openapi.model.RsGxsCircleGroup

## Load the model package
```dart
import 'package:openapi/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mMeta** | [**RsGroupMetaData**](RsGroupMetaData.md) |  | [optional] [default to null]
**mLocalFriends** | **List&lt;String&gt;** |  | [optional] [default to []]
**mInvitedMembers** | **List&lt;String&gt;** |  | [optional] [default to []]
**mSubCircles** | **List&lt;String&gt;** |  | [optional] [default to []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



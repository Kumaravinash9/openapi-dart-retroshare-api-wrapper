part of openapi.api;



class DefaultApi {
  final ApiClient apiClient;

  DefaultApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  /// Export full encrypted PGP identity to file with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsExportIdentityWithHttpInfo({ ReqExportIdentity reqExportIdentity }) async {
    Object postBody = reqExportIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/ExportIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Export full encrypted PGP identity to file
  ///
  ///ReqExportIdentity reqExportIdentity :
  ///     filePath: >              (string)path of certificate file          pgpId: >              (RsPgpId)PGP id to export  
  /// 
  Future<ResExportIdentity> rsAccountsExportIdentity({ ReqExportIdentity reqExportIdentity }) async {
    Response response = await rsAccountsExportIdentityWithHttpInfo( reqExportIdentity: reqExportIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportIdentity') as ResExportIdentity;
    } else {
      return null;
    }
  }

  /// Export full encrypted PGP identity to string with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsExportIdentityToStringWithHttpInfo({ ReqExportIdentityToString reqExportIdentityToString }) async {
    Object postBody = reqExportIdentityToString;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/exportIdentityToString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Export full encrypted PGP identity to string
  ///
  ///ReqExportIdentityToString reqExportIdentityToString :
  ///     pgpId: >              (RsPgpId)PGP id to export          includeSignatures: >              (boolean)true to include signatures  
  /// 
  Future<ResExportIdentityToString> rsAccountsExportIdentityToString({ ReqExportIdentityToString reqExportIdentityToString }) async {
    Response response = await rsAccountsExportIdentityToStringWithHttpInfo( reqExportIdentityToString: reqExportIdentityToString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportIdentityToString') as ResExportIdentityToString;
    } else {
      return null;
    }
  }

  /// Get current account id. Beware that an account may be selected without actually logging in. with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsGetCurrentAccountIdWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/getCurrentAccountId".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get current account id. Beware that an account may be selected without actually logging in.
  ///
  /// 
  Future<ResGetCurrentAccountId> rsAccountsGetCurrentAccountId() async {
    Response response = await rsAccountsGetCurrentAccountIdWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCurrentAccountId') as ResGetCurrentAccountId;
    } else {
      return null;
    }
  }

  /// Get available PGP identities id list with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsGetPGPLoginsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/GetPGPLogins".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get available PGP identities id list
  ///
  /// 
  Future<ResGetPGPLogins> rsAccountsGetPGPLogins() async {
    Response response = await rsAccountsGetPGPLoginsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPGPLogins') as ResGetPGPLogins;
    } else {
      return null;
    }
  }

  /// Import full encrypted PGP identity from file with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsImportIdentityWithHttpInfo({ ReqImportIdentity reqImportIdentity }) async {
    Object postBody = reqImportIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/ImportIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Import full encrypted PGP identity from file
  ///
  ///ReqImportIdentity reqImportIdentity :
  ///     filePath: >              (string)path of certificate file  
  /// 
  Future<ResImportIdentity> rsAccountsImportIdentity({ ReqImportIdentity reqImportIdentity }) async {
    Response response = await rsAccountsImportIdentityWithHttpInfo( reqImportIdentity: reqImportIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResImportIdentity') as ResImportIdentity;
    } else {
      return null;
    }
  }

  /// Import full encrypted PGP identity from string with HTTP info returned
  ///
  /// 
  Future<Response> rsAccountsImportIdentityFromStringWithHttpInfo({ ReqImportIdentityFromString reqImportIdentityFromString }) async {
    Object postBody = reqImportIdentityFromString;

    // verify required params are set

    // create path and map variables
    String path = "/rsAccounts/importIdentityFromString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Import full encrypted PGP identity from string
  ///
  ///ReqImportIdentityFromString reqImportIdentityFromString :
  ///     data: >              (string)certificate string  
  /// 
  Future<ResImportIdentityFromString> rsAccountsImportIdentityFromString({ ReqImportIdentityFromString reqImportIdentityFromString }) async {
    Response response = await rsAccountsImportIdentityFromStringWithHttpInfo( reqImportIdentityFromString: reqImportIdentityFromString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResImportIdentityFromString') as ResImportIdentityFromString;
    } else {
      return null;
    }
  }

  /// Enable or disable IP filtering service with HTTP info returned
  ///
  /// 
  Future rsBanListEnableIPFilteringWithHttpInfo({ ReqEnableIPFiltering reqEnableIPFiltering }) async {
    Object postBody = reqEnableIPFiltering;

    // verify required params are set

    // create path and map variables
    String path = "/rsBanList/enableIPFiltering".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Enable or disable IP filtering service
  ///
  ///ReqEnableIPFiltering reqEnableIPFiltering :
  ///     enable: >              (boolean)pass true to enable, false to disable  
  /// 
  Future rsBanListEnableIPFiltering({ ReqEnableIPFiltering reqEnableIPFiltering }) async {
    Response response = await rsBanListEnableIPFilteringWithHttpInfo( reqEnableIPFiltering: reqEnableIPFiltering );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get ip filtering service status with HTTP info returned
  ///
  /// 
  Future<Response> rsBanListIpFilteringEnabledWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsBanList/ipFilteringEnabled".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get ip filtering service status
  ///
  /// 
  Future<ResIpFilteringEnabled> rsBanListIpFilteringEnabled() async {
    Response response = await rsBanListIpFilteringEnabledWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIpFilteringEnabled') as ResIpFilteringEnabled;
    } else {
      return null;
    }
  }

  /// Disable multicast listening with HTTP info returned
  ///
  /// 
  Future<Response> rsBroadcastDiscoveryDisableMulticastListeningWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsBroadcastDiscovery/disableMulticastListening".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Disable multicast listening
  ///
  /// 
  Future<ResDisableMulticastListening> rsBroadcastDiscoveryDisableMulticastListening() async {
    Response response = await rsBroadcastDiscoveryDisableMulticastListeningWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResDisableMulticastListening') as ResDisableMulticastListening;
    } else {
      return null;
    }
  }

  /// On platforms that need it enable low level multicast listening with HTTP info returned
  ///
  /// 
  Future<Response> rsBroadcastDiscoveryEnableMulticastListeningWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsBroadcastDiscovery/enableMulticastListening".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// On platforms that need it enable low level multicast listening
  ///
  /// 
  Future<ResEnableMulticastListening> rsBroadcastDiscoveryEnableMulticastListening() async {
    Response response = await rsBroadcastDiscoveryEnableMulticastListeningWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResEnableMulticastListening') as ResEnableMulticastListening;
    } else {
      return null;
    }
  }

  /// Get potential peers that have been discovered up until now with HTTP info returned
  ///
  /// 
  Future<Response> rsBroadcastDiscoveryGetDiscoveredPeersWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsBroadcastDiscovery/getDiscoveredPeers".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get potential peers that have been discovered up until now
  ///
  /// 
  Future<ResGetDiscoveredPeers> rsBroadcastDiscoveryGetDiscoveredPeers() async {
    Response response = await rsBroadcastDiscoveryGetDiscoveredPeersWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDiscoveredPeers') as ResGetDiscoveredPeers;
    } else {
      return null;
    }
  }

  /// Check if multicast listening is enabled with HTTP info returned
  ///
  /// 
  Future<Response> rsBroadcastDiscoveryIsMulticastListeningEnabledWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsBroadcastDiscovery/isMulticastListeningEnabled".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if multicast listening is enabled
  ///
  /// 
  Future<ResIsMulticastListeningEnabled> rsBroadcastDiscoveryIsMulticastListeningEnabled() async {
    Response response = await rsBroadcastDiscoveryIsMulticastListeningEnabledWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsMulticastListeningEnabled') as ResIsMulticastListeningEnabled;
    } else {
      return null;
    }
  }

  /// getAllBandwidthRates get the bandwidth rates for all peers with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetAllBandwidthRatesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/getAllBandwidthRates".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getAllBandwidthRates get the bandwidth rates for all peers
  ///
  /// 
  Future<ResGetAllBandwidthRates> rsConfigGetAllBandwidthRates() async {
    Response response = await rsConfigGetAllBandwidthRatesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetAllBandwidthRates') as ResGetAllBandwidthRates;
    } else {
      return null;
    }
  }

  /// getConfigNetStatus return the net status with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetConfigNetStatusWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/getConfigNetStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getConfigNetStatus return the net status
  ///
  /// 
  Future<ResGetConfigNetStatus> rsConfigGetConfigNetStatus() async {
    Response response = await rsConfigGetConfigNetStatusWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetConfigNetStatus') as ResGetConfigNetStatus;
    } else {
      return null;
    }
  }

  /// GetCurrentDataRates get current upload and download rates with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetCurrentDataRatesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/GetCurrentDataRates".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// GetCurrentDataRates get current upload and download rates
  ///
  /// 
  Future<ResGetCurrentDataRates> rsConfigGetCurrentDataRates() async {
    Response response = await rsConfigGetCurrentDataRatesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCurrentDataRates') as ResGetCurrentDataRates;
    } else {
      return null;
    }
  }

  /// GetMaxDataRates get maximum upload and download rates with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetMaxDataRatesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/GetMaxDataRates".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// GetMaxDataRates get maximum upload and download rates
  ///
  /// 
  Future<ResGetMaxDataRates> rsConfigGetMaxDataRates() async {
    Response response = await rsConfigGetMaxDataRatesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMaxDataRates') as ResGetMaxDataRates;
    } else {
      return null;
    }
  }

  /// getOperatingMode get current operating mode with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetOperatingModeWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/getOperatingMode".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getOperatingMode get current operating mode
  ///
  /// 
  Future<ResGetOperatingMode> rsConfigGetOperatingMode() async {
    Response response = await rsConfigGetOperatingModeWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOperatingMode') as ResGetOperatingMode;
    } else {
      return null;
    }
  }

  /// getTotalBandwidthRates returns the current bandwidths rates with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetTotalBandwidthRatesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/getTotalBandwidthRates".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getTotalBandwidthRates returns the current bandwidths rates
  ///
  /// 
  Future<ResGetTotalBandwidthRates> rsConfigGetTotalBandwidthRates() async {
    Response response = await rsConfigGetTotalBandwidthRatesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetTotalBandwidthRates') as ResGetTotalBandwidthRates;
    } else {
      return null;
    }
  }

  /// getTrafficInfo returns a list of all tracked traffic clues with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigGetTrafficInfoWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/getTrafficInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getTrafficInfo returns a list of all tracked traffic clues
  ///
  /// 
  Future<ResGetTrafficInfo> rsConfigGetTrafficInfo() async {
    Response response = await rsConfigGetTrafficInfoWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetTrafficInfo') as ResGetTrafficInfo;
    } else {
      return null;
    }
  }

  /// SetMaxDataRates set maximum upload and download rates with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigSetMaxDataRatesWithHttpInfo({ ReqSetMaxDataRates reqSetMaxDataRates }) async {
    Object postBody = reqSetMaxDataRates;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/SetMaxDataRates".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// SetMaxDataRates set maximum upload and download rates
  ///
  ///ReqSetMaxDataRates reqSetMaxDataRates :
  ///     downKb: >              (integer)download rate in kB          upKb: >              (integer)upload rate in kB  
  /// 
  Future<ResSetMaxDataRates> rsConfigSetMaxDataRates({ ReqSetMaxDataRates reqSetMaxDataRates }) async {
    Response response = await rsConfigSetMaxDataRatesWithHttpInfo( reqSetMaxDataRates: reqSetMaxDataRates );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetMaxDataRates') as ResSetMaxDataRates;
    } else {
      return null;
    }
  }

  /// setOperatingMode set the current oprating mode with HTTP info returned
  ///
  /// 
  Future<Response> rsConfigSetOperatingModeWithHttpInfo({ ReqSetOperatingMode reqSetOperatingMode }) async {
    Object postBody = reqSetOperatingMode;

    // verify required params are set

    // create path and map variables
    String path = "/rsConfig/setOperatingMode".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setOperatingMode set the current oprating mode
  ///
  ///ReqSetOperatingMode reqSetOperatingMode :
  ///     opMode: >              (integer)new opearting mode  
  /// 
  Future<ResSetOperatingMode> rsConfigSetOperatingMode({ ReqSetOperatingMode reqSetOperatingMode }) async {
    Response response = await rsConfigSetOperatingModeWithHttpInfo( reqSetOperatingMode: reqSetOperatingMode );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetOperatingMode') as ResSetOperatingMode;
    } else {
      return null;
    }
  }

  /// Check if core is fully ready, true only after with HTTP info returned
  ///
  /// 
  Future<Response> rsControlIsReadyWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsControl/isReady".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if core is fully ready, true only after
  ///
  /// 
  Future<ResIsReady> rsControlIsReady() async {
    Response response = await rsControlIsReadyWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsReady') as ResIsReady;
    } else {
      return null;
    }
  }

  /// Turn off RetroShare with HTTP info returned
  ///
  /// 
  Future rsControlRsGlobalShutDownWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsControl/rsGlobalShutDown".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Turn off RetroShare
  ///
  /// 
  Future rsControlRsGlobalShutDown() async {
    Response response = await rsControlRsGlobalShutDownWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Add shared directory with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesAddSharedDirectoryWithHttpInfo({ ReqAddSharedDirectory reqAddSharedDirectory }) async {
    Object postBody = reqAddSharedDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/addSharedDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add shared directory
  ///
  ///ReqAddSharedDirectory reqAddSharedDirectory :
  ///     dir: >              (SharedDirInfo)directory to share with sharing options  
  /// 
  Future<ResAddSharedDirectory> rsFilesAddSharedDirectory({ ReqAddSharedDirectory reqAddSharedDirectory }) async {
    Response response = await rsFilesAddSharedDirectoryWithHttpInfo( reqAddSharedDirectory: reqAddSharedDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAddSharedDirectory') as ResAddSharedDirectory;
    } else {
      return null;
    }
  }

  /// Check if we already have a file with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesAlreadyHaveFileWithHttpInfo({ ReqAlreadyHaveFile reqAlreadyHaveFile }) async {
    Object postBody = reqAlreadyHaveFile;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/alreadyHaveFile".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if we already have a file
  ///
  ///ReqAlreadyHaveFile reqAlreadyHaveFile :
  ///     hash: >              (RsFileHash)file identifier  
  /// 
  Future<ResAlreadyHaveFile> rsFilesAlreadyHaveFile({ ReqAlreadyHaveFile reqAlreadyHaveFile }) async {
    Response response = await rsFilesAlreadyHaveFileWithHttpInfo( reqAlreadyHaveFile: reqAlreadyHaveFile );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAlreadyHaveFile') as ResAlreadyHaveFile;
    } else {
      return null;
    }
  }

  /// Ban unwanted file from being, searched and forwarded by this node with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesBanFileWithHttpInfo({ ReqBanFile reqBanFile }) async {
    Object postBody = reqBanFile;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/banFile".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Ban unwanted file from being, searched and forwarded by this node
  ///
  ///ReqBanFile reqBanFile :
  ///     realFileHash: >              (RsFileHash)this is what will really enforce banning          filename: >              (string)expected name of the file, for the user to read          fileSize: >              (integer64)expected file size, for the user to read  
  /// 
  Future<ResBanFile> rsFilesBanFile({ ReqBanFile reqBanFile }) async {
    Response response = await rsFilesBanFileWithHttpInfo( reqBanFile: reqBanFile );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResBanFile') as ResBanFile;
    } else {
      return null;
    }
  }

  /// Get default chunk strategy with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesDefaultChunkStrategyWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/defaultChunkStrategy".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get default chunk strategy
  ///
  /// 
  Future<ResDefaultChunkStrategy> rsFilesDefaultChunkStrategy() async {
    Response response = await rsFilesDefaultChunkStrategyWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResDefaultChunkStrategy') as ResDefaultChunkStrategy;
    } else {
      return null;
    }
  }

  /// Export link to a collection of files with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesExportCollectionLinkWithHttpInfo({ ReqExportCollectionLink reqExportCollectionLink }) async {
    Object postBody = reqExportCollectionLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/exportCollectionLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Export link to a collection of files
  ///
  ///ReqExportCollectionLink reqExportCollectionLink :
  ///     handle: >              (integer64)directory RetroShare handle          fragSneak: >              (boolean)when true the file data is sneaked into fragment instead of FILES_URL_DATA_FIELD query field, this way if using an http[s] link to pass around a disguised file link a misconfigured host attempting to visit that link with a web browser will not send the file data to the server thus protecting at least some of the privacy of the user even in a misconfiguration scenario.          baseUrl: >              (string)URL into which to sneak in the RetroShare file link base64, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the collection data link will be outputted in plain base64 format.  
  /// 
  Future<ResExportCollectionLink> rsFilesExportCollectionLink({ ReqExportCollectionLink reqExportCollectionLink }) async {
    Response response = await rsFilesExportCollectionLinkWithHttpInfo( reqExportCollectionLink: reqExportCollectionLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportCollectionLink') as ResExportCollectionLink;
    } else {
      return null;
    }
  }

  /// Export link to a file with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesExportFileLinkWithHttpInfo({ ReqExportFileLink reqExportFileLink }) async {
    Object postBody = reqExportFileLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/exportFileLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Export link to a file
  ///
  ///ReqExportFileLink reqExportFileLink :
  ///     fileHash: >              (RsFileHash)hash of the file          fileSize: >              (integer64)size of the file          fileName: >              (string)name of the file          fragSneak: >              (boolean)None         baseUrl: >              (string)None 
  /// 
  Future<ResExportFileLink> rsFilesExportFileLink({ ReqExportFileLink reqExportFileLink }) async {
    Response response = await rsFilesExportFileLinkWithHttpInfo( reqExportFileLink: reqExportFileLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportFileLink') as ResExportFileLink;
    } else {
      return null;
    }
  }

  /// Add file to extra shared file list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesExtraFileHashWithHttpInfo({ ReqExtraFileHash reqExtraFileHash }) async {
    Object postBody = reqExtraFileHash;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/ExtraFileHash".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add file to extra shared file list
  ///
  ///ReqExtraFileHash reqExtraFileHash :
  ///     localpath: >              (string)path of the file          period: >              (rstime_t)how much time the file will be kept in extra list in seconds          flags: >              (TransferRequestFlags)sharing policy flags ex: RS_FILE_REQ_ANONYMOUS_ROUTING  
  /// 
  Future<ResExtraFileHash> rsFilesExtraFileHash({ ReqExtraFileHash reqExtraFileHash }) async {
    Response response = await rsFilesExtraFileHashWithHttpInfo( reqExtraFileHash: reqExtraFileHash );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExtraFileHash') as ResExtraFileHash;
    } else {
      return null;
    }
  }

  /// Remove file from extra fila shared list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesExtraFileRemoveWithHttpInfo({ ReqExtraFileRemove reqExtraFileRemove }) async {
    Object postBody = reqExtraFileRemove;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/ExtraFileRemove".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove file from extra fila shared list
  ///
  ///ReqExtraFileRemove reqExtraFileRemove :
  ///     hash: >              (RsFileHash)hash of the file to remove  
  /// 
  Future<ResExtraFileRemove> rsFilesExtraFileRemove({ ReqExtraFileRemove reqExtraFileRemove }) async {
    Response response = await rsFilesExtraFileRemoveWithHttpInfo( reqExtraFileRemove: reqExtraFileRemove );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExtraFileRemove') as ResExtraFileRemove;
    } else {
      return null;
    }
  }

  /// Get extra file information with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesExtraFileStatusWithHttpInfo({ ReqExtraFileStatus reqExtraFileStatus }) async {
    Object postBody = reqExtraFileStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/ExtraFileStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get extra file information
  ///
  ///ReqExtraFileStatus reqExtraFileStatus :
  ///     localpath: >              (string)path of the file  
  /// 
  Future<ResExtraFileStatus> rsFilesExtraFileStatus({ ReqExtraFileStatus reqExtraFileStatus }) async {
    Response response = await rsFilesExtraFileStatusWithHttpInfo( reqExtraFileStatus: reqExtraFileStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExtraFileStatus') as ResExtraFileStatus;
    } else {
      return null;
    }
  }

  /// Cancel file downloading with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileCancelWithHttpInfo({ ReqFileCancel reqFileCancel }) async {
    Object postBody = reqFileCancel;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileCancel".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Cancel file downloading
  ///
  ///ReqFileCancel reqFileCancel :
  ///     hash: >              (RsFileHash)None 
  /// 
  Future<ResFileCancel> rsFilesFileCancel({ ReqFileCancel reqFileCancel }) async {
    Response response = await rsFilesFileCancelWithHttpInfo( reqFileCancel: reqFileCancel );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileCancel') as ResFileCancel;
    } else {
      return null;
    }
  }

  /// Clear completed downloaded files list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileClearCompletedWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileClearCompleted".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Clear completed downloaded files list
  ///
  /// 
  Future<ResFileClearCompleted> rsFilesFileClearCompleted() async {
    Response response = await rsFilesFileClearCompletedWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileClearCompleted') as ResFileClearCompleted;
    } else {
      return null;
    }
  }

  /// Controls file transfer with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileControlWithHttpInfo({ ReqFileControl reqFileControl }) async {
    Object postBody = reqFileControl;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileControl".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Controls file transfer
  ///
  ///ReqFileControl reqFileControl :
  ///     hash: >              (RsFileHash)file identifier          flags: >              (integer)action to perform. Pict into { RS_FILE_CTRL_PAUSE, RS_FILE_CTRL_START, RS_FILE_CTRL_FORCE_CHECK } }  
  /// 
  Future<ResFileControl> rsFilesFileControl({ ReqFileControl reqFileControl }) async {
    Response response = await rsFilesFileControlWithHttpInfo( reqFileControl: reqFileControl );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileControl') as ResFileControl;
    } else {
      return null;
    }
  }

  /// Get file details with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileDetailsWithHttpInfo({ ReqFileDetails reqFileDetails }) async {
    Object postBody = reqFileDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get file details
  ///
  ///ReqFileDetails reqFileDetails :
  ///     hash: >              (RsFileHash)file identifier          hintflags: >              (FileSearchFlags)filtering hint (RS_FILE_HINTS_EXTRA|...|RS_FILE_HINTS_LOCAL)  
  /// 
  Future<ResFileDetails> rsFilesFileDetails({ ReqFileDetails reqFileDetails }) async {
    Response response = await rsFilesFileDetailsWithHttpInfo( reqFileDetails: reqFileDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileDetails') as ResFileDetails;
    } else {
      return null;
    }
  }

  /// Get chunk details about the downloaded file with given hash. with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileDownloadChunksDetailsWithHttpInfo({ ReqFileDownloadChunksDetails reqFileDownloadChunksDetails }) async {
    Object postBody = reqFileDownloadChunksDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileDownloadChunksDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get chunk details about the downloaded file with given hash.
  ///
  ///ReqFileDownloadChunksDetails reqFileDownloadChunksDetails :
  ///     hash: >              (RsFileHash)file identifier  
  /// 
  Future<ResFileDownloadChunksDetails> rsFilesFileDownloadChunksDetails({ ReqFileDownloadChunksDetails reqFileDownloadChunksDetails }) async {
    Response response = await rsFilesFileDownloadChunksDetailsWithHttpInfo( reqFileDownloadChunksDetails: reqFileDownloadChunksDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileDownloadChunksDetails') as ResFileDownloadChunksDetails;
    } else {
      return null;
    }
  }

  /// Get incoming files list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileDownloadsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileDownloads".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get incoming files list
  ///
  /// 
  Future<ResFileDownloads> rsFilesFileDownloads() async {
    Response response = await rsFilesFileDownloadsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileDownloads') as ResFileDownloads;
    } else {
      return null;
    }
  }

  /// Initiate downloading of a file with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileRequestWithHttpInfo({ ReqFileRequest reqFileRequest }) async {
    Object postBody = reqFileRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Initiate downloading of a file
  ///
  ///ReqFileRequest reqFileRequest :
  ///     fileName: >              (string)file name          hash: >              (RsFileHash)file hash          size: >              (integer64)file size          destPath: >              (string)optional specify the destination directory          flags: >              (TransferRequestFlags)you usually want RS_FILE_REQ_ANONYMOUS_ROUTING          srcIds: >              (list<RsPeerId>)eventually specify known sources  
  /// 
  Future<ResFileRequest> rsFilesFileRequest({ ReqFileRequest reqFileRequest }) async {
    Response response = await rsFilesFileRequestWithHttpInfo( reqFileRequest: reqFileRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileRequest') as ResFileRequest;
    } else {
      return null;
    }
  }

  /// Get details about the upload with given hash with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileUploadChunksDetailsWithHttpInfo({ ReqFileUploadChunksDetails reqFileUploadChunksDetails }) async {
    Object postBody = reqFileUploadChunksDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileUploadChunksDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get details about the upload with given hash
  ///
  ///ReqFileUploadChunksDetails reqFileUploadChunksDetails :
  ///     hash: >              (RsFileHash)file identifier          peerId: >              (RsPeerId)peer identifier  
  /// 
  Future<ResFileUploadChunksDetails> rsFilesFileUploadChunksDetails({ ReqFileUploadChunksDetails reqFileUploadChunksDetails }) async {
    Response response = await rsFilesFileUploadChunksDetailsWithHttpInfo( reqFileUploadChunksDetails: reqFileUploadChunksDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileUploadChunksDetails') as ResFileUploadChunksDetails;
    } else {
      return null;
    }
  }

  /// Get outgoing files list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFileUploadsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/FileUploads".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get outgoing files list
  ///
  /// 
  Future<ResFileUploads> rsFilesFileUploads() async {
    Response response = await rsFilesFileUploadsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFileUploads') as ResFileUploads;
    } else {
      return null;
    }
  }

  /// Force shared directories check. with HTTP info returned
  ///
  /// 
  Future rsFilesForceDirectoryCheckWithHttpInfo({ ReqForceDirectoryCheck reqForceDirectoryCheck }) async {
    Object postBody = reqForceDirectoryCheck;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/ForceDirectoryCheck".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Force shared directories check.
  ///
  ///ReqForceDirectoryCheck reqForceDirectoryCheck :
  ///     add_safe_delay: >              (boolean)Schedule the check 20 seconds from now, to ensure to capture files written just now.   
  /// 
  Future rsFilesForceDirectoryCheck({ ReqForceDirectoryCheck reqForceDirectoryCheck }) async {
    Response response = await rsFilesForceDirectoryCheckWithHttpInfo( reqForceDirectoryCheck: reqForceDirectoryCheck );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get free disk space limit with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesFreeDiskSpaceLimitWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/freeDiskSpaceLimit".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get free disk space limit
  ///
  /// 
  Future<ResFreeDiskSpaceLimit> rsFilesFreeDiskSpaceLimit() async {
    Response response = await rsFilesFreeDiskSpaceLimitWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResFreeDiskSpaceLimit') as ResFreeDiskSpaceLimit;
    } else {
      return null;
    }
  }

  /// Get default complete downloads directory with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesGetDownloadDirectoryWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/getDownloadDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get default complete downloads directory
  ///
  /// 
  Future<ResGetDownloadDirectory> rsFilesGetDownloadDirectory() async {
    Response response = await rsFilesGetDownloadDirectoryWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDownloadDirectory') as ResGetDownloadDirectory;
    } else {
      return null;
    }
  }

  /// Provides file data for the GUI, media streaming or API clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets. with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesGetFileDataWithHttpInfo({ ReqGetFileData reqGetFileData }) async {
    Object postBody = reqGetFileData;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/getFileData".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Provides file data for the GUI, media streaming or API clients. It may return unverified chunks. This allows streaming without having to wait for hashes or completion of the file. This function returns an unspecified amount of bytes. Either as much data as available or a sensible maximum. Expect a block size of around 1MiB. To get more data, call this function repeatedly with different offsets.
  ///
  ///ReqGetFileData reqGetFileData :
  ///     hash: >              (RsFileHash)hash of the file. The file has to be available on this node or it has to be in downloading state.          offset: >              (integer64)where the desired block starts          requested_size: >              (integer)size of pre-allocated data. Will be updated by the function.  
  /// 
  Future<ResGetFileData> rsFilesGetFileData({ ReqGetFileData reqGetFileData }) async {
    Response response = await rsFilesGetFileDataWithHttpInfo( reqGetFileData: reqGetFileData );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetFileData') as ResGetFileData;
    } else {
      return null;
    }
  }

  /// Get partial downloads directory with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesGetPartialsDirectoryWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/getPartialsDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get partial downloads directory
  ///
  /// 
  Future<ResGetPartialsDirectory> rsFilesGetPartialsDirectory() async {
    Response response = await rsFilesGetPartialsDirectoryWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPartialsDirectory') as ResGetPartialsDirectory;
    } else {
      return null;
    }
  }

  /// Get list of banned files with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesGetPrimaryBannedFilesListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/getPrimaryBannedFilesList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get list of banned files
  ///
  /// 
  Future<ResGetPrimaryBannedFilesList> rsFilesGetPrimaryBannedFilesList() async {
    Response response = await rsFilesGetPrimaryBannedFilesListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPrimaryBannedFilesList') as ResGetPrimaryBannedFilesList;
    } else {
      return null;
    }
  }

  /// Get list of current shared directories with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesGetSharedDirectoriesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/getSharedDirectories".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get list of current shared directories
  ///
  /// 
  Future<ResGetSharedDirectories> rsFilesGetSharedDirectories() async {
    Response response = await rsFilesGetSharedDirectoriesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetSharedDirectories') as ResGetSharedDirectories;
    } else {
      return null;
    }
  }

  /// Check if a file is on banned list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesIsHashBannedWithHttpInfo({ ReqIsHashBanned reqIsHashBanned }) async {
    Object postBody = reqIsHashBanned;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/isHashBanned".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if a file is on banned list
  ///
  ///ReqIsHashBanned reqIsHashBanned :
  ///     hash: >              (RsFileHash)hash of the file  
  /// 
  Future<ResIsHashBanned> rsFilesIsHashBanned({ ReqIsHashBanned reqIsHashBanned }) async {
    Response response = await rsFilesIsHashBannedWithHttpInfo( reqIsHashBanned: reqIsHashBanned );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsHashBanned') as ResIsHashBanned;
    } else {
      return null;
    }
  }

  /// Parse RetroShare files link with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesParseFilesLinkWithHttpInfo({ ReqParseFilesLink reqParseFilesLink }) async {
    Object postBody = reqParseFilesLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/parseFilesLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Parse RetroShare files link
  ///
  ///ReqParseFilesLink reqParseFilesLink :
  ///     link: >              (string)files link either in base64 or URL format  
  /// 
  Future<ResParseFilesLink> rsFilesParseFilesLink({ ReqParseFilesLink reqParseFilesLink }) async {
    Response response = await rsFilesParseFilesLinkWithHttpInfo( reqParseFilesLink: reqParseFilesLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResParseFilesLink') as ResParseFilesLink;
    } else {
      return null;
    }
  }

  /// Remove directory from shared list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesRemoveSharedDirectoryWithHttpInfo({ ReqRemoveSharedDirectory reqRemoveSharedDirectory }) async {
    Object postBody = reqRemoveSharedDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/removeSharedDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove directory from shared list
  ///
  ///ReqRemoveSharedDirectory reqRemoveSharedDirectory :
  ///     dir: >              (string)Path of the directory to remove from shared list  
  /// 
  Future<ResRemoveSharedDirectory> rsFilesRemoveSharedDirectory({ ReqRemoveSharedDirectory reqRemoveSharedDirectory }) async {
    Response response = await rsFilesRemoveSharedDirectoryWithHttpInfo( reqRemoveSharedDirectory: reqRemoveSharedDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRemoveSharedDirectory') as ResRemoveSharedDirectory;
    } else {
      return null;
    }
  }

  /// Request directory details, subsequent multiple call may be used to explore a whole directory tree. with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesRequestDirDetailsWithHttpInfo({ ReqRequestDirDetails reqRequestDirDetails }) async {
    Object postBody = reqRequestDirDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/requestDirDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Request directory details, subsequent multiple call may be used to explore a whole directory tree.
  ///
  ///ReqRequestDirDetails reqRequestDirDetails :
  ///     handle: >              (integer64)element handle 0 for root, pass the content of DirDetails::child[x].ref after first call to explore deeper, be aware that is not a real pointer but an index used internally by RetroShare.          flags: >              (FileSearchFlags)file search flags RS_FILE_HINTS_*  
  /// 
  Future<ResRequestDirDetails> rsFilesRequestDirDetails({ ReqRequestDirDetails reqRequestDirDetails }) async {
    Response response = await rsFilesRequestDirDetailsWithHttpInfo( reqRequestDirDetails: reqRequestDirDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestDirDetails') as ResRequestDirDetails;
    } else {
      return null;
    }
  }

  /// Initiate download of a files collection with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesRequestFilesWithHttpInfo({ ReqRequestFiles reqRequestFiles }) async {
    Object postBody = reqRequestFiles;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/requestFiles".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Initiate download of a files collection
  ///
  ///ReqRequestFiles reqRequestFiles :
  ///     collection: >              (RsFileTree)collection of files to download          destPath: >              (string)optional base path on which to download the collection, if left empty the default download directory will be used          srcIds: >              (vector<RsPeerId>)optional peers id known as direct source of the collection          flags: >              (FileRequestFlags)optional flags to fine tune search and download algorithm  
  /// 
  Future<ResRequestFiles> rsFilesRequestFiles({ ReqRequestFiles reqRequestFiles }) async {
    Response response = await rsFilesRequestFilesWithHttpInfo( reqRequestFiles: reqRequestFiles );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestFiles') as ResRequestFiles;
    } else {
      return null;
    }
  }

  /// Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetChunkStrategyWithHttpInfo({ ReqSetChunkStrategy reqSetChunkStrategy }) async {
    Object postBody = reqSetChunkStrategy;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setChunkStrategy".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set chunk strategy for file, useful to set streaming mode to be able of see video or other media preview while it is still downloading
  ///
  ///ReqSetChunkStrategy reqSetChunkStrategy :
  ///     hash: >              (RsFileHash)file identifier          newStrategy: >              (FileChunksInfo_ChunkStrategy)None 
  /// 
  Future<ResSetChunkStrategy> rsFilesSetChunkStrategy({ ReqSetChunkStrategy reqSetChunkStrategy }) async {
    Response response = await rsFilesSetChunkStrategyWithHttpInfo( reqSetChunkStrategy: reqSetChunkStrategy );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetChunkStrategy') as ResSetChunkStrategy;
    } else {
      return null;
    }
  }

  /// Set default chunk strategy with HTTP info returned
  ///
  /// 
  Future rsFilesSetDefaultChunkStrategyWithHttpInfo({ ReqSetDefaultChunkStrategy reqSetDefaultChunkStrategy }) async {
    Object postBody = reqSetDefaultChunkStrategy;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setDefaultChunkStrategy".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set default chunk strategy
  ///
  ///ReqSetDefaultChunkStrategy reqSetDefaultChunkStrategy :
  ///     strategy: >              (FileChunksInfo_ChunkStrategy)None 
  /// 
  Future rsFilesSetDefaultChunkStrategy({ ReqSetDefaultChunkStrategy reqSetDefaultChunkStrategy }) async {
    Response response = await rsFilesSetDefaultChunkStrategyWithHttpInfo( reqSetDefaultChunkStrategy: reqSetDefaultChunkStrategy );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set destination directory for given file with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetDestinationDirectoryWithHttpInfo({ ReqSetDestinationDirectory reqSetDestinationDirectory }) async {
    Object postBody = reqSetDestinationDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setDestinationDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set destination directory for given file
  ///
  ///ReqSetDestinationDirectory reqSetDestinationDirectory :
  ///     hash: >              (RsFileHash)file identifier          newPath: >              (string)None 
  /// 
  Future<ResSetDestinationDirectory> rsFilesSetDestinationDirectory({ ReqSetDestinationDirectory reqSetDestinationDirectory }) async {
    Response response = await rsFilesSetDestinationDirectoryWithHttpInfo( reqSetDestinationDirectory: reqSetDestinationDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetDestinationDirectory') as ResSetDestinationDirectory;
    } else {
      return null;
    }
  }

  /// Set name for dowloaded file with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetDestinationNameWithHttpInfo({ ReqSetDestinationName reqSetDestinationName }) async {
    Object postBody = reqSetDestinationName;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setDestinationName".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set name for dowloaded file
  ///
  ///ReqSetDestinationName reqSetDestinationName :
  ///     hash: >              (RsFileHash)file identifier          newName: >              (string)None 
  /// 
  Future<ResSetDestinationName> rsFilesSetDestinationName({ ReqSetDestinationName reqSetDestinationName }) async {
    Response response = await rsFilesSetDestinationNameWithHttpInfo( reqSetDestinationName: reqSetDestinationName );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetDestinationName') as ResSetDestinationName;
    } else {
      return null;
    }
  }

  /// Set default complete downloads directory with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetDownloadDirectoryWithHttpInfo({ ReqSetDownloadDirectory reqSetDownloadDirectory }) async {
    Object postBody = reqSetDownloadDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setDownloadDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set default complete downloads directory
  ///
  ///ReqSetDownloadDirectory reqSetDownloadDirectory :
  ///     path: >              (string)directory path  
  /// 
  Future<ResSetDownloadDirectory> rsFilesSetDownloadDirectory({ ReqSetDownloadDirectory reqSetDownloadDirectory }) async {
    Response response = await rsFilesSetDownloadDirectoryWithHttpInfo( reqSetDownloadDirectory: reqSetDownloadDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetDownloadDirectory') as ResSetDownloadDirectory;
    } else {
      return null;
    }
  }

  /// Set minimum free disk space limit with HTTP info returned
  ///
  /// 
  Future rsFilesSetFreeDiskSpaceLimitWithHttpInfo({ ReqSetFreeDiskSpaceLimit reqSetFreeDiskSpaceLimit }) async {
    Object postBody = reqSetFreeDiskSpaceLimit;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setFreeDiskSpaceLimit".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set minimum free disk space limit
  ///
  ///ReqSetFreeDiskSpaceLimit reqSetFreeDiskSpaceLimit :
  ///     minimumFreeMB: >              (integer)minimum free space in MB  
  /// 
  Future rsFilesSetFreeDiskSpaceLimit({ ReqSetFreeDiskSpaceLimit reqSetFreeDiskSpaceLimit }) async {
    Response response = await rsFilesSetFreeDiskSpaceLimitWithHttpInfo( reqSetFreeDiskSpaceLimit: reqSetFreeDiskSpaceLimit );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set partial downloads directory with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetPartialsDirectoryWithHttpInfo({ ReqSetPartialsDirectory reqSetPartialsDirectory }) async {
    Object postBody = reqSetPartialsDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setPartialsDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set partial downloads directory
  ///
  ///ReqSetPartialsDirectory reqSetPartialsDirectory :
  ///     path: >              (string)directory path  
  /// 
  Future<ResSetPartialsDirectory> rsFilesSetPartialsDirectory({ ReqSetPartialsDirectory reqSetPartialsDirectory }) async {
    Response response = await rsFilesSetPartialsDirectoryWithHttpInfo( reqSetPartialsDirectory: reqSetPartialsDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetPartialsDirectory') as ResSetPartialsDirectory;
    } else {
      return null;
    }
  }

  /// Set shared directories with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesSetSharedDirectoriesWithHttpInfo({ ReqSetSharedDirectories reqSetSharedDirectories }) async {
    Object postBody = reqSetSharedDirectories;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/setSharedDirectories".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set shared directories
  ///
  ///ReqSetSharedDirectories reqSetSharedDirectories :
  ///     dirs: >              (list<SharedDirInfo>)list of shared directories with share options  
  /// 
  Future<ResSetSharedDirectories> rsFilesSetSharedDirectories({ ReqSetSharedDirectories reqSetSharedDirectories }) async {
    Response response = await rsFilesSetSharedDirectoriesWithHttpInfo( reqSetSharedDirectories: reqSetSharedDirectories );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetSharedDirectories') as ResSetSharedDirectories;
    } else {
      return null;
    }
  }

  /// This method is asynchronous. Request remote files search with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesTurtleSearchRequestWithHttpInfo({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async {
    Object postBody = reqTurtleSearchRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/turtleSearchRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This method is asynchronous. Request remote files search
  ///
  ///ReqTurtleSearchRequest reqTurtleSearchRequest :
  ///     matchString: >              (string)string to look for in the search. If files deep indexing is enabled at compile time support advanced features described at          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
  /// 
  Future<ResTurtleSearchRequest> rsFilesTurtleSearchRequest({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async {
    Response response = await rsFilesTurtleSearchRequestWithHttpInfo( reqTurtleSearchRequest: reqTurtleSearchRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResTurtleSearchRequest') as ResTurtleSearchRequest;
    } else {
      return null;
    }
  }

  /// Remove file from unwanted list with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesUnbanFileWithHttpInfo({ ReqUnbanFile reqUnbanFile }) async {
    Object postBody = reqUnbanFile;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/unbanFile".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove file from unwanted list
  ///
  ///ReqUnbanFile reqUnbanFile :
  ///     realFileHash: >              (RsFileHash)hash of the file  
  /// 
  Future<ResUnbanFile> rsFilesUnbanFile({ ReqUnbanFile reqUnbanFile }) async {
    Response response = await rsFilesUnbanFileWithHttpInfo( reqUnbanFile: reqUnbanFile );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResUnbanFile') as ResUnbanFile;
    } else {
      return null;
    }
  }

  /// Updates shared directory sharing flags. The directory should be already shared! with HTTP info returned
  ///
  /// 
  Future<Response> rsFilesUpdateShareFlagsWithHttpInfo({ ReqUpdateShareFlags reqUpdateShareFlags }) async {
    Object postBody = reqUpdateShareFlags;

    // verify required params are set

    // create path and map variables
    String path = "/rsFiles/updateShareFlags".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Updates shared directory sharing flags. The directory should be already shared!
  ///
  ///ReqUpdateShareFlags reqUpdateShareFlags :
  ///     dir: >              (SharedDirInfo)Shared directory with updated sharing options  
  /// 
  Future<ResUpdateShareFlags> rsFilesUpdateShareFlags({ ReqUpdateShareFlags reqUpdateShareFlags }) async {
    Response response = await rsFilesUpdateShareFlagsWithHttpInfo( reqUpdateShareFlags: reqUpdateShareFlags );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResUpdateShareFlags') as ResUpdateShareFlags;
    } else {
      return null;
    }
  }

  /// getDiscFriends get a list of all friends of a given friend with HTTP info returned
  ///
  /// 
  Future<Response> rsGossipDiscoveryGetDiscFriendsWithHttpInfo({ ReqGetDiscFriends reqGetDiscFriends }) async {
    Object postBody = reqGetDiscFriends;

    // verify required params are set

    // create path and map variables
    String path = "/rsGossipDiscovery/getDiscFriends".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getDiscFriends get a list of all friends of a given friend
  ///
  ///ReqGetDiscFriends reqGetDiscFriends :
  ///     id: >              (RsPeerId)peer to get the friends of  
  /// 
  Future<ResGetDiscFriends> rsGossipDiscoveryGetDiscFriends({ ReqGetDiscFriends reqGetDiscFriends }) async {
    Response response = await rsGossipDiscoveryGetDiscFriendsWithHttpInfo( reqGetDiscFriends: reqGetDiscFriends );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDiscFriends') as ResGetDiscFriends;
    } else {
      return null;
    }
  }

  /// getDiscPgpFriends get a list of all friends of a given friend with HTTP info returned
  ///
  /// 
  Future<Response> rsGossipDiscoveryGetDiscPgpFriendsWithHttpInfo({ ReqGetDiscPgpFriends reqGetDiscPgpFriends }) async {
    Object postBody = reqGetDiscPgpFriends;

    // verify required params are set

    // create path and map variables
    String path = "/rsGossipDiscovery/getDiscPgpFriends".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getDiscPgpFriends get a list of all friends of a given friend
  ///
  ///ReqGetDiscPgpFriends reqGetDiscPgpFriends :
  ///     pgpid: >              (RsPgpId)peer to get the friends of  
  /// 
  Future<ResGetDiscPgpFriends> rsGossipDiscoveryGetDiscPgpFriends({ ReqGetDiscPgpFriends reqGetDiscPgpFriends }) async {
    Response response = await rsGossipDiscoveryGetDiscPgpFriendsWithHttpInfo( reqGetDiscPgpFriends: reqGetDiscPgpFriends );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDiscPgpFriends') as ResGetDiscPgpFriends;
    } else {
      return null;
    }
  }

  /// getPeerVersion get the version string of a peer. with HTTP info returned
  ///
  /// 
  Future<Response> rsGossipDiscoveryGetPeerVersionWithHttpInfo({ ReqGetPeerVersion reqGetPeerVersion }) async {
    Object postBody = reqGetPeerVersion;

    // verify required params are set

    // create path and map variables
    String path = "/rsGossipDiscovery/getPeerVersion".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getPeerVersion get the version string of a peer.
  ///
  ///ReqGetPeerVersion reqGetPeerVersion :
  ///     id: >              (RsPeerId)peer to get the version string of  
  /// 
  Future<ResGetPeerVersion> rsGossipDiscoveryGetPeerVersion({ ReqGetPeerVersion reqGetPeerVersion }) async {
    Response response = await rsGossipDiscoveryGetPeerVersionWithHttpInfo( reqGetPeerVersion: reqGetPeerVersion );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPeerVersion') as ResGetPeerVersion;
    } else {
      return null;
    }
  }

  /// getWaitingDiscCount get the number of queued discovery packets. with HTTP info returned
  ///
  /// 
  Future<Response> rsGossipDiscoveryGetWaitingDiscCountWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGossipDiscovery/getWaitingDiscCount".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getWaitingDiscCount get the number of queued discovery packets.
  ///
  /// 
  Future<ResGetWaitingDiscCount> rsGossipDiscoveryGetWaitingDiscCount() async {
    Response response = await rsGossipDiscoveryGetWaitingDiscCountWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetWaitingDiscCount') as ResGetWaitingDiscCount;
    } else {
      return null;
    }
  }

  /// Deprecated{ substituted by createChannelV2 } with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateChannelWithHttpInfo({ ReqCreateChannel reqCreateChannel }) async {
    Object postBody = reqCreateChannel;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createChannel".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated{ substituted by createChannelV2 }
  ///
  ///ReqCreateChannel reqCreateChannel :
  ///     channel: >              (RsGxsChannelGroup)Channel data (name, description...)  
  /// 
  Future<ResCreateChannel> rsGxsChannelsCreateChannel({ ReqCreateChannel reqCreateChannel }) async {
    Response response = await rsGxsChannelsCreateChannelWithHttpInfo( reqCreateChannel: reqCreateChannel );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateChannel') as ResCreateChannel;
    } else {
      return null;
    }
  }

  /// Create channel. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateChannelV2WithHttpInfo({ ReqCreateChannelV2 reqCreateChannelV2 }) async {
    Object postBody = reqCreateChannelV2;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createChannelV2".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create channel. Blocking API.
  ///
  ///ReqCreateChannelV2 reqCreateChannelV2 :
  ///     name: >              (string)Name of the channel          description: >              (string)Description of the channel          thumbnail: >              (RsGxsImage)Optional image to show as channel thumbnail.          authorId: >              (RsGxsId)Optional id of the author. Leave empty for an anonymous channel.          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the channel is not public specify the id of the circle who can see the channel. Depending on the value you pass for circleType this should be be an external circle if EXTERNAL is passed, a local friend group id if NODES_GROUP is passed, empty otherwise.  
  /// 
  Future<ResCreateChannelV2> rsGxsChannelsCreateChannelV2({ ReqCreateChannelV2 reqCreateChannelV2 }) async {
    Response response = await rsGxsChannelsCreateChannelV2WithHttpInfo( reqCreateChannelV2: reqCreateChannelV2 );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateChannelV2') as ResCreateChannelV2;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateCommentWithHttpInfo({ ReqCreateComment reqCreateComment }) async {
    Object postBody = reqCreateComment;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createComment".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqCreateComment reqCreateComment :
  ///     comment: >              (RsGxsComment)None 
  /// 
  Future<ResCreateComment> rsGxsChannelsCreateComment({ ReqCreateComment reqCreateComment }) async {
    Response response = await rsGxsChannelsCreateCommentWithHttpInfo( reqCreateComment: reqCreateComment );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateComment') as ResCreateComment;
    } else {
      return null;
    }
  }

  /// Add a comment on a post or on another comment. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateCommentV2WithHttpInfo({ ReqCreateCommentV2 reqCreateCommentV2 }) async {
    Object postBody = reqCreateCommentV2;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createCommentV2".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add a comment on a post or on another comment. Blocking API.
  ///
  ///ReqCreateCommentV2 reqCreateCommentV2 :
  ///     channelId: >              (RsGxsGroupId)Id of the channel in which the comment is to be posted          threadId: >              (RsGxsMessageId)Id of the post (that is a thread) in the channel where the comment is placed          comment: >              (string)UTF-8 string containing the comment itself          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Id of the parent of the comment that is either a channel post Id or the Id of another comment.          origCommentId: >              (RsGxsMessageId)If this is supposed to replace an already existent comment, the id of the old post. If left blank a new post will be created.  
  /// 
  Future<ResCreateCommentV2> rsGxsChannelsCreateCommentV2({ ReqCreateCommentV2 reqCreateCommentV2 }) async {
    Response response = await rsGxsChannelsCreateCommentV2WithHttpInfo( reqCreateCommentV2: reqCreateCommentV2 );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateCommentV2') as ResCreateCommentV2;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreatePostWithHttpInfo({ ReqCreatePost reqCreatePost }) async {
    Object postBody = reqCreatePost;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createPost".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqCreatePost reqCreatePost :
  ///     post: >              (RsGxsChannelPost)None 
  /// 
  Future<ResCreatePost> rsGxsChannelsCreatePost({ ReqCreatePost reqCreatePost }) async {
    Response response = await rsGxsChannelsCreatePostWithHttpInfo( reqCreatePost: reqCreatePost );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreatePost') as ResCreatePost;
    } else {
      return null;
    }
  }

  /// Create channel post. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreatePostV2WithHttpInfo({ ReqCreatePostV2 reqCreatePostV2 }) async {
    Object postBody = reqCreatePostV2;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createPostV2".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create channel post. Blocking API.
  ///
  ///ReqCreatePostV2 reqCreatePostV2 :
  ///     channelId: >              (RsGxsGroupId)Id of the channel where to put the post. Beware you need publish rights on that channel to post.          title: >              (string)Title of the post          mBody: >              (string)Text content of the post          files: >              (list<RsGxsFile>)Optional list of attached files. These are supposed to be already shared,          thumbnail: >              (RsGxsImage)Optional thumbnail image for the post.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  
  /// 
  Future<ResCreatePostV2> rsGxsChannelsCreatePostV2({ ReqCreatePostV2 reqCreatePostV2 }) async {
    Response response = await rsGxsChannelsCreatePostV2WithHttpInfo( reqCreatePostV2: reqCreatePostV2 );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreatePostV2') as ResCreatePostV2;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateVoteWithHttpInfo({ ReqCreateVote reqCreateVote }) async {
    Object postBody = reqCreateVote;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createVote".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqCreateVote reqCreateVote :
  ///     vote: >              (RsGxsVote)None 
  /// 
  Future<ResCreateVote> rsGxsChannelsCreateVote({ ReqCreateVote reqCreateVote }) async {
    Response response = await rsGxsChannelsCreateVoteWithHttpInfo( reqCreateVote: reqCreateVote );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateVote') as ResCreateVote;
    } else {
      return null;
    }
  }

  /// Create a vote with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsCreateVoteV2WithHttpInfo({ ReqCreateVoteV2 reqCreateVoteV2 }) async {
    Object postBody = reqCreateVoteV2;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/createVoteV2".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create a vote
  ///
  ///ReqCreateVoteV2 reqCreateVoteV2 :
  ///     channelId: >              (RsGxsGroupId)Id of the channel where to vote          postId: >              (RsGxsMessageId)Id of the channel post of which a comment is voted.          commentId: >              (RsGxsMessageId)Id of the comment that is voted          authorId: >              (RsGxsId)Id of the author. Needs to be of an owned identity.          vote: >              (RsGxsVoteType)Vote value, either  
  /// 
  Future<ResCreateVoteV2> rsGxsChannelsCreateVoteV2({ ReqCreateVoteV2 reqCreateVoteV2 }) async {
    Response response = await rsGxsChannelsCreateVoteV2WithHttpInfo( reqCreateVoteV2: reqCreateVoteV2 );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateVoteV2') as ResCreateVoteV2;
    } else {
      return null;
    }
  }

  /// Edit channel details. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsEditChannelWithHttpInfo({ ReqEditChannel reqEditChannel }) async {
    Object postBody = reqEditChannel;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/editChannel".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Edit channel details.
  ///
  ///ReqEditChannel reqEditChannel :
  ///     channel: >              (RsGxsChannelGroup)Channel data (name, description...) with modifications  
  /// 
  Future<ResEditChannel> rsGxsChannelsEditChannel({ ReqEditChannel reqEditChannel }) async {
    Response response = await rsGxsChannelsEditChannelWithHttpInfo( reqEditChannel: reqEditChannel );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResEditChannel') as ResEditChannel;
    } else {
      return null;
    }
  }

  /// Get link to a channel with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsExportChannelLinkWithHttpInfo({ ReqExportChannelLink reqExportChannelLink }) async {
    Object postBody = reqExportChannelLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/exportChannelLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get link to a channel
  ///
  ///ReqExportChannelLink reqExportChannelLink :
  ///     chanId: >              (RsGxsGroupId)Id of the channel of which we want to generate a link          includeGxsData: >              (boolean)if true include the channel GXS group data so the receiver can subscribe to the channel even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  
  /// 
  Future<ResExportChannelLink> rsGxsChannelsExportChannelLink({ ReqExportChannelLink reqExportChannelLink }) async {
    Response response = await rsGxsChannelsExportChannelLinkWithHttpInfo( reqExportChannelLink: reqExportChannelLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportChannelLink') as ResExportChannelLink;
    } else {
      return null;
    }
  }

  /// Share extra file Can be used to share extra file attached to a channel post with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsExtraFileHashWithHttpInfo({ ReqExtraFileHash reqExtraFileHash }) async {
    Object postBody = reqExtraFileHash;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/ExtraFileHash".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Share extra file Can be used to share extra file attached to a channel post
  ///
  ///ReqExtraFileHash reqExtraFileHash :
  ///     path: >              (string)file path  
  /// 
  Future<ResExtraFileHash> rsGxsChannelsExtraFileHash({ ReqExtraFileHash reqExtraFileHash }) async {
    Response response = await rsGxsChannelsExtraFileHashWithHttpInfo( reqExtraFileHash: reqExtraFileHash );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExtraFileHash') as ResExtraFileHash;
    } else {
      return null;
    }
  }

  /// Remove extra file from shared files with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsExtraFileRemoveWithHttpInfo({ ReqExtraFileRemove reqExtraFileRemove }) async {
    Object postBody = reqExtraFileRemove;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/ExtraFileRemove".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove extra file from shared files
  ///
  ///ReqExtraFileRemove reqExtraFileRemove :
  ///     hash: >              (RsFileHash)hash of the file to remove  
  /// 
  Future<ResExtraFileRemove> rsGxsChannelsExtraFileRemove({ ReqExtraFileRemove reqExtraFileRemove }) async {
    Response response = await rsGxsChannelsExtraFileRemoveWithHttpInfo( reqExtraFileRemove: reqExtraFileRemove );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExtraFileRemove') as ResExtraFileRemove;
    } else {
      return null;
    }
  }

  /// Get all channel messages and comments in a given channel with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelAllContentWithHttpInfo({ ReqGetChannelAllContent reqGetChannelAllContent }) async {
    Object postBody = reqGetChannelAllContent;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelAllContent".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get all channel messages and comments in a given channel
  ///
  ///ReqGetChannelAllContent reqGetChannelAllContent :
  ///     channelId: >              (RsGxsGroupId)id of the channel of which the content is requested  
  /// 
  Future<ResGetChannelAllContent> rsGxsChannelsGetChannelAllContent({ ReqGetChannelAllContent reqGetChannelAllContent }) async {
    Response response = await rsGxsChannelsGetChannelAllContentWithHttpInfo( reqGetChannelAllContent: reqGetChannelAllContent );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelAllContent') as ResGetChannelAllContent;
    } else {
      return null;
    }
  }

  /// DeprecatedThis feature rely on very buggy code, the returned value is not reliable with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelAutoDownloadWithHttpInfo({ ReqGetChannelAutoDownload reqGetChannelAutoDownload }) async {
    Object postBody = reqGetChannelAutoDownload;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelAutoDownload".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// DeprecatedThis feature rely on very buggy code, the returned value is not reliable
  ///
  ///ReqGetChannelAutoDownload reqGetChannelAutoDownload :
  ///     channelId: >              (RsGxsGroupId)channel id  
  /// 
  Future<ResGetChannelAutoDownload> rsGxsChannelsGetChannelAutoDownload({ ReqGetChannelAutoDownload reqGetChannelAutoDownload }) async {
    Response response = await rsGxsChannelsGetChannelAutoDownloadWithHttpInfo( reqGetChannelAutoDownload: reqGetChannelAutoDownload );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelAutoDownload') as ResGetChannelAutoDownload;
    } else {
      return null;
    }
  }

  /// Get channel comments corresponding to the given IDs. If the set is empty, nothing is returned. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelCommentsWithHttpInfo({ ReqGetChannelComments reqGetChannelComments }) async {
    Object postBody = reqGetChannelComments;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelComments".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get channel comments corresponding to the given IDs. If the set is empty, nothing is returned.
  ///
  ///ReqGetChannelComments reqGetChannelComments :
  ///     channelId: >              (RsGxsGroupId)id of the channel of which the content is requested          contentIds: >              (set<RsGxsMessageId>)ids of requested contents  
  /// 
  Future<ResGetChannelComments> rsGxsChannelsGetChannelComments({ ReqGetChannelComments reqGetChannelComments }) async {
    Response response = await rsGxsChannelsGetChannelCommentsWithHttpInfo( reqGetChannelComments: reqGetChannelComments );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelComments') as ResGetChannelComments;
    } else {
      return null;
    }
  }

  /// Get channel messages and comments corresponding to the given IDs. If the set is empty, nothing is returned. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelContentWithHttpInfo({ ReqGetChannelContent reqGetChannelContent }) async {
    Object postBody = reqGetChannelContent;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelContent".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get channel messages and comments corresponding to the given IDs. If the set is empty, nothing is returned.
  ///
  ///ReqGetChannelContent reqGetChannelContent :
  ///     channelId: >              (RsGxsGroupId)id of the channel of which the content is requested          contentsIds: >              (set<RsGxsMessageId>)ids of requested contents  
  /// 
  Future<ResGetChannelContent> rsGxsChannelsGetChannelContent({ ReqGetChannelContent reqGetChannelContent }) async {
    Response response = await rsGxsChannelsGetChannelContentWithHttpInfo( reqGetChannelContent: reqGetChannelContent );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelContent') as ResGetChannelContent;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelDownloadDirectoryWithHttpInfo({ ReqGetChannelDownloadDirectory reqGetChannelDownloadDirectory }) async {
    Object postBody = reqGetChannelDownloadDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelDownloadDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqGetChannelDownloadDirectory reqGetChannelDownloadDirectory :
  ///     channelId: >              (RsGxsGroupId)id of the channel  
  /// 
  Future<ResGetChannelDownloadDirectory> rsGxsChannelsGetChannelDownloadDirectory({ ReqGetChannelDownloadDirectory reqGetChannelDownloadDirectory }) async {
    Response response = await rsGxsChannelsGetChannelDownloadDirectoryWithHttpInfo( reqGetChannelDownloadDirectory: reqGetChannelDownloadDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelDownloadDirectory') as ResGetChannelDownloadDirectory;
    } else {
      return null;
    }
  }

  /// Retrieve statistics about the channel service with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelServiceStatisticsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelServiceStatistics".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Retrieve statistics about the channel service
  ///
  /// 
  Future<ResGetChannelServiceStatistics> rsGxsChannelsGetChannelServiceStatistics() async {
    Response response = await rsGxsChannelsGetChannelServiceStatisticsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelServiceStatistics') as ResGetChannelServiceStatistics;
    } else {
      return null;
    }
  }

  /// Retrieve statistics about the given channel with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelStatisticsWithHttpInfo({ ReqGetChannelStatistics reqGetChannelStatistics }) async {
    Object postBody = reqGetChannelStatistics;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelStatistics".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Retrieve statistics about the given channel
  ///
  ///ReqGetChannelStatistics reqGetChannelStatistics :
  ///     channelId: >              (RsGxsGroupId)Id of the channel group  
  /// 
  Future<ResGetChannelStatistics> rsGxsChannelsGetChannelStatistics({ ReqGetChannelStatistics reqGetChannelStatistics }) async {
    Response response = await rsGxsChannelsGetChannelStatisticsWithHttpInfo( reqGetChannelStatistics: reqGetChannelStatistics );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelStatistics') as ResGetChannelStatistics;
    } else {
      return null;
    }
  }

  /// Get channels information (description, thumbnail...). Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelsInfoWithHttpInfo({ ReqGetChannelsInfo reqGetChannelsInfo }) async {
    Object postBody = reqGetChannelsInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelsInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get channels information (description, thumbnail...). Blocking API.
  ///
  ///ReqGetChannelsInfo reqGetChannelsInfo :
  ///     chanIds: >              (list<RsGxsGroupId>)ids of the channels of which to get the informations  
  /// 
  Future<ResGetChannelsInfo> rsGxsChannelsGetChannelsInfo({ ReqGetChannelsInfo reqGetChannelsInfo }) async {
    Response response = await rsGxsChannelsGetChannelsInfoWithHttpInfo( reqGetChannelsInfo: reqGetChannelsInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelsInfo') as ResGetChannelsInfo;
    } else {
      return null;
    }
  }

  /// Get channels summaries list. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetChannelsSummariesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getChannelsSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get channels summaries list. Blocking API.
  ///
  /// 
  Future<ResGetChannelsSummaries> rsGxsChannelsGetChannelsSummaries() async {
    Response response = await rsGxsChannelsGetChannelsSummariesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChannelsSummaries') as ResGetChannelsSummaries;
    } else {
      return null;
    }
  }

  /// Get channel content summaries with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsGetContentSummariesWithHttpInfo({ ReqGetContentSummaries reqGetContentSummaries }) async {
    Object postBody = reqGetContentSummaries;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/getContentSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get channel content summaries
  ///
  ///ReqGetContentSummaries reqGetContentSummaries :
  ///     channelId: >              (RsGxsGroupId)id of the channel of which the content is requested  
  /// 
  Future<ResGetContentSummaries> rsGxsChannelsGetContentSummaries({ ReqGetContentSummaries reqGetContentSummaries }) async {
    Response response = await rsGxsChannelsGetContentSummariesWithHttpInfo( reqGetContentSummaries: reqGetContentSummaries );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetContentSummaries') as ResGetContentSummaries;
    } else {
      return null;
    }
  }

  /// This method is asynchronous. Search local channels with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsLocalSearchRequestWithHttpInfo({ ReqLocalSearchRequest reqLocalSearchRequest }) async {
    Object postBody = reqLocalSearchRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/localSearchRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This method is asynchronous. Search local channels
  ///
  ///ReqLocalSearchRequest reqLocalSearchRequest :
  ///     matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
  /// 
  Future<ResLocalSearchRequest> rsGxsChannelsLocalSearchRequest({ ReqLocalSearchRequest reqLocalSearchRequest }) async {
    Response response = await rsGxsChannelsLocalSearchRequestWithHttpInfo( reqLocalSearchRequest: reqLocalSearchRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResLocalSearchRequest') as ResLocalSearchRequest;
    } else {
      return null;
    }
  }

  /// Toggle post read status. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsMarkReadWithHttpInfo({ ReqMarkRead reqMarkRead }) async {
    Object postBody = reqMarkRead;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/markRead".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Toggle post read status. Blocking API.
  ///
  ///ReqMarkRead reqMarkRead :
  ///     postId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  
  /// 
  Future<ResMarkRead> rsGxsChannelsMarkRead({ ReqMarkRead reqMarkRead }) async {
    Response response = await rsGxsChannelsMarkReadWithHttpInfo( reqMarkRead: reqMarkRead );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMarkRead') as ResMarkRead;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsRequestStatusWithHttpInfo({ ReqRequestStatus reqRequestStatus }) async {
    Object postBody = reqRequestStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/requestStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqRequestStatus reqRequestStatus :
  ///     token: >              (integer)None 
  /// 
  Future<ResRequestStatus> rsGxsChannelsRequestStatus({ ReqRequestStatus reqRequestStatus }) async {
    Response response = await rsGxsChannelsRequestStatusWithHttpInfo( reqRequestStatus: reqRequestStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestStatus') as ResRequestStatus;
    } else {
      return null;
    }
  }

  /// DeprecatedThis feature rely on very buggy code, when enabled the channel service start flooding erratically log with error messages, apparently without more dangerous consequences. Still those messages hints that something out of control is happening under the hood, use at your own risk. A safe alternative to this method can easly implemented at API client level instead. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsSetChannelAutoDownloadWithHttpInfo({ ReqSetChannelAutoDownload reqSetChannelAutoDownload }) async {
    Object postBody = reqSetChannelAutoDownload;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/setChannelAutoDownload".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// DeprecatedThis feature rely on very buggy code, when enabled the channel service start flooding erratically log with error messages, apparently without more dangerous consequences. Still those messages hints that something out of control is happening under the hood, use at your own risk. A safe alternative to this method can easly implemented at API client level instead.
  ///
  ///ReqSetChannelAutoDownload reqSetChannelAutoDownload :
  ///     channelId: >              (RsGxsGroupId)channel id          enable: >              (boolean)true to enable, false to disable  
  /// 
  Future<ResSetChannelAutoDownload> rsGxsChannelsSetChannelAutoDownload({ ReqSetChannelAutoDownload reqSetChannelAutoDownload }) async {
    Response response = await rsGxsChannelsSetChannelAutoDownloadWithHttpInfo( reqSetChannelAutoDownload: reqSetChannelAutoDownload );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetChannelAutoDownload') as ResSetChannelAutoDownload;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsSetChannelDownloadDirectoryWithHttpInfo({ ReqSetChannelDownloadDirectory reqSetChannelDownloadDirectory }) async {
    Object postBody = reqSetChannelDownloadDirectory;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/setChannelDownloadDirectory".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqSetChannelDownloadDirectory reqSetChannelDownloadDirectory :
  ///     channelId: >              (RsGxsGroupId)id of the channel          directory: >              (string)path  
  /// 
  Future<ResSetChannelDownloadDirectory> rsGxsChannelsSetChannelDownloadDirectory({ ReqSetChannelDownloadDirectory reqSetChannelDownloadDirectory }) async {
    Response response = await rsGxsChannelsSetChannelDownloadDirectoryWithHttpInfo( reqSetChannelDownloadDirectory: reqSetChannelDownloadDirectory );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetChannelDownloadDirectory') as ResSetChannelDownloadDirectory;
    } else {
      return null;
    }
  }

  /// Share channel publishing key This can be used to authorize other peers to post on the channel with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsShareChannelKeysWithHttpInfo({ ReqShareChannelKeys reqShareChannelKeys }) async {
    Object postBody = reqShareChannelKeys;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/shareChannelKeys".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Share channel publishing key This can be used to authorize other peers to post on the channel
  ///
  ///ReqShareChannelKeys reqShareChannelKeys :
  ///     channelId: >              (RsGxsGroupId)id of the channel          peers: >              (set<RsPeerId>)peers to share the key with  
  /// 
  Future<ResShareChannelKeys> rsGxsChannelsShareChannelKeys({ ReqShareChannelKeys reqShareChannelKeys }) async {
    Response response = await rsGxsChannelsShareChannelKeysWithHttpInfo( reqShareChannelKeys: reqShareChannelKeys );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResShareChannelKeys') as ResShareChannelKeys;
    } else {
      return null;
    }
  }

  /// Subscrbe to a channel. Blocking API with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsSubscribeToChannelWithHttpInfo({ ReqSubscribeToChannel reqSubscribeToChannel }) async {
    Object postBody = reqSubscribeToChannel;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/subscribeToChannel".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Subscrbe to a channel. Blocking API
  ///
  ///ReqSubscribeToChannel reqSubscribeToChannel :
  ///     channelId: >              (RsGxsGroupId)Channel id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  
  /// 
  Future<ResSubscribeToChannel> rsGxsChannelsSubscribeToChannel({ ReqSubscribeToChannel reqSubscribeToChannel }) async {
    Response response = await rsGxsChannelsSubscribeToChannelWithHttpInfo( reqSubscribeToChannel: reqSubscribeToChannel );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSubscribeToChannel') as ResSubscribeToChannel;
    } else {
      return null;
    }
  }

  /// This method is asynchronous. Request remote channel with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsTurtleChannelRequestWithHttpInfo({ ReqTurtleChannelRequest reqTurtleChannelRequest }) async {
    Object postBody = reqTurtleChannelRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/turtleChannelRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This method is asynchronous. Request remote channel
  ///
  ///ReqTurtleChannelRequest reqTurtleChannelRequest :
  ///     channelId: >              (RsGxsGroupId)id of the channel to request to distants peers          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
  /// 
  Future<ResTurtleChannelRequest> rsGxsChannelsTurtleChannelRequest({ ReqTurtleChannelRequest reqTurtleChannelRequest }) async {
    Response response = await rsGxsChannelsTurtleChannelRequestWithHttpInfo( reqTurtleChannelRequest: reqTurtleChannelRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResTurtleChannelRequest') as ResTurtleChannelRequest;
    } else {
      return null;
    }
  }

  /// This method is asynchronous. Request remote channels search with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsChannelsTurtleSearchRequestWithHttpInfo({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async {
    Object postBody = reqTurtleSearchRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsChannels/turtleSearchRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This method is asynchronous. Request remote channels search
  ///
  ///ReqTurtleSearchRequest reqTurtleSearchRequest :
  ///     matchString: >              (string)string to look for in the search          maxWait: >              (rstime_t)maximum wait time in seconds for search results  
  /// 
  Future<ResTurtleSearchRequest> rsGxsChannelsTurtleSearchRequest({ ReqTurtleSearchRequest reqTurtleSearchRequest }) async {
    Response response = await rsGxsChannelsTurtleSearchRequestWithHttpInfo( reqTurtleSearchRequest: reqTurtleSearchRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResTurtleSearchRequest') as ResTurtleSearchRequest;
    } else {
      return null;
    }
  }

  /// Leave given circle with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesCancelCircleMembershipWithHttpInfo({ ReqCancelCircleMembership reqCancelCircleMembership }) async {
    Object postBody = reqCancelCircleMembership;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/cancelCircleMembership".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Leave given circle
  ///
  ///ReqCancelCircleMembership reqCancelCircleMembership :
  ///     ownGxsId: >              (RsGxsId)Own id to remove from the circle          circleId: >              (RsGxsCircleId)Id of the circle to leave  
  /// 
  Future<ResCancelCircleMembership> rsGxsCirclesCancelCircleMembership({ ReqCancelCircleMembership reqCancelCircleMembership }) async {
    Response response = await rsGxsCirclesCancelCircleMembershipWithHttpInfo( reqCancelCircleMembership: reqCancelCircleMembership );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCancelCircleMembership') as ResCancelCircleMembership;
    } else {
      return null;
    }
  }

  /// Create new circle with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesCreateCircleWithHttpInfo({ ReqCreateCircle reqCreateCircle }) async {
    Object postBody = reqCreateCircle;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/createCircle".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create new circle
  ///
  ///ReqCreateCircle reqCreateCircle :
  ///     circleName: >              (string)String containing cirlce name          circleType: >              (RsGxsCircleType)Circle type          restrictedId: >              (RsGxsCircleId)Optional id of a pre-existent circle that see the created circle. Meaningful only if circleType == EXTERNAL, must be null in all other cases.          authorId: >              (RsGxsId)Optional author of the circle.          gxsIdMembers: >              (set<RsGxsId>)GXS ids of the members of the circle.          localMembers: >              (set<RsPgpId>)PGP ids of the members if the circle.  
  /// 
  Future<ResCreateCircle> rsGxsCirclesCreateCircle({ ReqCreateCircle reqCreateCircle }) async {
    Response response = await rsGxsCirclesCreateCircleWithHttpInfo( reqCreateCircle: reqCreateCircle );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateCircle') as ResCreateCircle;
    } else {
      return null;
    }
  }

  /// Edit own existing circle with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesEditCircleWithHttpInfo({ ReqEditCircle reqEditCircle }) async {
    Object postBody = reqEditCircle;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/editCircle".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Edit own existing circle
  ///
  ///ReqEditCircle reqEditCircle :
  ///     cData: >              (RsGxsCircleGroup)Circle data with modifications, storage for data updatedad during the operation.  
  /// 
  Future<ResEditCircle> rsGxsCirclesEditCircle({ ReqEditCircle reqEditCircle }) async {
    Response response = await rsGxsCirclesEditCircleWithHttpInfo( reqEditCircle: reqEditCircle );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResEditCircle') as ResEditCircle;
    } else {
      return null;
    }
  }

  /// Get link to a circle with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesExportCircleLinkWithHttpInfo({ ReqExportCircleLink reqExportCircleLink }) async {
    Object postBody = reqExportCircleLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/exportCircleLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get link to a circle
  ///
  ///ReqExportCircleLink reqExportCircleLink :
  ///     circleId: >              (RsGxsCircleId)Id of the circle of which we want to generate a link          includeGxsData: >              (boolean)if true include the circle GXS group data so the receiver can request circle membership even if the circle hasn't propagated through GXS to her yet          baseUrl: >              (string)URL into which to sneak in the RetroShare circle link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare circle link into a \"normal\" looking web link. If empty the circle data link will be outputted in plain base64 format.  
  /// 
  Future<ResExportCircleLink> rsGxsCirclesExportCircleLink({ ReqExportCircleLink reqExportCircleLink }) async {
    Response response = await rsGxsCirclesExportCircleLinkWithHttpInfo( reqExportCircleLink: reqExportCircleLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportCircleLink') as ResExportCircleLink;
    } else {
      return null;
    }
  }

  /// Get circle details. Memory cached with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCircleDetailsWithHttpInfo({ ReqGetCircleDetails reqGetCircleDetails }) async {
    Object postBody = reqGetCircleDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCircleDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get circle details. Memory cached
  ///
  ///ReqGetCircleDetails reqGetCircleDetails :
  ///     id: >              (RsGxsCircleId)Id of the circle  
  /// 
  Future<ResGetCircleDetails> rsGxsCirclesGetCircleDetails({ ReqGetCircleDetails reqGetCircleDetails }) async {
    Response response = await rsGxsCirclesGetCircleDetailsWithHttpInfo( reqGetCircleDetails: reqGetCircleDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCircleDetails') as ResGetCircleDetails;
    } else {
      return null;
    }
  }

  /// Get list of known external circles ids. Memory cached with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCircleExternalIdListWithHttpInfo({ ReqGetCircleExternalIdList reqGetCircleExternalIdList }) async {
    Object postBody = reqGetCircleExternalIdList;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCircleExternalIdList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get list of known external circles ids. Memory cached
  ///
  ///ReqGetCircleExternalIdList reqGetCircleExternalIdList :
  ///     circleIds: >              (list<RsGxsCircleId>)Storage for circles id list  
  /// 
  Future<ResGetCircleExternalIdList> rsGxsCirclesGetCircleExternalIdList({ ReqGetCircleExternalIdList reqGetCircleExternalIdList }) async {
    Response response = await rsGxsCirclesGetCircleExternalIdListWithHttpInfo( reqGetCircleExternalIdList: reqGetCircleExternalIdList );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCircleExternalIdList') as ResGetCircleExternalIdList;
    } else {
      return null;
    }
  }

  /// Get specific circle request with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCircleRequestWithHttpInfo({ ReqGetCircleRequest reqGetCircleRequest }) async {
    Object postBody = reqGetCircleRequest;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCircleRequest".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get specific circle request
  ///
  ///ReqGetCircleRequest reqGetCircleRequest :
  ///     circleId: >              (RsGxsGroupId)id of the circle of which the requests are requested          msgId: >              (RsGxsMessageId)id of the request  
  /// 
  Future<ResGetCircleRequest> rsGxsCirclesGetCircleRequest({ ReqGetCircleRequest reqGetCircleRequest }) async {
    Response response = await rsGxsCirclesGetCircleRequestWithHttpInfo( reqGetCircleRequest: reqGetCircleRequest );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCircleRequest') as ResGetCircleRequest;
    } else {
      return null;
    }
  }

  /// Get circle requests with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCircleRequestsWithHttpInfo({ ReqGetCircleRequests reqGetCircleRequests }) async {
    Object postBody = reqGetCircleRequests;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCircleRequests".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get circle requests
  ///
  ///ReqGetCircleRequests reqGetCircleRequests :
  ///     circleId: >              (RsGxsGroupId)id of the circle of which the requests are requested  
  /// 
  Future<ResGetCircleRequests> rsGxsCirclesGetCircleRequests({ ReqGetCircleRequests reqGetCircleRequests }) async {
    Response response = await rsGxsCirclesGetCircleRequestsWithHttpInfo( reqGetCircleRequests: reqGetCircleRequests );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCircleRequests') as ResGetCircleRequests;
    } else {
      return null;
    }
  }

  /// Get circles information with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCirclesInfoWithHttpInfo({ ReqGetCirclesInfo reqGetCirclesInfo }) async {
    Object postBody = reqGetCirclesInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCirclesInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get circles information
  ///
  ///ReqGetCirclesInfo reqGetCirclesInfo :
  ///     circlesIds: >              (list<RsGxsGroupId>)ids of the circles of which to get the informations  
  /// 
  Future<ResGetCirclesInfo> rsGxsCirclesGetCirclesInfo({ ReqGetCirclesInfo reqGetCirclesInfo }) async {
    Response response = await rsGxsCirclesGetCirclesInfoWithHttpInfo( reqGetCirclesInfo: reqGetCirclesInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCirclesInfo') as ResGetCirclesInfo;
    } else {
      return null;
    }
  }

  /// Get circles summaries list. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesGetCirclesSummariesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/getCirclesSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get circles summaries list.
  ///
  /// 
  Future<ResGetCirclesSummaries> rsGxsCirclesGetCirclesSummaries() async {
    Response response = await rsGxsCirclesGetCirclesSummariesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCirclesSummaries') as ResGetCirclesSummaries;
    } else {
      return null;
    }
  }

  /// Invite identities to circle (admin key is required) with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesInviteIdsToCircleWithHttpInfo({ ReqInviteIdsToCircle reqInviteIdsToCircle }) async {
    Object postBody = reqInviteIdsToCircle;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/inviteIdsToCircle".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Invite identities to circle (admin key is required)
  ///
  ///ReqInviteIdsToCircle reqInviteIdsToCircle :
  ///     identities: >              (set<RsGxsId>)ids of the identities to invite          circleId: >              (RsGxsCircleId)Id of the circle you own and want to invite ids in  
  /// 
  Future<ResInviteIdsToCircle> rsGxsCirclesInviteIdsToCircle({ ReqInviteIdsToCircle reqInviteIdsToCircle }) async {
    Response response = await rsGxsCirclesInviteIdsToCircleWithHttpInfo( reqInviteIdsToCircle: reqInviteIdsToCircle );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResInviteIdsToCircle') as ResInviteIdsToCircle;
    } else {
      return null;
    }
  }

  /// Request circle membership, or accept circle invitation with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesRequestCircleMembershipWithHttpInfo({ ReqRequestCircleMembership reqRequestCircleMembership }) async {
    Object postBody = reqRequestCircleMembership;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/requestCircleMembership".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Request circle membership, or accept circle invitation
  ///
  ///ReqRequestCircleMembership reqRequestCircleMembership :
  ///     ownGxsId: >              (RsGxsId)Id of own identity to introduce to the circle          circleId: >              (RsGxsCircleId)Id of the circle to which ask for inclusion  
  /// 
  Future<ResRequestCircleMembership> rsGxsCirclesRequestCircleMembership({ ReqRequestCircleMembership reqRequestCircleMembership }) async {
    Response response = await rsGxsCirclesRequestCircleMembershipWithHttpInfo( reqRequestCircleMembership: reqRequestCircleMembership );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestCircleMembership') as ResRequestCircleMembership;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesRequestStatusWithHttpInfo({ ReqRequestStatus reqRequestStatus }) async {
    Object postBody = reqRequestStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/requestStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqRequestStatus reqRequestStatus :
  ///     token: >              (integer)None 
  /// 
  Future<ResRequestStatus> rsGxsCirclesRequestStatus({ ReqRequestStatus reqRequestStatus }) async {
    Response response = await rsGxsCirclesRequestStatusWithHttpInfo( reqRequestStatus: reqRequestStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestStatus') as ResRequestStatus;
    } else {
      return null;
    }
  }

  /// Remove identities from circle (admin key is required) with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsCirclesRevokeIdsFromCircleWithHttpInfo({ ReqRevokeIdsFromCircle reqRevokeIdsFromCircle }) async {
    Object postBody = reqRevokeIdsFromCircle;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsCircles/revokeIdsFromCircle".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove identities from circle (admin key is required)
  ///
  ///ReqRevokeIdsFromCircle reqRevokeIdsFromCircle :
  ///     identities: >              (set<RsGxsId>)ids of the identities to remove from the invite list          circleId: >              (RsGxsCircleId)Id of the circle you own and want to invite ids in  
  /// 
  Future<ResRevokeIdsFromCircle> rsGxsCirclesRevokeIdsFromCircle({ ReqRevokeIdsFromCircle reqRevokeIdsFromCircle }) async {
    Response response = await rsGxsCirclesRevokeIdsFromCircleWithHttpInfo( reqRevokeIdsFromCircle: reqRevokeIdsFromCircle );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRevokeIdsFromCircle') as ResRevokeIdsFromCircle;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsCreateForumWithHttpInfo({ ReqCreateForum reqCreateForum }) async {
    Object postBody = reqCreateForum;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/createForum".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqCreateForum reqCreateForum :
  ///     forum: >              (RsGxsForumGroup)Forum data (name, description...)  
  /// 
  Future<ResCreateForum> rsGxsForumsCreateForum({ ReqCreateForum reqCreateForum }) async {
    Response response = await rsGxsForumsCreateForumWithHttpInfo( reqCreateForum: reqCreateForum );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateForum') as ResCreateForum;
    } else {
      return null;
    }
  }

  /// Create forum. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsCreateForumV2WithHttpInfo({ ReqCreateForumV2 reqCreateForumV2 }) async {
    Object postBody = reqCreateForumV2;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/createForumV2".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create forum.
  ///
  ///ReqCreateForumV2 reqCreateForumV2 :
  ///     name: >              (string)Name of the forum          description: >              (string)Optional description of the forum          authorId: >              (RsGxsId)Optional id of the froum owner author          moderatorsIds: >              (set<RsGxsId>)Optional list of forum moderators          circleType: >              (RsGxsCircleType)Optional visibility rule, default public.          circleId: >              (RsGxsCircleId)If the forum is not public specify the id of the circle who can see the forum. Depending on the value you pass for circleType this should be a circle if EXTERNAL is passed, a local friends group id if NODES_GROUP is passed, empty otherwise.  
  /// 
  Future<ResCreateForumV2> rsGxsForumsCreateForumV2({ ReqCreateForumV2 reqCreateForumV2 }) async {
    Response response = await rsGxsForumsCreateForumV2WithHttpInfo( reqCreateForumV2: reqCreateForumV2 );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateForumV2') as ResCreateForumV2;
    } else {
      return null;
    }
  }

  /// Deprecated with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsCreateMessageWithHttpInfo({ ReqCreateMessage reqCreateMessage }) async {
    Object postBody = reqCreateMessage;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/createMessage".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Deprecated
  ///
  ///ReqCreateMessage reqCreateMessage :
  ///     message: >              (RsGxsForumMsg)None 
  /// 
  Future<ResCreateMessage> rsGxsForumsCreateMessage({ ReqCreateMessage reqCreateMessage }) async {
    Response response = await rsGxsForumsCreateMessageWithHttpInfo( reqCreateMessage: reqCreateMessage );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateMessage') as ResCreateMessage;
    } else {
      return null;
    }
  }

  /// Create a post on the given forum. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsCreatePostWithHttpInfo({ ReqCreatePost reqCreatePost }) async {
    Object postBody = reqCreatePost;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/createPost".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create a post on the given forum.
  ///
  ///ReqCreatePost reqCreatePost :
  ///     forumId: >              (RsGxsGroupId)Id of the forum in which the post is to be submitted          title: >              (string)UTF-8 string containing the title of the post          mBody: >              (string)UTF-8 string containing the text of the post          authorId: >              (RsGxsId)Id of the author of the comment          parentId: >              (RsGxsMessageId)Optional Id of the parent post if this post is a reply to another post, empty otherwise.          origPostId: >              (RsGxsMessageId)If this is supposed to replace an already existent post, the id of the old post. If left blank a new post will be created.  
  /// 
  Future<ResCreatePost> rsGxsForumsCreatePost({ ReqCreatePost reqCreatePost }) async {
    Response response = await rsGxsForumsCreatePostWithHttpInfo( reqCreatePost: reqCreatePost );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreatePost') as ResCreatePost;
    } else {
      return null;
    }
  }

  /// Edit forum details. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsEditForumWithHttpInfo({ ReqEditForum reqEditForum }) async {
    Object postBody = reqEditForum;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/editForum".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Edit forum details.
  ///
  ///ReqEditForum reqEditForum :
  ///     forum: >              (RsGxsForumGroup)Forum data (name, description...) with modifications  
  /// 
  Future<ResEditForum> rsGxsForumsEditForum({ ReqEditForum reqEditForum }) async {
    Response response = await rsGxsForumsEditForumWithHttpInfo( reqEditForum: reqEditForum );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResEditForum') as ResEditForum;
    } else {
      return null;
    }
  }

  /// Get link to a forum with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsExportForumLinkWithHttpInfo({ ReqExportForumLink reqExportForumLink }) async {
    Object postBody = reqExportForumLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/exportForumLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get link to a forum
  ///
  ///ReqExportForumLink reqExportForumLink :
  ///     forumId: >              (RsGxsGroupId)Id of the forum of which we want to generate a link          includeGxsData: >              (boolean)if true include the forum GXS group data so the receiver can subscribe to the forum even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  
  /// 
  Future<ResExportForumLink> rsGxsForumsExportForumLink({ ReqExportForumLink reqExportForumLink }) async {
    Response response = await rsGxsForumsExportForumLinkWithHttpInfo( reqExportForumLink: reqExportForumLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportForumLink') as ResExportForumLink;
    } else {
      return null;
    }
  }

  /// Get specific list of messages from a single forum. Blocking API with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumContentWithHttpInfo({ ReqGetForumContent reqGetForumContent }) async {
    Object postBody = reqGetForumContent;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumContent".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get specific list of messages from a single forum. Blocking API
  ///
  ///ReqGetForumContent reqGetForumContent :
  ///     forumId: >              (RsGxsGroupId)id of the forum of which the content is requested          msgsIds: >              (set<RsGxsMessageId>)list of message ids to request  
  /// 
  Future<ResGetForumContent> rsGxsForumsGetForumContent({ ReqGetForumContent reqGetForumContent }) async {
    Response response = await rsGxsForumsGetForumContentWithHttpInfo( reqGetForumContent: reqGetForumContent );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumContent') as ResGetForumContent;
    } else {
      return null;
    }
  }

  /// Get message metadatas for a specific forum. Blocking API with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumMsgMetaDataWithHttpInfo({ ReqGetForumMsgMetaData reqGetForumMsgMetaData }) async {
    Object postBody = reqGetForumMsgMetaData;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumMsgMetaData".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get message metadatas for a specific forum. Blocking API
  ///
  ///ReqGetForumMsgMetaData reqGetForumMsgMetaData :
  ///     forumId: >              (RsGxsGroupId)id of the forum of which the content is requested  
  /// 
  Future<ResGetForumMsgMetaData> rsGxsForumsGetForumMsgMetaData({ ReqGetForumMsgMetaData reqGetForumMsgMetaData }) async {
    Response response = await rsGxsForumsGetForumMsgMetaDataWithHttpInfo( reqGetForumMsgMetaData: reqGetForumMsgMetaData );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumMsgMetaData') as ResGetForumMsgMetaData;
    } else {
      return null;
    }
  }

  /// returns statistics for the forum service with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumServiceStatisticsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumServiceStatistics".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// returns statistics for the forum service
  ///
  /// 
  Future<ResGetForumServiceStatistics> rsGxsForumsGetForumServiceStatistics() async {
    Response response = await rsGxsForumsGetForumServiceStatisticsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumServiceStatistics') as ResGetForumServiceStatistics;
    } else {
      return null;
    }
  }

  /// returns statistics about a particular forum with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumStatisticsWithHttpInfo({ ReqGetForumStatistics reqGetForumStatistics }) async {
    Object postBody = reqGetForumStatistics;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumStatistics".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// returns statistics about a particular forum
  ///
  ///ReqGetForumStatistics reqGetForumStatistics :
  ///     forumId: >              (RsGxsGroupId)Id of the forum  
  /// 
  Future<ResGetForumStatistics> rsGxsForumsGetForumStatistics({ ReqGetForumStatistics reqGetForumStatistics }) async {
    Response response = await rsGxsForumsGetForumStatisticsWithHttpInfo( reqGetForumStatistics: reqGetForumStatistics );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumStatistics') as ResGetForumStatistics;
    } else {
      return null;
    }
  }

  /// Get forums information (description, thumbnail...). Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumsInfoWithHttpInfo({ ReqGetForumsInfo reqGetForumsInfo }) async {
    Object postBody = reqGetForumsInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumsInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get forums information (description, thumbnail...). Blocking API.
  ///
  ///ReqGetForumsInfo reqGetForumsInfo :
  ///     forumIds: >              (list<RsGxsGroupId>)ids of the forums of which to get the informations  
  /// 
  Future<ResGetForumsInfo> rsGxsForumsGetForumsInfo({ ReqGetForumsInfo reqGetForumsInfo }) async {
    Response response = await rsGxsForumsGetForumsInfoWithHttpInfo( reqGetForumsInfo: reqGetForumsInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumsInfo') as ResGetForumsInfo;
    } else {
      return null;
    }
  }

  /// Get forums summaries list. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsGetForumsSummariesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/getForumsSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get forums summaries list. Blocking API.
  ///
  /// 
  Future<ResGetForumsSummaries> rsGxsForumsGetForumsSummaries() async {
    Response response = await rsGxsForumsGetForumsSummariesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetForumsSummaries') as ResGetForumsSummaries;
    } else {
      return null;
    }
  }

  /// Toggle message read status. Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsMarkReadWithHttpInfo({ ReqMarkRead reqMarkRead }) async {
    Object postBody = reqMarkRead;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/markRead".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Toggle message read status. Blocking API.
  ///
  ///ReqMarkRead reqMarkRead :
  ///     messageId: >              (RsGxsGrpMsgIdPair)post identifier          read: >              (boolean)true to mark as read, false to mark as unread  
  /// 
  Future<ResMarkRead> rsGxsForumsMarkRead({ ReqMarkRead reqMarkRead }) async {
    Response response = await rsGxsForumsMarkReadWithHttpInfo( reqMarkRead: reqMarkRead );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMarkRead') as ResMarkRead;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsRequestStatusWithHttpInfo({ ReqRequestStatus reqRequestStatus }) async {
    Object postBody = reqRequestStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/requestStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqRequestStatus reqRequestStatus :
  ///     token: >              (integer)None 
  /// 
  Future<ResRequestStatus> rsGxsForumsRequestStatus({ ReqRequestStatus reqRequestStatus }) async {
    Response response = await rsGxsForumsRequestStatusWithHttpInfo( reqRequestStatus: reqRequestStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestStatus') as ResRequestStatus;
    } else {
      return null;
    }
  }

  /// Subscrbe to a forum. Blocking API with HTTP info returned
  ///
  /// 
  Future<Response> rsGxsForumsSubscribeToForumWithHttpInfo({ ReqSubscribeToForum reqSubscribeToForum }) async {
    Object postBody = reqSubscribeToForum;

    // verify required params are set

    // create path and map variables
    String path = "/rsGxsForums/subscribeToForum".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Subscrbe to a forum. Blocking API
  ///
  ///ReqSubscribeToForum reqSubscribeToForum :
  ///     forumId: >              (RsGxsGroupId)Forum id          subscribe: >              (boolean)true to subscribe, false to unsubscribe  
  /// 
  Future<ResSubscribeToForum> rsGxsForumsSubscribeToForum({ ReqSubscribeToForum reqSubscribeToForum }) async {
    Response response = await rsGxsForumsSubscribeToForumWithHttpInfo( reqSubscribeToForum: reqSubscribeToForum );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSubscribeToForum') as ResSubscribeToForum;
    } else {
      return null;
    }
  }

  /// Check if automatic signed by friend identity contact flagging is enabled with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityAutoAddFriendIdsAsContactWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/autoAddFriendIdsAsContact".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if automatic signed by friend identity contact flagging is enabled
  ///
  /// 
  Future<ResAutoAddFriendIdsAsContact> rsIdentityAutoAddFriendIdsAsContact() async {
    Response response = await rsIdentityAutoAddFriendIdsAsContactWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAutoAddFriendIdsAsContact') as ResAutoAddFriendIdsAsContact;
    } else {
      return null;
    }
  }

  /// Create a new identity with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityCreateIdentityWithHttpInfo({ ReqCreateIdentity reqCreateIdentity }) async {
    Object postBody = reqCreateIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/createIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Create a new identity
  ///
  ///ReqCreateIdentity reqCreateIdentity :
  ///     name: >              (string)Name of the identity          avatar: >              (RsGxsImage)Image associated to the identity          pseudonimous: >              (boolean)true for unsigned identity, false otherwise          pgpPassword: >              (string)password to unlock PGP to sign identity, not implemented yet  
  /// 
  Future<ResCreateIdentity> rsIdentityCreateIdentity({ ReqCreateIdentity reqCreateIdentity }) async {
    Response response = await rsIdentityCreateIdentityWithHttpInfo( reqCreateIdentity: reqCreateIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateIdentity') as ResCreateIdentity;
    } else {
      return null;
    }
  }

  /// Get number of days after which delete a banned identities with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityDeleteBannedNodesThresholdWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/deleteBannedNodesThreshold".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get number of days after which delete a banned identities
  ///
  /// 
  Future<ResDeleteBannedNodesThreshold> rsIdentityDeleteBannedNodesThreshold() async {
    Response response = await rsIdentityDeleteBannedNodesThresholdWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResDeleteBannedNodesThreshold') as ResDeleteBannedNodesThreshold;
    } else {
      return null;
    }
  }

  /// Locally delete given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityDeleteIdentityWithHttpInfo({ ReqDeleteIdentity reqDeleteIdentity }) async {
    Object postBody = reqDeleteIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/deleteIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Locally delete given identity
  ///
  ///ReqDeleteIdentity reqDeleteIdentity :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResDeleteIdentity> rsIdentityDeleteIdentity({ ReqDeleteIdentity reqDeleteIdentity }) async {
    Response response = await rsIdentityDeleteIdentityWithHttpInfo( reqDeleteIdentity: reqDeleteIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResDeleteIdentity') as ResDeleteIdentity;
    } else {
      return null;
    }
  }

  /// Get link to a identity with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityExportIdentityLinkWithHttpInfo({ ReqExportIdentityLink reqExportIdentityLink }) async {
    Object postBody = reqExportIdentityLink;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/exportIdentityLink".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get link to a identity
  ///
  ///ReqExportIdentityLink reqExportIdentityLink :
  ///     id: >              (RsGxsId)Id of the identity of which you want to generate a link          includeGxsData: >              (boolean)if true include the identity GXS group data so the receiver can make use of the identity even if she hasn't received it through GXS yet          baseUrl: >              (string)URL into which to sneak in the RetroShare link radix, this is primarly useful to induce applications into making the link clickable, or to disguise the RetroShare link into a \"normal\" looking web link. If empty the GXS data link will be outputted in plain base64 format.  
  /// 
  Future<ResExportIdentityLink> rsIdentityExportIdentityLink({ ReqExportIdentityLink reqExportIdentityLink }) async {
    Response response = await rsIdentityExportIdentityLinkWithHttpInfo( reqExportIdentityLink: reqExportIdentityLink );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResExportIdentityLink') as ResExportIdentityLink;
    } else {
      return null;
    }
  }

  /// Get identity details, from the cache with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetIdDetailsWithHttpInfo({ ReqGetIdDetails reqGetIdDetails }) async {
    Object postBody = reqGetIdDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getIdDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get identity details, from the cache
  ///
  ///ReqGetIdDetails reqGetIdDetails :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResGetIdDetails> rsIdentityGetIdDetails({ ReqGetIdDetails reqGetIdDetails }) async {
    Response response = await rsIdentityGetIdDetailsWithHttpInfo( reqGetIdDetails: reqGetIdDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetIdDetails') as ResGetIdDetails;
    } else {
      return null;
    }
  }

  /// Get identities information (name, avatar...). Blocking API. with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetIdentitiesInfoWithHttpInfo({ ReqGetIdentitiesInfo reqGetIdentitiesInfo }) async {
    Object postBody = reqGetIdentitiesInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getIdentitiesInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get identities information (name, avatar...). Blocking API.
  ///
  ///ReqGetIdentitiesInfo reqGetIdentitiesInfo :
  ///     ids: >              (set<RsGxsId>)ids of the channels of which to get the informations  
  /// 
  Future<ResGetIdentitiesInfo> rsIdentityGetIdentitiesInfo({ ReqGetIdentitiesInfo reqGetIdentitiesInfo }) async {
    Response response = await rsIdentityGetIdentitiesInfoWithHttpInfo( reqGetIdentitiesInfo: reqGetIdentitiesInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetIdentitiesInfo') as ResGetIdentitiesInfo;
    } else {
      return null;
    }
  }

  /// Get identities summaries list. with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetIdentitiesSummariesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getIdentitiesSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get identities summaries list.
  ///
  /// 
  Future<ResGetIdentitiesSummaries> rsIdentityGetIdentitiesSummaries() async {
    Response response = await rsIdentityGetIdentitiesSummariesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetIdentitiesSummaries') as ResGetIdentitiesSummaries;
    } else {
      return null;
    }
  }

  /// Get last seen usage time of given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetLastUsageTSWithHttpInfo({ ReqGetLastUsageTS reqGetLastUsageTS }) async {
    Object postBody = reqGetLastUsageTS;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getLastUsageTS".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get last seen usage time of given identity
  ///
  ///ReqGetLastUsageTS reqGetLastUsageTS :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResGetLastUsageTS> rsIdentityGetLastUsageTS({ ReqGetLastUsageTS reqGetLastUsageTS }) async {
    Response response = await rsIdentityGetLastUsageTSWithHttpInfo( reqGetLastUsageTS: reqGetLastUsageTS );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetLastUsageTS') as ResGetLastUsageTS;
    } else {
      return null;
    }
  }

  /// Get own pseudonimous (unsigned) ids with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetOwnPseudonimousIdsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getOwnPseudonimousIds".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get own pseudonimous (unsigned) ids
  ///
  /// 
  Future<ResGetOwnPseudonimousIds> rsIdentityGetOwnPseudonimousIds() async {
    Response response = await rsIdentityGetOwnPseudonimousIdsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOwnPseudonimousIds') as ResGetOwnPseudonimousIds;
    } else {
      return null;
    }
  }

  /// Get own signed ids with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityGetOwnSignedIdsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/getOwnSignedIds".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get own signed ids
  ///
  /// 
  Future<ResGetOwnSignedIds> rsIdentityGetOwnSignedIds() async {
    Response response = await rsIdentityGetOwnSignedIdsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOwnSignedIds') as ResGetOwnSignedIds;
    } else {
      return null;
    }
  }

  /// Check if an identity is contact with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityIsARegularContactWithHttpInfo({ ReqIsARegularContact reqIsARegularContact }) async {
    Object postBody = reqIsARegularContact;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/isARegularContact".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if an identity is contact
  ///
  ///ReqIsARegularContact reqIsARegularContact :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResIsARegularContact> rsIdentityIsARegularContact({ ReqIsARegularContact reqIsARegularContact }) async {
    Response response = await rsIdentityIsARegularContactWithHttpInfo( reqIsARegularContact: reqIsARegularContact );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsARegularContact') as ResIsARegularContact;
    } else {
      return null;
    }
  }

  /// Check if an id is known with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityIsKnownIdWithHttpInfo({ ReqIsKnownId reqIsKnownId }) async {
    Object postBody = reqIsKnownId;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/isKnownId".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if an id is known
  ///
  ///ReqIsKnownId reqIsKnownId :
  ///     id: >              (RsGxsId)Id to check  
  /// 
  Future<ResIsKnownId> rsIdentityIsKnownId({ ReqIsKnownId reqIsKnownId }) async {
    Response response = await rsIdentityIsKnownIdWithHttpInfo( reqIsKnownId: reqIsKnownId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsKnownId') as ResIsKnownId;
    } else {
      return null;
    }
  }

  /// Check if an id is own with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityIsOwnIdWithHttpInfo({ ReqIsOwnId reqIsOwnId }) async {
    Object postBody = reqIsOwnId;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/isOwnId".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if an id is own
  ///
  ///ReqIsOwnId reqIsOwnId :
  ///     id: >              (RsGxsId)Id to check  
  /// 
  Future<ResIsOwnId> rsIdentityIsOwnId({ ReqIsOwnId reqIsOwnId }) async {
    Response response = await rsIdentityIsOwnIdWithHttpInfo( reqIsOwnId: reqIsOwnId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsOwnId') as ResIsOwnId;
    } else {
      return null;
    }
  }

  /// request details of a not yet known identity to the network with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityRequestIdentityWithHttpInfo({ ReqRequestIdentity reqRequestIdentity }) async {
    Object postBody = reqRequestIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/requestIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// request details of a not yet known identity to the network
  ///
  ///ReqRequestIdentity reqRequestIdentity :
  ///     id: >              (RsGxsId)id of the identity to request          peers: >              (vector<RsPeerId>)optional list of the peers to ask for the key, if empty all online peers are asked.  
  /// 
  Future<ResRequestIdentity> rsIdentityRequestIdentity({ ReqRequestIdentity reqRequestIdentity }) async {
    Response response = await rsIdentityRequestIdentityWithHttpInfo( reqRequestIdentity: reqRequestIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestIdentity') as ResRequestIdentity;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityRequestStatusWithHttpInfo({ ReqRequestStatus reqRequestStatus }) async {
    Object postBody = reqRequestStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/requestStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqRequestStatus reqRequestStatus :
  ///     token: >              (integer)None 
  /// 
  Future<ResRequestStatus> rsIdentityRequestStatus({ ReqRequestStatus reqRequestStatus }) async {
    Response response = await rsIdentityRequestStatusWithHttpInfo( reqRequestStatus: reqRequestStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestStatus') as ResRequestStatus;
    } else {
      return null;
    }
  }

  /// Set/unset identity as contact with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentitySetAsRegularContactWithHttpInfo({ ReqSetAsRegularContact reqSetAsRegularContact }) async {
    Object postBody = reqSetAsRegularContact;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/setAsRegularContact".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set/unset identity as contact
  ///
  ///ReqSetAsRegularContact reqSetAsRegularContact :
  ///     id: >              (RsGxsId)Id of the identity          isContact: >              (boolean)true to set, false to unset  
  /// 
  Future<ResSetAsRegularContact> rsIdentitySetAsRegularContact({ ReqSetAsRegularContact reqSetAsRegularContact }) async {
    Response response = await rsIdentitySetAsRegularContactWithHttpInfo( reqSetAsRegularContact: reqSetAsRegularContact );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetAsRegularContact') as ResSetAsRegularContact;
    } else {
      return null;
    }
  }

  /// Toggle automatic flagging signed by friends identity as contact with HTTP info returned
  ///
  /// 
  Future rsIdentitySetAutoAddFriendIdsAsContactWithHttpInfo({ ReqSetAutoAddFriendIdsAsContact reqSetAutoAddFriendIdsAsContact }) async {
    Object postBody = reqSetAutoAddFriendIdsAsContact;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/setAutoAddFriendIdsAsContact".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Toggle automatic flagging signed by friends identity as contact
  ///
  ///ReqSetAutoAddFriendIdsAsContact reqSetAutoAddFriendIdsAsContact :
  ///     enabled: >              (boolean)true to enable, false to disable  
  /// 
  Future rsIdentitySetAutoAddFriendIdsAsContact({ ReqSetAutoAddFriendIdsAsContact reqSetAutoAddFriendIdsAsContact }) async {
    Response response = await rsIdentitySetAutoAddFriendIdsAsContactWithHttpInfo( reqSetAutoAddFriendIdsAsContact: reqSetAutoAddFriendIdsAsContact );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set number of days after which delete a banned identities with HTTP info returned
  ///
  /// 
  Future rsIdentitySetDeleteBannedNodesThresholdWithHttpInfo({ ReqSetDeleteBannedNodesThreshold reqSetDeleteBannedNodesThreshold }) async {
    Object postBody = reqSetDeleteBannedNodesThreshold;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/setDeleteBannedNodesThreshold".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set number of days after which delete a banned identities
  ///
  ///ReqSetDeleteBannedNodesThreshold reqSetDeleteBannedNodesThreshold :
  ///     days: >              (integer)number of days  
  /// 
  Future rsIdentitySetDeleteBannedNodesThreshold({ ReqSetDeleteBannedNodesThreshold reqSetDeleteBannedNodesThreshold }) async {
    Response response = await rsIdentitySetDeleteBannedNodesThresholdWithHttpInfo( reqSetDeleteBannedNodesThreshold: reqSetDeleteBannedNodesThreshold );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Update identity data (name, avatar...) with HTTP info returned
  ///
  /// 
  Future<Response> rsIdentityUpdateIdentityWithHttpInfo({ ReqUpdateIdentity reqUpdateIdentity }) async {
    Object postBody = reqUpdateIdentity;

    // verify required params are set

    // create path and map variables
    String path = "/rsIdentity/updateIdentity".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Update identity data (name, avatar...)
  ///
  ///ReqUpdateIdentity reqUpdateIdentity :
  ///     identityData: >              (RsGxsIdGroup)updated identiy data  
  /// 
  Future<ResUpdateIdentity> rsIdentityUpdateIdentity({ ReqUpdateIdentity reqUpdateIdentity }) async {
    Response response = await rsIdentityUpdateIdentityWithHttpInfo( reqUpdateIdentity: reqUpdateIdentity );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResUpdateIdentity') as ResUpdateIdentity;
    } else {
      return null;
    }
  }

  /// Request with HTTP info returned
  ///
  /// 
  Future rsJsonApiAskForStopWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/askForStop".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Request
  ///
  /// 
  Future rsJsonApiAskForStop() async {
    Response response = await rsJsonApiAskForStopWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiAuthorizeUserWithHttpInfo({ ReqAuthorizeUser reqAuthorizeUser }) async {
    Object postBody = reqAuthorizeUser;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/authorizeUser".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqAuthorizeUser reqAuthorizeUser :
  ///     user: >              (string)user name to autorize, must not contain ':'          password: >              (string)password for the user  
  /// 
  Future<ResAuthorizeUser> rsJsonApiAuthorizeUser({ ReqAuthorizeUser reqAuthorizeUser }) async {
    Response response = await rsJsonApiAuthorizeUserWithHttpInfo( reqAuthorizeUser: reqAuthorizeUser );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAuthorizeUser') as ResAuthorizeUser;
    } else {
      return null;
    }
  }

  /// Get authorized tokens with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiGetAuthorizedTokensWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/getAuthorizedTokens".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get authorized tokens
  ///
  /// 
  Future<ResGetAuthorizedTokens> rsJsonApiGetAuthorizedTokens() async {
    Response response = await rsJsonApiGetAuthorizedTokensWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetAuthorizedTokens') as ResGetAuthorizedTokens;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiGetBindingAddressWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/getBindingAddress".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  /// 
  Future<ResGetBindingAddress> rsJsonApiGetBindingAddress() async {
    Response response = await rsJsonApiGetBindingAddressWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetBindingAddress') as ResGetBindingAddress;
    } else {
      return null;
    }
  }

  /// Check if given JSON API auth token is authorized with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiIsAuthTokenValidWithHttpInfo({ ReqIsAuthTokenValid reqIsAuthTokenValid }) async {
    Object postBody = reqIsAuthTokenValid;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/isAuthTokenValid".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if given JSON API auth token is authorized
  ///
  ///ReqIsAuthTokenValid reqIsAuthTokenValid :
  ///     token: >              (string)decoded  
  /// 
  Future<ResIsAuthTokenValid> rsJsonApiIsAuthTokenValid({ ReqIsAuthTokenValid reqIsAuthTokenValid }) async {
    Response response = await rsJsonApiIsAuthTokenValidWithHttpInfo( reqIsAuthTokenValid: reqIsAuthTokenValid );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsAuthTokenValid') as ResIsAuthTokenValid;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiListeningPortWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/listeningPort".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  /// 
  Future<ResListeningPort> rsJsonApiListeningPort() async {
    Response response = await rsJsonApiListeningPortWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResListeningPort') as ResListeningPort;
    } else {
      return null;
    }
  }

  /// This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process. with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiRequestNewTokenAutorizationWithHttpInfo({ ReqRequestNewTokenAutorization reqRequestNewTokenAutorization }) async {
    Object postBody = reqRequestNewTokenAutorization;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/requestNewTokenAutorization".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This function should be used by JSON API clients that aren&#39;t authenticated yet, to ask their token to be authorized, the success or failure will depend on mNewAccessRequestCallback return value, and it will likely need human user interaction in the process.
  ///
  ///ReqRequestNewTokenAutorization reqRequestNewTokenAutorization :
  ///     user: >              (string)user name to authorize          password: >              (string)password for the new user  
  /// 
  Future<ResRequestNewTokenAutorization> rsJsonApiRequestNewTokenAutorization({ ReqRequestNewTokenAutorization reqRequestNewTokenAutorization }) async {
    Response response = await rsJsonApiRequestNewTokenAutorizationWithHttpInfo( reqRequestNewTokenAutorization: reqRequestNewTokenAutorization );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestNewTokenAutorization') as ResRequestNewTokenAutorization;
    } else {
      return null;
    }
  }

  /// Restart with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiRestartWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/restart".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Restart
  ///
  /// 
  Future<ResRestart> rsJsonApiRestart() async {
    Response response = await rsJsonApiRestartWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRestart') as ResRestart;
    } else {
      return null;
    }
  }

  /// Revoke given auth token with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiRevokeAuthTokenWithHttpInfo({ ReqRevokeAuthToken reqRevokeAuthToken }) async {
    Object postBody = reqRevokeAuthToken;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/revokeAuthToken".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Revoke given auth token
  ///
  ///ReqRevokeAuthToken reqRevokeAuthToken :
  ///     user: >              (string)user name of which to revoke authorization  
  /// 
  Future<ResRevokeAuthToken> rsJsonApiRevokeAuthToken({ ReqRevokeAuthToken reqRevokeAuthToken }) async {
    Response response = await rsJsonApiRevokeAuthTokenWithHttpInfo( reqRevokeAuthToken: reqRevokeAuthToken );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRevokeAuthToken') as ResRevokeAuthToken;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future rsJsonApiSetBindingAddressWithHttpInfo({ ReqSetBindingAddress reqSetBindingAddress }) async {
    Object postBody = reqSetBindingAddress;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/setBindingAddress".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqSetBindingAddress reqSetBindingAddress :
  ///     address: >              (string)Binding address in IPv4 or IPv6 format.  
  /// 
  Future rsJsonApiSetBindingAddress({ ReqSetBindingAddress reqSetBindingAddress }) async {
    Response response = await rsJsonApiSetBindingAddressWithHttpInfo( reqSetBindingAddress: reqSetBindingAddress );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future rsJsonApiSetListeningPortWithHttpInfo({ ReqSetListeningPort reqSetListeningPort }) async {
    Object postBody = reqSetListeningPort;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/setListeningPort".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqSetListeningPort reqSetListeningPort :
  ///     port: >              (integer)Must be available otherwise the binding will fail  
  /// 
  Future rsJsonApiSetListeningPort({ ReqSetListeningPort reqSetListeningPort }) async {
    Response response = await rsJsonApiSetListeningPortWithHttpInfo( reqSetListeningPort: reqSetListeningPort );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Write version information to given paramethers with HTTP info returned
  ///
  /// 
  Future<Response> rsJsonApiVersionWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsJsonApi/version".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Write version information to given paramethers
  ///
  /// 
  Future<ResVersion> rsJsonApiVersion() async {
    Response response = await rsJsonApiVersionWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResVersion') as ResVersion;
    } else {
      return null;
    }
  }

  /// Normal way to attempt login with HTTP info returned
  ///
  /// 
  Future<Response> rsLoginHelperAttemptLoginWithHttpInfo({ ReqAttemptLogin reqAttemptLogin }) async {
    Object postBody = reqAttemptLogin;

    // verify required params are set

    // create path and map variables
    String path = "/rsLoginHelper/attemptLogin".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Normal way to attempt login
  ///
  ///ReqAttemptLogin reqAttemptLogin :
  ///     account: >              (RsPeerId)Id of the account to which attempt login          password: >              (string)Password for the given account  
  /// 
  Future<ResAttemptLogin> rsLoginHelperAttemptLogin({ ReqAttemptLogin reqAttemptLogin }) async {
    Response response = await rsLoginHelperAttemptLoginWithHttpInfo( reqAttemptLogin: reqAttemptLogin );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAttemptLogin') as ResAttemptLogin;
    } else {
      return null;
    }
  }

  /// Feed extra entropy to the crypto libraries with HTTP info returned
  ///
  /// 
  Future<Response> rsLoginHelperCollectEntropyWithHttpInfo({ ReqCollectEntropy reqCollectEntropy }) async {
    Object postBody = reqCollectEntropy;

    // verify required params are set

    // create path and map variables
    String path = "/rsLoginHelper/collectEntropy".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Feed extra entropy to the crypto libraries
  ///
  ///ReqCollectEntropy reqCollectEntropy :
  ///     bytes: >              (integer)number to feed to the entropy pool  
  /// 
  Future<ResCollectEntropy> rsLoginHelperCollectEntropy({ ReqCollectEntropy reqCollectEntropy }) async {
    Response response = await rsLoginHelperCollectEntropyWithHttpInfo( reqCollectEntropy: reqCollectEntropy );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCollectEntropy') as ResCollectEntropy;
    } else {
      return null;
    }
  }

  /// Creates a new RetroShare location, and log in once is created with HTTP info returned
  ///
  /// 
  Future<Response> rsLoginHelperCreateLocationWithHttpInfo({ ReqCreateLocation reqCreateLocation }) async {
    Object postBody = reqCreateLocation;

    // verify required params are set

    // create path and map variables
    String path = "/rsLoginHelper/createLocation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Creates a new RetroShare location, and log in once is created
  ///
  ///ReqCreateLocation reqCreateLocation :
  ///     location: >              (RsLoginHelper_Location)provide input information to generate the location and storage to output the data of the generated location          password: >              (string)to protect and unlock the associated PGP key          makeHidden: >              (boolean)pass true to create an hidden location. UNTESTED!          makeAutoTor: >              (boolean)pass true to create an automatically configured Tor hidden location. UNTESTED!  
  /// 
  Future<ResCreateLocation> rsLoginHelperCreateLocation({ ReqCreateLocation reqCreateLocation }) async {
    Response response = await rsLoginHelperCreateLocationWithHttpInfo( reqCreateLocation: reqCreateLocation );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateLocation') as ResCreateLocation;
    } else {
      return null;
    }
  }

  /// Get locations and associated information with HTTP info returned
  ///
  /// 
  Future<Response> rsLoginHelperGetLocationsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsLoginHelper/getLocations".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get locations and associated information
  ///
  /// 
  Future<ResGetLocations> rsLoginHelperGetLocations() async {
    Response response = await rsLoginHelperGetLocationsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetLocations') as ResGetLocations;
    } else {
      return null;
    }
  }

  /// Check if RetroShare is already logged in, this usually return true after a successfull with HTTP info returned
  ///
  /// 
  Future<Response> rsLoginHelperIsLoggedInWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsLoginHelper/isLoggedIn".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = [];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if RetroShare is already logged in, this usually return true after a successfull
  ///
  /// 
  Future<ResIsLoggedIn> rsLoginHelperIsLoggedIn() async {
    Response response = await rsLoginHelperIsLoggedInWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsLoggedIn') as ResIsLoggedIn;
    } else {
      return null;
    }
  }

  /// acceptLobbyInvite accept a chat invite with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsAcceptLobbyInviteWithHttpInfo({ ReqAcceptLobbyInvite reqAcceptLobbyInvite }) async {
    Object postBody = reqAcceptLobbyInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/acceptLobbyInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// acceptLobbyInvite accept a chat invite
  ///
  ///ReqAcceptLobbyInvite reqAcceptLobbyInvite :
  ///     id: >              (ChatLobbyId)chat lobby id you were invited into and you want to join          identity: >              (RsGxsId)chat identity to use  
  /// 
  Future<ResAcceptLobbyInvite> rsMsgsAcceptLobbyInvite({ ReqAcceptLobbyInvite reqAcceptLobbyInvite }) async {
    Response response = await rsMsgsAcceptLobbyInviteWithHttpInfo( reqAcceptLobbyInvite: reqAcceptLobbyInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAcceptLobbyInvite') as ResAcceptLobbyInvite;
    } else {
      return null;
    }
  }

  /// clearChatLobby clear a chat lobby with HTTP info returned
  ///
  /// 
  Future rsMsgsClearChatLobbyWithHttpInfo({ ReqClearChatLobby reqClearChatLobby }) async {
    Object postBody = reqClearChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/clearChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// clearChatLobby clear a chat lobby
  ///
  ///ReqClearChatLobby reqClearChatLobby :
  ///     id: >              (ChatId)chat lobby id to clear  
  /// 
  Future rsMsgsClearChatLobby({ ReqClearChatLobby reqClearChatLobby }) async {
    Response response = await rsMsgsClearChatLobbyWithHttpInfo( reqClearChatLobby: reqClearChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// closeDistantChatConnexion with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsCloseDistantChatConnexionWithHttpInfo({ ReqCloseDistantChatConnexion reqCloseDistantChatConnexion }) async {
    Object postBody = reqCloseDistantChatConnexion;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/closeDistantChatConnexion".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// closeDistantChatConnexion
  ///
  ///ReqCloseDistantChatConnexion reqCloseDistantChatConnexion :
  ///     pid: >              (DistantChatPeerId)distant chat id to close the connection  
  /// 
  Future<ResCloseDistantChatConnexion> rsMsgsCloseDistantChatConnexion({ ReqCloseDistantChatConnexion reqCloseDistantChatConnexion }) async {
    Response response = await rsMsgsCloseDistantChatConnexionWithHttpInfo( reqCloseDistantChatConnexion: reqCloseDistantChatConnexion );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCloseDistantChatConnexion') as ResCloseDistantChatConnexion;
    } else {
      return null;
    }
  }

  /// createChatLobby create a new chat lobby with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsCreateChatLobbyWithHttpInfo({ ReqCreateChatLobby reqCreateChatLobby }) async {
    Object postBody = reqCreateChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/createChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// createChatLobby create a new chat lobby
  ///
  ///ReqCreateChatLobby reqCreateChatLobby :
  ///     lobby_name: >              (string)lobby name          lobby_identity: >              (RsGxsId)chat id to use for new lobby          lobby_topic: >              (string)lobby toppic          invited_friends: >              (set<RsPeerId>)list of friends to invite          lobby_privacy_type: >              (ChatLobbyFlags)flag for new chat lobby  
  /// 
  Future<ResCreateChatLobby> rsMsgsCreateChatLobby({ ReqCreateChatLobby reqCreateChatLobby }) async {
    Response response = await rsMsgsCreateChatLobbyWithHttpInfo( reqCreateChatLobby: reqCreateChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResCreateChatLobby') as ResCreateChatLobby;
    } else {
      return null;
    }
  }

  /// denyLobbyInvite deny a chat lobby invite with HTTP info returned
  ///
  /// 
  Future rsMsgsDenyLobbyInviteWithHttpInfo({ ReqDenyLobbyInvite reqDenyLobbyInvite }) async {
    Object postBody = reqDenyLobbyInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/denyLobbyInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// denyLobbyInvite deny a chat lobby invite
  ///
  ///ReqDenyLobbyInvite reqDenyLobbyInvite :
  ///     id: >              (ChatLobbyId)chat lobby id you were invited into  
  /// 
  Future rsMsgsDenyLobbyInvite({ ReqDenyLobbyInvite reqDenyLobbyInvite }) async {
    Response response = await rsMsgsDenyLobbyInviteWithHttpInfo( reqDenyLobbyInvite: reqDenyLobbyInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid. with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetChatLobbyInfoWithHttpInfo({ ReqGetChatLobbyInfo reqGetChatLobbyInfo }) async {
    Object postBody = reqGetChatLobbyInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getChatLobbyInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getChatLobbyInfo get lobby info of a subscribed chat lobby. Returns true if lobby id is valid.
  ///
  ///ReqGetChatLobbyInfo reqGetChatLobbyInfo :
  ///     id: >              (ChatLobbyId)id to get infos from  
  /// 
  Future<ResGetChatLobbyInfo> rsMsgsGetChatLobbyInfo({ ReqGetChatLobbyInfo reqGetChatLobbyInfo }) async {
    Response response = await rsMsgsGetChatLobbyInfoWithHttpInfo( reqGetChatLobbyInfo: reqGetChatLobbyInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChatLobbyInfo') as ResGetChatLobbyInfo;
    } else {
      return null;
    }
  }

  /// getChatLobbyList get ids of subscribed lobbies with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetChatLobbyListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getChatLobbyList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getChatLobbyList get ids of subscribed lobbies
  ///
  /// 
  Future<ResGetChatLobbyList> rsMsgsGetChatLobbyList() async {
    Response response = await rsMsgsGetChatLobbyListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetChatLobbyList') as ResGetChatLobbyList;
    } else {
      return null;
    }
  }

  /// getCustomStateString get the custom status message from a peer with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetCustomStateStringWithHttpInfo({ ReqGetCustomStateString reqGetCustomStateString }) async {
    Object postBody = reqGetCustomStateString;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getCustomStateString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getCustomStateString get the custom status message from a peer
  ///
  ///ReqGetCustomStateString reqGetCustomStateString :
  ///     peer_id: >              (RsPeerId)peer id to the peer you want to get the status message from  
  /// 
  Future<ResGetCustomStateString> rsMsgsGetCustomStateString({ ReqGetCustomStateString reqGetCustomStateString }) async {
    Response response = await rsMsgsGetCustomStateStringWithHttpInfo( reqGetCustomStateString: reqGetCustomStateString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetCustomStateString') as ResGetCustomStateString;
    } else {
      return null;
    }
  }

  /// getDefaultIdentityForChatLobby get the default identity used for chat lobbies with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetDefaultIdentityForChatLobbyWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getDefaultIdentityForChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getDefaultIdentityForChatLobby get the default identity used for chat lobbies
  ///
  /// 
  Future<ResGetDefaultIdentityForChatLobby> rsMsgsGetDefaultIdentityForChatLobby() async {
    Response response = await rsMsgsGetDefaultIdentityForChatLobbyWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDefaultIdentityForChatLobby') as ResGetDefaultIdentityForChatLobby;
    } else {
      return null;
    }
  }

  /// getDistantChatStatus receives distant chat info to a given distant chat id with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetDistantChatStatusWithHttpInfo({ ReqGetDistantChatStatus reqGetDistantChatStatus }) async {
    Object postBody = reqGetDistantChatStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getDistantChatStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getDistantChatStatus receives distant chat info to a given distant chat id
  ///
  ///ReqGetDistantChatStatus reqGetDistantChatStatus :
  ///     pid: >              (DistantChatPeerId)distant chat id  
  /// 
  Future<ResGetDistantChatStatus> rsMsgsGetDistantChatStatus({ ReqGetDistantChatStatus reqGetDistantChatStatus }) async {
    Response response = await rsMsgsGetDistantChatStatusWithHttpInfo( reqGetDistantChatStatus: reqGetDistantChatStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetDistantChatStatus') as ResGetDistantChatStatus;
    } else {
      return null;
    }
  }

  /// getIdentityForChatLobby with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetIdentityForChatLobbyWithHttpInfo({ ReqGetIdentityForChatLobby reqGetIdentityForChatLobby }) async {
    Object postBody = reqGetIdentityForChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getIdentityForChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getIdentityForChatLobby
  ///
  ///ReqGetIdentityForChatLobby reqGetIdentityForChatLobby :
  ///     lobby_id: >              (ChatLobbyId)lobby to get the chat id from  
  /// 
  Future<ResGetIdentityForChatLobby> rsMsgsGetIdentityForChatLobby({ ReqGetIdentityForChatLobby reqGetIdentityForChatLobby }) async {
    Response response = await rsMsgsGetIdentityForChatLobbyWithHttpInfo( reqGetIdentityForChatLobby: reqGetIdentityForChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetIdentityForChatLobby') as ResGetIdentityForChatLobby;
    } else {
      return null;
    }
  }

  /// getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetListOfNearbyChatLobbiesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getListOfNearbyChatLobbies".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getListOfNearbyChatLobbies get info about all lobbies, subscribed and unsubscribed
  ///
  /// 
  Future<ResGetListOfNearbyChatLobbies> rsMsgsGetListOfNearbyChatLobbies() async {
    Response response = await rsMsgsGetListOfNearbyChatLobbiesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetListOfNearbyChatLobbies') as ResGetListOfNearbyChatLobbies;
    } else {
      return null;
    }
  }

  /// getLobbyAutoSubscribe get current value of auto subscribe with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetLobbyAutoSubscribeWithHttpInfo({ ReqGetLobbyAutoSubscribe reqGetLobbyAutoSubscribe }) async {
    Object postBody = reqGetLobbyAutoSubscribe;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getLobbyAutoSubscribe".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getLobbyAutoSubscribe get current value of auto subscribe
  ///
  ///ReqGetLobbyAutoSubscribe reqGetLobbyAutoSubscribe :
  ///     lobby_id: >              (ChatLobbyId)lobby to get value from  
  /// 
  Future<ResGetLobbyAutoSubscribe> rsMsgsGetLobbyAutoSubscribe({ ReqGetLobbyAutoSubscribe reqGetLobbyAutoSubscribe }) async {
    Response response = await rsMsgsGetLobbyAutoSubscribeWithHttpInfo( reqGetLobbyAutoSubscribe: reqGetLobbyAutoSubscribe );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetLobbyAutoSubscribe') as ResGetLobbyAutoSubscribe;
    } else {
      return null;
    }
  }

  /// getMaxMessageSecuritySize get the maximum size of a chta message with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMaxMessageSecuritySizeWithHttpInfo({ ReqGetMaxMessageSecuritySize reqGetMaxMessageSecuritySize }) async {
    Object postBody = reqGetMaxMessageSecuritySize;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMaxMessageSecuritySize".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMaxMessageSecuritySize get the maximum size of a chta message
  ///
  ///ReqGetMaxMessageSecuritySize reqGetMaxMessageSecuritySize :
  ///     type: >              (integer)chat type  
  /// 
  Future<ResGetMaxMessageSecuritySize> rsMsgsGetMaxMessageSecuritySize({ ReqGetMaxMessageSecuritySize reqGetMaxMessageSecuritySize }) async {
    Response response = await rsMsgsGetMaxMessageSecuritySizeWithHttpInfo( reqGetMaxMessageSecuritySize: reqGetMaxMessageSecuritySize );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMaxMessageSecuritySize') as ResGetMaxMessageSecuritySize;
    } else {
      return null;
    }
  }

  /// getMessage with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMessageWithHttpInfo({ ReqGetMessage reqGetMessage }) async {
    Object postBody = reqGetMessage;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMessage".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMessage
  ///
  ///ReqGetMessage reqGetMessage :
  ///     msgId: >              (string)message ID to lookup  
  /// 
  Future<ResGetMessage> rsMsgsGetMessage({ ReqGetMessage reqGetMessage }) async {
    Response response = await rsMsgsGetMessageWithHttpInfo( reqGetMessage: reqGetMessage );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMessage') as ResGetMessage;
    } else {
      return null;
    }
  }

  /// getMessageCount with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMessageCountWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMessageCount".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMessageCount
  ///
  /// 
  Future<ResGetMessageCount> rsMsgsGetMessageCount() async {
    Response response = await rsMsgsGetMessageCountWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMessageCount') as ResGetMessageCount;
    } else {
      return null;
    }
  }

  /// getMessageSummaries with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMessageSummariesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMessageSummaries".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMessageSummaries
  ///
  /// 
  Future<ResGetMessageSummaries> rsMsgsGetMessageSummaries() async {
    Response response = await rsMsgsGetMessageSummariesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMessageSummaries') as ResGetMessageSummaries;
    } else {
      return null;
    }
  }

  /// getMessageTag with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMessageTagWithHttpInfo({ ReqGetMessageTag reqGetMessageTag }) async {
    Object postBody = reqGetMessageTag;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMessageTag".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMessageTag
  ///
  ///ReqGetMessageTag reqGetMessageTag :
  ///     msgId: >              (string)None 
  /// 
  Future<ResGetMessageTag> rsMsgsGetMessageTag({ ReqGetMessageTag reqGetMessageTag }) async {
    Response response = await rsMsgsGetMessageTagWithHttpInfo( reqGetMessageTag: reqGetMessageTag );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMessageTag') as ResGetMessageTag;
    } else {
      return null;
    }
  }

  /// getMessageTagTypes with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMessageTagTypesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMessageTagTypes".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMessageTagTypes
  ///
  /// 
  Future<ResGetMessageTagTypes> rsMsgsGetMessageTagTypes() async {
    Response response = await rsMsgsGetMessageTagTypesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMessageTagTypes') as ResGetMessageTagTypes;
    } else {
      return null;
    }
  }

  /// getMsgParentId with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetMsgParentIdWithHttpInfo({ ReqGetMsgParentId reqGetMsgParentId }) async {
    Object postBody = reqGetMsgParentId;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getMsgParentId".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getMsgParentId
  ///
  ///ReqGetMsgParentId reqGetMsgParentId :
  ///     msgId: >              (string)None 
  /// 
  Future<ResGetMsgParentId> rsMsgsGetMsgParentId({ ReqGetMsgParentId reqGetMsgParentId }) async {
    Response response = await rsMsgsGetMsgParentIdWithHttpInfo( reqGetMsgParentId: reqGetMsgParentId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetMsgParentId') as ResGetMsgParentId;
    } else {
      return null;
    }
  }

  /// getPendingChatLobbyInvites get a list of all pending chat lobby invites with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsGetPendingChatLobbyInvitesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/getPendingChatLobbyInvites".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getPendingChatLobbyInvites get a list of all pending chat lobby invites
  ///
  /// 
  Future<ResGetPendingChatLobbyInvites> rsMsgsGetPendingChatLobbyInvites() async {
    Response response = await rsMsgsGetPendingChatLobbyInvitesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPendingChatLobbyInvites') as ResGetPendingChatLobbyInvites;
    } else {
      return null;
    }
  }

  /// initiateDistantChatConnexion initiate a connexion for a distant chat with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsInitiateDistantChatConnexionWithHttpInfo({ ReqInitiateDistantChatConnexion reqInitiateDistantChatConnexion }) async {
    Object postBody = reqInitiateDistantChatConnexion;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/initiateDistantChatConnexion".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// initiateDistantChatConnexion initiate a connexion for a distant chat
  ///
  ///ReqInitiateDistantChatConnexion reqInitiateDistantChatConnexion :
  ///     to_pid: >              (RsGxsId)RsGxsId to start the connection          from_pid: >              (RsGxsId)owned RsGxsId who start the connection          notify: >              (boolean)notify remote that the connection is stablished  
  /// 
  Future<ResInitiateDistantChatConnexion> rsMsgsInitiateDistantChatConnexion({ ReqInitiateDistantChatConnexion reqInitiateDistantChatConnexion }) async {
    Response response = await rsMsgsInitiateDistantChatConnexionWithHttpInfo( reqInitiateDistantChatConnexion: reqInitiateDistantChatConnexion );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResInitiateDistantChatConnexion') as ResInitiateDistantChatConnexion;
    } else {
      return null;
    }
  }

  /// invitePeerToLobby invite a peer to join a lobby with HTTP info returned
  ///
  /// 
  Future rsMsgsInvitePeerToLobbyWithHttpInfo({ ReqInvitePeerToLobby reqInvitePeerToLobby }) async {
    Object postBody = reqInvitePeerToLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/invitePeerToLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// invitePeerToLobby invite a peer to join a lobby
  ///
  ///ReqInvitePeerToLobby reqInvitePeerToLobby :
  ///     lobby_id: >              (ChatLobbyId)lobby it to invite into          peer_id: >              (RsPeerId)peer to invite  
  /// 
  Future rsMsgsInvitePeerToLobby({ ReqInvitePeerToLobby reqInvitePeerToLobby }) async {
    Response response = await rsMsgsInvitePeerToLobbyWithHttpInfo( reqInvitePeerToLobby: reqInvitePeerToLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// joinVisibleChatLobby join a lobby that is visible with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsJoinVisibleChatLobbyWithHttpInfo({ ReqJoinVisibleChatLobby reqJoinVisibleChatLobby }) async {
    Object postBody = reqJoinVisibleChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/joinVisibleChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// joinVisibleChatLobby join a lobby that is visible
  ///
  ///ReqJoinVisibleChatLobby reqJoinVisibleChatLobby :
  ///     lobby_id: >              (ChatLobbyId)lobby to join to          own_id: >              (RsGxsId)chat id to use  
  /// 
  Future<ResJoinVisibleChatLobby> rsMsgsJoinVisibleChatLobby({ ReqJoinVisibleChatLobby reqJoinVisibleChatLobby }) async {
    Response response = await rsMsgsJoinVisibleChatLobbyWithHttpInfo( reqJoinVisibleChatLobby: reqJoinVisibleChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResJoinVisibleChatLobby') as ResJoinVisibleChatLobby;
    } else {
      return null;
    }
  }

  /// MessageDelete with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageDeleteWithHttpInfo({ ReqMessageDelete reqMessageDelete }) async {
    Object postBody = reqMessageDelete;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageDelete".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageDelete
  ///
  ///ReqMessageDelete reqMessageDelete :
  ///     msgId: >              (string)None 
  /// 
  Future<ResMessageDelete> rsMsgsMessageDelete({ ReqMessageDelete reqMessageDelete }) async {
    Response response = await rsMsgsMessageDeleteWithHttpInfo( reqMessageDelete: reqMessageDelete );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageDelete') as ResMessageDelete;
    } else {
      return null;
    }
  }

  /// MessageForwarded with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageForwardedWithHttpInfo({ ReqMessageForwarded reqMessageForwarded }) async {
    Object postBody = reqMessageForwarded;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageForwarded".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageForwarded
  ///
  ///ReqMessageForwarded reqMessageForwarded :
  ///     msgId: >              (string)None         forwarded: >              (boolean)None 
  /// 
  Future<ResMessageForwarded> rsMsgsMessageForwarded({ ReqMessageForwarded reqMessageForwarded }) async {
    Response response = await rsMsgsMessageForwardedWithHttpInfo( reqMessageForwarded: reqMessageForwarded );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageForwarded') as ResMessageForwarded;
    } else {
      return null;
    }
  }

  /// MessageJunk with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageJunkWithHttpInfo({ ReqMessageJunk reqMessageJunk }) async {
    Object postBody = reqMessageJunk;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageJunk".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageJunk
  ///
  ///ReqMessageJunk reqMessageJunk :
  ///     msgId: >              (string)None         mark: >              (boolean)None 
  /// 
  Future<ResMessageJunk> rsMsgsMessageJunk({ ReqMessageJunk reqMessageJunk }) async {
    Response response = await rsMsgsMessageJunkWithHttpInfo( reqMessageJunk: reqMessageJunk );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageJunk') as ResMessageJunk;
    } else {
      return null;
    }
  }

  /// MessageLoadEmbeddedImages with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageLoadEmbeddedImagesWithHttpInfo({ ReqMessageLoadEmbeddedImages reqMessageLoadEmbeddedImages }) async {
    Object postBody = reqMessageLoadEmbeddedImages;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageLoadEmbeddedImages".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageLoadEmbeddedImages
  ///
  ///ReqMessageLoadEmbeddedImages reqMessageLoadEmbeddedImages :
  ///     msgId: >              (string)None         load: >              (boolean)None 
  /// 
  Future<ResMessageLoadEmbeddedImages> rsMsgsMessageLoadEmbeddedImages({ ReqMessageLoadEmbeddedImages reqMessageLoadEmbeddedImages }) async {
    Response response = await rsMsgsMessageLoadEmbeddedImagesWithHttpInfo( reqMessageLoadEmbeddedImages: reqMessageLoadEmbeddedImages );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageLoadEmbeddedImages') as ResMessageLoadEmbeddedImages;
    } else {
      return null;
    }
  }

  /// MessageRead with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageReadWithHttpInfo({ ReqMessageRead reqMessageRead }) async {
    Object postBody = reqMessageRead;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageRead".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageRead
  ///
  ///ReqMessageRead reqMessageRead :
  ///     msgId: >              (string)None         unreadByUser: >              (boolean)None 
  /// 
  Future<ResMessageRead> rsMsgsMessageRead({ ReqMessageRead reqMessageRead }) async {
    Response response = await rsMsgsMessageReadWithHttpInfo( reqMessageRead: reqMessageRead );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageRead') as ResMessageRead;
    } else {
      return null;
    }
  }

  /// MessageReplied with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageRepliedWithHttpInfo({ ReqMessageReplied reqMessageReplied }) async {
    Object postBody = reqMessageReplied;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageReplied".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageReplied
  ///
  ///ReqMessageReplied reqMessageReplied :
  ///     msgId: >              (string)None         replied: >              (boolean)None 
  /// 
  Future<ResMessageReplied> rsMsgsMessageReplied({ ReqMessageReplied reqMessageReplied }) async {
    Response response = await rsMsgsMessageRepliedWithHttpInfo( reqMessageReplied: reqMessageReplied );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageReplied') as ResMessageReplied;
    } else {
      return null;
    }
  }

  /// MessageSend with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageSendWithHttpInfo({ ReqMessageSend reqMessageSend }) async {
    Object postBody = reqMessageSend;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageSend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageSend
  ///
  ///ReqMessageSend reqMessageSend :
  ///     info: >              (Rs_Msgs_MessageInfo)None 
  /// 
  Future<ResMessageSend> rsMsgsMessageSend({ ReqMessageSend reqMessageSend }) async {
    Response response = await rsMsgsMessageSendWithHttpInfo( reqMessageSend: reqMessageSend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageSend') as ResMessageSend;
    } else {
      return null;
    }
  }

  /// MessageStar with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageStarWithHttpInfo({ ReqMessageStar reqMessageStar }) async {
    Object postBody = reqMessageStar;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageStar".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageStar
  ///
  ///ReqMessageStar reqMessageStar :
  ///     msgId: >              (string)None         mark: >              (boolean)None 
  /// 
  Future<ResMessageStar> rsMsgsMessageStar({ ReqMessageStar reqMessageStar }) async {
    Response response = await rsMsgsMessageStarWithHttpInfo( reqMessageStar: reqMessageStar );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageStar') as ResMessageStar;
    } else {
      return null;
    }
  }

  /// MessageToDraft with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageToDraftWithHttpInfo({ ReqMessageToDraft reqMessageToDraft }) async {
    Object postBody = reqMessageToDraft;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageToDraft".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageToDraft
  ///
  ///ReqMessageToDraft reqMessageToDraft :
  ///     info: >              (Rs_Msgs_MessageInfo)None         msgParentId: >              (string)None 
  /// 
  Future<ResMessageToDraft> rsMsgsMessageToDraft({ ReqMessageToDraft reqMessageToDraft }) async {
    Response response = await rsMsgsMessageToDraftWithHttpInfo( reqMessageToDraft: reqMessageToDraft );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageToDraft') as ResMessageToDraft;
    } else {
      return null;
    }
  }

  /// MessageToTrash with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsMessageToTrashWithHttpInfo({ ReqMessageToTrash reqMessageToTrash }) async {
    Object postBody = reqMessageToTrash;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/MessageToTrash".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// MessageToTrash
  ///
  ///ReqMessageToTrash reqMessageToTrash :
  ///     msgId: >              (string)Id of the message to mode to trash box          bTrash: >              (boolean)Move to trash if true, otherwise remove from trash  
  /// 
  Future<ResMessageToTrash> rsMsgsMessageToTrash({ ReqMessageToTrash reqMessageToTrash }) async {
    Response response = await rsMsgsMessageToTrashWithHttpInfo( reqMessageToTrash: reqMessageToTrash );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResMessageToTrash') as ResMessageToTrash;
    } else {
      return null;
    }
  }

  /// removeMessageTagType with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsRemoveMessageTagTypeWithHttpInfo({ ReqRemoveMessageTagType reqRemoveMessageTagType }) async {
    Object postBody = reqRemoveMessageTagType;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/removeMessageTagType".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// removeMessageTagType
  ///
  ///ReqRemoveMessageTagType reqRemoveMessageTagType :
  ///     tagId: >              (integer)None 
  /// 
  Future<ResRemoveMessageTagType> rsMsgsRemoveMessageTagType({ ReqRemoveMessageTagType reqRemoveMessageTagType }) async {
    Response response = await rsMsgsRemoveMessageTagTypeWithHttpInfo( reqRemoveMessageTagType: reqRemoveMessageTagType );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRemoveMessageTagType') as ResRemoveMessageTagType;
    } else {
      return null;
    }
  }

  /// resetMessageStandardTagTypes with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsResetMessageStandardTagTypesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/resetMessageStandardTagTypes".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// resetMessageStandardTagTypes
  ///
  /// 
  Future<ResResetMessageStandardTagTypes> rsMsgsResetMessageStandardTagTypes() async {
    Response response = await rsMsgsResetMessageStandardTagTypesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResResetMessageStandardTagTypes') as ResResetMessageStandardTagTypes;
    } else {
      return null;
    }
  }

  /// sendChat send a chat message to a given id with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSendChatWithHttpInfo({ ReqSendChat reqSendChat }) async {
    Object postBody = reqSendChat;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/sendChat".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// sendChat send a chat message to a given id
  ///
  ///ReqSendChat reqSendChat :
  ///     id: >              (ChatId)id to send the message          msg: >              (string)message to send  
  /// 
  Future<ResSendChat> rsMsgsSendChat({ ReqSendChat reqSendChat }) async {
    Response response = await rsMsgsSendChatWithHttpInfo( reqSendChat: reqSendChat );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSendChat') as ResSendChat;
    } else {
      return null;
    }
  }

  /// sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby with HTTP info returned
  ///
  /// 
  Future rsMsgsSendLobbyStatusPeerLeavingWithHttpInfo({ ReqSendLobbyStatusPeerLeaving reqSendLobbyStatusPeerLeaving }) async {
    Object postBody = reqSendLobbyStatusPeerLeaving;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/sendLobbyStatusPeerLeaving".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// sendLobbyStatusPeerLeaving notify friend nodes that we&#39;re leaving a subscribed lobby
  ///
  ///ReqSendLobbyStatusPeerLeaving reqSendLobbyStatusPeerLeaving :
  ///     lobby_id: >              (ChatLobbyId)lobby to leave  
  /// 
  Future rsMsgsSendLobbyStatusPeerLeaving({ ReqSendLobbyStatusPeerLeaving reqSendLobbyStatusPeerLeaving }) async {
    Response response = await rsMsgsSendLobbyStatusPeerLeavingWithHttpInfo( reqSendLobbyStatusPeerLeaving: reqSendLobbyStatusPeerLeaving );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// sendMail with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSendMailWithHttpInfo({ ReqSendMail reqSendMail }) async {
    Object postBody = reqSendMail;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/sendMail".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// sendMail
  ///
  ///ReqSendMail reqSendMail :
  ///     from: >              (RsGxsId)GXS id of the author          subject: >              (string)Mail subject          mailBody: >              (string)Mail body          to: >              (set<RsGxsId>)list of To: recipients          cc: >              (set<RsGxsId>)list of CC: recipients          bcc: >              (set<RsGxsId>)list of BCC: recipients          attachments: >              (vector<FileInfo>)list of suggested files  
  /// 
  Future<ResSendMail> rsMsgsSendMail({ ReqSendMail reqSendMail }) async {
    Response response = await rsMsgsSendMailWithHttpInfo( reqSendMail: reqSendMail );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSendMail') as ResSendMail;
    } else {
      return null;
    }
  }

  /// sendStatusString send a status string with HTTP info returned
  ///
  /// 
  Future rsMsgsSendStatusStringWithHttpInfo({ ReqSendStatusString reqSendStatusString }) async {
    Object postBody = reqSendStatusString;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/sendStatusString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// sendStatusString send a status string
  ///
  ///ReqSendStatusString reqSendStatusString :
  ///     id: >              (ChatId)chat id to send the status string to          status_string: >              (string)status string  
  /// 
  Future rsMsgsSendStatusString({ ReqSendStatusString reqSendStatusString }) async {
    Response response = await rsMsgsSendStatusStringWithHttpInfo( reqSendStatusString: reqSendStatusString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// setCustomStateString set your custom status message with HTTP info returned
  ///
  /// 
  Future rsMsgsSetCustomStateStringWithHttpInfo({ ReqSetCustomStateString reqSetCustomStateString }) async {
    Object postBody = reqSetCustomStateString;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setCustomStateString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setCustomStateString set your custom status message
  ///
  ///ReqSetCustomStateString reqSetCustomStateString :
  ///     status_string: >              (string)status message  
  /// 
  Future rsMsgsSetCustomStateString({ ReqSetCustomStateString reqSetCustomStateString }) async {
    Response response = await rsMsgsSetCustomStateStringWithHttpInfo( reqSetCustomStateString: reqSetCustomStateString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// setDefaultIdentityForChatLobby set the default identity used for chat lobbies with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSetDefaultIdentityForChatLobbyWithHttpInfo({ ReqSetDefaultIdentityForChatLobby reqSetDefaultIdentityForChatLobby }) async {
    Object postBody = reqSetDefaultIdentityForChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setDefaultIdentityForChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setDefaultIdentityForChatLobby set the default identity used for chat lobbies
  ///
  ///ReqSetDefaultIdentityForChatLobby reqSetDefaultIdentityForChatLobby :
  ///     nick: >              (RsGxsId)chat identitiy to use  
  /// 
  Future<ResSetDefaultIdentityForChatLobby> rsMsgsSetDefaultIdentityForChatLobby({ ReqSetDefaultIdentityForChatLobby reqSetDefaultIdentityForChatLobby }) async {
    Response response = await rsMsgsSetDefaultIdentityForChatLobbyWithHttpInfo( reqSetDefaultIdentityForChatLobby: reqSetDefaultIdentityForChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetDefaultIdentityForChatLobby') as ResSetDefaultIdentityForChatLobby;
    } else {
      return null;
    }
  }

  /// setIdentityForChatLobby set the chat identit with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSetIdentityForChatLobbyWithHttpInfo({ ReqSetIdentityForChatLobby reqSetIdentityForChatLobby }) async {
    Object postBody = reqSetIdentityForChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setIdentityForChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setIdentityForChatLobby set the chat identit
  ///
  ///ReqSetIdentityForChatLobby reqSetIdentityForChatLobby :
  ///     lobby_id: >              (ChatLobbyId)lobby to change the chat idnetity for          nick: >              (RsGxsId)new chat identity  
  /// 
  Future<ResSetIdentityForChatLobby> rsMsgsSetIdentityForChatLobby({ ReqSetIdentityForChatLobby reqSetIdentityForChatLobby }) async {
    Response response = await rsMsgsSetIdentityForChatLobbyWithHttpInfo( reqSetIdentityForChatLobby: reqSetIdentityForChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetIdentityForChatLobby') as ResSetIdentityForChatLobby;
    } else {
      return null;
    }
  }

  /// setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby with HTTP info returned
  ///
  /// 
  Future rsMsgsSetLobbyAutoSubscribeWithHttpInfo({ ReqSetLobbyAutoSubscribe reqSetLobbyAutoSubscribe }) async {
    Object postBody = reqSetLobbyAutoSubscribe;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setLobbyAutoSubscribe".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setLobbyAutoSubscribe enable or disable auto subscribe for a chat lobby
  ///
  ///ReqSetLobbyAutoSubscribe reqSetLobbyAutoSubscribe :
  ///     lobby_id: >              (ChatLobbyId)lobby to auto (un)subscribe          autoSubscribe: >              (boolean)set value for auto subscribe  
  /// 
  Future rsMsgsSetLobbyAutoSubscribe({ ReqSetLobbyAutoSubscribe reqSetLobbyAutoSubscribe }) async {
    Response response = await rsMsgsSetLobbyAutoSubscribeWithHttpInfo( reqSetLobbyAutoSubscribe: reqSetLobbyAutoSubscribe );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0 with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSetMessageTagWithHttpInfo({ ReqSetMessageTag reqSetMessageTag }) async {
    Object postBody = reqSetMessageTag;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setMessageTag".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setMessageTag set &#x3D;&#x3D; false &amp;&amp; tagId &#x3D;&#x3D; 0
  ///
  ///ReqSetMessageTag reqSetMessageTag :
  ///     msgId: >              (string)None         tagId: >              (integer)None         set: >              (boolean)None 
  /// 
  Future<ResSetMessageTag> rsMsgsSetMessageTag({ ReqSetMessageTag reqSetMessageTag }) async {
    Response response = await rsMsgsSetMessageTagWithHttpInfo( reqSetMessageTag: reqSetMessageTag );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetMessageTag') as ResSetMessageTag;
    } else {
      return null;
    }
  }

  /// setMessageTagType with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSetMessageTagTypeWithHttpInfo({ ReqSetMessageTagType reqSetMessageTagType }) async {
    Object postBody = reqSetMessageTagType;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/setMessageTagType".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// setMessageTagType
  ///
  ///ReqSetMessageTagType reqSetMessageTagType :
  ///     tagId: >              (integer)None         text: >              (string)None         rgb_color: >              (integer)None 
  /// 
  Future<ResSetMessageTagType> rsMsgsSetMessageTagType({ ReqSetMessageTagType reqSetMessageTagType }) async {
    Response response = await rsMsgsSetMessageTagTypeWithHttpInfo( reqSetMessageTagType: reqSetMessageTagType );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetMessageTagType') as ResSetMessageTagType;
    } else {
      return null;
    }
  }

  /// SystemMessage with HTTP info returned
  ///
  /// 
  Future<Response> rsMsgsSystemMessageWithHttpInfo({ ReqSystemMessage reqSystemMessage }) async {
    Object postBody = reqSystemMessage;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/SystemMessage".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// SystemMessage
  ///
  ///ReqSystemMessage reqSystemMessage :
  ///     title: >              (string)None         message: >              (string)None         systemFlag: >              (integer)None 
  /// 
  Future<ResSystemMessage> rsMsgsSystemMessage({ ReqSystemMessage reqSystemMessage }) async {
    Response response = await rsMsgsSystemMessageWithHttpInfo( reqSystemMessage: reqSystemMessage );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSystemMessage') as ResSystemMessage;
    } else {
      return null;
    }
  }

  /// unsubscribeChatLobby leave a chat lobby with HTTP info returned
  ///
  /// 
  Future rsMsgsUnsubscribeChatLobbyWithHttpInfo({ ReqUnsubscribeChatLobby reqUnsubscribeChatLobby }) async {
    Object postBody = reqUnsubscribeChatLobby;

    // verify required params are set

    // create path and map variables
    String path = "/rsMsgs/unsubscribeChatLobby".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// unsubscribeChatLobby leave a chat lobby
  ///
  ///ReqUnsubscribeChatLobby reqUnsubscribeChatLobby :
  ///     lobby_id: >              (ChatLobbyId)lobby to leave  
  /// 
  Future rsMsgsUnsubscribeChatLobby({ ReqUnsubscribeChatLobby reqUnsubscribeChatLobby }) async {
    Response response = await rsMsgsUnsubscribeChatLobbyWithHttpInfo( reqUnsubscribeChatLobby: reqUnsubscribeChatLobby );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Add trusted node from invite with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAcceptInviteWithHttpInfo({ ReqAcceptInvite reqAcceptInvite }) async {
    Object postBody = reqAcceptInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/acceptInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add trusted node from invite
  ///
  ///ReqAcceptInvite reqAcceptInvite :
  ///     invite: >              (string)invite string being it in cert or URL format          flags: >              (ServicePermissionFlags)service permissions flag  
  /// 
  Future<ResAcceptInvite> rsPeersAcceptInvite({ ReqAcceptInvite reqAcceptInvite }) async {
    Response response = await rsPeersAcceptInviteWithHttpInfo( reqAcceptInvite: reqAcceptInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAcceptInvite') as ResAcceptInvite;
    } else {
      return null;
    }
  }

  /// Add trusted node with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAddFriendWithHttpInfo({ ReqAddFriend reqAddFriend }) async {
    Object postBody = reqAddFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/addFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add trusted node
  ///
  ///ReqAddFriend reqAddFriend :
  ///     sslId: >              (RsPeerId)SSL id of the node to add          gpgId: >              (RsPgpId)PGP id of the node to add          flags: >              (ServicePermissionFlags)service permissions flag  
  /// 
  Future<ResAddFriend> rsPeersAddFriend({ ReqAddFriend reqAddFriend }) async {
    Response response = await rsPeersAddFriendWithHttpInfo( reqAddFriend: reqAddFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAddFriend') as ResAddFriend;
    } else {
      return null;
    }
  }

  /// addGroup create a new group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAddGroupWithHttpInfo({ ReqAddGroup reqAddGroup }) async {
    Object postBody = reqAddGroup;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/addGroup".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// addGroup create a new group
  ///
  ///ReqAddGroup reqAddGroup :
  ///     groupInfo: >              (RsGroupInfo)None 
  /// 
  Future<ResAddGroup> rsPeersAddGroup({ ReqAddGroup reqAddGroup }) async {
    Response response = await rsPeersAddGroupWithHttpInfo( reqAddGroup: reqAddGroup );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAddGroup') as ResAddGroup;
    } else {
      return null;
    }
  }

  /// Add URL locator for given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAddPeerLocatorWithHttpInfo({ ReqAddPeerLocator reqAddPeerLocator }) async {
    Object postBody = reqAddPeerLocator;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/addPeerLocator".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add URL locator for given peer
  ///
  ///ReqAddPeerLocator reqAddPeerLocator :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          locator: >              (RsUrl)peer url locator  
  /// 
  Future<ResAddPeerLocator> rsPeersAddPeerLocator({ ReqAddPeerLocator reqAddPeerLocator }) async {
    Response response = await rsPeersAddPeerLocatorWithHttpInfo( reqAddPeerLocator: reqAddPeerLocator );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAddPeerLocator') as ResAddPeerLocator;
    } else {
      return null;
    }
  }

  /// Add SSL-only trusted node When adding an SSL-only node, it is authorized to connect. Every time a connection is established the user is notified about the need to verify the PGP fingerprint, until she does, at that point the node become a full SSL+PGP friend. with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAddSslOnlyFriendWithHttpInfo({ ReqAddSslOnlyFriend reqAddSslOnlyFriend }) async {
    Object postBody = reqAddSslOnlyFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/addSslOnlyFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Add SSL-only trusted node When adding an SSL-only node, it is authorized to connect. Every time a connection is established the user is notified about the need to verify the PGP fingerprint, until she does, at that point the node become a full SSL+PGP friend.
  ///
  ///ReqAddSslOnlyFriend reqAddSslOnlyFriend :
  ///     sslId: >              (RsPeerId)SSL id of the node to add          pgpId: >              (RsPgpId)PGP id of the node to add. Will be used for validation when the key is available.          details: >              (RsPeerDetails)Optional extra details known about the node to add  
  /// 
  Future<ResAddSslOnlyFriend> rsPeersAddSslOnlyFriend({ ReqAddSslOnlyFriend reqAddSslOnlyFriend }) async {
    Response response = await rsPeersAddSslOnlyFriendWithHttpInfo( reqAddSslOnlyFriend: reqAddSslOnlyFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAddSslOnlyFriend') as ResAddSslOnlyFriend;
    } else {
      return null;
    }
  }

  /// assignPeerToGroup add a peer to a group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAssignPeerToGroupWithHttpInfo({ ReqAssignPeerToGroup reqAssignPeerToGroup }) async {
    Object postBody = reqAssignPeerToGroup;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/assignPeerToGroup".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// assignPeerToGroup add a peer to a group
  ///
  ///ReqAssignPeerToGroup reqAssignPeerToGroup :
  ///     groupId: >              (RsNodeGroupId)None         peerId: >              (RsPgpId)None         assign: >              (boolean)true to assign a peer, false to remove a peer  
  /// 
  Future<ResAssignPeerToGroup> rsPeersAssignPeerToGroup({ ReqAssignPeerToGroup reqAssignPeerToGroup }) async {
    Response response = await rsPeersAssignPeerToGroupWithHttpInfo( reqAssignPeerToGroup: reqAssignPeerToGroup );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAssignPeerToGroup') as ResAssignPeerToGroup;
    } else {
      return null;
    }
  }

  /// assignPeersToGroup add a list of peers to a group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersAssignPeersToGroupWithHttpInfo({ ReqAssignPeersToGroup reqAssignPeersToGroup }) async {
    Object postBody = reqAssignPeersToGroup;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/assignPeersToGroup".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// assignPeersToGroup add a list of peers to a group
  ///
  ///ReqAssignPeersToGroup reqAssignPeersToGroup :
  ///     groupId: >              (RsNodeGroupId)None         peerIds: >              (list<RsPgpId>)None         assign: >              (boolean)true to assign a peer, false to remove a peer  
  /// 
  Future<ResAssignPeersToGroup> rsPeersAssignPeersToGroup({ ReqAssignPeersToGroup reqAssignPeersToGroup }) async {
    Response response = await rsPeersAssignPeersToGroupWithHttpInfo( reqAssignPeersToGroup: reqAssignPeersToGroup );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAssignPeersToGroup') as ResAssignPeersToGroup;
    } else {
      return null;
    }
  }

  /// Trigger connection attempt to given node with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersConnectAttemptWithHttpInfo({ ReqConnectAttempt reqConnectAttempt }) async {
    Object postBody = reqConnectAttempt;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/connectAttempt".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Trigger connection attempt to given node
  ///
  ///ReqConnectAttempt reqConnectAttempt :
  ///     sslId: >              (RsPeerId)SSL id of the node to connect  
  /// 
  Future<ResConnectAttempt> rsPeersConnectAttempt({ ReqConnectAttempt reqConnectAttempt }) async {
    Response response = await rsPeersConnectAttemptWithHttpInfo( reqConnectAttempt: reqConnectAttempt );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResConnectAttempt') as ResConnectAttempt;
    } else {
      return null;
    }
  }

  /// editGroup edit an existing group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersEditGroupWithHttpInfo({ ReqEditGroup reqEditGroup }) async {
    Object postBody = reqEditGroup;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/editGroup".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// editGroup edit an existing group
  ///
  ///ReqEditGroup reqEditGroup :
  ///     groupId: >              (RsNodeGroupId)None         groupInfo: >              (RsGroupInfo)None 
  /// 
  Future<ResEditGroup> rsPeersEditGroup({ ReqEditGroup reqEditGroup }) async {
    Response response = await rsPeersEditGroupWithHttpInfo( reqEditGroup: reqEditGroup );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResEditGroup') as ResEditGroup;
    } else {
      return null;
    }
  }

  /// Get trusted peers list with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetFriendListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getFriendList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get trusted peers list
  ///
  /// 
  Future<ResGetFriendList> rsPeersGetFriendList() async {
    Response response = await rsPeersGetFriendListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetFriendList') as ResGetFriendList;
    } else {
      return null;
    }
  }

  /// Get PGP id for the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetGPGIdWithHttpInfo({ ReqGetGPGId reqGetGPGId }) async {
    Object postBody = reqGetGPGId;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getGPGId".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get PGP id for the given peer
  ///
  ///ReqGetGPGId reqGetGPGId :
  ///     sslId: >              (RsPeerId)SSL id of the peer  
  /// 
  Future<ResGetGPGId> rsPeersGetGPGId({ ReqGetGPGId reqGetGPGId }) async {
    Response response = await rsPeersGetGPGIdWithHttpInfo( reqGetGPGId: reqGetGPGId );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetGPGId') as ResGetGPGId;
    } else {
      return null;
    }
  }

  /// getGroupInfo get group information to one group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetGroupInfoWithHttpInfo({ ReqGetGroupInfo reqGetGroupInfo }) async {
    Object postBody = reqGetGroupInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getGroupInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getGroupInfo get group information to one group
  ///
  ///ReqGetGroupInfo reqGetGroupInfo :
  ///     groupId: >              (RsNodeGroupId)None 
  /// 
  Future<ResGetGroupInfo> rsPeersGetGroupInfo({ ReqGetGroupInfo reqGetGroupInfo }) async {
    Response response = await rsPeersGetGroupInfoWithHttpInfo( reqGetGroupInfo: reqGetGroupInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetGroupInfo') as ResGetGroupInfo;
    } else {
      return null;
    }
  }

  /// getGroupInfoByName get group information by group name with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetGroupInfoByNameWithHttpInfo({ ReqGetGroupInfoByName reqGetGroupInfoByName }) async {
    Object postBody = reqGetGroupInfoByName;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getGroupInfoByName".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getGroupInfoByName get group information by group name
  ///
  ///ReqGetGroupInfoByName reqGetGroupInfoByName :
  ///     groupName: >              (string)None 
  /// 
  Future<ResGetGroupInfoByName> rsPeersGetGroupInfoByName({ ReqGetGroupInfoByName reqGetGroupInfoByName }) async {
    Response response = await rsPeersGetGroupInfoByNameWithHttpInfo( reqGetGroupInfoByName: reqGetGroupInfoByName );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetGroupInfoByName') as ResGetGroupInfoByName;
    } else {
      return null;
    }
  }

  /// getGroupInfoList get list of all groups with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetGroupInfoListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getGroupInfoList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getGroupInfoList get list of all groups
  ///
  /// 
  Future<ResGetGroupInfoList> rsPeersGetGroupInfoList() async {
    Response response = await rsPeersGetGroupInfoListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetGroupInfoList') as ResGetGroupInfoList;
    } else {
      return null;
    }
  }

  /// Get connected peers list with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetOnlineListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getOnlineList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get connected peers list
  ///
  /// 
  Future<ResGetOnlineList> rsPeersGetOnlineList() async {
    Response response = await rsPeersGetOnlineListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOnlineList') as ResGetOnlineList;
    } else {
      return null;
    }
  }

  /// Get details details of the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetPeerDetailsWithHttpInfo({ ReqGetPeerDetails reqGetPeerDetails }) async {
    Object postBody = reqGetPeerDetails;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getPeerDetails".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get details details of the given peer
  ///
  ///ReqGetPeerDetails reqGetPeerDetails :
  ///     sslId: >              (RsPeerId)id of the peer  
  /// 
  Future<ResGetPeerDetails> rsPeersGetPeerDetails({ ReqGetPeerDetails reqGetPeerDetails }) async {
    Response response = await rsPeersGetPeerDetailsWithHttpInfo( reqGetPeerDetails: reqGetPeerDetails );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPeerDetails') as ResGetPeerDetails;
    } else {
      return null;
    }
  }

  /// Get peers count with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetPeersCountWithHttpInfo({ ReqGetPeersCount reqGetPeersCount }) async {
    Object postBody = reqGetPeersCount;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getPeersCount".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get peers count
  ///
  ///ReqGetPeersCount reqGetPeersCount :
  ///     countLocations: >              (boolean)true to count multiple locations of same owner  
  /// 
  Future<ResGetPeersCount> rsPeersGetPeersCount({ ReqGetPeersCount reqGetPeersCount }) async {
    Response response = await rsPeersGetPeersCountWithHttpInfo( reqGetPeersCount: reqGetPeersCount );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPeersCount') as ResGetPeersCount;
    } else {
      return null;
    }
  }

  /// Get trusted PGP ids list with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetPgpFriendListWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getPgpFriendList".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get trusted PGP ids list
  ///
  /// 
  Future<ResGetPgpFriendList> rsPeersGetPgpFriendList() async {
    Response response = await rsPeersGetPgpFriendListWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPgpFriendList') as ResGetPgpFriendList;
    } else {
      return null;
    }
  }

  /// Get RetroShare invite of the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetRetroshareInviteWithHttpInfo({ ReqGetRetroshareInvite reqGetRetroshareInvite }) async {
    Object postBody = reqGetRetroshareInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/GetRetroshareInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get RetroShare invite of the given peer
  ///
  ///ReqGetRetroshareInvite reqGetRetroshareInvite :
  ///     sslId: >              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          includeSignatures: >              (boolean)true to add key signatures to the invite          includeExtraLocators: >              (boolean)false to avoid to add extra locators  
  /// 
  Future<ResGetRetroshareInvite> rsPeersGetRetroshareInvite({ ReqGetRetroshareInvite reqGetRetroshareInvite }) async {
    Response response = await rsPeersGetRetroshareInviteWithHttpInfo( reqGetRetroshareInvite: reqGetRetroshareInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetRetroshareInvite') as ResGetRetroshareInvite;
    } else {
      return null;
    }
  }

  /// Get RetroShare short invite of the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersGetShortInviteWithHttpInfo({ ReqGetShortInvite reqGetShortInvite }) async {
    Object postBody = reqGetShortInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/getShortInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get RetroShare short invite of the given peer
  ///
  ///ReqGetShortInvite reqGetShortInvite :
  ///     sslId: >              (RsPeerId)Id of the peer of which we want to generate an invite, a null id (all 0) is passed, an invite for own node is returned.          formatRadix: >              (boolean)true to get in base64 format false to get URL.          bareBones: >              (boolean)true to get smallest invite, which miss also the information necessary to attempt an outgoing connection, but still enough to accept an incoming one.          baseUrl: >              (string)URL into which to sneak in the RetroShare invite radix, this is primarly useful to trick other applications into making the invite clickable, or to disguise the RetroShare invite into a \"normal\" looking web link. Used only if formatRadix is false.  
  /// 
  Future<ResGetShortInvite> rsPeersGetShortInvite({ ReqGetShortInvite reqGetShortInvite }) async {
    Response response = await rsPeersGetShortInviteWithHttpInfo( reqGetShortInvite: reqGetShortInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetShortInvite') as ResGetShortInvite;
    } else {
      return null;
    }
  }

  /// Check if given peer is a trusted node with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersIsFriendWithHttpInfo({ ReqIsFriend reqIsFriend }) async {
    Object postBody = reqIsFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/isFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if given peer is a trusted node
  ///
  ///ReqIsFriend reqIsFriend :
  ///     sslId: >              (RsPeerId)id of the peer to check  
  /// 
  Future<ResIsFriend> rsPeersIsFriend({ ReqIsFriend reqIsFriend }) async {
    Response response = await rsPeersIsFriendWithHttpInfo( reqIsFriend: reqIsFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsFriend') as ResIsFriend;
    } else {
      return null;
    }
  }

  /// Check if there is an established connection to the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersIsOnlineWithHttpInfo({ ReqIsOnline reqIsOnline }) async {
    Object postBody = reqIsOnline;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/isOnline".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if there is an established connection to the given peer
  ///
  ///ReqIsOnline reqIsOnline :
  ///     sslId: >              (RsPeerId)id of the peer to check  
  /// 
  Future<ResIsOnline> rsPeersIsOnline({ ReqIsOnline reqIsOnline }) async {
    Response response = await rsPeersIsOnlineWithHttpInfo( reqIsOnline: reqIsOnline );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsOnline') as ResIsOnline;
    } else {
      return null;
    }
  }

  /// Check if given PGP id is trusted with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersIsPgpFriendWithHttpInfo({ ReqIsPgpFriend reqIsPgpFriend }) async {
    Object postBody = reqIsPgpFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/isPgpFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if given PGP id is trusted
  ///
  ///ReqIsPgpFriend reqIsPgpFriend :
  ///     pgpId: >              (RsPgpId)PGP id to check  
  /// 
  Future<ResIsPgpFriend> rsPeersIsPgpFriend({ ReqIsPgpFriend reqIsPgpFriend }) async {
    Response response = await rsPeersIsPgpFriendWithHttpInfo( reqIsPgpFriend: reqIsPgpFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsPgpFriend') as ResIsPgpFriend;
    } else {
      return null;
    }
  }

  /// Check if given peer is a trusted SSL node pending PGP approval Peers added through short invite remain in this state as long as their PGP key is not received and verified/approved by the user. with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersIsSslOnlyFriendWithHttpInfo({ ReqIsSslOnlyFriend reqIsSslOnlyFriend }) async {
    Object postBody = reqIsSslOnlyFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/isSslOnlyFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if given peer is a trusted SSL node pending PGP approval Peers added through short invite remain in this state as long as their PGP key is not received and verified/approved by the user.
  ///
  ///ReqIsSslOnlyFriend reqIsSslOnlyFriend :
  ///     sslId: >              (RsPeerId)id of the peer to check  
  /// 
  Future<ResIsSslOnlyFriend> rsPeersIsSslOnlyFriend({ ReqIsSslOnlyFriend reqIsSslOnlyFriend }) async {
    Response response = await rsPeersIsSslOnlyFriendWithHttpInfo( reqIsSslOnlyFriend: reqIsSslOnlyFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsSslOnlyFriend') as ResIsSslOnlyFriend;
    } else {
      return null;
    }
  }

  /// Import certificate into the keyring with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersLoadCertificateFromStringWithHttpInfo({ ReqLoadCertificateFromString reqLoadCertificateFromString }) async {
    Object postBody = reqLoadCertificateFromString;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/loadCertificateFromString".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Import certificate into the keyring
  ///
  ///ReqLoadCertificateFromString reqLoadCertificateFromString :
  ///     cert: >              (string)string representation of the certificate  
  /// 
  Future<ResLoadCertificateFromString> rsPeersLoadCertificateFromString({ ReqLoadCertificateFromString reqLoadCertificateFromString }) async {
    Response response = await rsPeersLoadCertificateFromStringWithHttpInfo( reqLoadCertificateFromString: reqLoadCertificateFromString );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResLoadCertificateFromString') as ResLoadCertificateFromString;
    } else {
      return null;
    }
  }

  /// Examine certificate and get details without importing into the keyring with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersLoadDetailsFromStringCertWithHttpInfo({ ReqLoadDetailsFromStringCert reqLoadDetailsFromStringCert }) async {
    Object postBody = reqLoadDetailsFromStringCert;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/loadDetailsFromStringCert".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Examine certificate and get details without importing into the keyring
  ///
  ///ReqLoadDetailsFromStringCert reqLoadDetailsFromStringCert :
  ///     cert: >              (string)string representation of the certificate  
  /// 
  Future<ResLoadDetailsFromStringCert> rsPeersLoadDetailsFromStringCert({ ReqLoadDetailsFromStringCert reqLoadDetailsFromStringCert }) async {
    Response response = await rsPeersLoadDetailsFromStringCertWithHttpInfo( reqLoadDetailsFromStringCert: reqLoadDetailsFromStringCert );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResLoadDetailsFromStringCert') as ResLoadDetailsFromStringCert;
    } else {
      return null;
    }
  }

  /// Parse the give short invite to extract contained information with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersParseShortInviteWithHttpInfo({ ReqParseShortInvite reqParseShortInvite }) async {
    Object postBody = reqParseShortInvite;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/parseShortInvite".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Parse the give short invite to extract contained information
  ///
  ///ReqParseShortInvite reqParseShortInvite :
  ///     invite: >              (string)string containing the short invite to parse  
  /// 
  Future<ResParseShortInvite> rsPeersParseShortInvite({ ReqParseShortInvite reqParseShortInvite }) async {
    Response response = await rsPeersParseShortInviteWithHttpInfo( reqParseShortInvite: reqParseShortInvite );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResParseShortInvite') as ResParseShortInvite;
    } else {
      return null;
    }
  }

  /// Convert PGP fingerprint to PGP id with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersPgpIdFromFingerprintWithHttpInfo({ ReqPgpIdFromFingerprint reqPgpIdFromFingerprint }) async {
    Object postBody = reqPgpIdFromFingerprint;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/pgpIdFromFingerprint".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Convert PGP fingerprint to PGP id
  ///
  ///ReqPgpIdFromFingerprint reqPgpIdFromFingerprint :
  ///     fpr: >              (RsPgpFingerprint)PGP fingerprint to convert  
  /// 
  Future<ResPgpIdFromFingerprint> rsPeersPgpIdFromFingerprint({ ReqPgpIdFromFingerprint reqPgpIdFromFingerprint }) async {
    Response response = await rsPeersPgpIdFromFingerprintWithHttpInfo( reqPgpIdFromFingerprint: reqPgpIdFromFingerprint );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResPgpIdFromFingerprint') as ResPgpIdFromFingerprint;
    } else {
      return null;
    }
  }

  /// Revoke connection trust from to node with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersRemoveFriendWithHttpInfo({ ReqRemoveFriend reqRemoveFriend }) async {
    Object postBody = reqRemoveFriend;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/removeFriend".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Revoke connection trust from to node
  ///
  ///ReqRemoveFriend reqRemoveFriend :
  ///     pgpId: >              (RsPgpId)PGP id of the node  
  /// 
  Future<ResRemoveFriend> rsPeersRemoveFriend({ ReqRemoveFriend reqRemoveFriend }) async {
    Response response = await rsPeersRemoveFriendWithHttpInfo( reqRemoveFriend: reqRemoveFriend );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRemoveFriend') as ResRemoveFriend;
    } else {
      return null;
    }
  }

  /// Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersRemoveFriendLocationWithHttpInfo({ ReqRemoveFriendLocation reqRemoveFriendLocation }) async {
    Object postBody = reqRemoveFriendLocation;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/removeFriendLocation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Remove location of a trusted node, useful to prune old unused locations of a trusted peer without revoking trust
  ///
  ///ReqRemoveFriendLocation reqRemoveFriendLocation :
  ///     sslId: >              (RsPeerId)SSL id of the location to remove  
  /// 
  Future<ResRemoveFriendLocation> rsPeersRemoveFriendLocation({ ReqRemoveFriendLocation reqRemoveFriendLocation }) async {
    Response response = await rsPeersRemoveFriendLocationWithHttpInfo( reqRemoveFriendLocation: reqRemoveFriendLocation );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRemoveFriendLocation') as ResRemoveFriendLocation;
    } else {
      return null;
    }
  }

  /// removeGroup remove a group with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersRemoveGroupWithHttpInfo({ ReqRemoveGroup reqRemoveGroup }) async {
    Object postBody = reqRemoveGroup;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/removeGroup".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// removeGroup remove a group
  ///
  ///ReqRemoveGroup reqRemoveGroup :
  ///     groupId: >              (RsNodeGroupId)None 
  /// 
  Future<ResRemoveGroup> rsPeersRemoveGroup({ ReqRemoveGroup reqRemoveGroup }) async {
    Response response = await rsPeersRemoveGroupWithHttpInfo( reqRemoveGroup: reqRemoveGroup );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRemoveGroup') as ResRemoveGroup;
    } else {
      return null;
    }
  }

  /// Set (dynamical) domain name associated to the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersSetDynDNSWithHttpInfo({ ReqSetDynDNS reqSetDynDNS }) async {
    Object postBody = reqSetDynDNS;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/setDynDNS".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set (dynamical) domain name associated to the given peer
  ///
  ///ReqSetDynDNS reqSetDynDNS :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)domain name string representation  
  /// 
  Future<ResSetDynDNS> rsPeersSetDynDNS({ ReqSetDynDNS reqSetDynDNS }) async {
    Response response = await rsPeersSetDynDNSWithHttpInfo( reqSetDynDNS: reqSetDynDNS );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetDynDNS') as ResSetDynDNS;
    } else {
      return null;
    }
  }

  /// Set external IPv4 address for given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersSetExtAddressWithHttpInfo({ ReqSetExtAddress reqSetExtAddress }) async {
    Object postBody = reqSetExtAddress;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/setExtAddress".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set external IPv4 address for given peer
  ///
  ///ReqSetExtAddress reqSetExtAddress :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the external IPv4 address          port: >              (integer)external listening port  
  /// 
  Future<ResSetExtAddress> rsPeersSetExtAddress({ ReqSetExtAddress reqSetExtAddress }) async {
    Response response = await rsPeersSetExtAddressWithHttpInfo( reqSetExtAddress: reqSetExtAddress );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetExtAddress') as ResSetExtAddress;
    } else {
      return null;
    }
  }

  /// Set local IPv4 address for the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersSetLocalAddressWithHttpInfo({ ReqSetLocalAddress reqSetLocalAddress }) async {
    Object postBody = reqSetLocalAddress;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/setLocalAddress".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set local IPv4 address for the given peer
  ///
  ///ReqSetLocalAddress reqSetLocalAddress :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          addr: >              (string)string representation of the local IPv4 address          port: >              (integer)local listening port  
  /// 
  Future<ResSetLocalAddress> rsPeersSetLocalAddress({ ReqSetLocalAddress reqSetLocalAddress }) async {
    Response response = await rsPeersSetLocalAddressWithHttpInfo( reqSetLocalAddress: reqSetLocalAddress );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetLocalAddress') as ResSetLocalAddress;
    } else {
      return null;
    }
  }

  /// Set network mode of the given peer with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersSetNetworkModeWithHttpInfo({ ReqSetNetworkMode reqSetNetworkMode }) async {
    Object postBody = reqSetNetworkMode;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/setNetworkMode".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set network mode of the given peer
  ///
  ///ReqSetNetworkMode reqSetNetworkMode :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          netMode: >              (integer)one of RS_NETMODE_*  
  /// 
  Future<ResSetNetworkMode> rsPeersSetNetworkMode({ ReqSetNetworkMode reqSetNetworkMode }) async {
    Response response = await rsPeersSetNetworkModeWithHttpInfo( reqSetNetworkMode: reqSetNetworkMode );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetNetworkMode') as ResSetNetworkMode;
    } else {
      return null;
    }
  }

  /// set DHT and discovery modes with HTTP info returned
  ///
  /// 
  Future<Response> rsPeersSetVisStateWithHttpInfo({ ReqSetVisState reqSetVisState }) async {
    Object postBody = reqSetVisState;

    // verify required params are set

    // create path and map variables
    String path = "/rsPeers/setVisState".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// set DHT and discovery modes
  ///
  ///ReqSetVisState reqSetVisState :
  ///     sslId: >              (RsPeerId)SSL id of the peer, own id is accepted too          vsDisc: >              (integer)one of RS_VS_DISC_*          vsDht: >              (integer)one of RS_VS_DHT_*  
  /// 
  Future<ResSetVisState> rsPeersSetVisState({ ReqSetVisState reqSetVisState }) async {
    Response response = await rsPeersSetVisStateWithHttpInfo( reqSetVisState: reqSetVisState );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetVisState') as ResSetVisState;
    } else {
      return null;
    }
  }

  /// null with HTTP info returned
  ///
  /// 
  Future<Response> rsPostedRequestStatusWithHttpInfo({ ReqRequestStatus reqRequestStatus }) async {
    Object postBody = reqRequestStatus;

    // verify required params are set

    // create path and map variables
    String path = "/rsPosted/requestStatus".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// null
  ///
  ///ReqRequestStatus reqRequestStatus :
  ///     token: >              (integer)None 
  /// 
  Future<ResRequestStatus> rsPostedRequestStatus({ ReqRequestStatus reqRequestStatus }) async {
    Response response = await rsPostedRequestStatusWithHttpInfo( reqRequestStatus: reqRequestStatus );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRequestStatus') as ResRequestStatus;
    } else {
      return null;
    }
  }

  /// check if giving automatic positive opinion when flagging as contact is enbaled with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsAutoPositiveOpinionForContactsWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/autoPositiveOpinionForContacts".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// check if giving automatic positive opinion when flagging as contact is enbaled
  ///
  /// 
  Future<ResAutoPositiveOpinionForContacts> rsReputationsAutoPositiveOpinionForContacts() async {
    Response response = await rsReputationsAutoPositiveOpinionForContactsWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResAutoPositiveOpinionForContacts') as ResAutoPositiveOpinionForContacts;
    } else {
      return null;
    }
  }

  /// Enable automatic banning of all identities signed by the given node with HTTP info returned
  ///
  /// 
  Future rsReputationsBanNodeWithHttpInfo({ ReqBanNode reqBanNode }) async {
    Object postBody = reqBanNode;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/banNode".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Enable automatic banning of all identities signed by the given node
  ///
  ///ReqBanNode reqBanNode :
  ///     id: >              (RsPgpId)PGP id of the node          b: >              (boolean)true to enable, false to disable  
  /// 
  Future rsReputationsBanNode({ ReqBanNode reqBanNode }) async {
    Response response = await rsReputationsBanNodeWithHttpInfo( reqBanNode: reqBanNode );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get own opition about the given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsGetOwnOpinionWithHttpInfo({ ReqGetOwnOpinion reqGetOwnOpinion }) async {
    Object postBody = reqGetOwnOpinion;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/getOwnOpinion".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get own opition about the given identity
  ///
  ///ReqGetOwnOpinion reqGetOwnOpinion :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResGetOwnOpinion> rsReputationsGetOwnOpinion({ ReqGetOwnOpinion reqGetOwnOpinion }) async {
    Response response = await rsReputationsGetOwnOpinionWithHttpInfo( reqGetOwnOpinion: reqGetOwnOpinion );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOwnOpinion') as ResGetOwnOpinion;
    } else {
      return null;
    }
  }

  /// Get reputation data of given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsGetReputationInfoWithHttpInfo({ ReqGetReputationInfo reqGetReputationInfo }) async {
    Object postBody = reqGetReputationInfo;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/getReputationInfo".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get reputation data of given identity
  ///
  ///ReqGetReputationInfo reqGetReputationInfo :
  ///     id: >              (RsGxsId)Id of the identity          ownerNode: >              (RsPgpId)Optiona PGP id of the signed identity, accept a null (all zero/noninitialized) PGP id          stamp: >              (boolean)if true, timestamo the information  
  /// 
  Future<ResGetReputationInfo> rsReputationsGetReputationInfo({ ReqGetReputationInfo reqGetReputationInfo }) async {
    Response response = await rsReputationsGetReputationInfoWithHttpInfo( reqGetReputationInfo: reqGetReputationInfo );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetReputationInfo') as ResGetReputationInfo;
    } else {
      return null;
    }
  }

  /// This method allow fast checking if a GXS identity is banned. with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsIsIdentityBannedWithHttpInfo({ ReqIsIdentityBanned reqIsIdentityBanned }) async {
    Object postBody = reqIsIdentityBanned;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/isIdentityBanned".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// This method allow fast checking if a GXS identity is banned.
  ///
  ///ReqIsIdentityBanned reqIsIdentityBanned :
  ///     id: >              (RsGxsId)Id of the identity to check  
  /// 
  Future<ResIsIdentityBanned> rsReputationsIsIdentityBanned({ ReqIsIdentityBanned reqIsIdentityBanned }) async {
    Response response = await rsReputationsIsIdentityBannedWithHttpInfo( reqIsIdentityBanned: reqIsIdentityBanned );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsIdentityBanned') as ResIsIdentityBanned;
    } else {
      return null;
    }
  }

  /// Check if automatic banning of all identities signed by the given node is enabled with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsIsNodeBannedWithHttpInfo({ ReqIsNodeBanned reqIsNodeBanned }) async {
    Object postBody = reqIsNodeBanned;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/isNodeBanned".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Check if automatic banning of all identities signed by the given node is enabled
  ///
  ///ReqIsNodeBanned reqIsNodeBanned :
  ///     id: >              (RsPgpId)PGP id of the node  
  /// 
  Future<ResIsNodeBanned> rsReputationsIsNodeBanned({ ReqIsNodeBanned reqIsNodeBanned }) async {
    Response response = await rsReputationsIsNodeBannedWithHttpInfo( reqIsNodeBanned: reqIsNodeBanned );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResIsNodeBanned') as ResIsNodeBanned;
    } else {
      return null;
    }
  }

  /// Get overall reputation level of given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsOverallReputationLevelWithHttpInfo({ ReqOverallReputationLevel reqOverallReputationLevel }) async {
    Object postBody = reqOverallReputationLevel;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/overallReputationLevel".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get overall reputation level of given identity
  ///
  ///ReqOverallReputationLevel reqOverallReputationLevel :
  ///     id: >              (RsGxsId)Id of the identity  
  /// 
  Future<ResOverallReputationLevel> rsReputationsOverallReputationLevel({ ReqOverallReputationLevel reqOverallReputationLevel }) async {
    Response response = await rsReputationsOverallReputationLevelWithHttpInfo( reqOverallReputationLevel: reqOverallReputationLevel );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResOverallReputationLevel') as ResOverallReputationLevel;
    } else {
      return null;
    }
  }

  /// Get number of days to wait before deleting a banned identity from local storage with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsRememberBannedIdThresholdWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/rememberBannedIdThreshold".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get number of days to wait before deleting a banned identity from local storage
  ///
  /// 
  Future<ResRememberBannedIdThreshold> rsReputationsRememberBannedIdThreshold() async {
    Response response = await rsReputationsRememberBannedIdThresholdWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResRememberBannedIdThreshold') as ResRememberBannedIdThreshold;
    } else {
      return null;
    }
  }

  /// Enable giving automatic positive opinion when flagging as contact with HTTP info returned
  ///
  /// 
  Future rsReputationsSetAutoPositiveOpinionForContactsWithHttpInfo({ ReqSetAutoPositiveOpinionForContacts reqSetAutoPositiveOpinionForContacts }) async {
    Object postBody = reqSetAutoPositiveOpinionForContacts;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/setAutoPositiveOpinionForContacts".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Enable giving automatic positive opinion when flagging as contact
  ///
  ///ReqSetAutoPositiveOpinionForContacts reqSetAutoPositiveOpinionForContacts :
  ///     b: >              (boolean)true to enable, false to disable  
  /// 
  Future rsReputationsSetAutoPositiveOpinionForContacts({ ReqSetAutoPositiveOpinionForContacts reqSetAutoPositiveOpinionForContacts }) async {
    Response response = await rsReputationsSetAutoPositiveOpinionForContactsWithHttpInfo( reqSetAutoPositiveOpinionForContacts: reqSetAutoPositiveOpinionForContacts );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set own opinion about the given identity with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsSetOwnOpinionWithHttpInfo({ ReqSetOwnOpinion reqSetOwnOpinion }) async {
    Object postBody = reqSetOwnOpinion;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/setOwnOpinion".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set own opinion about the given identity
  ///
  ///ReqSetOwnOpinion reqSetOwnOpinion :
  ///     id: >              (RsGxsId)Id of the identity          op: >              (RsOpinion)Own opinion  
  /// 
  Future<ResSetOwnOpinion> rsReputationsSetOwnOpinion({ ReqSetOwnOpinion reqSetOwnOpinion }) async {
    Response response = await rsReputationsSetOwnOpinionWithHttpInfo( reqSetOwnOpinion: reqSetOwnOpinion );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResSetOwnOpinion') as ResSetOwnOpinion;
    } else {
      return null;
    }
  }

  /// Set number of days to wait before deleting a banned identity from local storage with HTTP info returned
  ///
  /// 
  Future rsReputationsSetRememberBannedIdThresholdWithHttpInfo({ ReqSetRememberBannedIdThreshold reqSetRememberBannedIdThreshold }) async {
    Object postBody = reqSetRememberBannedIdThreshold;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/setRememberBannedIdThreshold".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set number of days to wait before deleting a banned identity from local storage
  ///
  ///ReqSetRememberBannedIdThreshold reqSetRememberBannedIdThreshold :
  ///     days: >              (integer)number of days to wait, 0 means never delete  
  /// 
  Future rsReputationsSetRememberBannedIdThreshold({ ReqSetRememberBannedIdThreshold reqSetRememberBannedIdThreshold }) async {
    Response response = await rsReputationsSetRememberBannedIdThresholdWithHttpInfo( reqSetRememberBannedIdThreshold: reqSetRememberBannedIdThreshold );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set threshold on remote reputation to consider it remotely negative with HTTP info returned
  ///
  /// 
  Future rsReputationsSetThresholdForRemotelyNegativeReputationWithHttpInfo({ ReqSetThresholdForRemotelyNegativeReputation reqSetThresholdForRemotelyNegativeReputation }) async {
    Object postBody = reqSetThresholdForRemotelyNegativeReputation;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/setThresholdForRemotelyNegativeReputation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set threshold on remote reputation to consider it remotely negative
  ///
  ///ReqSetThresholdForRemotelyNegativeReputation reqSetThresholdForRemotelyNegativeReputation :
  ///     thresh: >              (integer)Threshold value  
  /// 
  Future rsReputationsSetThresholdForRemotelyNegativeReputation({ ReqSetThresholdForRemotelyNegativeReputation reqSetThresholdForRemotelyNegativeReputation }) async {
    Response response = await rsReputationsSetThresholdForRemotelyNegativeReputationWithHttpInfo( reqSetThresholdForRemotelyNegativeReputation: reqSetThresholdForRemotelyNegativeReputation );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Set threshold on remote reputation to consider it remotely positive with HTTP info returned
  ///
  /// 
  Future rsReputationsSetThresholdForRemotelyPositiveReputationWithHttpInfo({ ReqSetThresholdForRemotelyPositiveReputation reqSetThresholdForRemotelyPositiveReputation }) async {
    Object postBody = reqSetThresholdForRemotelyPositiveReputation;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/setThresholdForRemotelyPositiveReputation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Set threshold on remote reputation to consider it remotely positive
  ///
  ///ReqSetThresholdForRemotelyPositiveReputation reqSetThresholdForRemotelyPositiveReputation :
  ///     thresh: >              (integer)Threshold value  
  /// 
  Future rsReputationsSetThresholdForRemotelyPositiveReputation({ ReqSetThresholdForRemotelyPositiveReputation reqSetThresholdForRemotelyPositiveReputation }) async {
    Response response = await rsReputationsSetThresholdForRemotelyPositiveReputationWithHttpInfo( reqSetThresholdForRemotelyPositiveReputation: reqSetThresholdForRemotelyPositiveReputation );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
    } else {
      return;
    }
  }

  /// Get threshold on remote reputation to consider it remotely negative with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsThresholdForRemotelyNegativeReputationWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/thresholdForRemotelyNegativeReputation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get threshold on remote reputation to consider it remotely negative
  ///
  /// 
  Future<ResThresholdForRemotelyNegativeReputation> rsReputationsThresholdForRemotelyNegativeReputation() async {
    Response response = await rsReputationsThresholdForRemotelyNegativeReputationWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResThresholdForRemotelyNegativeReputation') as ResThresholdForRemotelyNegativeReputation;
    } else {
      return null;
    }
  }

  /// Get threshold on remote reputation to consider it remotely negative with HTTP info returned
  ///
  /// 
  Future<Response> rsReputationsThresholdForRemotelyPositiveReputationWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsReputations/thresholdForRemotelyPositiveReputation".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// Get threshold on remote reputation to consider it remotely negative
  ///
  /// 
  Future<ResThresholdForRemotelyPositiveReputation> rsReputationsThresholdForRemotelyPositiveReputation() async {
    Response response = await rsReputationsThresholdForRemotelyPositiveReputationWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResThresholdForRemotelyPositiveReputation') as ResThresholdForRemotelyPositiveReputation;
    } else {
      return null;
    }
  }

  /// get a map off all services. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetOwnServicesWithHttpInfo() async {
    Object postBody;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getOwnServices".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = [];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// get a map off all services.
  ///
  /// 
  Future<ResGetOwnServices> rsServiceControlGetOwnServices() async {
    Response response = await rsServiceControlGetOwnServicesWithHttpInfo();
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetOwnServices') as ResGetOwnServices;
    } else {
      return null;
    }
  }

  /// getPeersConnected return peers using a service. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetPeersConnectedWithHttpInfo({ ReqGetPeersConnected reqGetPeersConnected }) async {
    Object postBody = reqGetPeersConnected;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getPeersConnected".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getPeersConnected return peers using a service.
  ///
  ///ReqGetPeersConnected reqGetPeersConnected :
  ///     serviceId: >              (integer)service to look up.  
  /// 
  Future<ResGetPeersConnected> rsServiceControlGetPeersConnected({ ReqGetPeersConnected reqGetPeersConnected }) async {
    Response response = await rsServiceControlGetPeersConnectedWithHttpInfo( reqGetPeersConnected: reqGetPeersConnected );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetPeersConnected') as ResGetPeersConnected;
    } else {
      return null;
    }
  }

  /// getServiceItemNames return a map of service item names. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetServiceItemNamesWithHttpInfo({ ReqGetServiceItemNames reqGetServiceItemNames }) async {
    Object postBody = reqGetServiceItemNames;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getServiceItemNames".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getServiceItemNames return a map of service item names.
  ///
  ///ReqGetServiceItemNames reqGetServiceItemNames :
  ///     serviceId: >              (integer)service to look up  
  /// 
  Future<ResGetServiceItemNames> rsServiceControlGetServiceItemNames({ ReqGetServiceItemNames reqGetServiceItemNames }) async {
    Response response = await rsServiceControlGetServiceItemNamesWithHttpInfo( reqGetServiceItemNames: reqGetServiceItemNames );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetServiceItemNames') as ResGetServiceItemNames;
    } else {
      return null;
    }
  }

  /// getServiceName lookup the name of a service. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetServiceNameWithHttpInfo({ ReqGetServiceName reqGetServiceName }) async {
    Object postBody = reqGetServiceName;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getServiceName".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getServiceName lookup the name of a service.
  ///
  ///ReqGetServiceName reqGetServiceName :
  ///     serviceId: >              (integer)service to look up  
  /// 
  Future<ResGetServiceName> rsServiceControlGetServiceName({ ReqGetServiceName reqGetServiceName }) async {
    Response response = await rsServiceControlGetServiceNameWithHttpInfo( reqGetServiceName: reqGetServiceName );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetServiceName') as ResGetServiceName;
    } else {
      return null;
    }
  }

  /// getServicePermissions return permissions of one service. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetServicePermissionsWithHttpInfo({ ReqGetServicePermissions reqGetServicePermissions }) async {
    Object postBody = reqGetServicePermissions;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getServicePermissions".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getServicePermissions return permissions of one service.
  ///
  ///ReqGetServicePermissions reqGetServicePermissions :
  ///     serviceId: >              (integer)service id to look up  
  /// 
  Future<ResGetServicePermissions> rsServiceControlGetServicePermissions({ ReqGetServicePermissions reqGetServicePermissions }) async {
    Response response = await rsServiceControlGetServicePermissionsWithHttpInfo( reqGetServicePermissions: reqGetServicePermissions );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetServicePermissions') as ResGetServicePermissions;
    } else {
      return null;
    }
  }

  /// getServicesAllowed return a mpa with allowed service information. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetServicesAllowedWithHttpInfo({ ReqGetServicesAllowed reqGetServicesAllowed }) async {
    Object postBody = reqGetServicesAllowed;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getServicesAllowed".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getServicesAllowed return a mpa with allowed service information.
  ///
  ///ReqGetServicesAllowed reqGetServicesAllowed :
  ///     peerId: >              (RsPeerId)peer to look up  
  /// 
  Future<ResGetServicesAllowed> rsServiceControlGetServicesAllowed({ ReqGetServicesAllowed reqGetServicesAllowed }) async {
    Response response = await rsServiceControlGetServicesAllowedWithHttpInfo( reqGetServicesAllowed: reqGetServicesAllowed );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetServicesAllowed') as ResGetServicesAllowed;
    } else {
      return null;
    }
  }

  /// getServicesProvided return services provided by a peer. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlGetServicesProvidedWithHttpInfo({ ReqGetServicesProvided reqGetServicesProvided }) async {
    Object postBody = reqGetServicesProvided;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/getServicesProvided".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// getServicesProvided return services provided by a peer.
  ///
  ///ReqGetServicesProvided reqGetServicesProvided :
  ///     peerId: >              (RsPeerId)peer to look up  
  /// 
  Future<ResGetServicesProvided> rsServiceControlGetServicesProvided({ ReqGetServicesProvided reqGetServicesProvided }) async {
    Response response = await rsServiceControlGetServicesProvidedWithHttpInfo( reqGetServicesProvided: reqGetServicesProvided );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResGetServicesProvided') as ResGetServicesProvided;
    } else {
      return null;
    }
  }

  /// updateServicePermissions update service permissions of one service. with HTTP info returned
  ///
  /// 
  Future<Response> rsServiceControlUpdateServicePermissionsWithHttpInfo({ ReqUpdateServicePermissions reqUpdateServicePermissions }) async {
    Object postBody = reqUpdateServicePermissions;

    // verify required params are set

    // create path and map variables
    String path = "/rsServiceControl/updateServicePermissions".replaceAll("{format}","json");

    // query params
    List<QueryParam> queryParams = [];
    Map<String, String> headerParams = {};
    Map<String, String> formParams = {};

    List<String> contentTypes = ["application/json"];

    String nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    List<String> authNames = ["BasicAuth"];

    if(nullableContentType != null && nullableContentType.startsWith("multipart/form-data")) {
      bool hasFields = false;
      MultipartRequest mp = MultipartRequest(null, null);
      if(hasFields)
        postBody = mp;
    }
    else {
    }

    var response = await apiClient.invokeAPI(path,
                                             'POST',
                                             queryParams,
                                             postBody,
                                             headerParams,
                                             formParams,
                                             nullableContentType,
                                             authNames);
    return response;
  }

  /// updateServicePermissions update service permissions of one service.
  ///
  ///ReqUpdateServicePermissions reqUpdateServicePermissions :
  ///     serviceId: >              (integer)service to update          permissions: >              (RsServicePermissions)new permissions  
  /// 
  Future<ResUpdateServicePermissions> rsServiceControlUpdateServicePermissions({ ReqUpdateServicePermissions reqUpdateServicePermissions }) async {
    Response response = await rsServiceControlUpdateServicePermissionsWithHttpInfo( reqUpdateServicePermissions: reqUpdateServicePermissions );
    if(response.statusCode >= 400) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    } else if(response.body != null) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'ResUpdateServicePermissions') as ResUpdateServicePermissions;
    } else {
      return null;
    }
  }

}

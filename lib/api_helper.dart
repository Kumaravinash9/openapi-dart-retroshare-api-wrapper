part of openapi.api;

const _delimiters = const {'csv': ',', 'ssv': ' ', 'tsv': '\t', 'pipes': '|'};

// port from Java version
Iterable<QueryParam> _convertParametersForCollectionFormat(
  String collectionFormat, String name, dynamic value) {
  var params = <QueryParam>[];

  // preconditions
  if (name == null || name.isEmpty || value == null) return params;

  if (value is! List) {
    params.add(QueryParam(name, parameterToString(value)));
    return params;
  }

  List values = value as List;

  // get the collection format
  collectionFormat = (collectionFormat == null || collectionFormat.isEmpty)
                     ? "csv"
                     : collectionFormat; // default: csv

  if (collectionFormat == "multi") {
    return values.map((v) => QueryParam(name, parameterToString(v)));
  }

  String delimiter = _delimiters[collectionFormat] ?? ",";

  params.add(QueryParam(name, values.map((v) => parameterToString(v)).join(delimiter)));
  return params;
}

/// Format the given parameter object into string.
String parameterToString(dynamic value) {
  if (value == null) {
    return '';
  } else if (value is DateTime) {
    return value.toUtc().toIso8601String();
  } else if (value is ChatIdType) {
    return ChatIdTypeTypeTransformer().encode(value).toString();
  } else if (value is ChunkState) {
    return ChunkStateTypeTransformer().encode(value).toString();
  } else if (value is DwlSpeed) {
    return DwlSpeedTypeTransformer().encode(value).toString();
  } else if (value is FileChunksInfoChunkStrategy) {
    return FileChunksInfoChunkStrategyTypeTransformer().encode(value).toString();
  } else if (value is FileRequestFlags) {
    return FileRequestFlagsTypeTransformer().encode(value).toString();
  } else if (value is RsGxsCircleSubscriptionType) {
    return RsGxsCircleSubscriptionTypeTypeTransformer().encode(value).toString();
  } else if (value is RsGxsCircleType) {
    return RsGxsCircleTypeTypeTransformer().encode(value).toString();
  } else if (value is RsGxsVoteType) {
    return RsGxsVoteTypeTypeTransformer().encode(value).toString();
  } else if (value is RsInitLoadCertificateStatus) {
    return RsInitLoadCertificateStatusTypeTransformer().encode(value).toString();
  } else if (value is RsOpinion) {
    return RsOpinionTypeTransformer().encode(value).toString();
  } else if (value is RsReputationLevel) {
    return RsReputationLevelTypeTransformer().encode(value).toString();
  } else if (value is RsServiceType) {
    return RsServiceTypeTypeTransformer().encode(value).toString();
  } else if (value is RsTokenServiceGxsRequestStatus) {
    return RsTokenServiceGxsRequestStatusTypeTransformer().encode(value).toString();
  } else if (value is UsageCode) {
    return UsageCodeTypeTransformer().encode(value).toString();
  } else {
    return value.toString();
  }
}

/// Returns the decoded body by utf-8 if application/json with the given headers.
/// Else, returns the decoded body by default algorithm of dart:http.
/// Because avoid to text garbling when header only contains "application/json" without "; charset=utf-8".
String _decodeBodyBytes(Response response) {
  var contentType = response.headers['content-type'];
  if (contentType != null && contentType.contains("application/json")) {
    return utf8.decode(response.bodyBytes);
  } else {
    return response.body;
  }
}

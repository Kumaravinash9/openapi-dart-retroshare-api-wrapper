part of openapi.api;

class RsGroupInfo {
  
  String id = null;
  
  String name = null;
  
  int flag = null;
  
  List<String> peerIds = [];
  RsGroupInfo();

  @override
  String toString() {
    return 'RsGroupInfo[id=$id, name=$name, flag=$flag, peerIds=$peerIds, ]';
  }

  RsGroupInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    name = json['name'];
    flag = json['flag'];
    peerIds = (json['peerIds'] == null) ?
      null :
      (json['peerIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (name != null)
      json['name'] = name;
    if (flag != null)
      json['flag'] = flag;
    if (peerIds != null)
      json['peerIds'] = peerIds;
    return json;
  }

  static List<RsGroupInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGroupInfo>() : json.map((value) => RsGroupInfo.fromJson(value)).toList();
  }

  static Map<String, RsGroupInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGroupInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGroupInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGroupInfo-objects as value to a dart map
  static Map<String, List<RsGroupInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGroupInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGroupInfo.listFromJson(value);
       });
     }
     return map;
  }
}


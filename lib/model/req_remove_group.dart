part of openapi.api;

class ReqRemoveGroup {
  
  String groupId = null;
  ReqRemoveGroup();

  @override
  String toString() {
    return 'ReqRemoveGroup[groupId=$groupId, ]';
  }

  ReqRemoveGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupId = json['groupId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupId != null)
      json['groupId'] = groupId;
    return json;
  }

  static List<ReqRemoveGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRemoveGroup>() : json.map((value) => ReqRemoveGroup.fromJson(value)).toList();
  }

  static Map<String, ReqRemoveGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRemoveGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRemoveGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRemoveGroup-objects as value to a dart map
  static Map<String, List<ReqRemoveGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRemoveGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRemoveGroup.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetContentSummaries {
  
  String channelId = null;
  ReqGetContentSummaries();

  @override
  String toString() {
    return 'ReqGetContentSummaries[channelId=$channelId, ]';
  }

  ReqGetContentSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    return json;
  }

  static List<ReqGetContentSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetContentSummaries>() : json.map((value) => ReqGetContentSummaries.fromJson(value)).toList();
  }

  static Map<String, ReqGetContentSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetContentSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetContentSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetContentSummaries-objects as value to a dart map
  static Map<String, List<ReqGetContentSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetContentSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetContentSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


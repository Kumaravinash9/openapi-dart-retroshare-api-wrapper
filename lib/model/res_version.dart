part of openapi.api;

class ResVersion {
  
  int major = null;
  
  int minor = null;
  
  int mini = null;
  
  String extra = null;
  
  String human = null;
  ResVersion();

  @override
  String toString() {
    return 'ResVersion[major=$major, minor=$minor, mini=$mini, extra=$extra, human=$human, ]';
  }

  ResVersion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    major = json['major'];
    minor = json['minor'];
    mini = json['mini'];
    extra = json['extra'];
    human = json['human'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (major != null)
      json['major'] = major;
    if (minor != null)
      json['minor'] = minor;
    if (mini != null)
      json['mini'] = mini;
    if (extra != null)
      json['extra'] = extra;
    if (human != null)
      json['human'] = human;
    return json;
  }

  static List<ResVersion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResVersion>() : json.map((value) => ResVersion.fromJson(value)).toList();
  }

  static Map<String, ResVersion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResVersion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResVersion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResVersion-objects as value to a dart map
  static Map<String, List<ResVersion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResVersion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResVersion.listFromJson(value);
       });
     }
     return map;
  }
}


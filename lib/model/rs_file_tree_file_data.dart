part of openapi.api;

class RsFileTreeFileData {
  
  String name = null;
  
  ReqBanFileFileSize size = null;
  
  String hash = null;
  RsFileTreeFileData();

  @override
  String toString() {
    return 'RsFileTreeFileData[name=$name, size=$size, hash=$hash, ]';
  }

  RsFileTreeFileData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    size = (json['size'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['size']);
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (size != null)
      json['size'] = size;
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<RsFileTreeFileData> listFromJson(List<dynamic> json) {
    return json == null ? List<RsFileTreeFileData>() : json.map((value) => RsFileTreeFileData.fromJson(value)).toList();
  }

  static Map<String, RsFileTreeFileData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsFileTreeFileData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsFileTreeFileData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsFileTreeFileData-objects as value to a dart map
  static Map<String, List<RsFileTreeFileData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsFileTreeFileData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsFileTreeFileData.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetCirclesSummaries {
  
  bool retval = null;
  
  List<RsGroupMetaData> circles = [];
  ResGetCirclesSummaries();

  @override
  String toString() {
    return 'ResGetCirclesSummaries[retval=$retval, circles=$circles, ]';
  }

  ResGetCirclesSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    circles = (json['circles'] == null) ?
      null :
      RsGroupMetaData.listFromJson(json['circles']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (circles != null)
      json['circles'] = circles;
    return json;
  }

  static List<ResGetCirclesSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCirclesSummaries>() : json.map((value) => ResGetCirclesSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetCirclesSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCirclesSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCirclesSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCirclesSummaries-objects as value to a dart map
  static Map<String, List<ResGetCirclesSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCirclesSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCirclesSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetCircleDetails {
  
  bool retval = null;
  
  RsGxsCircleDetails details = null;
  ResGetCircleDetails();

  @override
  String toString() {
    return 'ResGetCircleDetails[retval=$retval, details=$details, ]';
  }

  ResGetCircleDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    details = (json['details'] == null) ?
      null :
      RsGxsCircleDetails.fromJson(json['details']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (details != null)
      json['details'] = details;
    return json;
  }

  static List<ResGetCircleDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCircleDetails>() : json.map((value) => ResGetCircleDetails.fromJson(value)).toList();
  }

  static Map<String, ResGetCircleDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCircleDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCircleDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCircleDetails-objects as value to a dart map
  static Map<String, List<ResGetCircleDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCircleDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCircleDetails.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetFriendList {
  
  bool retval = null;
  
  List<String> sslIds = [];
  ResGetFriendList();

  @override
  String toString() {
    return 'ResGetFriendList[retval=$retval, sslIds=$sslIds, ]';
  }

  ResGetFriendList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    sslIds = (json['sslIds'] == null) ?
      null :
      (json['sslIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (sslIds != null)
      json['sslIds'] = sslIds;
    return json;
  }

  static List<ResGetFriendList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetFriendList>() : json.map((value) => ResGetFriendList.fromJson(value)).toList();
  }

  static Map<String, ResGetFriendList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetFriendList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetFriendList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetFriendList-objects as value to a dart map
  static Map<String, List<ResGetFriendList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetFriendList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetFriendList.listFromJson(value);
       });
     }
     return map;
  }
}


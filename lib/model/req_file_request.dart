part of openapi.api;

class ReqFileRequest {
  
  String fileName = null;
  
  String hash = null;
  
  ReqBanFileFileSize size = null;
  
  String destPath = null;
  
  int flags = null;
  
  List<String> srcIds = [];
  ReqFileRequest();

  @override
  String toString() {
    return 'ReqFileRequest[fileName=$fileName, hash=$hash, size=$size, destPath=$destPath, flags=$flags, srcIds=$srcIds, ]';
  }

  ReqFileRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fileName = json['fileName'];
    hash = json['hash'];
    size = (json['size'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['size']);
    destPath = json['destPath'];
    flags = json['flags'];
    srcIds = (json['srcIds'] == null) ?
      null :
      (json['srcIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (fileName != null)
      json['fileName'] = fileName;
    if (hash != null)
      json['hash'] = hash;
    if (size != null)
      json['size'] = size;
    if (destPath != null)
      json['destPath'] = destPath;
    if (flags != null)
      json['flags'] = flags;
    if (srcIds != null)
      json['srcIds'] = srcIds;
    return json;
  }

  static List<ReqFileRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileRequest>() : json.map((value) => ReqFileRequest.fromJson(value)).toList();
  }

  static Map<String, ReqFileRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileRequest-objects as value to a dart map
  static Map<String, List<ReqFileRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileRequest.listFromJson(value);
       });
     }
     return map;
  }
}


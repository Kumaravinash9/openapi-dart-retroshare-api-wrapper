part of openapi.api;

class ReqGetServiceItemNames {
  
  int serviceId = null;
  ReqGetServiceItemNames();

  @override
  String toString() {
    return 'ReqGetServiceItemNames[serviceId=$serviceId, ]';
  }

  ReqGetServiceItemNames.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    serviceId = json['serviceId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (serviceId != null)
      json['serviceId'] = serviceId;
    return json;
  }

  static List<ReqGetServiceItemNames> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetServiceItemNames>() : json.map((value) => ReqGetServiceItemNames.fromJson(value)).toList();
  }

  static Map<String, ReqGetServiceItemNames> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetServiceItemNames>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetServiceItemNames.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetServiceItemNames-objects as value to a dart map
  static Map<String, List<ReqGetServiceItemNames>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetServiceItemNames>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetServiceItemNames.listFromJson(value);
       });
     }
     return map;
  }
}


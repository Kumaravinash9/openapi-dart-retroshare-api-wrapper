part of openapi.api;

class ResMessageForwarded {
  
  bool retval = null;
  ResMessageForwarded();

  @override
  String toString() {
    return 'ResMessageForwarded[retval=$retval, ]';
  }

  ResMessageForwarded.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageForwarded> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageForwarded>() : json.map((value) => ResMessageForwarded.fromJson(value)).toList();
  }

  static Map<String, ResMessageForwarded> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageForwarded>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageForwarded.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageForwarded-objects as value to a dart map
  static Map<String, List<ResMessageForwarded>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageForwarded>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageForwarded.listFromJson(value);
       });
     }
     return map;
  }
}


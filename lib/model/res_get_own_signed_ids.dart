part of openapi.api;

class ResGetOwnSignedIds {
  
  bool retval = null;
  
  List<String> ids = [];
  ResGetOwnSignedIds();

  @override
  String toString() {
    return 'ResGetOwnSignedIds[retval=$retval, ids=$ids, ]';
  }

  ResGetOwnSignedIds.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    ids = (json['ids'] == null) ?
      null :
      (json['ids'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (ids != null)
      json['ids'] = ids;
    return json;
  }

  static List<ResGetOwnSignedIds> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOwnSignedIds>() : json.map((value) => ResGetOwnSignedIds.fromJson(value)).toList();
  }

  static Map<String, ResGetOwnSignedIds> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOwnSignedIds>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOwnSignedIds.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOwnSignedIds-objects as value to a dart map
  static Map<String, List<ResGetOwnSignedIds>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOwnSignedIds>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOwnSignedIds.listFromJson(value);
       });
     }
     return map;
  }
}


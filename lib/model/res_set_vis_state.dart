part of openapi.api;

class ResSetVisState {
  
  bool retval = null;
  ResSetVisState();

  @override
  String toString() {
    return 'ResSetVisState[retval=$retval, ]';
  }

  ResSetVisState.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetVisState> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetVisState>() : json.map((value) => ResSetVisState.fromJson(value)).toList();
  }

  static Map<String, ResSetVisState> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetVisState>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetVisState.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetVisState-objects as value to a dart map
  static Map<String, List<ResSetVisState>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetVisState>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetVisState.listFromJson(value);
       });
     }
     return map;
  }
}


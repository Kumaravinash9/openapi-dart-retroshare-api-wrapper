part of openapi.api;

class RstimeT {
  
  num xint64 = null;
  
  String xstr64 = null;
  RstimeT();

  @override
  String toString() {
    return 'RstimeT[xint64=$xint64, xstr64=$xstr64, ]';
  }

  RstimeT.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    xint64 = json['xint64'];
    xstr64 = json['xstr64'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (xint64 != null)
      json['xint64'] = xint64;
    if (xstr64 != null)
      json['xstr64'] = xstr64;
    return json;
  }

  static List<RstimeT> listFromJson(List<dynamic> json) {
    return json == null ? List<RstimeT>() : json.map((value) => RstimeT.fromJson(value)).toList();
  }

  static Map<String, RstimeT> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RstimeT>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RstimeT.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RstimeT-objects as value to a dart map
  static Map<String, List<RstimeT>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RstimeT>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RstimeT.listFromJson(value);
       });
     }
     return map;
  }
}


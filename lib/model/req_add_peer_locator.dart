part of openapi.api;

class ReqAddPeerLocator {
  
  String sslId = null;
  
  RsUrl locator = null;
  ReqAddPeerLocator();

  @override
  String toString() {
    return 'ReqAddPeerLocator[sslId=$sslId, locator=$locator, ]';
  }

  ReqAddPeerLocator.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    locator = (json['locator'] == null) ?
      null :
      RsUrl.fromJson(json['locator']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (locator != null)
      json['locator'] = locator;
    return json;
  }

  static List<ReqAddPeerLocator> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAddPeerLocator>() : json.map((value) => ReqAddPeerLocator.fromJson(value)).toList();
  }

  static Map<String, ReqAddPeerLocator> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAddPeerLocator>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAddPeerLocator.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAddPeerLocator-objects as value to a dart map
  static Map<String, List<ReqAddPeerLocator>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAddPeerLocator>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAddPeerLocator.listFromJson(value);
       });
     }
     return map;
  }
}


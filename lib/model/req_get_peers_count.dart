part of openapi.api;

class ReqGetPeersCount {
  
  bool countLocations = null;
  ReqGetPeersCount();

  @override
  String toString() {
    return 'ReqGetPeersCount[countLocations=$countLocations, ]';
  }

  ReqGetPeersCount.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    countLocations = json['countLocations'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (countLocations != null)
      json['countLocations'] = countLocations;
    return json;
  }

  static List<ReqGetPeersCount> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetPeersCount>() : json.map((value) => ReqGetPeersCount.fromJson(value)).toList();
  }

  static Map<String, ReqGetPeersCount> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetPeersCount>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetPeersCount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetPeersCount-objects as value to a dart map
  static Map<String, List<ReqGetPeersCount>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetPeersCount>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetPeersCount.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetChannelAllContent {
  
  String channelId = null;
  ReqGetChannelAllContent();

  @override
  String toString() {
    return 'ReqGetChannelAllContent[channelId=$channelId, ]';
  }

  ReqGetChannelAllContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    return json;
  }

  static List<ReqGetChannelAllContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelAllContent>() : json.map((value) => ReqGetChannelAllContent.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelAllContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelAllContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelAllContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelAllContent-objects as value to a dart map
  static Map<String, List<ReqGetChannelAllContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelAllContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelAllContent.listFromJson(value);
       });
     }
     return map;
  }
}


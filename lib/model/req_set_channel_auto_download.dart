part of openapi.api;

class ReqSetChannelAutoDownload {
  
  String channelId = null;
  
  bool enable = null;
  ReqSetChannelAutoDownload();

  @override
  String toString() {
    return 'ReqSetChannelAutoDownload[channelId=$channelId, enable=$enable, ]';
  }

  ReqSetChannelAutoDownload.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    enable = json['enable'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (enable != null)
      json['enable'] = enable;
    return json;
  }

  static List<ReqSetChannelAutoDownload> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetChannelAutoDownload>() : json.map((value) => ReqSetChannelAutoDownload.fromJson(value)).toList();
  }

  static Map<String, ReqSetChannelAutoDownload> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetChannelAutoDownload>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetChannelAutoDownload.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetChannelAutoDownload-objects as value to a dart map
  static Map<String, List<ReqSetChannelAutoDownload>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetChannelAutoDownload>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetChannelAutoDownload.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class GxsGroupStatistic {
  
  String mGrpId = null;
  
  int mNumMsgs = null;
  
  int mTotalSizeOfMsgs = null;
  
  int mNumThreadMsgsNew = null;
  
  int mNumThreadMsgsUnread = null;
  
  int mNumChildMsgsNew = null;
  
  int mNumChildMsgsUnread = null;
  GxsGroupStatistic();

  @override
  String toString() {
    return 'GxsGroupStatistic[mGrpId=$mGrpId, mNumMsgs=$mNumMsgs, mTotalSizeOfMsgs=$mTotalSizeOfMsgs, mNumThreadMsgsNew=$mNumThreadMsgsNew, mNumThreadMsgsUnread=$mNumThreadMsgsUnread, mNumChildMsgsNew=$mNumChildMsgsNew, mNumChildMsgsUnread=$mNumChildMsgsUnread, ]';
  }

  GxsGroupStatistic.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mGrpId = json['mGrpId'];
    mNumMsgs = json['mNumMsgs'];
    mTotalSizeOfMsgs = json['mTotalSizeOfMsgs'];
    mNumThreadMsgsNew = json['mNumThreadMsgsNew'];
    mNumThreadMsgsUnread = json['mNumThreadMsgsUnread'];
    mNumChildMsgsNew = json['mNumChildMsgsNew'];
    mNumChildMsgsUnread = json['mNumChildMsgsUnread'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mGrpId != null)
      json['mGrpId'] = mGrpId;
    if (mNumMsgs != null)
      json['mNumMsgs'] = mNumMsgs;
    if (mTotalSizeOfMsgs != null)
      json['mTotalSizeOfMsgs'] = mTotalSizeOfMsgs;
    if (mNumThreadMsgsNew != null)
      json['mNumThreadMsgsNew'] = mNumThreadMsgsNew;
    if (mNumThreadMsgsUnread != null)
      json['mNumThreadMsgsUnread'] = mNumThreadMsgsUnread;
    if (mNumChildMsgsNew != null)
      json['mNumChildMsgsNew'] = mNumChildMsgsNew;
    if (mNumChildMsgsUnread != null)
      json['mNumChildMsgsUnread'] = mNumChildMsgsUnread;
    return json;
  }

  static List<GxsGroupStatistic> listFromJson(List<dynamic> json) {
    return json == null ? List<GxsGroupStatistic>() : json.map((value) => GxsGroupStatistic.fromJson(value)).toList();
  }

  static Map<String, GxsGroupStatistic> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, GxsGroupStatistic>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = GxsGroupStatistic.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GxsGroupStatistic-objects as value to a dart map
  static Map<String, List<GxsGroupStatistic>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<GxsGroupStatistic>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = GxsGroupStatistic.listFromJson(value);
       });
     }
     return map;
  }
}


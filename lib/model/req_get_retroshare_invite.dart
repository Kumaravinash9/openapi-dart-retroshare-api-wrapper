part of openapi.api;

class ReqGetRetroshareInvite {
  
  String sslId = null;
  
  bool includeSignatures = null;
  
  bool includeExtraLocators = null;
  ReqGetRetroshareInvite();

  @override
  String toString() {
    return 'ReqGetRetroshareInvite[sslId=$sslId, includeSignatures=$includeSignatures, includeExtraLocators=$includeExtraLocators, ]';
  }

  ReqGetRetroshareInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    includeSignatures = json['includeSignatures'];
    includeExtraLocators = json['includeExtraLocators'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (includeSignatures != null)
      json['includeSignatures'] = includeSignatures;
    if (includeExtraLocators != null)
      json['includeExtraLocators'] = includeExtraLocators;
    return json;
  }

  static List<ReqGetRetroshareInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetRetroshareInvite>() : json.map((value) => ReqGetRetroshareInvite.fromJson(value)).toList();
  }

  static Map<String, ReqGetRetroshareInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetRetroshareInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetRetroshareInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetRetroshareInvite-objects as value to a dart map
  static Map<String, List<ReqGetRetroshareInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetRetroshareInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetRetroshareInvite.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetMessageTag {
  
  bool retval = null;
  
  RsMsgsMsgTagInfo info = null;
  ResGetMessageTag();

  @override
  String toString() {
    return 'ResGetMessageTag[retval=$retval, info=$info, ]';
  }

  ResGetMessageTag.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      RsMsgsMsgTagInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetMessageTag> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMessageTag>() : json.map((value) => ResGetMessageTag.fromJson(value)).toList();
  }

  static Map<String, ResGetMessageTag> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMessageTag>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMessageTag.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMessageTag-objects as value to a dart map
  static Map<String, List<ResGetMessageTag>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMessageTag>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMessageTag.listFromJson(value);
       });
     }
     return map;
  }
}


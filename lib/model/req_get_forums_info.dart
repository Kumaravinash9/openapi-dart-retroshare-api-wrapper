part of openapi.api;

class ReqGetForumsInfo {
  
  List<String> forumIds = [];
  ReqGetForumsInfo();

  @override
  String toString() {
    return 'ReqGetForumsInfo[forumIds=$forumIds, ]';
  }

  ReqGetForumsInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumIds = (json['forumIds'] == null) ?
      null :
      (json['forumIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumIds != null)
      json['forumIds'] = forumIds;
    return json;
  }

  static List<ReqGetForumsInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetForumsInfo>() : json.map((value) => ReqGetForumsInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetForumsInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetForumsInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetForumsInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetForumsInfo-objects as value to a dart map
  static Map<String, List<ReqGetForumsInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetForumsInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetForumsInfo.listFromJson(value);
       });
     }
     return map;
  }
}


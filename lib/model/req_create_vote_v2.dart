part of openapi.api;

class ReqCreateVoteV2 {
  
  String channelId = null;
  
  String postId = null;
  
  String commentId = null;
  
  String authorId = null;
  
  RsGxsVoteType vote = null;
  //enum voteEnum {  0,  1,  2,  };{
  ReqCreateVoteV2();

  @override
  String toString() {
    return 'ReqCreateVoteV2[channelId=$channelId, postId=$postId, commentId=$commentId, authorId=$authorId, vote=$vote, ]';
  }

  ReqCreateVoteV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    postId = json['postId'];
    commentId = json['commentId'];
    authorId = json['authorId'];
    vote = (json['vote'] == null) ?
      null :
      RsGxsVoteType.fromJson(json['vote']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (postId != null)
      json['postId'] = postId;
    if (commentId != null)
      json['commentId'] = commentId;
    if (authorId != null)
      json['authorId'] = authorId;
    if (vote != null)
      json['vote'] = vote;
    return json;
  }

  static List<ReqCreateVoteV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateVoteV2>() : json.map((value) => ReqCreateVoteV2.fromJson(value)).toList();
  }

  static Map<String, ReqCreateVoteV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateVoteV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateVoteV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateVoteV2-objects as value to a dart map
  static Map<String, List<ReqCreateVoteV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateVoteV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateVoteV2.listFromJson(value);
       });
     }
     return map;
  }
}


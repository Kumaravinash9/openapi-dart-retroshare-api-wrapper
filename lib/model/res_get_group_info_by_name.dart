part of openapi.api;

class ResGetGroupInfoByName {
  
  bool retval = null;
  
  RsGroupInfo groupInfo = null;
  ResGetGroupInfoByName();

  @override
  String toString() {
    return 'ResGetGroupInfoByName[retval=$retval, groupInfo=$groupInfo, ]';
  }

  ResGetGroupInfoByName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    groupInfo = (json['groupInfo'] == null) ?
      null :
      RsGroupInfo.fromJson(json['groupInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (groupInfo != null)
      json['groupInfo'] = groupInfo;
    return json;
  }

  static List<ResGetGroupInfoByName> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetGroupInfoByName>() : json.map((value) => ResGetGroupInfoByName.fromJson(value)).toList();
  }

  static Map<String, ResGetGroupInfoByName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetGroupInfoByName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetGroupInfoByName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetGroupInfoByName-objects as value to a dart map
  static Map<String, List<ResGetGroupInfoByName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetGroupInfoByName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetGroupInfoByName.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSetAutoPositiveOpinionForContacts {
  
  bool b = null;
  ReqSetAutoPositiveOpinionForContacts();

  @override
  String toString() {
    return 'ReqSetAutoPositiveOpinionForContacts[b=$b, ]';
  }

  ReqSetAutoPositiveOpinionForContacts.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    b = json['b'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (b != null)
      json['b'] = b;
    return json;
  }

  static List<ReqSetAutoPositiveOpinionForContacts> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetAutoPositiveOpinionForContacts>() : json.map((value) => ReqSetAutoPositiveOpinionForContacts.fromJson(value)).toList();
  }

  static Map<String, ReqSetAutoPositiveOpinionForContacts> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetAutoPositiveOpinionForContacts>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetAutoPositiveOpinionForContacts.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetAutoPositiveOpinionForContacts-objects as value to a dart map
  static Map<String, List<ReqSetAutoPositiveOpinionForContacts>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetAutoPositiveOpinionForContacts>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetAutoPositiveOpinionForContacts.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetMsgParentId {
  
  String msgId = null;
  ReqGetMsgParentId();

  @override
  String toString() {
    return 'ReqGetMsgParentId[msgId=$msgId, ]';
  }

  ReqGetMsgParentId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    return json;
  }

  static List<ReqGetMsgParentId> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetMsgParentId>() : json.map((value) => ReqGetMsgParentId.fromJson(value)).toList();
  }

  static Map<String, ReqGetMsgParentId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetMsgParentId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetMsgParentId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetMsgParentId-objects as value to a dart map
  static Map<String, List<ReqGetMsgParentId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetMsgParentId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetMsgParentId.listFromJson(value);
       });
     }
     return map;
  }
}


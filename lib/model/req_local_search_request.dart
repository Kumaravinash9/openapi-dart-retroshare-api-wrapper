part of openapi.api;

class ReqLocalSearchRequest {
  
  String matchString = null;
  
  RstimeT maxWait = null;
  ReqLocalSearchRequest();

  @override
  String toString() {
    return 'ReqLocalSearchRequest[matchString=$matchString, maxWait=$maxWait, ]';
  }

  ReqLocalSearchRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    matchString = json['matchString'];
    maxWait = (json['maxWait'] == null) ?
      null :
      RstimeT.fromJson(json['maxWait']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (matchString != null)
      json['matchString'] = matchString;
    if (maxWait != null)
      json['maxWait'] = maxWait;
    return json;
  }

  static List<ReqLocalSearchRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqLocalSearchRequest>() : json.map((value) => ReqLocalSearchRequest.fromJson(value)).toList();
  }

  static Map<String, ReqLocalSearchRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqLocalSearchRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqLocalSearchRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqLocalSearchRequest-objects as value to a dart map
  static Map<String, List<ReqLocalSearchRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqLocalSearchRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqLocalSearchRequest.listFromJson(value);
       });
     }
     return map;
  }
}


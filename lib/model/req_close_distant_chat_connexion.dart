part of openapi.api;

class ReqCloseDistantChatConnexion {
  
  String pid = null;
  ReqCloseDistantChatConnexion();

  @override
  String toString() {
    return 'ReqCloseDistantChatConnexion[pid=$pid, ]';
  }

  ReqCloseDistantChatConnexion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pid = json['pid'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pid != null)
      json['pid'] = pid;
    return json;
  }

  static List<ReqCloseDistantChatConnexion> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCloseDistantChatConnexion>() : json.map((value) => ReqCloseDistantChatConnexion.fromJson(value)).toList();
  }

  static Map<String, ReqCloseDistantChatConnexion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCloseDistantChatConnexion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCloseDistantChatConnexion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCloseDistantChatConnexion-objects as value to a dart map
  static Map<String, List<ReqCloseDistantChatConnexion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCloseDistantChatConnexion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCloseDistantChatConnexion.listFromJson(value);
       });
     }
     return map;
  }
}


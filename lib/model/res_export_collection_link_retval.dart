part of openapi.api;

class ResExportCollectionLinkRetval {
  
  int errorNumber = null;
  
  String errorCategory = null;
  
  String errorMessage = null;
  ResExportCollectionLinkRetval();

  @override
  String toString() {
    return 'ResExportCollectionLinkRetval[errorNumber=$errorNumber, errorCategory=$errorCategory, errorMessage=$errorMessage, ]';
  }

  ResExportCollectionLinkRetval.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    errorNumber = json['errorNumber'];
    errorCategory = json['errorCategory'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (errorNumber != null)
      json['errorNumber'] = errorNumber;
    if (errorCategory != null)
      json['errorCategory'] = errorCategory;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResExportCollectionLinkRetval> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportCollectionLinkRetval>() : json.map((value) => ResExportCollectionLinkRetval.fromJson(value)).toList();
  }

  static Map<String, ResExportCollectionLinkRetval> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportCollectionLinkRetval>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportCollectionLinkRetval.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportCollectionLinkRetval-objects as value to a dart map
  static Map<String, List<ResExportCollectionLinkRetval>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportCollectionLinkRetval>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportCollectionLinkRetval.listFromJson(value);
       });
     }
     return map;
  }
}


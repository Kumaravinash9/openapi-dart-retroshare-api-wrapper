part of openapi.api;

class ResIsReady {
  
  bool retval = null;
  ResIsReady();

  @override
  String toString() {
    return 'ResIsReady[retval=$retval, ]';
  }

  ResIsReady.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsReady> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsReady>() : json.map((value) => ResIsReady.fromJson(value)).toList();
  }

  static Map<String, ResIsReady> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsReady>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsReady.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsReady-objects as value to a dart map
  static Map<String, List<ResIsReady>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsReady>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsReady.listFromJson(value);
       });
     }
     return map;
  }
}


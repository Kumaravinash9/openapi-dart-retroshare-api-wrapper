part of openapi.api;

class ResLoadDetailsFromStringCert {
  
  bool retval = null;
  
  RsPeerDetails certDetails = null;
  
  int errorCode = null;
  ResLoadDetailsFromStringCert();

  @override
  String toString() {
    return 'ResLoadDetailsFromStringCert[retval=$retval, certDetails=$certDetails, errorCode=$errorCode, ]';
  }

  ResLoadDetailsFromStringCert.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    certDetails = (json['certDetails'] == null) ?
      null :
      RsPeerDetails.fromJson(json['certDetails']);
    errorCode = json['errorCode'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (certDetails != null)
      json['certDetails'] = certDetails;
    if (errorCode != null)
      json['errorCode'] = errorCode;
    return json;
  }

  static List<ResLoadDetailsFromStringCert> listFromJson(List<dynamic> json) {
    return json == null ? List<ResLoadDetailsFromStringCert>() : json.map((value) => ResLoadDetailsFromStringCert.fromJson(value)).toList();
  }

  static Map<String, ResLoadDetailsFromStringCert> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResLoadDetailsFromStringCert>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResLoadDetailsFromStringCert.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResLoadDetailsFromStringCert-objects as value to a dart map
  static Map<String, List<ResLoadDetailsFromStringCert>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResLoadDetailsFromStringCert>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResLoadDetailsFromStringCert.listFromJson(value);
       });
     }
     return map;
  }
}


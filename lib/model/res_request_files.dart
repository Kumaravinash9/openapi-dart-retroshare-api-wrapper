part of openapi.api;

class ResRequestFiles {
  
  ResExportCollectionLinkRetval retval = null;
  ResRequestFiles();

  @override
  String toString() {
    return 'ResRequestFiles[retval=$retval, ]';
  }

  ResRequestFiles.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRequestFiles> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestFiles>() : json.map((value) => ResRequestFiles.fromJson(value)).toList();
  }

  static Map<String, ResRequestFiles> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestFiles>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestFiles.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestFiles-objects as value to a dart map
  static Map<String, List<ResRequestFiles>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestFiles>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestFiles.listFromJson(value);
       });
     }
     return map;
  }
}


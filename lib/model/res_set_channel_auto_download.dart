part of openapi.api;

class ResSetChannelAutoDownload {
  
  bool retval = null;
  ResSetChannelAutoDownload();

  @override
  String toString() {
    return 'ResSetChannelAutoDownload[retval=$retval, ]';
  }

  ResSetChannelAutoDownload.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetChannelAutoDownload> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetChannelAutoDownload>() : json.map((value) => ResSetChannelAutoDownload.fromJson(value)).toList();
  }

  static Map<String, ResSetChannelAutoDownload> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetChannelAutoDownload>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetChannelAutoDownload.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetChannelAutoDownload-objects as value to a dart map
  static Map<String, List<ResSetChannelAutoDownload>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetChannelAutoDownload>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetChannelAutoDownload.listFromJson(value);
       });
     }
     return map;
  }
}


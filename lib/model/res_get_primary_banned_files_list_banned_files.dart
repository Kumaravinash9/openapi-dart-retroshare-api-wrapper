part of openapi.api;

class ResGetPrimaryBannedFilesListBannedFiles {
  
  String key = null;
  
  BannedFileEntry value = null;
  ResGetPrimaryBannedFilesListBannedFiles();

  @override
  String toString() {
    return 'ResGetPrimaryBannedFilesListBannedFiles[key=$key, value=$value, ]';
  }

  ResGetPrimaryBannedFilesListBannedFiles.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      BannedFileEntry.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<ResGetPrimaryBannedFilesListBannedFiles> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPrimaryBannedFilesListBannedFiles>() : json.map((value) => ResGetPrimaryBannedFilesListBannedFiles.fromJson(value)).toList();
  }

  static Map<String, ResGetPrimaryBannedFilesListBannedFiles> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPrimaryBannedFilesListBannedFiles>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPrimaryBannedFilesListBannedFiles.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPrimaryBannedFilesListBannedFiles-objects as value to a dart map
  static Map<String, List<ResGetPrimaryBannedFilesListBannedFiles>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPrimaryBannedFilesListBannedFiles>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPrimaryBannedFilesListBannedFiles.listFromJson(value);
       });
     }
     return map;
  }
}


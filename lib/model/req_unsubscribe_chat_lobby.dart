part of openapi.api;

class ReqUnsubscribeChatLobby {
  
  ChatLobbyId lobbyId = null;
  ReqUnsubscribeChatLobby();

  @override
  String toString() {
    return 'ReqUnsubscribeChatLobby[lobbyId=$lobbyId, ]';
  }

  ReqUnsubscribeChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    return json;
  }

  static List<ReqUnsubscribeChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqUnsubscribeChatLobby>() : json.map((value) => ReqUnsubscribeChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqUnsubscribeChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqUnsubscribeChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqUnsubscribeChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqUnsubscribeChatLobby-objects as value to a dart map
  static Map<String, List<ReqUnsubscribeChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqUnsubscribeChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqUnsubscribeChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetChannelsSummaries {
  
  bool retval = null;
  
  List<RsGroupMetaData> channels = [];
  ResGetChannelsSummaries();

  @override
  String toString() {
    return 'ResGetChannelsSummaries[retval=$retval, channels=$channels, ]';
  }

  ResGetChannelsSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    channels = (json['channels'] == null) ?
      null :
      RsGroupMetaData.listFromJson(json['channels']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (channels != null)
      json['channels'] = channels;
    return json;
  }

  static List<ResGetChannelsSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelsSummaries>() : json.map((value) => ResGetChannelsSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelsSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelsSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelsSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelsSummaries-objects as value to a dart map
  static Map<String, List<ResGetChannelsSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelsSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelsSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResIsNodeBanned {
  
  bool retval = null;
  ResIsNodeBanned();

  @override
  String toString() {
    return 'ResIsNodeBanned[retval=$retval, ]';
  }

  ResIsNodeBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsNodeBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsNodeBanned>() : json.map((value) => ResIsNodeBanned.fromJson(value)).toList();
  }

  static Map<String, ResIsNodeBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsNodeBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsNodeBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsNodeBanned-objects as value to a dart map
  static Map<String, List<ResIsNodeBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsNodeBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsNodeBanned.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqCreateIdentity {
  
  String name = null;
  
  RsGxsImage avatar = null;
  
  bool pseudonimous = null;
  
  String pgpPassword = null;
  ReqCreateIdentity();

  @override
  String toString() {
    return 'ReqCreateIdentity[name=$name, avatar=$avatar, pseudonimous=$pseudonimous, pgpPassword=$pgpPassword, ]';
  }

  ReqCreateIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    avatar = (json['avatar'] == null) ?
      null :
      RsGxsImage.fromJson(json['avatar']);
    pseudonimous = json['pseudonimous'];
    pgpPassword = json['pgpPassword'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (avatar != null)
      json['avatar'] = avatar;
    if (pseudonimous != null)
      json['pseudonimous'] = pseudonimous;
    if (pgpPassword != null)
      json['pgpPassword'] = pgpPassword;
    return json;
  }

  static List<ReqCreateIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateIdentity>() : json.map((value) => ReqCreateIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqCreateIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateIdentity-objects as value to a dart map
  static Map<String, List<ReqCreateIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


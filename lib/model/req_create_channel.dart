part of openapi.api;

class ReqCreateChannel {
  
  RsGxsChannelGroup channel = null;
  ReqCreateChannel();

  @override
  String toString() {
    return 'ReqCreateChannel[channel=$channel, ]';
  }

  ReqCreateChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channel = (json['channel'] == null) ?
      null :
      RsGxsChannelGroup.fromJson(json['channel']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channel != null)
      json['channel'] = channel;
    return json;
  }

  static List<ReqCreateChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateChannel>() : json.map((value) => ReqCreateChannel.fromJson(value)).toList();
  }

  static Map<String, ReqCreateChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateChannel-objects as value to a dart map
  static Map<String, List<ReqCreateChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateChannel.listFromJson(value);
       });
     }
     return map;
  }
}


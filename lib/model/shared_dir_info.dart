part of openapi.api;

class SharedDirInfo {
  
  String filename = null;
  
  String virtualname = null;
  
  int shareflags = null;
  
  List<String> parentGroups = [];
  SharedDirInfo();

  @override
  String toString() {
    return 'SharedDirInfo[filename=$filename, virtualname=$virtualname, shareflags=$shareflags, parentGroups=$parentGroups, ]';
  }

  SharedDirInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    filename = json['filename'];
    virtualname = json['virtualname'];
    shareflags = json['shareflags'];
    parentGroups = (json['parent_groups'] == null) ?
      null :
      (json['parent_groups'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (filename != null)
      json['filename'] = filename;
    if (virtualname != null)
      json['virtualname'] = virtualname;
    if (shareflags != null)
      json['shareflags'] = shareflags;
    if (parentGroups != null)
      json['parent_groups'] = parentGroups;
    return json;
  }

  static List<SharedDirInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<SharedDirInfo>() : json.map((value) => SharedDirInfo.fromJson(value)).toList();
  }

  static Map<String, SharedDirInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, SharedDirInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = SharedDirInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of SharedDirInfo-objects as value to a dart map
  static Map<String, List<SharedDirInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<SharedDirInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = SharedDirInfo.listFromJson(value);
       });
     }
     return map;
  }
}


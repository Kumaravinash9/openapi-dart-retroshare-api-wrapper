part of openapi.api;

class ReqGetChannelContent {
  
  String channelId = null;
  
  List<String> contentsIds = [];
  ReqGetChannelContent();

  @override
  String toString() {
    return 'ReqGetChannelContent[channelId=$channelId, contentsIds=$contentsIds, ]';
  }

  ReqGetChannelContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    contentsIds = (json['contentsIds'] == null) ?
      null :
      (json['contentsIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (contentsIds != null)
      json['contentsIds'] = contentsIds;
    return json;
  }

  static List<ReqGetChannelContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetChannelContent>() : json.map((value) => ReqGetChannelContent.fromJson(value)).toList();
  }

  static Map<String, ReqGetChannelContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetChannelContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetChannelContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetChannelContent-objects as value to a dart map
  static Map<String, List<ReqGetChannelContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetChannelContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetChannelContent.listFromJson(value);
       });
     }
     return map;
  }
}


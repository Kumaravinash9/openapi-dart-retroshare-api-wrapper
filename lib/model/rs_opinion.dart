part of openapi.api;

class RsOpinion {
  /// The underlying value of this enum member.
  final int value;

  const RsOpinion._internal(this.value);

  static const RsOpinion number0_ = const RsOpinion._internal(0);
  static const RsOpinion number1_ = const RsOpinion._internal(1);
  static const RsOpinion number2_ = const RsOpinion._internal(2);
  
  int toJson (){
    return this.value;
  }

  static RsOpinion fromJson(int value) {
    return new RsOpinionTypeTransformer().decode(value);
  }
  
  static List<RsOpinion> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsOpinion>() : json.map((value) => RsOpinion.fromJson(value)).toList();
  }
}

class RsOpinionTypeTransformer {

  dynamic encode(RsOpinion data) {
    return data.value;
  }

  RsOpinion decode(dynamic data) {
    switch (data) {
      case 0: return RsOpinion.number0_;
      case 1: return RsOpinion.number1_;
      case 2: return RsOpinion.number2_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


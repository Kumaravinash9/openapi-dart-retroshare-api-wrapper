part of openapi.api;

class ResGetCurrentAccountId {
  
  bool retval = null;
  
  String id = null;
  ResGetCurrentAccountId();

  @override
  String toString() {
    return 'ResGetCurrentAccountId[retval=$retval, id=$id, ]';
  }

  ResGetCurrentAccountId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ResGetCurrentAccountId> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCurrentAccountId>() : json.map((value) => ResGetCurrentAccountId.fromJson(value)).toList();
  }

  static Map<String, ResGetCurrentAccountId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCurrentAccountId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCurrentAccountId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCurrentAccountId-objects as value to a dart map
  static Map<String, List<ResGetCurrentAccountId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCurrentAccountId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCurrentAccountId.listFromJson(value);
       });
     }
     return map;
  }
}


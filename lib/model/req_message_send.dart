part of openapi.api;

class ReqMessageSend {
  
  RsMsgsMessageInfo info = null;
  ReqMessageSend();

  @override
  String toString() {
    return 'ReqMessageSend[info=$info, ]';
  }

  ReqMessageSend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    info = (json['info'] == null) ?
      null :
      RsMsgsMessageInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ReqMessageSend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageSend>() : json.map((value) => ReqMessageSend.fromJson(value)).toList();
  }

  static Map<String, ReqMessageSend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageSend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageSend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageSend-objects as value to a dart map
  static Map<String, List<ReqMessageSend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageSend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageSend.listFromJson(value);
       });
     }
     return map;
  }
}


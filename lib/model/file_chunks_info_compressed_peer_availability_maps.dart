part of openapi.api;

class FileChunksInfoCompressedPeerAvailabilityMaps {
  
  String key = null;
  
  CompressedChunkMap value = null;
  FileChunksInfoCompressedPeerAvailabilityMaps();

  @override
  String toString() {
    return 'FileChunksInfoCompressedPeerAvailabilityMaps[key=$key, value=$value, ]';
  }

  FileChunksInfoCompressedPeerAvailabilityMaps.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      CompressedChunkMap.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<FileChunksInfoCompressedPeerAvailabilityMaps> listFromJson(List<dynamic> json) {
    return json == null ? List<FileChunksInfoCompressedPeerAvailabilityMaps>() : json.map((value) => FileChunksInfoCompressedPeerAvailabilityMaps.fromJson(value)).toList();
  }

  static Map<String, FileChunksInfoCompressedPeerAvailabilityMaps> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileChunksInfoCompressedPeerAvailabilityMaps>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileChunksInfoCompressedPeerAvailabilityMaps.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileChunksInfoCompressedPeerAvailabilityMaps-objects as value to a dart map
  static Map<String, List<FileChunksInfoCompressedPeerAvailabilityMaps>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileChunksInfoCompressedPeerAvailabilityMaps>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileChunksInfoCompressedPeerAvailabilityMaps.listFromJson(value);
       });
     }
     return map;
  }
}


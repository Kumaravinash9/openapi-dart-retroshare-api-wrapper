part of openapi.api;

class ResFileRequest {
  
  bool retval = null;
  ResFileRequest();

  @override
  String toString() {
    return 'ResFileRequest[retval=$retval, ]';
  }

  ResFileRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResFileRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileRequest>() : json.map((value) => ResFileRequest.fromJson(value)).toList();
  }

  static Map<String, ResFileRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileRequest-objects as value to a dart map
  static Map<String, List<ResFileRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileRequest.listFromJson(value);
       });
     }
     return map;
  }
}


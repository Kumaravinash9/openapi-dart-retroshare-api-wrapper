part of openapi.api;

class ReqClearChatLobby {
  
  ChatId id = null;
  ReqClearChatLobby();

  @override
  String toString() {
    return 'ReqClearChatLobby[id=$id, ]';
  }

  ReqClearChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatId.fromJson(json['id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqClearChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqClearChatLobby>() : json.map((value) => ReqClearChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqClearChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqClearChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqClearChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqClearChatLobby-objects as value to a dart map
  static Map<String, List<ReqClearChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqClearChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqClearChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}


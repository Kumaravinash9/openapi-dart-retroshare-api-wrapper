part of openapi.api;

class ReqCreateLocation {
  
  RsLoginHelperLocation location = null;
  
  String password = null;
  
  bool makeHidden = null;
  
  bool makeAutoTor = null;
  ReqCreateLocation();

  @override
  String toString() {
    return 'ReqCreateLocation[location=$location, password=$password, makeHidden=$makeHidden, makeAutoTor=$makeAutoTor, ]';
  }

  ReqCreateLocation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    location = (json['location'] == null) ?
      null :
      RsLoginHelperLocation.fromJson(json['location']);
    password = json['password'];
    makeHidden = json['makeHidden'];
    makeAutoTor = json['makeAutoTor'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (location != null)
      json['location'] = location;
    if (password != null)
      json['password'] = password;
    if (makeHidden != null)
      json['makeHidden'] = makeHidden;
    if (makeAutoTor != null)
      json['makeAutoTor'] = makeAutoTor;
    return json;
  }

  static List<ReqCreateLocation> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateLocation>() : json.map((value) => ReqCreateLocation.fromJson(value)).toList();
  }

  static Map<String, ReqCreateLocation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateLocation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateLocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateLocation-objects as value to a dart map
  static Map<String, List<ReqCreateLocation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateLocation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateLocation.listFromJson(value);
       });
     }
     return map;
  }
}


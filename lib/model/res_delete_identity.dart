part of openapi.api;

class ResDeleteIdentity {
  
  bool retval = null;
  ResDeleteIdentity();

  @override
  String toString() {
    return 'ResDeleteIdentity[retval=$retval, ]';
  }

  ResDeleteIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResDeleteIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResDeleteIdentity>() : json.map((value) => ResDeleteIdentity.fromJson(value)).toList();
  }

  static Map<String, ResDeleteIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResDeleteIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResDeleteIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResDeleteIdentity-objects as value to a dart map
  static Map<String, List<ResDeleteIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResDeleteIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResDeleteIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


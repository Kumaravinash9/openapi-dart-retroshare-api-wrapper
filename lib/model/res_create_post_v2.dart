part of openapi.api;

class ResCreatePostV2 {
  
  bool retval = null;
  
  String postId = null;
  
  String errorMessage = null;
  ResCreatePostV2();

  @override
  String toString() {
    return 'ResCreatePostV2[retval=$retval, postId=$postId, errorMessage=$errorMessage, ]';
  }

  ResCreatePostV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    postId = json['postId'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (postId != null)
      json['postId'] = postId;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreatePostV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreatePostV2>() : json.map((value) => ResCreatePostV2.fromJson(value)).toList();
  }

  static Map<String, ResCreatePostV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreatePostV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreatePostV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreatePostV2-objects as value to a dart map
  static Map<String, List<ResCreatePostV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreatePostV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreatePostV2.listFromJson(value);
       });
     }
     return map;
  }
}


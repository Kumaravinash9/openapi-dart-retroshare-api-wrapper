part of openapi.api;

class ResSetPartialsDirectory {
  
  bool retval = null;
  ResSetPartialsDirectory();

  @override
  String toString() {
    return 'ResSetPartialsDirectory[retval=$retval, ]';
  }

  ResSetPartialsDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetPartialsDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetPartialsDirectory>() : json.map((value) => ResSetPartialsDirectory.fromJson(value)).toList();
  }

  static Map<String, ResSetPartialsDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetPartialsDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetPartialsDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetPartialsDirectory-objects as value to a dart map
  static Map<String, List<ResSetPartialsDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetPartialsDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetPartialsDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


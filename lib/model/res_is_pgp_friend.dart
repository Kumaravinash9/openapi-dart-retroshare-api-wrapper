part of openapi.api;

class ResIsPgpFriend {
  
  bool retval = null;
  ResIsPgpFriend();

  @override
  String toString() {
    return 'ResIsPgpFriend[retval=$retval, ]';
  }

  ResIsPgpFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsPgpFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsPgpFriend>() : json.map((value) => ResIsPgpFriend.fromJson(value)).toList();
  }

  static Map<String, ResIsPgpFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsPgpFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsPgpFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsPgpFriend-objects as value to a dart map
  static Map<String, List<ResIsPgpFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsPgpFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsPgpFriend.listFromJson(value);
       });
     }
     return map;
  }
}


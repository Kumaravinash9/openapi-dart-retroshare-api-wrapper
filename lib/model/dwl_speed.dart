part of openapi.api;

class DwlSpeed {
  /// The underlying value of this enum member.
  final int value;

  const DwlSpeed._internal(this.value);

  static const DwlSpeed number0_ = const DwlSpeed._internal(0);
  static const DwlSpeed number1_ = const DwlSpeed._internal(1);
  static const DwlSpeed number2_ = const DwlSpeed._internal(2);
  
  int toJson (){
    return this.value;
  }

  static DwlSpeed fromJson(int value) {
    return new DwlSpeedTypeTransformer().decode(value);
  }
  
  static List<DwlSpeed> listFromJson(List<dynamic> json) {
    return json == null ? new List<DwlSpeed>() : json.map((value) => DwlSpeed.fromJson(value)).toList();
  }
}

class DwlSpeedTypeTransformer {

  dynamic encode(DwlSpeed data) {
    return data.value;
  }

  DwlSpeed decode(dynamic data) {
    switch (data) {
      case 0: return DwlSpeed.number0_;
      case 1: return DwlSpeed.number1_;
      case 2: return DwlSpeed.number2_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


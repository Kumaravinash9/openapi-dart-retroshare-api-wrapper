part of openapi.api;

class ResGetChannelServiceStatistics {
  
  bool retval = null;
  
  GxsServiceStatistic stat = null;
  ResGetChannelServiceStatistics();

  @override
  String toString() {
    return 'ResGetChannelServiceStatistics[retval=$retval, stat=$stat, ]';
  }

  ResGetChannelServiceStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    stat = (json['stat'] == null) ?
      null :
      GxsServiceStatistic.fromJson(json['stat']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (stat != null)
      json['stat'] = stat;
    return json;
  }

  static List<ResGetChannelServiceStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelServiceStatistics>() : json.map((value) => ResGetChannelServiceStatistics.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelServiceStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelServiceStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelServiceStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelServiceStatistics-objects as value to a dart map
  static Map<String, List<ResGetChannelServiceStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelServiceStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelServiceStatistics.listFromJson(value);
       });
     }
     return map;
  }
}


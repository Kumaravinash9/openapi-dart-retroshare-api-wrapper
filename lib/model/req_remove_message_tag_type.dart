part of openapi.api;

class ReqRemoveMessageTagType {
  
  int tagId = null;
  ReqRemoveMessageTagType();

  @override
  String toString() {
    return 'ReqRemoveMessageTagType[tagId=$tagId, ]';
  }

  ReqRemoveMessageTagType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    tagId = json['tagId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (tagId != null)
      json['tagId'] = tagId;
    return json;
  }

  static List<ReqRemoveMessageTagType> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRemoveMessageTagType>() : json.map((value) => ReqRemoveMessageTagType.fromJson(value)).toList();
  }

  static Map<String, ReqRemoveMessageTagType> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRemoveMessageTagType>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRemoveMessageTagType.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRemoveMessageTagType-objects as value to a dart map
  static Map<String, List<ReqRemoveMessageTagType>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRemoveMessageTagType>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRemoveMessageTagType.listFromJson(value);
       });
     }
     return map;
  }
}


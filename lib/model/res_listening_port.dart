part of openapi.api;

class ResListeningPort {
  
  int retval = null;
  ResListeningPort();

  @override
  String toString() {
    return 'ResListeningPort[retval=$retval, ]';
  }

  ResListeningPort.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResListeningPort> listFromJson(List<dynamic> json) {
    return json == null ? List<ResListeningPort>() : json.map((value) => ResListeningPort.fromJson(value)).toList();
  }

  static Map<String, ResListeningPort> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResListeningPort>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResListeningPort.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResListeningPort-objects as value to a dart map
  static Map<String, List<ResListeningPort>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResListeningPort>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResListeningPort.listFromJson(value);
       });
     }
     return map;
  }
}


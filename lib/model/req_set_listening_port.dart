part of openapi.api;

class ReqSetListeningPort {
  
  int port = null;
  ReqSetListeningPort();

  @override
  String toString() {
    return 'ReqSetListeningPort[port=$port, ]';
  }

  ReqSetListeningPort.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    port = json['port'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (port != null)
      json['port'] = port;
    return json;
  }

  static List<ReqSetListeningPort> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetListeningPort>() : json.map((value) => ReqSetListeningPort.fromJson(value)).toList();
  }

  static Map<String, ReqSetListeningPort> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetListeningPort>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetListeningPort.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetListeningPort-objects as value to a dart map
  static Map<String, List<ReqSetListeningPort>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetListeningPort>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetListeningPort.listFromJson(value);
       });
     }
     return map;
  }
}


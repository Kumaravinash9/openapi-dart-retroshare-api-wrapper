part of openapi.api;

class ResInviteIdsToCircle {
  
  bool retval = null;
  ResInviteIdsToCircle();

  @override
  String toString() {
    return 'ResInviteIdsToCircle[retval=$retval, ]';
  }

  ResInviteIdsToCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResInviteIdsToCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ResInviteIdsToCircle>() : json.map((value) => ResInviteIdsToCircle.fromJson(value)).toList();
  }

  static Map<String, ResInviteIdsToCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResInviteIdsToCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResInviteIdsToCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResInviteIdsToCircle-objects as value to a dart map
  static Map<String, List<ResInviteIdsToCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResInviteIdsToCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResInviteIdsToCircle.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqAssignPeerToGroup {
  
  String groupId = null;
  
  String peerId = null;
  
  bool assign = null;
  ReqAssignPeerToGroup();

  @override
  String toString() {
    return 'ReqAssignPeerToGroup[groupId=$groupId, peerId=$peerId, assign=$assign, ]';
  }

  ReqAssignPeerToGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupId = json['groupId'];
    peerId = json['peerId'];
    assign = json['assign'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupId != null)
      json['groupId'] = groupId;
    if (peerId != null)
      json['peerId'] = peerId;
    if (assign != null)
      json['assign'] = assign;
    return json;
  }

  static List<ReqAssignPeerToGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAssignPeerToGroup>() : json.map((value) => ReqAssignPeerToGroup.fromJson(value)).toList();
  }

  static Map<String, ReqAssignPeerToGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAssignPeerToGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAssignPeerToGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAssignPeerToGroup-objects as value to a dart map
  static Map<String, List<ReqAssignPeerToGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAssignPeerToGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAssignPeerToGroup.listFromJson(value);
       });
     }
     return map;
  }
}


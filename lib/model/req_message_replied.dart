part of openapi.api;

class ReqMessageReplied {
  
  String msgId = null;
  
  bool replied = null;
  ReqMessageReplied();

  @override
  String toString() {
    return 'ReqMessageReplied[msgId=$msgId, replied=$replied, ]';
  }

  ReqMessageReplied.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    replied = json['replied'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (replied != null)
      json['replied'] = replied;
    return json;
  }

  static List<ReqMessageReplied> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageReplied>() : json.map((value) => ReqMessageReplied.fromJson(value)).toList();
  }

  static Map<String, ReqMessageReplied> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageReplied>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageReplied.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageReplied-objects as value to a dart map
  static Map<String, List<ReqMessageReplied>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageReplied>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageReplied.listFromJson(value);
       });
     }
     return map;
  }
}


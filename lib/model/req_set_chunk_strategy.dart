part of openapi.api;

class ReqSetChunkStrategy {
  
  String hash = null;
  
  FileChunksInfoChunkStrategy newStrategy = null;
  //enum newStrategyEnum {  0,  1,  2,  };{
  ReqSetChunkStrategy();

  @override
  String toString() {
    return 'ReqSetChunkStrategy[hash=$hash, newStrategy=$newStrategy, ]';
  }

  ReqSetChunkStrategy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    newStrategy = (json['newStrategy'] == null) ?
      null :
      FileChunksInfoChunkStrategy.fromJson(json['newStrategy']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (newStrategy != null)
      json['newStrategy'] = newStrategy;
    return json;
  }

  static List<ReqSetChunkStrategy> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetChunkStrategy>() : json.map((value) => ReqSetChunkStrategy.fromJson(value)).toList();
  }

  static Map<String, ReqSetChunkStrategy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetChunkStrategy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetChunkStrategy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetChunkStrategy-objects as value to a dart map
  static Map<String, List<ReqSetChunkStrategy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetChunkStrategy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetChunkStrategy.listFromJson(value);
       });
     }
     return map;
  }
}


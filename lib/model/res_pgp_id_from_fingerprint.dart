part of openapi.api;

class ResPgpIdFromFingerprint {
  
  String retval = null;
  ResPgpIdFromFingerprint();

  @override
  String toString() {
    return 'ResPgpIdFromFingerprint[retval=$retval, ]';
  }

  ResPgpIdFromFingerprint.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResPgpIdFromFingerprint> listFromJson(List<dynamic> json) {
    return json == null ? List<ResPgpIdFromFingerprint>() : json.map((value) => ResPgpIdFromFingerprint.fromJson(value)).toList();
  }

  static Map<String, ResPgpIdFromFingerprint> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResPgpIdFromFingerprint>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResPgpIdFromFingerprint.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResPgpIdFromFingerprint-objects as value to a dart map
  static Map<String, List<ResPgpIdFromFingerprint>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResPgpIdFromFingerprint>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResPgpIdFromFingerprint.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetGroupInfo {
  
  String groupId = null;
  ReqGetGroupInfo();

  @override
  String toString() {
    return 'ReqGetGroupInfo[groupId=$groupId, ]';
  }

  ReqGetGroupInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    groupId = json['groupId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (groupId != null)
      json['groupId'] = groupId;
    return json;
  }

  static List<ReqGetGroupInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetGroupInfo>() : json.map((value) => ReqGetGroupInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetGroupInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetGroupInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetGroupInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetGroupInfo-objects as value to a dart map
  static Map<String, List<ReqGetGroupInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetGroupInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetGroupInfo.listFromJson(value);
       });
     }
     return map;
  }
}


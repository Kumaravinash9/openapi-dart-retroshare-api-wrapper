part of openapi.api;

class ResDefaultChunkStrategy {
  
  FileChunksInfoChunkStrategy retval = null;
  //enum retvalEnum {  0,  1,  2,  };{
  ResDefaultChunkStrategy();

  @override
  String toString() {
    return 'ResDefaultChunkStrategy[retval=$retval, ]';
  }

  ResDefaultChunkStrategy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      FileChunksInfoChunkStrategy.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResDefaultChunkStrategy> listFromJson(List<dynamic> json) {
    return json == null ? List<ResDefaultChunkStrategy>() : json.map((value) => ResDefaultChunkStrategy.fromJson(value)).toList();
  }

  static Map<String, ResDefaultChunkStrategy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResDefaultChunkStrategy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResDefaultChunkStrategy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResDefaultChunkStrategy-objects as value to a dart map
  static Map<String, List<ResDefaultChunkStrategy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResDefaultChunkStrategy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResDefaultChunkStrategy.listFromJson(value);
       });
     }
     return map;
  }
}


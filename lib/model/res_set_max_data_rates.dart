part of openapi.api;

class ResSetMaxDataRates {
  
  int retval = null;
  ResSetMaxDataRates();

  @override
  String toString() {
    return 'ResSetMaxDataRates[retval=$retval, ]';
  }

  ResSetMaxDataRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetMaxDataRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetMaxDataRates>() : json.map((value) => ResSetMaxDataRates.fromJson(value)).toList();
  }

  static Map<String, ResSetMaxDataRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetMaxDataRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetMaxDataRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetMaxDataRates-objects as value to a dart map
  static Map<String, List<ResSetMaxDataRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetMaxDataRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetMaxDataRates.listFromJson(value);
       });
     }
     return map;
  }
}


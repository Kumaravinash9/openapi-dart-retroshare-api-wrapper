part of openapi.api;

class ResRequestCircleMembership {
  
  bool retval = null;
  ResRequestCircleMembership();

  @override
  String toString() {
    return 'ResRequestCircleMembership[retval=$retval, ]';
  }

  ResRequestCircleMembership.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRequestCircleMembership> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestCircleMembership>() : json.map((value) => ResRequestCircleMembership.fromJson(value)).toList();
  }

  static Map<String, ResRequestCircleMembership> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestCircleMembership>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestCircleMembership.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestCircleMembership-objects as value to a dart map
  static Map<String, List<ResRequestCircleMembership>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestCircleMembership>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestCircleMembership.listFromJson(value);
       });
     }
     return map;
  }
}


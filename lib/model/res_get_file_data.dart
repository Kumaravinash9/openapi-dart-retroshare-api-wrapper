part of openapi.api;

class ResGetFileData {
  
  bool retval = null;
  
  int requestedSize = null;
  
  int data = null;
  ResGetFileData();

  @override
  String toString() {
    return 'ResGetFileData[retval=$retval, requestedSize=$requestedSize, data=$data, ]';
  }

  ResGetFileData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    requestedSize = json['requested_size'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (requestedSize != null)
      json['requested_size'] = requestedSize;
    if (data != null)
      json['data'] = data;
    return json;
  }

  static List<ResGetFileData> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetFileData>() : json.map((value) => ResGetFileData.fromJson(value)).toList();
  }

  static Map<String, ResGetFileData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetFileData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetFileData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetFileData-objects as value to a dart map
  static Map<String, List<ResGetFileData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetFileData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetFileData.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResSetOwnOpinion {
  
  bool retval = null;
  ResSetOwnOpinion();

  @override
  String toString() {
    return 'ResSetOwnOpinion[retval=$retval, ]';
  }

  ResSetOwnOpinion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetOwnOpinion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetOwnOpinion>() : json.map((value) => ResSetOwnOpinion.fromJson(value)).toList();
  }

  static Map<String, ResSetOwnOpinion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetOwnOpinion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetOwnOpinion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetOwnOpinion-objects as value to a dart map
  static Map<String, List<ResSetOwnOpinion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetOwnOpinion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetOwnOpinion.listFromJson(value);
       });
     }
     return map;
  }
}


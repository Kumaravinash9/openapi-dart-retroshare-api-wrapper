part of openapi.api;

class ResLoadCertificateFromString {
  
  bool retval = null;
  
  String sslId = null;
  
  String pgpId = null;
  
  String errorString = null;
  ResLoadCertificateFromString();

  @override
  String toString() {
    return 'ResLoadCertificateFromString[retval=$retval, sslId=$sslId, pgpId=$pgpId, errorString=$errorString, ]';
  }

  ResLoadCertificateFromString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    sslId = json['sslId'];
    pgpId = json['pgpId'];
    errorString = json['errorString'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (sslId != null)
      json['sslId'] = sslId;
    if (pgpId != null)
      json['pgpId'] = pgpId;
    if (errorString != null)
      json['errorString'] = errorString;
    return json;
  }

  static List<ResLoadCertificateFromString> listFromJson(List<dynamic> json) {
    return json == null ? List<ResLoadCertificateFromString>() : json.map((value) => ResLoadCertificateFromString.fromJson(value)).toList();
  }

  static Map<String, ResLoadCertificateFromString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResLoadCertificateFromString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResLoadCertificateFromString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResLoadCertificateFromString-objects as value to a dart map
  static Map<String, List<ResLoadCertificateFromString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResLoadCertificateFromString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResLoadCertificateFromString.listFromJson(value);
       });
     }
     return map;
  }
}


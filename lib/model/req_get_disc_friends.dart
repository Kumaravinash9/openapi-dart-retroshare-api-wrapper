part of openapi.api;

class ReqGetDiscFriends {
  
  String id = null;
  ReqGetDiscFriends();

  @override
  String toString() {
    return 'ReqGetDiscFriends[id=$id, ]';
  }

  ReqGetDiscFriends.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqGetDiscFriends> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetDiscFriends>() : json.map((value) => ReqGetDiscFriends.fromJson(value)).toList();
  }

  static Map<String, ReqGetDiscFriends> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetDiscFriends>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetDiscFriends.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetDiscFriends-objects as value to a dart map
  static Map<String, List<ReqGetDiscFriends>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetDiscFriends>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetDiscFriends.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqRequestNewTokenAutorization {
  
  String user = null;
  
  String password = null;
  ReqRequestNewTokenAutorization();

  @override
  String toString() {
    return 'ReqRequestNewTokenAutorization[user=$user, password=$password, ]';
  }

  ReqRequestNewTokenAutorization.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    user = json['user'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (user != null)
      json['user'] = user;
    if (password != null)
      json['password'] = password;
    return json;
  }

  static List<ReqRequestNewTokenAutorization> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestNewTokenAutorization>() : json.map((value) => ReqRequestNewTokenAutorization.fromJson(value)).toList();
  }

  static Map<String, ReqRequestNewTokenAutorization> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestNewTokenAutorization>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestNewTokenAutorization.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestNewTokenAutorization-objects as value to a dart map
  static Map<String, List<ReqRequestNewTokenAutorization>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestNewTokenAutorization>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestNewTokenAutorization.listFromJson(value);
       });
     }
     return map;
  }
}


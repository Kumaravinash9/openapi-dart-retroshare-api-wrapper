part of openapi.api;

class ResGetCurrentDataRates {
  
  int retval = null;
  
  num inKb = null;
  
  num outKb = null;
  ResGetCurrentDataRates();

  @override
  String toString() {
    return 'ResGetCurrentDataRates[retval=$retval, inKb=$inKb, outKb=$outKb, ]';
  }

  ResGetCurrentDataRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    inKb = json['inKb'];
    outKb = json['outKb'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (inKb != null)
      json['inKb'] = inKb;
    if (outKb != null)
      json['outKb'] = outKb;
    return json;
  }

  static List<ResGetCurrentDataRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCurrentDataRates>() : json.map((value) => ResGetCurrentDataRates.fromJson(value)).toList();
  }

  static Map<String, ResGetCurrentDataRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCurrentDataRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCurrentDataRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCurrentDataRates-objects as value to a dart map
  static Map<String, List<ResGetCurrentDataRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCurrentDataRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCurrentDataRates.listFromJson(value);
       });
     }
     return map;
  }
}


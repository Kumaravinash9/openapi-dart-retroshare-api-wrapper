part of openapi.api;

class ReqGetCircleRequests {
  
  String circleId = null;
  ReqGetCircleRequests();

  @override
  String toString() {
    return 'ReqGetCircleRequests[circleId=$circleId, ]';
  }

  ReqGetCircleRequests.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqGetCircleRequests> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetCircleRequests>() : json.map((value) => ReqGetCircleRequests.fromJson(value)).toList();
  }

  static Map<String, ReqGetCircleRequests> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetCircleRequests>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetCircleRequests.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetCircleRequests-objects as value to a dart map
  static Map<String, List<ReqGetCircleRequests>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetCircleRequests>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetCircleRequests.listFromJson(value);
       });
     }
     return map;
  }
}


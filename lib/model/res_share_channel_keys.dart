part of openapi.api;

class ResShareChannelKeys {
  
  bool retval = null;
  ResShareChannelKeys();

  @override
  String toString() {
    return 'ResShareChannelKeys[retval=$retval, ]';
  }

  ResShareChannelKeys.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResShareChannelKeys> listFromJson(List<dynamic> json) {
    return json == null ? List<ResShareChannelKeys>() : json.map((value) => ResShareChannelKeys.fromJson(value)).toList();
  }

  static Map<String, ResShareChannelKeys> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResShareChannelKeys>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResShareChannelKeys.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResShareChannelKeys-objects as value to a dart map
  static Map<String, List<ResShareChannelKeys>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResShareChannelKeys>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResShareChannelKeys.listFromJson(value);
       });
     }
     return map;
  }
}


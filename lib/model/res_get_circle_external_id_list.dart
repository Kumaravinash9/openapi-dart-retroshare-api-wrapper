part of openapi.api;

class ResGetCircleExternalIdList {
  
  bool retval = null;
  ResGetCircleExternalIdList();

  @override
  String toString() {
    return 'ResGetCircleExternalIdList[retval=$retval, ]';
  }

  ResGetCircleExternalIdList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetCircleExternalIdList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCircleExternalIdList>() : json.map((value) => ResGetCircleExternalIdList.fromJson(value)).toList();
  }

  static Map<String, ResGetCircleExternalIdList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCircleExternalIdList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCircleExternalIdList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCircleExternalIdList-objects as value to a dart map
  static Map<String, List<ResGetCircleExternalIdList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCircleExternalIdList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCircleExternalIdList.listFromJson(value);
       });
     }
     return map;
  }
}


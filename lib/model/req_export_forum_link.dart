part of openapi.api;

class ReqExportForumLink {
  
  String forumId = null;
  
  bool includeGxsData = null;
  
  String baseUrl = null;
  ReqExportForumLink();

  @override
  String toString() {
    return 'ReqExportForumLink[forumId=$forumId, includeGxsData=$includeGxsData, baseUrl=$baseUrl, ]';
  }

  ReqExportForumLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumId = json['forumId'];
    includeGxsData = json['includeGxsData'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumId != null)
      json['forumId'] = forumId;
    if (includeGxsData != null)
      json['includeGxsData'] = includeGxsData;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportForumLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportForumLink>() : json.map((value) => ReqExportForumLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportForumLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportForumLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportForumLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportForumLink-objects as value to a dart map
  static Map<String, List<ReqExportForumLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportForumLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportForumLink.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResCreateChannelV2 {
  
  bool retval = null;
  
  String channelId = null;
  
  String errorMessage = null;
  ResCreateChannelV2();

  @override
  String toString() {
    return 'ResCreateChannelV2[retval=$retval, channelId=$channelId, errorMessage=$errorMessage, ]';
  }

  ResCreateChannelV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    channelId = json['channelId'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (channelId != null)
      json['channelId'] = channelId;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreateChannelV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateChannelV2>() : json.map((value) => ResCreateChannelV2.fromJson(value)).toList();
  }

  static Map<String, ResCreateChannelV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateChannelV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateChannelV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateChannelV2-objects as value to a dart map
  static Map<String, List<ResCreateChannelV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateChannelV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateChannelV2.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResExtraFileHash {
  
  bool retval = null;
  ResExtraFileHash();

  @override
  String toString() {
    return 'ResExtraFileHash[retval=$retval, ]';
  }

  ResExtraFileHash.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResExtraFileHash> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExtraFileHash>() : json.map((value) => ResExtraFileHash.fromJson(value)).toList();
  }

  static Map<String, ResExtraFileHash> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExtraFileHash>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExtraFileHash.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExtraFileHash-objects as value to a dart map
  static Map<String, List<ResExtraFileHash>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExtraFileHash>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExtraFileHash.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqTurtleSearchRequest {
  
  String matchString = null;
  
  RstimeT maxWait = null;
  ReqTurtleSearchRequest();

  @override
  String toString() {
    return 'ReqTurtleSearchRequest[matchString=$matchString, maxWait=$maxWait, ]';
  }

  ReqTurtleSearchRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    matchString = json['matchString'];
    maxWait = (json['maxWait'] == null) ?
      null :
      RstimeT.fromJson(json['maxWait']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (matchString != null)
      json['matchString'] = matchString;
    if (maxWait != null)
      json['maxWait'] = maxWait;
    return json;
  }

  static List<ReqTurtleSearchRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqTurtleSearchRequest>() : json.map((value) => ReqTurtleSearchRequest.fromJson(value)).toList();
  }

  static Map<String, ReqTurtleSearchRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqTurtleSearchRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqTurtleSearchRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqTurtleSearchRequest-objects as value to a dart map
  static Map<String, List<ReqTurtleSearchRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqTurtleSearchRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqTurtleSearchRequest.listFromJson(value);
       });
     }
     return map;
  }
}


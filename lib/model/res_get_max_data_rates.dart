part of openapi.api;

class ResGetMaxDataRates {
  
  int retval = null;
  
  int inKb = null;
  
  int outKb = null;
  ResGetMaxDataRates();

  @override
  String toString() {
    return 'ResGetMaxDataRates[retval=$retval, inKb=$inKb, outKb=$outKb, ]';
  }

  ResGetMaxDataRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    inKb = json['inKb'];
    outKb = json['outKb'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (inKb != null)
      json['inKb'] = inKb;
    if (outKb != null)
      json['outKb'] = outKb;
    return json;
  }

  static List<ResGetMaxDataRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMaxDataRates>() : json.map((value) => ResGetMaxDataRates.fromJson(value)).toList();
  }

  static Map<String, ResGetMaxDataRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMaxDataRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMaxDataRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMaxDataRates-objects as value to a dart map
  static Map<String, List<ResGetMaxDataRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMaxDataRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMaxDataRates.listFromJson(value);
       });
     }
     return map;
  }
}


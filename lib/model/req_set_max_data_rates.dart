part of openapi.api;

class ReqSetMaxDataRates {
  
  int downKb = null;
  
  int upKb = null;
  ReqSetMaxDataRates();

  @override
  String toString() {
    return 'ReqSetMaxDataRates[downKb=$downKb, upKb=$upKb, ]';
  }

  ReqSetMaxDataRates.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    downKb = json['downKb'];
    upKb = json['upKb'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (downKb != null)
      json['downKb'] = downKb;
    if (upKb != null)
      json['upKb'] = upKb;
    return json;
  }

  static List<ReqSetMaxDataRates> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetMaxDataRates>() : json.map((value) => ReqSetMaxDataRates.fromJson(value)).toList();
  }

  static Map<String, ReqSetMaxDataRates> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetMaxDataRates>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetMaxDataRates.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetMaxDataRates-objects as value to a dart map
  static Map<String, List<ReqSetMaxDataRates>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetMaxDataRates>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetMaxDataRates.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSetDefaultChunkStrategy {
  
  FileChunksInfoChunkStrategy strategy = null;
  //enum strategyEnum {  0,  1,  2,  };{
  ReqSetDefaultChunkStrategy();

  @override
  String toString() {
    return 'ReqSetDefaultChunkStrategy[strategy=$strategy, ]';
  }

  ReqSetDefaultChunkStrategy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    strategy = (json['strategy'] == null) ?
      null :
      FileChunksInfoChunkStrategy.fromJson(json['strategy']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (strategy != null)
      json['strategy'] = strategy;
    return json;
  }

  static List<ReqSetDefaultChunkStrategy> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDefaultChunkStrategy>() : json.map((value) => ReqSetDefaultChunkStrategy.fromJson(value)).toList();
  }

  static Map<String, ReqSetDefaultChunkStrategy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDefaultChunkStrategy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDefaultChunkStrategy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDefaultChunkStrategy-objects as value to a dart map
  static Map<String, List<ReqSetDefaultChunkStrategy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDefaultChunkStrategy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDefaultChunkStrategy.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class BannedFileEntry {
  
  String mFilename = null;
  
  ReqBanFileFileSize mSize = null;
  
  RstimeT mBanTimeStamp = null;
  BannedFileEntry();

  @override
  String toString() {
    return 'BannedFileEntry[mFilename=$mFilename, mSize=$mSize, mBanTimeStamp=$mBanTimeStamp, ]';
  }

  BannedFileEntry.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mFilename = json['mFilename'];
    mSize = (json['mSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['mSize']);
    mBanTimeStamp = (json['mBanTimeStamp'] == null) ?
      null :
      RstimeT.fromJson(json['mBanTimeStamp']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mFilename != null)
      json['mFilename'] = mFilename;
    if (mSize != null)
      json['mSize'] = mSize;
    if (mBanTimeStamp != null)
      json['mBanTimeStamp'] = mBanTimeStamp;
    return json;
  }

  static List<BannedFileEntry> listFromJson(List<dynamic> json) {
    return json == null ? List<BannedFileEntry>() : json.map((value) => BannedFileEntry.fromJson(value)).toList();
  }

  static Map<String, BannedFileEntry> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, BannedFileEntry>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = BannedFileEntry.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of BannedFileEntry-objects as value to a dart map
  static Map<String, List<BannedFileEntry>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<BannedFileEntry>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = BannedFileEntry.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqInitiateDistantChatConnexion {
  
  String toPid = null;
  
  String fromPid = null;
  
  bool notify = null;
  ReqInitiateDistantChatConnexion();

  @override
  String toString() {
    return 'ReqInitiateDistantChatConnexion[toPid=$toPid, fromPid=$fromPid, notify=$notify, ]';
  }

  ReqInitiateDistantChatConnexion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    toPid = json['to_pid'];
    fromPid = json['from_pid'];
    notify = json['notify'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (toPid != null)
      json['to_pid'] = toPid;
    if (fromPid != null)
      json['from_pid'] = fromPid;
    if (notify != null)
      json['notify'] = notify;
    return json;
  }

  static List<ReqInitiateDistantChatConnexion> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqInitiateDistantChatConnexion>() : json.map((value) => ReqInitiateDistantChatConnexion.fromJson(value)).toList();
  }

  static Map<String, ReqInitiateDistantChatConnexion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqInitiateDistantChatConnexion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqInitiateDistantChatConnexion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqInitiateDistantChatConnexion-objects as value to a dart map
  static Map<String, List<ReqInitiateDistantChatConnexion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqInitiateDistantChatConnexion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqInitiateDistantChatConnexion.listFromJson(value);
       });
     }
     return map;
  }
}


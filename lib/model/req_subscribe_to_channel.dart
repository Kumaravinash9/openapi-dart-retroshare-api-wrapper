part of openapi.api;

class ReqSubscribeToChannel {
  
  String channelId = null;
  
  bool subscribe = null;
  ReqSubscribeToChannel();

  @override
  String toString() {
    return 'ReqSubscribeToChannel[channelId=$channelId, subscribe=$subscribe, ]';
  }

  ReqSubscribeToChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    subscribe = json['subscribe'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (subscribe != null)
      json['subscribe'] = subscribe;
    return json;
  }

  static List<ReqSubscribeToChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSubscribeToChannel>() : json.map((value) => ReqSubscribeToChannel.fromJson(value)).toList();
  }

  static Map<String, ReqSubscribeToChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSubscribeToChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSubscribeToChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSubscribeToChannel-objects as value to a dart map
  static Map<String, List<ReqSubscribeToChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSubscribeToChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSubscribeToChannel.listFromJson(value);
       });
     }
     return map;
  }
}


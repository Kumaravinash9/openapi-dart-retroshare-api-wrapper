part of openapi.api;

class ResGetServicesAllowed {
  
  bool retval = null;
  
  RsPeerServiceInfo info = null;
  ResGetServicesAllowed();

  @override
  String toString() {
    return 'ResGetServicesAllowed[retval=$retval, info=$info, ]';
  }

  ResGetServicesAllowed.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      RsPeerServiceInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetServicesAllowed> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServicesAllowed>() : json.map((value) => ResGetServicesAllowed.fromJson(value)).toList();
  }

  static Map<String, ResGetServicesAllowed> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServicesAllowed>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServicesAllowed.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServicesAllowed-objects as value to a dart map
  static Map<String, List<ResGetServicesAllowed>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServicesAllowed>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServicesAllowed.listFromJson(value);
       });
     }
     return map;
  }
}


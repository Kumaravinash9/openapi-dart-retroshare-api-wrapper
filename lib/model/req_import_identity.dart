part of openapi.api;

class ReqImportIdentity {
  
  String filePath = null;
  ReqImportIdentity();

  @override
  String toString() {
    return 'ReqImportIdentity[filePath=$filePath, ]';
  }

  ReqImportIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    filePath = json['filePath'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (filePath != null)
      json['filePath'] = filePath;
    return json;
  }

  static List<ReqImportIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqImportIdentity>() : json.map((value) => ReqImportIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqImportIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqImportIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqImportIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqImportIdentity-objects as value to a dart map
  static Map<String, List<ReqImportIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqImportIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqImportIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


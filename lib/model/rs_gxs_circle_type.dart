part of openapi.api;

class RsGxsCircleType {
  /// The underlying value of this enum member.
  final int value;

  const RsGxsCircleType._internal(this.value);

  static const RsGxsCircleType number0_ = const RsGxsCircleType._internal(0);
  static const RsGxsCircleType number1_ = const RsGxsCircleType._internal(1);
  static const RsGxsCircleType number2_ = const RsGxsCircleType._internal(2);
  static const RsGxsCircleType number3_ = const RsGxsCircleType._internal(3);
  static const RsGxsCircleType number4_ = const RsGxsCircleType._internal(4);
  static const RsGxsCircleType number5_ = const RsGxsCircleType._internal(5);
  static const RsGxsCircleType number6_ = const RsGxsCircleType._internal(6);
  
  int toJson (){
    return this.value;
  }

  static RsGxsCircleType fromJson(int value) {
    return new RsGxsCircleTypeTypeTransformer().decode(value);
  }
  
  static List<RsGxsCircleType> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsGxsCircleType>() : json.map((value) => RsGxsCircleType.fromJson(value)).toList();
  }
}

class RsGxsCircleTypeTypeTransformer {

  dynamic encode(RsGxsCircleType data) {
    return data.value;
  }

  RsGxsCircleType decode(dynamic data) {
    switch (data) {
      case 0: return RsGxsCircleType.number0_;
      case 1: return RsGxsCircleType.number1_;
      case 2: return RsGxsCircleType.number2_;
      case 3: return RsGxsCircleType.number3_;
      case 4: return RsGxsCircleType.number4_;
      case 5: return RsGxsCircleType.number5_;
      case 6: return RsGxsCircleType.number6_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


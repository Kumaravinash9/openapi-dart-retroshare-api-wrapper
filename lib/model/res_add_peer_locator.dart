part of openapi.api;

class ResAddPeerLocator {
  
  bool retval = null;
  ResAddPeerLocator();

  @override
  String toString() {
    return 'ResAddPeerLocator[retval=$retval, ]';
  }

  ResAddPeerLocator.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAddPeerLocator> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAddPeerLocator>() : json.map((value) => ResAddPeerLocator.fromJson(value)).toList();
  }

  static Map<String, ResAddPeerLocator> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAddPeerLocator>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAddPeerLocator.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAddPeerLocator-objects as value to a dart map
  static Map<String, List<ResAddPeerLocator>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAddPeerLocator>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAddPeerLocator.listFromJson(value);
       });
     }
     return map;
  }
}


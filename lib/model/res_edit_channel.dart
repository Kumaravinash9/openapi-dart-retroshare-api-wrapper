part of openapi.api;

class ResEditChannel {
  
  bool retval = null;
  ResEditChannel();

  @override
  String toString() {
    return 'ResEditChannel[retval=$retval, ]';
  }

  ResEditChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResEditChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ResEditChannel>() : json.map((value) => ResEditChannel.fromJson(value)).toList();
  }

  static Map<String, ResEditChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResEditChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResEditChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResEditChannel-objects as value to a dart map
  static Map<String, List<ResEditChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResEditChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResEditChannel.listFromJson(value);
       });
     }
     return map;
  }
}


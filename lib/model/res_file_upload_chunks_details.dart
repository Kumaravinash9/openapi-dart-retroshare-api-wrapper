part of openapi.api;

class ResFileUploadChunksDetails {
  
  bool retval = null;
  
  CompressedChunkMap map = null;
  ResFileUploadChunksDetails();

  @override
  String toString() {
    return 'ResFileUploadChunksDetails[retval=$retval, map=$map, ]';
  }

  ResFileUploadChunksDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    map = (json['map'] == null) ?
      null :
      CompressedChunkMap.fromJson(json['map']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (map != null)
      json['map'] = map;
    return json;
  }

  static List<ResFileUploadChunksDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileUploadChunksDetails>() : json.map((value) => ResFileUploadChunksDetails.fromJson(value)).toList();
  }

  static Map<String, ResFileUploadChunksDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileUploadChunksDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileUploadChunksDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileUploadChunksDetails-objects as value to a dart map
  static Map<String, List<ResFileUploadChunksDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileUploadChunksDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileUploadChunksDetails.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetGroupInfo {
  
  bool retval = null;
  
  RsGroupInfo groupInfo = null;
  ResGetGroupInfo();

  @override
  String toString() {
    return 'ResGetGroupInfo[retval=$retval, groupInfo=$groupInfo, ]';
  }

  ResGetGroupInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    groupInfo = (json['groupInfo'] == null) ?
      null :
      RsGroupInfo.fromJson(json['groupInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (groupInfo != null)
      json['groupInfo'] = groupInfo;
    return json;
  }

  static List<ResGetGroupInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetGroupInfo>() : json.map((value) => ResGetGroupInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetGroupInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetGroupInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetGroupInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetGroupInfo-objects as value to a dart map
  static Map<String, List<ResGetGroupInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetGroupInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetGroupInfo.listFromJson(value);
       });
     }
     return map;
  }
}


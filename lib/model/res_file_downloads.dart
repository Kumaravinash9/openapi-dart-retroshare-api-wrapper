part of openapi.api;

class ResFileDownloads {
  
  List<String> hashs = [];
  ResFileDownloads();

  @override
  String toString() {
    return 'ResFileDownloads[hashs=$hashs, ]';
  }

  ResFileDownloads.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hashs = (json['hashs'] == null) ?
      null :
      (json['hashs'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hashs != null)
      json['hashs'] = hashs;
    return json;
  }

  static List<ResFileDownloads> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileDownloads>() : json.map((value) => ResFileDownloads.fromJson(value)).toList();
  }

  static Map<String, ResFileDownloads> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileDownloads>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileDownloads.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileDownloads-objects as value to a dart map
  static Map<String, List<ResFileDownloads>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileDownloads>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileDownloads.listFromJson(value);
       });
     }
     return map;
  }
}


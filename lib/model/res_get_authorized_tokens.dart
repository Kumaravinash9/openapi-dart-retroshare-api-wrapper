part of openapi.api;

class ResGetAuthorizedTokens {
  
  List<ResGetAuthorizedTokensRetval> retval = [];
  ResGetAuthorizedTokens();

  @override
  String toString() {
    return 'ResGetAuthorizedTokens[retval=$retval, ]';
  }

  ResGetAuthorizedTokens.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResGetAuthorizedTokensRetval.listFromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetAuthorizedTokens> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetAuthorizedTokens>() : json.map((value) => ResGetAuthorizedTokens.fromJson(value)).toList();
  }

  static Map<String, ResGetAuthorizedTokens> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetAuthorizedTokens>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetAuthorizedTokens.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetAuthorizedTokens-objects as value to a dart map
  static Map<String, List<ResGetAuthorizedTokens>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetAuthorizedTokens>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetAuthorizedTokens.listFromJson(value);
       });
     }
     return map;
  }
}


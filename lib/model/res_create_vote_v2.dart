part of openapi.api;

class ResCreateVoteV2 {
  
  bool retval = null;
  
  String voteId = null;
  
  String errorMessage = null;
  ResCreateVoteV2();

  @override
  String toString() {
    return 'ResCreateVoteV2[retval=$retval, voteId=$voteId, errorMessage=$errorMessage, ]';
  }

  ResCreateVoteV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    voteId = json['voteId'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (voteId != null)
      json['voteId'] = voteId;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreateVoteV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateVoteV2>() : json.map((value) => ResCreateVoteV2.fromJson(value)).toList();
  }

  static Map<String, ResCreateVoteV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateVoteV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateVoteV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateVoteV2-objects as value to a dart map
  static Map<String, List<ResCreateVoteV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateVoteV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateVoteV2.listFromJson(value);
       });
     }
     return map;
  }
}


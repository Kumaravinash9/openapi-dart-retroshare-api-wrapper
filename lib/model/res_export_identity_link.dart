part of openapi.api;

class ResExportIdentityLink {
  
  bool retval = null;
  
  String link = null;
  
  String errMsg = null;
  ResExportIdentityLink();

  @override
  String toString() {
    return 'ResExportIdentityLink[retval=$retval, link=$link, errMsg=$errMsg, ]';
  }

  ResExportIdentityLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    link = json['link'];
    errMsg = json['errMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    if (errMsg != null)
      json['errMsg'] = errMsg;
    return json;
  }

  static List<ResExportIdentityLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportIdentityLink>() : json.map((value) => ResExportIdentityLink.fromJson(value)).toList();
  }

  static Map<String, ResExportIdentityLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportIdentityLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportIdentityLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportIdentityLink-objects as value to a dart map
  static Map<String, List<ResExportIdentityLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportIdentityLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportIdentityLink.listFromJson(value);
       });
     }
     return map;
  }
}


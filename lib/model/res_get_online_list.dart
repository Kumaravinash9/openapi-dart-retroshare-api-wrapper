part of openapi.api;

class ResGetOnlineList {
  
  bool retval = null;
  
  List<String> sslIds = [];
  ResGetOnlineList();

  @override
  String toString() {
    return 'ResGetOnlineList[retval=$retval, sslIds=$sslIds, ]';
  }

  ResGetOnlineList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    sslIds = (json['sslIds'] == null) ?
      null :
      (json['sslIds'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (sslIds != null)
      json['sslIds'] = sslIds;
    return json;
  }

  static List<ResGetOnlineList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOnlineList>() : json.map((value) => ResGetOnlineList.fromJson(value)).toList();
  }

  static Map<String, ResGetOnlineList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOnlineList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOnlineList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOnlineList-objects as value to a dart map
  static Map<String, List<ResGetOnlineList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOnlineList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOnlineList.listFromJson(value);
       });
     }
     return map;
  }
}


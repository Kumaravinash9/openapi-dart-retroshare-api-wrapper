part of openapi.api;

class RsGxsChannelPost {
  
  List<String> mOlderVersions = [];
  
  String mMsg = null;
  
  List<RsGxsFile> mFiles = [];
  
  int mCount = null;
  
  ReqBanFileFileSize mSize = null;
  
  RsGxsImage mThumbnail = null;
  RsGxsChannelPost();

  @override
  String toString() {
    return 'RsGxsChannelPost[mOlderVersions=$mOlderVersions, mMsg=$mMsg, mFiles=$mFiles, mCount=$mCount, mSize=$mSize, mThumbnail=$mThumbnail, ]';
  }

  RsGxsChannelPost.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mOlderVersions = (json['mOlderVersions'] == null) ?
      null :
      (json['mOlderVersions'] as List).cast<String>();
    mMsg = json['mMsg'];
    mFiles = (json['mFiles'] == null) ?
      null :
      RsGxsFile.listFromJson(json['mFiles']);
    mCount = json['mCount'];
    mSize = (json['mSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['mSize']);
    mThumbnail = (json['mThumbnail'] == null) ?
      null :
      RsGxsImage.fromJson(json['mThumbnail']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mOlderVersions != null)
      json['mOlderVersions'] = mOlderVersions;
    if (mMsg != null)
      json['mMsg'] = mMsg;
    if (mFiles != null)
      json['mFiles'] = mFiles;
    if (mCount != null)
      json['mCount'] = mCount;
    if (mSize != null)
      json['mSize'] = mSize;
    if (mThumbnail != null)
      json['mThumbnail'] = mThumbnail;
    return json;
  }

  static List<RsGxsChannelPost> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsChannelPost>() : json.map((value) => RsGxsChannelPost.fromJson(value)).toList();
  }

  static Map<String, RsGxsChannelPost> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsChannelPost>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsChannelPost.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsChannelPost-objects as value to a dart map
  static Map<String, List<RsGxsChannelPost>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsChannelPost>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsChannelPost.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetPeersConnected {
  
  int serviceId = null;
  ReqGetPeersConnected();

  @override
  String toString() {
    return 'ReqGetPeersConnected[serviceId=$serviceId, ]';
  }

  ReqGetPeersConnected.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    serviceId = json['serviceId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (serviceId != null)
      json['serviceId'] = serviceId;
    return json;
  }

  static List<ReqGetPeersConnected> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetPeersConnected>() : json.map((value) => ReqGetPeersConnected.fromJson(value)).toList();
  }

  static Map<String, ReqGetPeersConnected> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetPeersConnected>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetPeersConnected.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetPeersConnected-objects as value to a dart map
  static Map<String, List<ReqGetPeersConnected>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetPeersConnected>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetPeersConnected.listFromJson(value);
       });
     }
     return map;
  }
}


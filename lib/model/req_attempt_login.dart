part of openapi.api;

class ReqAttemptLogin {
  
  String account = null;
  
  String password = null;
  ReqAttemptLogin();

  @override
  String toString() {
    return 'ReqAttemptLogin[account=$account, password=$password, ]';
  }

  ReqAttemptLogin.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    account = json['account'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (account != null)
      json['account'] = account;
    if (password != null)
      json['password'] = password;
    return json;
  }

  static List<ReqAttemptLogin> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAttemptLogin>() : json.map((value) => ReqAttemptLogin.fromJson(value)).toList();
  }

  static Map<String, ReqAttemptLogin> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAttemptLogin>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAttemptLogin.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAttemptLogin-objects as value to a dart map
  static Map<String, List<ReqAttemptLogin>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAttemptLogin>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAttemptLogin.listFromJson(value);
       });
     }
     return map;
  }
}


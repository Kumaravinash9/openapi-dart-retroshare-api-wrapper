part of openapi.api;

class ReqCreateForumV2 {
  
  String name = null;
  
  String description = null;
  
  String authorId = null;
  
  List<String> moderatorsIds = [];
  
  RsGxsCircleType circleType = null;
  //enum circleTypeEnum {  0,  1,  2,  3,  4,  5,  6,  };{
  
  String circleId = null;
  ReqCreateForumV2();

  @override
  String toString() {
    return 'ReqCreateForumV2[name=$name, description=$description, authorId=$authorId, moderatorsIds=$moderatorsIds, circleType=$circleType, circleId=$circleId, ]';
  }

  ReqCreateForumV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    name = json['name'];
    description = json['description'];
    authorId = json['authorId'];
    moderatorsIds = (json['moderatorsIds'] == null) ?
      null :
      (json['moderatorsIds'] as List).cast<String>();
    circleType = (json['circleType'] == null) ?
      null :
      RsGxsCircleType.fromJson(json['circleType']);
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (name != null)
      json['name'] = name;
    if (description != null)
      json['description'] = description;
    if (authorId != null)
      json['authorId'] = authorId;
    if (moderatorsIds != null)
      json['moderatorsIds'] = moderatorsIds;
    if (circleType != null)
      json['circleType'] = circleType;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqCreateForumV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateForumV2>() : json.map((value) => ReqCreateForumV2.fromJson(value)).toList();
  }

  static Map<String, ReqCreateForumV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateForumV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateForumV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateForumV2-objects as value to a dart map
  static Map<String, List<ReqCreateForumV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateForumV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateForumV2.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqCreateForum {
  
  RsGxsForumGroup forum = null;
  ReqCreateForum();

  @override
  String toString() {
    return 'ReqCreateForum[forum=$forum, ]';
  }

  ReqCreateForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forum = (json['forum'] == null) ?
      null :
      RsGxsForumGroup.fromJson(json['forum']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forum != null)
      json['forum'] = forum;
    return json;
  }

  static List<ReqCreateForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateForum>() : json.map((value) => ReqCreateForum.fromJson(value)).toList();
  }

  static Map<String, ReqCreateForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateForum-objects as value to a dart map
  static Map<String, List<ReqCreateForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateForum.listFromJson(value);
       });
     }
     return map;
  }
}


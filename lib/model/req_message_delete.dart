part of openapi.api;

class ReqMessageDelete {
  
  String msgId = null;
  ReqMessageDelete();

  @override
  String toString() {
    return 'ReqMessageDelete[msgId=$msgId, ]';
  }

  ReqMessageDelete.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    return json;
  }

  static List<ReqMessageDelete> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageDelete>() : json.map((value) => ReqMessageDelete.fromJson(value)).toList();
  }

  static Map<String, ReqMessageDelete> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageDelete>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageDelete.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageDelete-objects as value to a dart map
  static Map<String, List<ReqMessageDelete>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageDelete>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageDelete.listFromJson(value);
       });
     }
     return map;
  }
}


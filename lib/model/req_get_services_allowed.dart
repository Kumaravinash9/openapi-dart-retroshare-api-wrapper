part of openapi.api;

class ReqGetServicesAllowed {
  
  String peerId = null;
  ReqGetServicesAllowed();

  @override
  String toString() {
    return 'ReqGetServicesAllowed[peerId=$peerId, ]';
  }

  ReqGetServicesAllowed.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    peerId = json['peerId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (peerId != null)
      json['peerId'] = peerId;
    return json;
  }

  static List<ReqGetServicesAllowed> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetServicesAllowed>() : json.map((value) => ReqGetServicesAllowed.fromJson(value)).toList();
  }

  static Map<String, ReqGetServicesAllowed> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetServicesAllowed>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetServicesAllowed.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetServicesAllowed-objects as value to a dart map
  static Map<String, List<ReqGetServicesAllowed>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetServicesAllowed>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetServicesAllowed.listFromJson(value);
       });
     }
     return map;
  }
}


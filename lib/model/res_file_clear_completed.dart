part of openapi.api;

class ResFileClearCompleted {
  
  bool retval = null;
  ResFileClearCompleted();

  @override
  String toString() {
    return 'ResFileClearCompleted[retval=$retval, ]';
  }

  ResFileClearCompleted.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResFileClearCompleted> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileClearCompleted>() : json.map((value) => ResFileClearCompleted.fromJson(value)).toList();
  }

  static Map<String, ResFileClearCompleted> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileClearCompleted>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileClearCompleted.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileClearCompleted-objects as value to a dart map
  static Map<String, List<ResFileClearCompleted>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileClearCompleted>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileClearCompleted.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSetDynDNS {
  
  String sslId = null;
  
  String addr = null;
  ReqSetDynDNS();

  @override
  String toString() {
    return 'ReqSetDynDNS[sslId=$sslId, addr=$addr, ]';
  }

  ReqSetDynDNS.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    addr = json['addr'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (addr != null)
      json['addr'] = addr;
    return json;
  }

  static List<ReqSetDynDNS> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDynDNS>() : json.map((value) => ReqSetDynDNS.fromJson(value)).toList();
  }

  static Map<String, ReqSetDynDNS> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDynDNS>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDynDNS.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDynDNS-objects as value to a dart map
  static Map<String, List<ReqSetDynDNS>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDynDNS>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDynDNS.listFromJson(value);
       });
     }
     return map;
  }
}


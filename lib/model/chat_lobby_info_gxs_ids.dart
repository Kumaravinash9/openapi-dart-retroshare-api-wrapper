part of openapi.api;

class ChatLobbyInfoGxsIds {
  
  String key = null;
  
  RstimeT value = null;
  ChatLobbyInfoGxsIds();

  @override
  String toString() {
    return 'ChatLobbyInfoGxsIds[key=$key, value=$value, ]';
  }

  ChatLobbyInfoGxsIds.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      RstimeT.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<ChatLobbyInfoGxsIds> listFromJson(List<dynamic> json) {
    return json == null ? List<ChatLobbyInfoGxsIds>() : json.map((value) => ChatLobbyInfoGxsIds.fromJson(value)).toList();
  }

  static Map<String, ChatLobbyInfoGxsIds> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ChatLobbyInfoGxsIds>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ChatLobbyInfoGxsIds.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ChatLobbyInfoGxsIds-objects as value to a dart map
  static Map<String, List<ChatLobbyInfoGxsIds>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ChatLobbyInfoGxsIds>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ChatLobbyInfoGxsIds.listFromJson(value);
       });
     }
     return map;
  }
}


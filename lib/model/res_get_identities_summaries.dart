part of openapi.api;

class ResGetIdentitiesSummaries {
  
  bool retval = null;
  
  List<RsGroupMetaData> ids = [];
  ResGetIdentitiesSummaries();

  @override
  String toString() {
    return 'ResGetIdentitiesSummaries[retval=$retval, ids=$ids, ]';
  }

  ResGetIdentitiesSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    ids = (json['ids'] == null) ?
      null :
      RsGroupMetaData.listFromJson(json['ids']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (ids != null)
      json['ids'] = ids;
    return json;
  }

  static List<ResGetIdentitiesSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetIdentitiesSummaries>() : json.map((value) => ResGetIdentitiesSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetIdentitiesSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetIdentitiesSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetIdentitiesSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetIdentitiesSummaries-objects as value to a dart map
  static Map<String, List<ResGetIdentitiesSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetIdentitiesSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetIdentitiesSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class RsGxsGroupSummary {
  
  String mGroupId = null;
  
  String mGroupName = null;
  
  String mAuthorId = null;
  
  RstimeT mPublishTs = null;
  
  int mNumberOfMessages = null;
  
  RstimeT mLastMessageTs = null;
  
  int mSignFlags = null;
  
  int mPopularity = null;
  
  String mSearchContext = null;
  RsGxsGroupSummary();

  @override
  String toString() {
    return 'RsGxsGroupSummary[mGroupId=$mGroupId, mGroupName=$mGroupName, mAuthorId=$mAuthorId, mPublishTs=$mPublishTs, mNumberOfMessages=$mNumberOfMessages, mLastMessageTs=$mLastMessageTs, mSignFlags=$mSignFlags, mPopularity=$mPopularity, mSearchContext=$mSearchContext, ]';
  }

  RsGxsGroupSummary.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mGroupId = json['mGroupId'];
    mGroupName = json['mGroupName'];
    mAuthorId = json['mAuthorId'];
    mPublishTs = (json['mPublishTs'] == null) ?
      null :
      RstimeT.fromJson(json['mPublishTs']);
    mNumberOfMessages = json['mNumberOfMessages'];
    mLastMessageTs = (json['mLastMessageTs'] == null) ?
      null :
      RstimeT.fromJson(json['mLastMessageTs']);
    mSignFlags = json['mSignFlags'];
    mPopularity = json['mPopularity'];
    mSearchContext = json['mSearchContext'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mGroupId != null)
      json['mGroupId'] = mGroupId;
    if (mGroupName != null)
      json['mGroupName'] = mGroupName;
    if (mAuthorId != null)
      json['mAuthorId'] = mAuthorId;
    if (mPublishTs != null)
      json['mPublishTs'] = mPublishTs;
    if (mNumberOfMessages != null)
      json['mNumberOfMessages'] = mNumberOfMessages;
    if (mLastMessageTs != null)
      json['mLastMessageTs'] = mLastMessageTs;
    if (mSignFlags != null)
      json['mSignFlags'] = mSignFlags;
    if (mPopularity != null)
      json['mPopularity'] = mPopularity;
    if (mSearchContext != null)
      json['mSearchContext'] = mSearchContext;
    return json;
  }

  static List<RsGxsGroupSummary> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsGroupSummary>() : json.map((value) => RsGxsGroupSummary.fromJson(value)).toList();
  }

  static Map<String, RsGxsGroupSummary> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsGroupSummary>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsGroupSummary.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsGroupSummary-objects as value to a dart map
  static Map<String, List<RsGxsGroupSummary>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsGroupSummary>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsGroupSummary.listFromJson(value);
       });
     }
     return map;
  }
}


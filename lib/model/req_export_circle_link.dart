part of openapi.api;

class ReqExportCircleLink {
  
  String circleId = null;
  
  bool includeGxsData = null;
  
  String baseUrl = null;
  ReqExportCircleLink();

  @override
  String toString() {
    return 'ReqExportCircleLink[circleId=$circleId, includeGxsData=$includeGxsData, baseUrl=$baseUrl, ]';
  }

  ReqExportCircleLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    circleId = json['circleId'];
    includeGxsData = json['includeGxsData'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (circleId != null)
      json['circleId'] = circleId;
    if (includeGxsData != null)
      json['includeGxsData'] = includeGxsData;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportCircleLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportCircleLink>() : json.map((value) => ReqExportCircleLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportCircleLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportCircleLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportCircleLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportCircleLink-objects as value to a dart map
  static Map<String, List<ReqExportCircleLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportCircleLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportCircleLink.listFromJson(value);
       });
     }
     return map;
  }
}


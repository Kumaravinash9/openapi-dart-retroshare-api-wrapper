part of openapi.api;

class ResEditForum {
  
  bool retval = null;
  ResEditForum();

  @override
  String toString() {
    return 'ResEditForum[retval=$retval, ]';
  }

  ResEditForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResEditForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ResEditForum>() : json.map((value) => ResEditForum.fromJson(value)).toList();
  }

  static Map<String, ResEditForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResEditForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResEditForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResEditForum-objects as value to a dart map
  static Map<String, List<ResEditForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResEditForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResEditForum.listFromJson(value);
       });
     }
     return map;
  }
}


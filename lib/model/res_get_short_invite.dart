part of openapi.api;

class ResGetShortInvite {
  
  bool retval = null;
  
  String invite = null;
  ResGetShortInvite();

  @override
  String toString() {
    return 'ResGetShortInvite[retval=$retval, invite=$invite, ]';
  }

  ResGetShortInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    invite = json['invite'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (invite != null)
      json['invite'] = invite;
    return json;
  }

  static List<ResGetShortInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetShortInvite>() : json.map((value) => ResGetShortInvite.fromJson(value)).toList();
  }

  static Map<String, ResGetShortInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetShortInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetShortInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetShortInvite-objects as value to a dart map
  static Map<String, List<ResGetShortInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetShortInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetShortInvite.listFromJson(value);
       });
     }
     return map;
  }
}


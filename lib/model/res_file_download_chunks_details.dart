part of openapi.api;

class ResFileDownloadChunksDetails {
  
  bool retval = null;
  
  FileChunksInfo info = null;
  ResFileDownloadChunksDetails();

  @override
  String toString() {
    return 'ResFileDownloadChunksDetails[retval=$retval, info=$info, ]';
  }

  ResFileDownloadChunksDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      FileChunksInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResFileDownloadChunksDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileDownloadChunksDetails>() : json.map((value) => ResFileDownloadChunksDetails.fromJson(value)).toList();
  }

  static Map<String, ResFileDownloadChunksDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileDownloadChunksDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileDownloadChunksDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileDownloadChunksDetails-objects as value to a dart map
  static Map<String, List<ResFileDownloadChunksDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileDownloadChunksDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileDownloadChunksDetails.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResCreateCircle {
  
  bool retval = null;
  
  String circleId = null;
  ResCreateCircle();

  @override
  String toString() {
    return 'ResCreateCircle[retval=$retval, circleId=$circleId, ]';
  }

  ResCreateCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ResCreateCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateCircle>() : json.map((value) => ResCreateCircle.fromJson(value)).toList();
  }

  static Map<String, ResCreateCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateCircle-objects as value to a dart map
  static Map<String, List<ResCreateCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateCircle.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqUpdateIdentity {
  
  RsGxsIdGroup identityData = null;
  ReqUpdateIdentity();

  @override
  String toString() {
    return 'ReqUpdateIdentity[identityData=$identityData, ]';
  }

  ReqUpdateIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    identityData = (json['identityData'] == null) ?
      null :
      RsGxsIdGroup.fromJson(json['identityData']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (identityData != null)
      json['identityData'] = identityData;
    return json;
  }

  static List<ReqUpdateIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqUpdateIdentity>() : json.map((value) => ReqUpdateIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqUpdateIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqUpdateIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqUpdateIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqUpdateIdentity-objects as value to a dart map
  static Map<String, List<ReqUpdateIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqUpdateIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqUpdateIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


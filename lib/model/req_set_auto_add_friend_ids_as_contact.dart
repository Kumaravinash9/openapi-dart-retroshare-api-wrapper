part of openapi.api;

class ReqSetAutoAddFriendIdsAsContact {
  
  bool enabled = null;
  ReqSetAutoAddFriendIdsAsContact();

  @override
  String toString() {
    return 'ReqSetAutoAddFriendIdsAsContact[enabled=$enabled, ]';
  }

  ReqSetAutoAddFriendIdsAsContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    enabled = json['enabled'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (enabled != null)
      json['enabled'] = enabled;
    return json;
  }

  static List<ReqSetAutoAddFriendIdsAsContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetAutoAddFriendIdsAsContact>() : json.map((value) => ReqSetAutoAddFriendIdsAsContact.fromJson(value)).toList();
  }

  static Map<String, ReqSetAutoAddFriendIdsAsContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetAutoAddFriendIdsAsContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetAutoAddFriendIdsAsContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetAutoAddFriendIdsAsContact-objects as value to a dart map
  static Map<String, List<ReqSetAutoAddFriendIdsAsContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetAutoAddFriendIdsAsContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetAutoAddFriendIdsAsContact.listFromJson(value);
       });
     }
     return map;
  }
}


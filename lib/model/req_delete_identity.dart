part of openapi.api;

class ReqDeleteIdentity {
  
  String id = null;
  ReqDeleteIdentity();

  @override
  String toString() {
    return 'ReqDeleteIdentity[id=$id, ]';
  }

  ReqDeleteIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqDeleteIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqDeleteIdentity>() : json.map((value) => ReqDeleteIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqDeleteIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqDeleteIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqDeleteIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqDeleteIdentity-objects as value to a dart map
  static Map<String, List<ReqDeleteIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqDeleteIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqDeleteIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


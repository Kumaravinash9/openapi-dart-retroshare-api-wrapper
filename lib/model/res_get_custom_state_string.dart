part of openapi.api;

class ResGetCustomStateString {
  
  String retval = null;
  ResGetCustomStateString();

  @override
  String toString() {
    return 'ResGetCustomStateString[retval=$retval, ]';
  }

  ResGetCustomStateString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetCustomStateString> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCustomStateString>() : json.map((value) => ResGetCustomStateString.fromJson(value)).toList();
  }

  static Map<String, ResGetCustomStateString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCustomStateString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCustomStateString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCustomStateString-objects as value to a dart map
  static Map<String, List<ResGetCustomStateString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCustomStateString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCustomStateString.listFromJson(value);
       });
     }
     return map;
  }
}


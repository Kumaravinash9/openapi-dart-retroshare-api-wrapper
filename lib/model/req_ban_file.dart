part of openapi.api;

class ReqBanFile {
  
  String realFileHash = null;
  
  String filename = null;
  
  ReqBanFileFileSize fileSize = null;
  ReqBanFile();

  @override
  String toString() {
    return 'ReqBanFile[realFileHash=$realFileHash, filename=$filename, fileSize=$fileSize, ]';
  }

  ReqBanFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    realFileHash = json['realFileHash'];
    filename = json['filename'];
    fileSize = (json['fileSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['fileSize']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (realFileHash != null)
      json['realFileHash'] = realFileHash;
    if (filename != null)
      json['filename'] = filename;
    if (fileSize != null)
      json['fileSize'] = fileSize;
    return json;
  }

  static List<ReqBanFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqBanFile>() : json.map((value) => ReqBanFile.fromJson(value)).toList();
  }

  static Map<String, ReqBanFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqBanFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqBanFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqBanFile-objects as value to a dart map
  static Map<String, List<ReqBanFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqBanFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqBanFile.listFromJson(value);
       });
     }
     return map;
  }
}


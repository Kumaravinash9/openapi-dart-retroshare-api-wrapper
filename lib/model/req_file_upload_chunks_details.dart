part of openapi.api;

class ReqFileUploadChunksDetails {
  
  String hash = null;
  
  String peerId = null;
  ReqFileUploadChunksDetails();

  @override
  String toString() {
    return 'ReqFileUploadChunksDetails[hash=$hash, peerId=$peerId, ]';
  }

  ReqFileUploadChunksDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    peerId = json['peerId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (peerId != null)
      json['peerId'] = peerId;
    return json;
  }

  static List<ReqFileUploadChunksDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileUploadChunksDetails>() : json.map((value) => ReqFileUploadChunksDetails.fromJson(value)).toList();
  }

  static Map<String, ReqFileUploadChunksDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileUploadChunksDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileUploadChunksDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileUploadChunksDetails-objects as value to a dart map
  static Map<String, List<ReqFileUploadChunksDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileUploadChunksDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileUploadChunksDetails.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqOverallReputationLevel {
  
  String id = null;
  ReqOverallReputationLevel();

  @override
  String toString() {
    return 'ReqOverallReputationLevel[id=$id, ]';
  }

  ReqOverallReputationLevel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqOverallReputationLevel> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqOverallReputationLevel>() : json.map((value) => ReqOverallReputationLevel.fromJson(value)).toList();
  }

  static Map<String, ReqOverallReputationLevel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqOverallReputationLevel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqOverallReputationLevel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqOverallReputationLevel-objects as value to a dart map
  static Map<String, List<ReqOverallReputationLevel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqOverallReputationLevel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqOverallReputationLevel.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetPartialsDirectory {
  
  String retval = null;
  ResGetPartialsDirectory();

  @override
  String toString() {
    return 'ResGetPartialsDirectory[retval=$retval, ]';
  }

  ResGetPartialsDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetPartialsDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPartialsDirectory>() : json.map((value) => ResGetPartialsDirectory.fromJson(value)).toList();
  }

  static Map<String, ResGetPartialsDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPartialsDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPartialsDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPartialsDirectory-objects as value to a dart map
  static Map<String, List<ResGetPartialsDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPartialsDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPartialsDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


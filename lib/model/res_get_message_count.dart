part of openapi.api;

class ResGetMessageCount {
  
  int nInbox = null;
  
  int nInboxNew = null;
  
  int nOutbox = null;
  
  int nDraftbox = null;
  
  int nSentbox = null;
  
  int nTrashbox = null;
  ResGetMessageCount();

  @override
  String toString() {
    return 'ResGetMessageCount[nInbox=$nInbox, nInboxNew=$nInboxNew, nOutbox=$nOutbox, nDraftbox=$nDraftbox, nSentbox=$nSentbox, nTrashbox=$nTrashbox, ]';
  }

  ResGetMessageCount.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    nInbox = json['nInbox'];
    nInboxNew = json['nInboxNew'];
    nOutbox = json['nOutbox'];
    nDraftbox = json['nDraftbox'];
    nSentbox = json['nSentbox'];
    nTrashbox = json['nTrashbox'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (nInbox != null)
      json['nInbox'] = nInbox;
    if (nInboxNew != null)
      json['nInboxNew'] = nInboxNew;
    if (nOutbox != null)
      json['nOutbox'] = nOutbox;
    if (nDraftbox != null)
      json['nDraftbox'] = nDraftbox;
    if (nSentbox != null)
      json['nSentbox'] = nSentbox;
    if (nTrashbox != null)
      json['nTrashbox'] = nTrashbox;
    return json;
  }

  static List<ResGetMessageCount> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMessageCount>() : json.map((value) => ResGetMessageCount.fromJson(value)).toList();
  }

  static Map<String, ResGetMessageCount> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMessageCount>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMessageCount.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMessageCount-objects as value to a dart map
  static Map<String, List<ResGetMessageCount>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMessageCount>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMessageCount.listFromJson(value);
       });
     }
     return map;
  }
}


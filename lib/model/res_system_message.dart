part of openapi.api;

class ResSystemMessage {
  
  bool retval = null;
  ResSystemMessage();

  @override
  String toString() {
    return 'ResSystemMessage[retval=$retval, ]';
  }

  ResSystemMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSystemMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSystemMessage>() : json.map((value) => ResSystemMessage.fromJson(value)).toList();
  }

  static Map<String, ResSystemMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSystemMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSystemMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSystemMessage-objects as value to a dart map
  static Map<String, List<ResSystemMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSystemMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSystemMessage.listFromJson(value);
       });
     }
     return map;
  }
}


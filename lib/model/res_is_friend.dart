part of openapi.api;

class ResIsFriend {
  
  bool retval = null;
  ResIsFriend();

  @override
  String toString() {
    return 'ResIsFriend[retval=$retval, ]';
  }

  ResIsFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsFriend>() : json.map((value) => ResIsFriend.fromJson(value)).toList();
  }

  static Map<String, ResIsFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsFriend-objects as value to a dart map
  static Map<String, List<ResIsFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsFriend.listFromJson(value);
       });
     }
     return map;
  }
}


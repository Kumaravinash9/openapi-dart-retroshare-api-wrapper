part of openapi.api;

class DistantChatPeerInfo {
  
  String toId = null;
  
  String ownId = null;
  
  String peerId = null;
  
  int status = null;
  
  int pendingItems = null;
  DistantChatPeerInfo();

  @override
  String toString() {
    return 'DistantChatPeerInfo[toId=$toId, ownId=$ownId, peerId=$peerId, status=$status, pendingItems=$pendingItems, ]';
  }

  DistantChatPeerInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    toId = json['to_id'];
    ownId = json['own_id'];
    peerId = json['peer_id'];
    status = json['status'];
    pendingItems = json['pending_items'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (toId != null)
      json['to_id'] = toId;
    if (ownId != null)
      json['own_id'] = ownId;
    if (peerId != null)
      json['peer_id'] = peerId;
    if (status != null)
      json['status'] = status;
    if (pendingItems != null)
      json['pending_items'] = pendingItems;
    return json;
  }

  static List<DistantChatPeerInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<DistantChatPeerInfo>() : json.map((value) => DistantChatPeerInfo.fromJson(value)).toList();
  }

  static Map<String, DistantChatPeerInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, DistantChatPeerInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = DistantChatPeerInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DistantChatPeerInfo-objects as value to a dart map
  static Map<String, List<DistantChatPeerInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<DistantChatPeerInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = DistantChatPeerInfo.listFromJson(value);
       });
     }
     return map;
  }
}


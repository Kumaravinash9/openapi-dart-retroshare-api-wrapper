part of openapi.api;

class ReqRequestCircleMembership {
  
  String ownGxsId = null;
  
  String circleId = null;
  ReqRequestCircleMembership();

  @override
  String toString() {
    return 'ReqRequestCircleMembership[ownGxsId=$ownGxsId, circleId=$circleId, ]';
  }

  ReqRequestCircleMembership.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    ownGxsId = json['ownGxsId'];
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (ownGxsId != null)
      json['ownGxsId'] = ownGxsId;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqRequestCircleMembership> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestCircleMembership>() : json.map((value) => ReqRequestCircleMembership.fromJson(value)).toList();
  }

  static Map<String, ReqRequestCircleMembership> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestCircleMembership>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestCircleMembership.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestCircleMembership-objects as value to a dart map
  static Map<String, List<ReqRequestCircleMembership>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestCircleMembership>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestCircleMembership.listFromJson(value);
       });
     }
     return map;
  }
}


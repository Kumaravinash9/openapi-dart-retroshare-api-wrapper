part of openapi.api;

class ResGetMessageSummaries {
  
  bool retval = null;
  
  List<RsMsgsMsgInfoSummary> msgList = [];
  ResGetMessageSummaries();

  @override
  String toString() {
    return 'ResGetMessageSummaries[retval=$retval, msgList=$msgList, ]';
  }

  ResGetMessageSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msgList = (json['msgList'] == null) ?
      null :
      RsMsgsMsgInfoSummary.listFromJson(json['msgList']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msgList != null)
      json['msgList'] = msgList;
    return json;
  }

  static List<ResGetMessageSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMessageSummaries>() : json.map((value) => ResGetMessageSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetMessageSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMessageSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMessageSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMessageSummaries-objects as value to a dart map
  static Map<String, List<ResGetMessageSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMessageSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMessageSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqUpdateServicePermissions {
  
  int serviceId = null;
  
  RsServicePermissions permissions = null;
  ReqUpdateServicePermissions();

  @override
  String toString() {
    return 'ReqUpdateServicePermissions[serviceId=$serviceId, permissions=$permissions, ]';
  }

  ReqUpdateServicePermissions.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    serviceId = json['serviceId'];
    permissions = (json['permissions'] == null) ?
      null :
      RsServicePermissions.fromJson(json['permissions']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (serviceId != null)
      json['serviceId'] = serviceId;
    if (permissions != null)
      json['permissions'] = permissions;
    return json;
  }

  static List<ReqUpdateServicePermissions> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqUpdateServicePermissions>() : json.map((value) => ReqUpdateServicePermissions.fromJson(value)).toList();
  }

  static Map<String, ReqUpdateServicePermissions> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqUpdateServicePermissions>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqUpdateServicePermissions.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqUpdateServicePermissions-objects as value to a dart map
  static Map<String, List<ReqUpdateServicePermissions>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqUpdateServicePermissions>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqUpdateServicePermissions.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResSetDestinationDirectory {
  
  bool retval = null;
  ResSetDestinationDirectory();

  @override
  String toString() {
    return 'ResSetDestinationDirectory[retval=$retval, ]';
  }

  ResSetDestinationDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetDestinationDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetDestinationDirectory>() : json.map((value) => ResSetDestinationDirectory.fromJson(value)).toList();
  }

  static Map<String, ResSetDestinationDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetDestinationDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetDestinationDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetDestinationDirectory-objects as value to a dart map
  static Map<String, List<ResSetDestinationDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetDestinationDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetDestinationDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


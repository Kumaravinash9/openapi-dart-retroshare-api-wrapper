part of openapi.api;

class ResCreateLocation {
  
  bool retval = null;
  
  RsLoginHelperLocation location = null;
  
  String errorMessage = null;
  ResCreateLocation();

  @override
  String toString() {
    return 'ResCreateLocation[retval=$retval, location=$location, errorMessage=$errorMessage, ]';
  }

  ResCreateLocation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    location = (json['location'] == null) ?
      null :
      RsLoginHelperLocation.fromJson(json['location']);
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (location != null)
      json['location'] = location;
    if (errorMessage != null)
      json['errorMessage'] = errorMessage;
    return json;
  }

  static List<ResCreateLocation> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateLocation>() : json.map((value) => ResCreateLocation.fromJson(value)).toList();
  }

  static Map<String, ResCreateLocation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateLocation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateLocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateLocation-objects as value to a dart map
  static Map<String, List<ResCreateLocation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateLocation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateLocation.listFromJson(value);
       });
     }
     return map;
  }
}


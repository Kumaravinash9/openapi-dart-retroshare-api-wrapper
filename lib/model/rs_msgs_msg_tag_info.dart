part of openapi.api;

class RsMsgsMsgTagInfo {
  
  String msgId = null;
  
  List<int> tagIds = [];
  RsMsgsMsgTagInfo();

  @override
  String toString() {
    return 'RsMsgsMsgTagInfo[msgId=$msgId, tagIds=$tagIds, ]';
  }

  RsMsgsMsgTagInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    tagIds = (json['tagIds'] == null) ?
      null :
      (json['tagIds'] as List).cast<int>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (tagIds != null)
      json['tagIds'] = tagIds;
    return json;
  }

  static List<RsMsgsMsgTagInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMsgTagInfo>() : json.map((value) => RsMsgsMsgTagInfo.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMsgTagInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMsgTagInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMsgTagInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMsgTagInfo-objects as value to a dart map
  static Map<String, List<RsMsgsMsgTagInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMsgTagInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMsgTagInfo.listFromJson(value);
       });
     }
     return map;
  }
}


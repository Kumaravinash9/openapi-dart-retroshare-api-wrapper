part of openapi.api;

class ResAddGroup {
  
  bool retval = null;
  ResAddGroup();

  @override
  String toString() {
    return 'ResAddGroup[retval=$retval, ]';
  }

  ResAddGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAddGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAddGroup>() : json.map((value) => ResAddGroup.fromJson(value)).toList();
  }

  static Map<String, ResAddGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAddGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAddGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAddGroup-objects as value to a dart map
  static Map<String, List<ResAddGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAddGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAddGroup.listFromJson(value);
       });
     }
     return map;
  }
}


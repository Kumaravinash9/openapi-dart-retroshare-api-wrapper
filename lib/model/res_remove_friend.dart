part of openapi.api;

class ResRemoveFriend {
  
  bool retval = null;
  ResRemoveFriend();

  @override
  String toString() {
    return 'ResRemoveFriend[retval=$retval, ]';
  }

  ResRemoveFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRemoveFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRemoveFriend>() : json.map((value) => ResRemoveFriend.fromJson(value)).toList();
  }

  static Map<String, ResRemoveFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRemoveFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRemoveFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRemoveFriend-objects as value to a dart map
  static Map<String, List<ResRemoveFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRemoveFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRemoveFriend.listFromJson(value);
       });
     }
     return map;
  }
}


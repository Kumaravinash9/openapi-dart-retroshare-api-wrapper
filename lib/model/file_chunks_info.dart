part of openapi.api;

class FileChunksInfo {
  
  ReqBanFileFileSize fileSize = null;
  
  int chunkSize = null;
  
  FileChunksInfoChunkStrategy strategy = null;
  //enum strategyEnum {  0,  1,  2,  };{
  
  List<ChunkState> chunks = [];
  
  List<FileChunksInfoCompressedPeerAvailabilityMaps> compressedPeerAvailabilityMaps = [];
  
  List<FileChunksInfoActiveChunks> activeChunks = [];
  
  List<FileChunksInfoPendingSlices> pendingSlices = [];
  FileChunksInfo();

  @override
  String toString() {
    return 'FileChunksInfo[fileSize=$fileSize, chunkSize=$chunkSize, strategy=$strategy, chunks=$chunks, compressedPeerAvailabilityMaps=$compressedPeerAvailabilityMaps, activeChunks=$activeChunks, pendingSlices=$pendingSlices, ]';
  }

  FileChunksInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fileSize = (json['file_size'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['file_size']);
    chunkSize = json['chunk_size'];
    strategy = (json['strategy'] == null) ?
      null :
      FileChunksInfoChunkStrategy.fromJson(json['strategy']);
    chunks = (json['chunks'] == null) ?
      null :
      ChunkState.listFromJson(json['chunks']);
    compressedPeerAvailabilityMaps = (json['compressed_peer_availability_maps'] == null) ?
      null :
      FileChunksInfoCompressedPeerAvailabilityMaps.listFromJson(json['compressed_peer_availability_maps']);
    activeChunks = (json['active_chunks'] == null) ?
      null :
      FileChunksInfoActiveChunks.listFromJson(json['active_chunks']);
    pendingSlices = (json['pending_slices'] == null) ?
      null :
      FileChunksInfoPendingSlices.listFromJson(json['pending_slices']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (fileSize != null)
      json['file_size'] = fileSize;
    if (chunkSize != null)
      json['chunk_size'] = chunkSize;
    if (strategy != null)
      json['strategy'] = strategy;
    if (chunks != null)
      json['chunks'] = chunks;
    if (compressedPeerAvailabilityMaps != null)
      json['compressed_peer_availability_maps'] = compressedPeerAvailabilityMaps;
    if (activeChunks != null)
      json['active_chunks'] = activeChunks;
    if (pendingSlices != null)
      json['pending_slices'] = pendingSlices;
    return json;
  }

  static List<FileChunksInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<FileChunksInfo>() : json.map((value) => FileChunksInfo.fromJson(value)).toList();
  }

  static Map<String, FileChunksInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, FileChunksInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = FileChunksInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of FileChunksInfo-objects as value to a dart map
  static Map<String, List<FileChunksInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<FileChunksInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = FileChunksInfo.listFromJson(value);
       });
     }
     return map;
  }
}


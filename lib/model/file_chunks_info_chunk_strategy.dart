part of openapi.api;

class FileChunksInfoChunkStrategy {
  /// The underlying value of this enum member.
  final int value;

  const FileChunksInfoChunkStrategy._internal(this.value);

  static const FileChunksInfoChunkStrategy number0_ = const FileChunksInfoChunkStrategy._internal(0);
  static const FileChunksInfoChunkStrategy number1_ = const FileChunksInfoChunkStrategy._internal(1);
  static const FileChunksInfoChunkStrategy number2_ = const FileChunksInfoChunkStrategy._internal(2);
  
  int toJson (){
    return this.value;
  }

  static FileChunksInfoChunkStrategy fromJson(int value) {
    return new FileChunksInfoChunkStrategyTypeTransformer().decode(value);
  }
  
  static List<FileChunksInfoChunkStrategy> listFromJson(List<dynamic> json) {
    return json == null ? new List<FileChunksInfoChunkStrategy>() : json.map((value) => FileChunksInfoChunkStrategy.fromJson(value)).toList();
  }
}

class FileChunksInfoChunkStrategyTypeTransformer {

  dynamic encode(FileChunksInfoChunkStrategy data) {
    return data.value;
  }

  FileChunksInfoChunkStrategy decode(dynamic data) {
    switch (data) {
      case 0: return FileChunksInfoChunkStrategy.number0_;
      case 1: return FileChunksInfoChunkStrategy.number1_;
      case 2: return FileChunksInfoChunkStrategy.number2_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


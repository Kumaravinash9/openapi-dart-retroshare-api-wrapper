part of openapi.api;

class ReqMessageForwarded {
  
  String msgId = null;
  
  bool forwarded = null;
  ReqMessageForwarded();

  @override
  String toString() {
    return 'ReqMessageForwarded[msgId=$msgId, forwarded=$forwarded, ]';
  }

  ReqMessageForwarded.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    forwarded = json['forwarded'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (forwarded != null)
      json['forwarded'] = forwarded;
    return json;
  }

  static List<ReqMessageForwarded> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageForwarded>() : json.map((value) => ReqMessageForwarded.fromJson(value)).toList();
  }

  static Map<String, ReqMessageForwarded> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageForwarded>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageForwarded.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageForwarded-objects as value to a dart map
  static Map<String, List<ReqMessageForwarded>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageForwarded>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageForwarded.listFromJson(value);
       });
     }
     return map;
  }
}


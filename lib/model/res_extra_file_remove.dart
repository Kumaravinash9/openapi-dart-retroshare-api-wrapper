part of openapi.api;

class ResExtraFileRemove {
  
  bool retval = null;
  ResExtraFileRemove();

  @override
  String toString() {
    return 'ResExtraFileRemove[retval=$retval, ]';
  }

  ResExtraFileRemove.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResExtraFileRemove> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExtraFileRemove>() : json.map((value) => ResExtraFileRemove.fromJson(value)).toList();
  }

  static Map<String, ResExtraFileRemove> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExtraFileRemove>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExtraFileRemove.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExtraFileRemove-objects as value to a dart map
  static Map<String, List<ResExtraFileRemove>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExtraFileRemove>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExtraFileRemove.listFromJson(value);
       });
     }
     return map;
  }
}


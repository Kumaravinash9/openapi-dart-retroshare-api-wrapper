part of openapi.api;

class ResRequestDirDetails {
  
  bool retval = null;
  
  DirDetails details = null;
  ResRequestDirDetails();

  @override
  String toString() {
    return 'ResRequestDirDetails[retval=$retval, details=$details, ]';
  }

  ResRequestDirDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    details = (json['details'] == null) ?
      null :
      DirDetails.fromJson(json['details']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (details != null)
      json['details'] = details;
    return json;
  }

  static List<ResRequestDirDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRequestDirDetails>() : json.map((value) => ResRequestDirDetails.fromJson(value)).toList();
  }

  static Map<String, ResRequestDirDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRequestDirDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRequestDirDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRequestDirDetails-objects as value to a dart map
  static Map<String, List<ResRequestDirDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRequestDirDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRequestDirDetails.listFromJson(value);
       });
     }
     return map;
  }
}


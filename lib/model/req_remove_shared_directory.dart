part of openapi.api;

class ReqRemoveSharedDirectory {
  
  String dir = null;
  ReqRemoveSharedDirectory();

  @override
  String toString() {
    return 'ReqRemoveSharedDirectory[dir=$dir, ]';
  }

  ReqRemoveSharedDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dir = json['dir'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (dir != null)
      json['dir'] = dir;
    return json;
  }

  static List<ReqRemoveSharedDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRemoveSharedDirectory>() : json.map((value) => ReqRemoveSharedDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqRemoveSharedDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRemoveSharedDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRemoveSharedDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRemoveSharedDirectory-objects as value to a dart map
  static Map<String, List<ReqRemoveSharedDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRemoveSharedDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRemoveSharedDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


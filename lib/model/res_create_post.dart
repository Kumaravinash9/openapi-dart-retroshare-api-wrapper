part of openapi.api;

class ResCreatePost {
  
  bool retval = null;
  
  RsGxsChannelPost post = null;
  ResCreatePost();

  @override
  String toString() {
    return 'ResCreatePost[retval=$retval, post=$post, ]';
  }

  ResCreatePost.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    post = (json['post'] == null) ?
      null :
      RsGxsChannelPost.fromJson(json['post']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (post != null)
      json['post'] = post;
    return json;
  }

  static List<ResCreatePost> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreatePost>() : json.map((value) => ResCreatePost.fromJson(value)).toList();
  }

  static Map<String, ResCreatePost> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreatePost>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreatePost.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreatePost-objects as value to a dart map
  static Map<String, List<ResCreatePost>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreatePost>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreatePost.listFromJson(value);
       });
     }
     return map;
  }
}


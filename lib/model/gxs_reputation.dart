part of openapi.api;

class GxsReputation {
  
  int mOverallScore = null;
  
  int mIdScore = null;
  
  int mOwnOpinion = null;
  
  int mPeerOpinion = null;
  GxsReputation();

  @override
  String toString() {
    return 'GxsReputation[mOverallScore=$mOverallScore, mIdScore=$mIdScore, mOwnOpinion=$mOwnOpinion, mPeerOpinion=$mPeerOpinion, ]';
  }

  GxsReputation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mOverallScore = json['mOverallScore'];
    mIdScore = json['mIdScore'];
    mOwnOpinion = json['mOwnOpinion'];
    mPeerOpinion = json['mPeerOpinion'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mOverallScore != null)
      json['mOverallScore'] = mOverallScore;
    if (mIdScore != null)
      json['mIdScore'] = mIdScore;
    if (mOwnOpinion != null)
      json['mOwnOpinion'] = mOwnOpinion;
    if (mPeerOpinion != null)
      json['mPeerOpinion'] = mPeerOpinion;
    return json;
  }

  static List<GxsReputation> listFromJson(List<dynamic> json) {
    return json == null ? List<GxsReputation>() : json.map((value) => GxsReputation.fromJson(value)).toList();
  }

  static Map<String, GxsReputation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, GxsReputation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = GxsReputation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of GxsReputation-objects as value to a dart map
  static Map<String, List<GxsReputation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<GxsReputation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = GxsReputation.listFromJson(value);
       });
     }
     return map;
  }
}


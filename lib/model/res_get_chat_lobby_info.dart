part of openapi.api;

class ResGetChatLobbyInfo {
  
  bool retval = null;
  
  ChatLobbyInfo info = null;
  ResGetChatLobbyInfo();

  @override
  String toString() {
    return 'ResGetChatLobbyInfo[retval=$retval, info=$info, ]';
  }

  ResGetChatLobbyInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      ChatLobbyInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetChatLobbyInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChatLobbyInfo>() : json.map((value) => ResGetChatLobbyInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetChatLobbyInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChatLobbyInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChatLobbyInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChatLobbyInfo-objects as value to a dart map
  static Map<String, List<ResGetChatLobbyInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChatLobbyInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChatLobbyInfo.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqFileDetails {
  
  String hash = null;
  
  int hintflags = null;
  ReqFileDetails();

  @override
  String toString() {
    return 'ReqFileDetails[hash=$hash, hintflags=$hintflags, ]';
  }

  ReqFileDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    hintflags = json['hintflags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (hintflags != null)
      json['hintflags'] = hintflags;
    return json;
  }

  static List<ReqFileDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileDetails>() : json.map((value) => ReqFileDetails.fromJson(value)).toList();
  }

  static Map<String, ReqFileDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileDetails-objects as value to a dart map
  static Map<String, List<ReqFileDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileDetails.listFromJson(value);
       });
     }
     return map;
  }
}


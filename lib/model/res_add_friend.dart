part of openapi.api;

class ResAddFriend {
  
  bool retval = null;
  ResAddFriend();

  @override
  String toString() {
    return 'ResAddFriend[retval=$retval, ]';
  }

  ResAddFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAddFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAddFriend>() : json.map((value) => ResAddFriend.fromJson(value)).toList();
  }

  static Map<String, ResAddFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAddFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAddFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAddFriend-objects as value to a dart map
  static Map<String, List<ResAddFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAddFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAddFriend.listFromJson(value);
       });
     }
     return map;
  }
}


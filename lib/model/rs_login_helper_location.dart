part of openapi.api;

class RsLoginHelperLocation {
  
  String mLocationId = null;
  
  String mPgpId = null;
  
  String mLocationName = null;
  
  String mPgpName = null;
  RsLoginHelperLocation();

  @override
  String toString() {
    return 'RsLoginHelperLocation[mLocationId=$mLocationId, mPgpId=$mPgpId, mLocationName=$mLocationName, mPgpName=$mPgpName, ]';
  }

  RsLoginHelperLocation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mLocationId = json['mLocationId'];
    mPgpId = json['mPgpId'];
    mLocationName = json['mLocationName'];
    mPgpName = json['mPgpName'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mLocationId != null)
      json['mLocationId'] = mLocationId;
    if (mPgpId != null)
      json['mPgpId'] = mPgpId;
    if (mLocationName != null)
      json['mLocationName'] = mLocationName;
    if (mPgpName != null)
      json['mPgpName'] = mPgpName;
    return json;
  }

  static List<RsLoginHelperLocation> listFromJson(List<dynamic> json) {
    return json == null ? List<RsLoginHelperLocation>() : json.map((value) => RsLoginHelperLocation.fromJson(value)).toList();
  }

  static Map<String, RsLoginHelperLocation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsLoginHelperLocation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsLoginHelperLocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsLoginHelperLocation-objects as value to a dart map
  static Map<String, List<RsLoginHelperLocation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsLoginHelperLocation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsLoginHelperLocation.listFromJson(value);
       });
     }
     return map;
  }
}


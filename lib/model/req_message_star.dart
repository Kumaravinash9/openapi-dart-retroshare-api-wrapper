part of openapi.api;

class ReqMessageStar {
  
  String msgId = null;
  
  bool mark = null;
  ReqMessageStar();

  @override
  String toString() {
    return 'ReqMessageStar[msgId=$msgId, mark=$mark, ]';
  }

  ReqMessageStar.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    mark = json['mark'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (mark != null)
      json['mark'] = mark;
    return json;
  }

  static List<ReqMessageStar> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageStar>() : json.map((value) => ReqMessageStar.fromJson(value)).toList();
  }

  static Map<String, ReqMessageStar> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageStar>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageStar.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageStar-objects as value to a dart map
  static Map<String, List<ReqMessageStar>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageStar>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageStar.listFromJson(value);
       });
     }
     return map;
  }
}


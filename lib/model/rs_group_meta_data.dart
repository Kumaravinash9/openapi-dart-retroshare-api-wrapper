part of openapi.api;

class RsGroupMetaData {
  
  String mGroupId = null;
  
  String mGroupName = null;
  
  int mGroupFlags = null;
  
  int mSignFlags = null;
  
  RstimeT mPublishTs = null;
  
  String mAuthorId = null;
  
  String mCircleId = null;
  
  int mCircleType = null;
  
  int mAuthenFlags = null;
  
  String mParentGrpId = null;
  
  int mSubscribeFlags = null;
  
  int mPop = null;
  
  int mVisibleMsgCount = null;
  
  RstimeT mLastPost = null;
  
  int mGroupStatus = null;
  
  String mServiceString = null;
  
  String mOriginator = null;
  
  String mInternalCircle = null;
  RsGroupMetaData();

  @override
  String toString() {
    return 'RsGroupMetaData[mGroupId=$mGroupId, mGroupName=$mGroupName, mGroupFlags=$mGroupFlags, mSignFlags=$mSignFlags, mPublishTs=$mPublishTs, mAuthorId=$mAuthorId, mCircleId=$mCircleId, mCircleType=$mCircleType, mAuthenFlags=$mAuthenFlags, mParentGrpId=$mParentGrpId, mSubscribeFlags=$mSubscribeFlags, mPop=$mPop, mVisibleMsgCount=$mVisibleMsgCount, mLastPost=$mLastPost, mGroupStatus=$mGroupStatus, mServiceString=$mServiceString, mOriginator=$mOriginator, mInternalCircle=$mInternalCircle, ]';
  }

  RsGroupMetaData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mGroupId = json['mGroupId'];
    mGroupName = json['mGroupName'];
    mGroupFlags = json['mGroupFlags'];
    mSignFlags = json['mSignFlags'];
    mPublishTs = (json['mPublishTs'] == null) ?
      null :
      RstimeT.fromJson(json['mPublishTs']);
    mAuthorId = json['mAuthorId'];
    mCircleId = json['mCircleId'];
    mCircleType = json['mCircleType'];
    mAuthenFlags = json['mAuthenFlags'];
    mParentGrpId = json['mParentGrpId'];
    mSubscribeFlags = json['mSubscribeFlags'];
    mPop = json['mPop'];
    mVisibleMsgCount = json['mVisibleMsgCount'];
    mLastPost = (json['mLastPost'] == null) ?
      null :
      RstimeT.fromJson(json['mLastPost']);
    mGroupStatus = json['mGroupStatus'];
    mServiceString = json['mServiceString'];
    mOriginator = json['mOriginator'];
    mInternalCircle = json['mInternalCircle'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mGroupId != null)
      json['mGroupId'] = mGroupId;
    if (mGroupName != null)
      json['mGroupName'] = mGroupName;
    if (mGroupFlags != null)
      json['mGroupFlags'] = mGroupFlags;
    if (mSignFlags != null)
      json['mSignFlags'] = mSignFlags;
    if (mPublishTs != null)
      json['mPublishTs'] = mPublishTs;
    if (mAuthorId != null)
      json['mAuthorId'] = mAuthorId;
    if (mCircleId != null)
      json['mCircleId'] = mCircleId;
    if (mCircleType != null)
      json['mCircleType'] = mCircleType;
    if (mAuthenFlags != null)
      json['mAuthenFlags'] = mAuthenFlags;
    if (mParentGrpId != null)
      json['mParentGrpId'] = mParentGrpId;
    if (mSubscribeFlags != null)
      json['mSubscribeFlags'] = mSubscribeFlags;
    if (mPop != null)
      json['mPop'] = mPop;
    if (mVisibleMsgCount != null)
      json['mVisibleMsgCount'] = mVisibleMsgCount;
    if (mLastPost != null)
      json['mLastPost'] = mLastPost;
    if (mGroupStatus != null)
      json['mGroupStatus'] = mGroupStatus;
    if (mServiceString != null)
      json['mServiceString'] = mServiceString;
    if (mOriginator != null)
      json['mOriginator'] = mOriginator;
    if (mInternalCircle != null)
      json['mInternalCircle'] = mInternalCircle;
    return json;
  }

  static List<RsGroupMetaData> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGroupMetaData>() : json.map((value) => RsGroupMetaData.fromJson(value)).toList();
  }

  static Map<String, RsGroupMetaData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGroupMetaData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGroupMetaData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGroupMetaData-objects as value to a dart map
  static Map<String, List<RsGroupMetaData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGroupMetaData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGroupMetaData.listFromJson(value);
       });
     }
     return map;
  }
}


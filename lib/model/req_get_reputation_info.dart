part of openapi.api;

class ReqGetReputationInfo {
  
  String id = null;
  
  String ownerNode = null;
  
  bool stamp = null;
  ReqGetReputationInfo();

  @override
  String toString() {
    return 'ReqGetReputationInfo[id=$id, ownerNode=$ownerNode, stamp=$stamp, ]';
  }

  ReqGetReputationInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    ownerNode = json['ownerNode'];
    stamp = json['stamp'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (ownerNode != null)
      json['ownerNode'] = ownerNode;
    if (stamp != null)
      json['stamp'] = stamp;
    return json;
  }

  static List<ReqGetReputationInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetReputationInfo>() : json.map((value) => ReqGetReputationInfo.fromJson(value)).toList();
  }

  static Map<String, ReqGetReputationInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetReputationInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetReputationInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetReputationInfo-objects as value to a dart map
  static Map<String, List<ReqGetReputationInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetReputationInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetReputationInfo.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetDownloadDirectory {
  
  String retval = null;
  ResGetDownloadDirectory();

  @override
  String toString() {
    return 'ResGetDownloadDirectory[retval=$retval, ]';
  }

  ResGetDownloadDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetDownloadDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDownloadDirectory>() : json.map((value) => ResGetDownloadDirectory.fromJson(value)).toList();
  }

  static Map<String, ResGetDownloadDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDownloadDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDownloadDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDownloadDirectory-objects as value to a dart map
  static Map<String, List<ResGetDownloadDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDownloadDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDownloadDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSetDestinationDirectory {
  
  String hash = null;
  
  String newPath = null;
  ReqSetDestinationDirectory();

  @override
  String toString() {
    return 'ReqSetDestinationDirectory[hash=$hash, newPath=$newPath, ]';
  }

  ReqSetDestinationDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    newPath = json['newPath'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (newPath != null)
      json['newPath'] = newPath;
    return json;
  }

  static List<ReqSetDestinationDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDestinationDirectory>() : json.map((value) => ReqSetDestinationDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqSetDestinationDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDestinationDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDestinationDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDestinationDirectory-objects as value to a dart map
  static Map<String, List<ReqSetDestinationDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDestinationDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDestinationDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


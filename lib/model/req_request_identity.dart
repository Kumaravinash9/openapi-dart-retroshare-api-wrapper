part of openapi.api;

class ReqRequestIdentity {
  
  String id = null;
  
  List<String> peers = [];
  ReqRequestIdentity();

  @override
  String toString() {
    return 'ReqRequestIdentity[id=$id, peers=$peers, ]';
  }

  ReqRequestIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    peers = (json['peers'] == null) ?
      null :
      (json['peers'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (peers != null)
      json['peers'] = peers;
    return json;
  }

  static List<ReqRequestIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestIdentity>() : json.map((value) => ReqRequestIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqRequestIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestIdentity-objects as value to a dart map
  static Map<String, List<ReqRequestIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


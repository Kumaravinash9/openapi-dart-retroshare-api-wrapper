part of openapi.api;

class ResIsMulticastListeningEnabled {
  
  bool retval = null;
  ResIsMulticastListeningEnabled();

  @override
  String toString() {
    return 'ResIsMulticastListeningEnabled[retval=$retval, ]';
  }

  ResIsMulticastListeningEnabled.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsMulticastListeningEnabled> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsMulticastListeningEnabled>() : json.map((value) => ResIsMulticastListeningEnabled.fromJson(value)).toList();
  }

  static Map<String, ResIsMulticastListeningEnabled> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsMulticastListeningEnabled>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsMulticastListeningEnabled.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsMulticastListeningEnabled-objects as value to a dart map
  static Map<String, List<ResIsMulticastListeningEnabled>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsMulticastListeningEnabled>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsMulticastListeningEnabled.listFromJson(value);
       });
     }
     return map;
  }
}


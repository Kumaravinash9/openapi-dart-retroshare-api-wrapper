part of openapi.api;

class ReqAddSharedDirectory {
  
  SharedDirInfo dir = null;
  ReqAddSharedDirectory();

  @override
  String toString() {
    return 'ReqAddSharedDirectory[dir=$dir, ]';
  }

  ReqAddSharedDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dir = (json['dir'] == null) ?
      null :
      SharedDirInfo.fromJson(json['dir']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (dir != null)
      json['dir'] = dir;
    return json;
  }

  static List<ReqAddSharedDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAddSharedDirectory>() : json.map((value) => ReqAddSharedDirectory.fromJson(value)).toList();
  }

  static Map<String, ReqAddSharedDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAddSharedDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAddSharedDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAddSharedDirectory-objects as value to a dart map
  static Map<String, List<ReqAddSharedDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAddSharedDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAddSharedDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class RsMsgsMsgTagType {
  
  List<RsMsgsMsgTagTypeTypes> types = [];
  RsMsgsMsgTagType();

  @override
  String toString() {
    return 'RsMsgsMsgTagType[types=$types, ]';
  }

  RsMsgsMsgTagType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    types = (json['types'] == null) ?
      null :
      RsMsgsMsgTagTypeTypes.listFromJson(json['types']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (types != null)
      json['types'] = types;
    return json;
  }

  static List<RsMsgsMsgTagType> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMsgTagType>() : json.map((value) => RsMsgsMsgTagType.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMsgTagType> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMsgTagType>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMsgTagType.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMsgTagType-objects as value to a dart map
  static Map<String, List<RsMsgsMsgTagType>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMsgTagType>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMsgTagType.listFromJson(value);
       });
     }
     return map;
  }
}


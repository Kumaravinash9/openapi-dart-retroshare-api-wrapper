part of openapi.api;

class ResCreateMessage {
  
  bool retval = null;
  
  RsGxsForumMsg message = null;
  ResCreateMessage();

  @override
  String toString() {
    return 'ResCreateMessage[retval=$retval, message=$message, ]';
  }

  ResCreateMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    message = (json['message'] == null) ?
      null :
      RsGxsForumMsg.fromJson(json['message']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (message != null)
      json['message'] = message;
    return json;
  }

  static List<ResCreateMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateMessage>() : json.map((value) => ResCreateMessage.fromJson(value)).toList();
  }

  static Map<String, ResCreateMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateMessage-objects as value to a dart map
  static Map<String, List<ResCreateMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateMessage.listFromJson(value);
       });
     }
     return map;
  }
}


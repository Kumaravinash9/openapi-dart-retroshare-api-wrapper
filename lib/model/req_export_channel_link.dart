part of openapi.api;

class ReqExportChannelLink {
  
  String chanId = null;
  
  bool includeGxsData = null;
  
  String baseUrl = null;
  ReqExportChannelLink();

  @override
  String toString() {
    return 'ReqExportChannelLink[chanId=$chanId, includeGxsData=$includeGxsData, baseUrl=$baseUrl, ]';
  }

  ReqExportChannelLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    chanId = json['chanId'];
    includeGxsData = json['includeGxsData'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (chanId != null)
      json['chanId'] = chanId;
    if (includeGxsData != null)
      json['includeGxsData'] = includeGxsData;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportChannelLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportChannelLink>() : json.map((value) => ReqExportChannelLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportChannelLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportChannelLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportChannelLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportChannelLink-objects as value to a dart map
  static Map<String, List<ReqExportChannelLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportChannelLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportChannelLink.listFromJson(value);
       });
     }
     return map;
  }
}


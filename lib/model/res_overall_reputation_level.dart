part of openapi.api;

class ResOverallReputationLevel {
  
  RsReputationLevel retval = null;
  //enum retvalEnum {  0,  1,  2,  3,  4,  5,  };{
  ResOverallReputationLevel();

  @override
  String toString() {
    return 'ResOverallReputationLevel[retval=$retval, ]';
  }

  ResOverallReputationLevel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      RsReputationLevel.fromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResOverallReputationLevel> listFromJson(List<dynamic> json) {
    return json == null ? List<ResOverallReputationLevel>() : json.map((value) => ResOverallReputationLevel.fromJson(value)).toList();
  }

  static Map<String, ResOverallReputationLevel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResOverallReputationLevel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResOverallReputationLevel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResOverallReputationLevel-objects as value to a dart map
  static Map<String, List<ResOverallReputationLevel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResOverallReputationLevel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResOverallReputationLevel.listFromJson(value);
       });
     }
     return map;
  }
}


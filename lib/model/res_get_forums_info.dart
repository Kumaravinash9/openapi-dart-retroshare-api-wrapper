part of openapi.api;

class ResGetForumsInfo {
  
  bool retval = null;
  
  List<RsGxsForumGroup> forumsInfo = [];
  ResGetForumsInfo();

  @override
  String toString() {
    return 'ResGetForumsInfo[retval=$retval, forumsInfo=$forumsInfo, ]';
  }

  ResGetForumsInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    forumsInfo = (json['forumsInfo'] == null) ?
      null :
      RsGxsForumGroup.listFromJson(json['forumsInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (forumsInfo != null)
      json['forumsInfo'] = forumsInfo;
    return json;
  }

  static List<ResGetForumsInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetForumsInfo>() : json.map((value) => ResGetForumsInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetForumsInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetForumsInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetForumsInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetForumsInfo-objects as value to a dart map
  static Map<String, List<ResGetForumsInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetForumsInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetForumsInfo.listFromJson(value);
       });
     }
     return map;
  }
}


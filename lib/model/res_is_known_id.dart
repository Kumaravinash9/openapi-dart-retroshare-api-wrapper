part of openapi.api;

class ResIsKnownId {
  
  bool retval = null;
  ResIsKnownId();

  @override
  String toString() {
    return 'ResIsKnownId[retval=$retval, ]';
  }

  ResIsKnownId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsKnownId> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsKnownId>() : json.map((value) => ResIsKnownId.fromJson(value)).toList();
  }

  static Map<String, ResIsKnownId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsKnownId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsKnownId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsKnownId-objects as value to a dart map
  static Map<String, List<ResIsKnownId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsKnownId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsKnownId.listFromJson(value);
       });
     }
     return map;
  }
}


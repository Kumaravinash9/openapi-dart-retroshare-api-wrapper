part of openapi.api;

class DirDetails {
  
  ReqBanFileFileSize parent = null;
  
  int prow = null;
  
  ReqBanFileFileSize ref = null;
  
  int type = null;
  
  String id = null;
  
  String name = null;
  
  String hash = null;
  
  String path = null;
  
  ReqBanFileFileSize count = null;
  
  int mtime = null;
  
  int flags = null;
  
  int maxMtime = null;
  
  List<DirStub> children = [];
  
  List<String> parentGroups = [];
  DirDetails();

  @override
  String toString() {
    return 'DirDetails[parent=$parent, prow=$prow, ref=$ref, type=$type, id=$id, name=$name, hash=$hash, path=$path, count=$count, mtime=$mtime, flags=$flags, maxMtime=$maxMtime, children=$children, parentGroups=$parentGroups, ]';
  }

  DirDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    parent = (json['parent'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['parent']);
    prow = json['prow'];
    ref = (json['ref'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['ref']);
    type = json['type'];
    id = json['id'];
    name = json['name'];
    hash = json['hash'];
    path = json['path'];
    count = (json['count'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['count']);
    mtime = json['mtime'];
    flags = json['flags'];
    maxMtime = json['max_mtime'];
    children = (json['children'] == null) ?
      null :
      DirStub.listFromJson(json['children']);
    parentGroups = (json['parent_groups'] == null) ?
      null :
      (json['parent_groups'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (parent != null)
      json['parent'] = parent;
    if (prow != null)
      json['prow'] = prow;
    if (ref != null)
      json['ref'] = ref;
    if (type != null)
      json['type'] = type;
    if (id != null)
      json['id'] = id;
    if (name != null)
      json['name'] = name;
    if (hash != null)
      json['hash'] = hash;
    if (path != null)
      json['path'] = path;
    if (count != null)
      json['count'] = count;
    if (mtime != null)
      json['mtime'] = mtime;
    if (flags != null)
      json['flags'] = flags;
    if (maxMtime != null)
      json['max_mtime'] = maxMtime;
    if (children != null)
      json['children'] = children;
    if (parentGroups != null)
      json['parent_groups'] = parentGroups;
    return json;
  }

  static List<DirDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<DirDetails>() : json.map((value) => DirDetails.fromJson(value)).toList();
  }

  static Map<String, DirDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, DirDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = DirDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of DirDetails-objects as value to a dart map
  static Map<String, List<DirDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<DirDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = DirDetails.listFromJson(value);
       });
     }
     return map;
  }
}


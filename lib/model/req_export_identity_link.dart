part of openapi.api;

class ReqExportIdentityLink {
  
  String id = null;
  
  bool includeGxsData = null;
  
  String baseUrl = null;
  ReqExportIdentityLink();

  @override
  String toString() {
    return 'ReqExportIdentityLink[id=$id, includeGxsData=$includeGxsData, baseUrl=$baseUrl, ]';
  }

  ReqExportIdentityLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
    includeGxsData = json['includeGxsData'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (includeGxsData != null)
      json['includeGxsData'] = includeGxsData;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportIdentityLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportIdentityLink>() : json.map((value) => ReqExportIdentityLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportIdentityLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportIdentityLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportIdentityLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportIdentityLink-objects as value to a dart map
  static Map<String, List<ReqExportIdentityLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportIdentityLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportIdentityLink.listFromJson(value);
       });
     }
     return map;
  }
}


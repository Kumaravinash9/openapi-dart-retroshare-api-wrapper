part of openapi.api;

class ResGetServicePermissions {
  
  bool retval = null;
  
  RsServicePermissions permissions = null;
  ResGetServicePermissions();

  @override
  String toString() {
    return 'ResGetServicePermissions[retval=$retval, permissions=$permissions, ]';
  }

  ResGetServicePermissions.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    permissions = (json['permissions'] == null) ?
      null :
      RsServicePermissions.fromJson(json['permissions']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (permissions != null)
      json['permissions'] = permissions;
    return json;
  }

  static List<ResGetServicePermissions> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServicePermissions>() : json.map((value) => ResGetServicePermissions.fromJson(value)).toList();
  }

  static Map<String, ResGetServicePermissions> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServicePermissions>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServicePermissions.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServicePermissions-objects as value to a dart map
  static Map<String, List<ResGetServicePermissions>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServicePermissions>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServicePermissions.listFromJson(value);
       });
     }
     return map;
  }
}


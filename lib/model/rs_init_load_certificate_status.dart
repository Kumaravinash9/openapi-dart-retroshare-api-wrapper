part of openapi.api;

class RsInitLoadCertificateStatus {
  /// The underlying value of this enum member.
  final int value;

  const RsInitLoadCertificateStatus._internal(this.value);

  static const RsInitLoadCertificateStatus number0_ = const RsInitLoadCertificateStatus._internal(0);
  static const RsInitLoadCertificateStatus number1_ = const RsInitLoadCertificateStatus._internal(1);
  static const RsInitLoadCertificateStatus number2_ = const RsInitLoadCertificateStatus._internal(2);
  static const RsInitLoadCertificateStatus number3_ = const RsInitLoadCertificateStatus._internal(3);
  
  int toJson (){
    return this.value;
  }

  static RsInitLoadCertificateStatus fromJson(int value) {
    return new RsInitLoadCertificateStatusTypeTransformer().decode(value);
  }
  
  static List<RsInitLoadCertificateStatus> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsInitLoadCertificateStatus>() : json.map((value) => RsInitLoadCertificateStatus.fromJson(value)).toList();
  }
}

class RsInitLoadCertificateStatusTypeTransformer {

  dynamic encode(RsInitLoadCertificateStatus data) {
    return data.value;
  }

  RsInitLoadCertificateStatus decode(dynamic data) {
    switch (data) {
      case 0: return RsInitLoadCertificateStatus.number0_;
      case 1: return RsInitLoadCertificateStatus.number1_;
      case 2: return RsInitLoadCertificateStatus.number2_;
      case 3: return RsInitLoadCertificateStatus.number3_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


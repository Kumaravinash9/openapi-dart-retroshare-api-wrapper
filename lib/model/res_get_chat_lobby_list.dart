part of openapi.api;

class ResGetChatLobbyList {
  
  List<ChatLobbyId> clList = [];
  ResGetChatLobbyList();

  @override
  String toString() {
    return 'ResGetChatLobbyList[clList=$clList, ]';
  }

  ResGetChatLobbyList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    clList = (json['cl_list'] == null) ?
      null :
      ChatLobbyId.listFromJson(json['cl_list']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (clList != null)
      json['cl_list'] = clList;
    return json;
  }

  static List<ResGetChatLobbyList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChatLobbyList>() : json.map((value) => ResGetChatLobbyList.fromJson(value)).toList();
  }

  static Map<String, ResGetChatLobbyList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChatLobbyList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChatLobbyList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChatLobbyList-objects as value to a dart map
  static Map<String, List<ResGetChatLobbyList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChatLobbyList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChatLobbyList.listFromJson(value);
       });
     }
     return map;
  }
}


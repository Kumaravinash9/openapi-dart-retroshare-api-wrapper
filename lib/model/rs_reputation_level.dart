part of openapi.api;

class RsReputationLevel {
  /// The underlying value of this enum member.
  final int value;

  const RsReputationLevel._internal(this.value);

  static const RsReputationLevel number0_ = const RsReputationLevel._internal(0);
  static const RsReputationLevel number1_ = const RsReputationLevel._internal(1);
  static const RsReputationLevel number2_ = const RsReputationLevel._internal(2);
  static const RsReputationLevel number3_ = const RsReputationLevel._internal(3);
  static const RsReputationLevel number4_ = const RsReputationLevel._internal(4);
  static const RsReputationLevel number5_ = const RsReputationLevel._internal(5);
  
  int toJson (){
    return this.value;
  }

  static RsReputationLevel fromJson(int value) {
    return new RsReputationLevelTypeTransformer().decode(value);
  }
  
  static List<RsReputationLevel> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsReputationLevel>() : json.map((value) => RsReputationLevel.fromJson(value)).toList();
  }
}

class RsReputationLevelTypeTransformer {

  dynamic encode(RsReputationLevel data) {
    return data.value;
  }

  RsReputationLevel decode(dynamic data) {
    switch (data) {
      case 0: return RsReputationLevel.number0_;
      case 1: return RsReputationLevel.number1_;
      case 2: return RsReputationLevel.number2_;
      case 3: return RsReputationLevel.number3_;
      case 4: return RsReputationLevel.number4_;
      case 5: return RsReputationLevel.number5_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


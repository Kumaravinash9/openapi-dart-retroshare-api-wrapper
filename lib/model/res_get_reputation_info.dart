part of openapi.api;

class ResGetReputationInfo {
  
  bool retval = null;
  
  RsReputationInfo info = null;
  ResGetReputationInfo();

  @override
  String toString() {
    return 'ResGetReputationInfo[retval=$retval, info=$info, ]';
  }

  ResGetReputationInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      RsReputationInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetReputationInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetReputationInfo>() : json.map((value) => ResGetReputationInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetReputationInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetReputationInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetReputationInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetReputationInfo-objects as value to a dart map
  static Map<String, List<ResGetReputationInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetReputationInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetReputationInfo.listFromJson(value);
       });
     }
     return map;
  }
}


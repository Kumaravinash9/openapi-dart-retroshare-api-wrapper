part of openapi.api;

class ReqFileControl {
  
  String hash = null;
  
  int flags = null;
  ReqFileControl();

  @override
  String toString() {
    return 'ReqFileControl[hash=$hash, flags=$flags, ]';
  }

  ReqFileControl.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
    flags = json['flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqFileControl> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqFileControl>() : json.map((value) => ReqFileControl.fromJson(value)).toList();
  }

  static Map<String, ReqFileControl> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqFileControl>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqFileControl.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqFileControl-objects as value to a dart map
  static Map<String, List<ReqFileControl>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqFileControl>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqFileControl.listFromJson(value);
       });
     }
     return map;
  }
}


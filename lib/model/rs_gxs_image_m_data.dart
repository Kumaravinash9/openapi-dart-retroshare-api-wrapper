part of openapi.api;

class RsGxsImageMData {
  
  String base64 = null;
  RsGxsImageMData();

  @override
  String toString() {
    return 'RsGxsImageMData[base64=$base64, ]';
  }

  RsGxsImageMData.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    base64 = json['base64'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (base64 != null)
      json['base64'] = base64;
    return json;
  }

  static List<RsGxsImageMData> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsImageMData>() : json.map((value) => RsGxsImageMData.fromJson(value)).toList();
  }

  static Map<String, RsGxsImageMData> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsImageMData>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsImageMData.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsImageMData-objects as value to a dart map
  static Map<String, List<RsGxsImageMData>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsImageMData>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsImageMData.listFromJson(value);
       });
     }
     return map;
  }
}


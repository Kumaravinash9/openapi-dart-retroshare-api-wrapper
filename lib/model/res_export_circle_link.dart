part of openapi.api;

class ResExportCircleLink {
  
  bool retval = null;
  
  String link = null;
  
  String errMsg = null;
  ResExportCircleLink();

  @override
  String toString() {
    return 'ResExportCircleLink[retval=$retval, link=$link, errMsg=$errMsg, ]';
  }

  ResExportCircleLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    link = json['link'];
    errMsg = json['errMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    if (errMsg != null)
      json['errMsg'] = errMsg;
    return json;
  }

  static List<ResExportCircleLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportCircleLink>() : json.map((value) => ResExportCircleLink.fromJson(value)).toList();
  }

  static Map<String, ResExportCircleLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportCircleLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportCircleLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportCircleLink-objects as value to a dart map
  static Map<String, List<ResExportCircleLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportCircleLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportCircleLink.listFromJson(value);
       });
     }
     return map;
  }
}


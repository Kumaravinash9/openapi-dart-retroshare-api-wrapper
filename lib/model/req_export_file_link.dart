part of openapi.api;

class ReqExportFileLink {
  
  String fileHash = null;
  
  ReqBanFileFileSize fileSize = null;
  
  String fileName = null;
  
  bool fragSneak = null;
  
  String baseUrl = null;
  ReqExportFileLink();

  @override
  String toString() {
    return 'ReqExportFileLink[fileHash=$fileHash, fileSize=$fileSize, fileName=$fileName, fragSneak=$fragSneak, baseUrl=$baseUrl, ]';
  }

  ReqExportFileLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fileHash = json['fileHash'];
    fileSize = (json['fileSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['fileSize']);
    fileName = json['fileName'];
    fragSneak = json['fragSneak'];
    baseUrl = json['baseUrl'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (fileHash != null)
      json['fileHash'] = fileHash;
    if (fileSize != null)
      json['fileSize'] = fileSize;
    if (fileName != null)
      json['fileName'] = fileName;
    if (fragSneak != null)
      json['fragSneak'] = fragSneak;
    if (baseUrl != null)
      json['baseUrl'] = baseUrl;
    return json;
  }

  static List<ReqExportFileLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportFileLink>() : json.map((value) => ReqExportFileLink.fromJson(value)).toList();
  }

  static Map<String, ReqExportFileLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportFileLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportFileLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportFileLink-objects as value to a dart map
  static Map<String, List<ReqExportFileLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportFileLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportFileLink.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqCreateCommentV2 {
  
  String channelId = null;
  
  String threadId = null;
  
  String comment = null;
  
  String authorId = null;
  
  String parentId = null;
  
  String origCommentId = null;
  ReqCreateCommentV2();

  @override
  String toString() {
    return 'ReqCreateCommentV2[channelId=$channelId, threadId=$threadId, comment=$comment, authorId=$authorId, parentId=$parentId, origCommentId=$origCommentId, ]';
  }

  ReqCreateCommentV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    threadId = json['threadId'];
    comment = json['comment'];
    authorId = json['authorId'];
    parentId = json['parentId'];
    origCommentId = json['origCommentId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (threadId != null)
      json['threadId'] = threadId;
    if (comment != null)
      json['comment'] = comment;
    if (authorId != null)
      json['authorId'] = authorId;
    if (parentId != null)
      json['parentId'] = parentId;
    if (origCommentId != null)
      json['origCommentId'] = origCommentId;
    return json;
  }

  static List<ReqCreateCommentV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateCommentV2>() : json.map((value) => ReqCreateCommentV2.fromJson(value)).toList();
  }

  static Map<String, ReqCreateCommentV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateCommentV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateCommentV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateCommentV2-objects as value to a dart map
  static Map<String, List<ReqCreateCommentV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateCommentV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateCommentV2.listFromJson(value);
       });
     }
     return map;
  }
}


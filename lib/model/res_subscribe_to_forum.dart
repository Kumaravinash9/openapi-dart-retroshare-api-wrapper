part of openapi.api;

class ResSubscribeToForum {
  
  bool retval = null;
  ResSubscribeToForum();

  @override
  String toString() {
    return 'ResSubscribeToForum[retval=$retval, ]';
  }

  ResSubscribeToForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSubscribeToForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSubscribeToForum>() : json.map((value) => ResSubscribeToForum.fromJson(value)).toList();
  }

  static Map<String, ResSubscribeToForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSubscribeToForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSubscribeToForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSubscribeToForum-objects as value to a dart map
  static Map<String, List<ResSubscribeToForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSubscribeToForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSubscribeToForum.listFromJson(value);
       });
     }
     return map;
  }
}


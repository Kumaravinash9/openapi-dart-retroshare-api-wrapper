part of openapi.api;

class ResCancelCircleMembership {
  
  bool retval = null;
  ResCancelCircleMembership();

  @override
  String toString() {
    return 'ResCancelCircleMembership[retval=$retval, ]';
  }

  ResCancelCircleMembership.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResCancelCircleMembership> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCancelCircleMembership>() : json.map((value) => ResCancelCircleMembership.fromJson(value)).toList();
  }

  static Map<String, ResCancelCircleMembership> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCancelCircleMembership>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCancelCircleMembership.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCancelCircleMembership-objects as value to a dart map
  static Map<String, List<ResCancelCircleMembership>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCancelCircleMembership>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCancelCircleMembership.listFromJson(value);
       });
     }
     return map;
  }
}


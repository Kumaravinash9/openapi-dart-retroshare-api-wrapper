part of openapi.api;

class ResGetContentSummaries {
  
  bool retval = null;
  
  List<RsMsgMetaData> summaries = [];
  ResGetContentSummaries();

  @override
  String toString() {
    return 'ResGetContentSummaries[retval=$retval, summaries=$summaries, ]';
  }

  ResGetContentSummaries.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    summaries = (json['summaries'] == null) ?
      null :
      RsMsgMetaData.listFromJson(json['summaries']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (summaries != null)
      json['summaries'] = summaries;
    return json;
  }

  static List<ResGetContentSummaries> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetContentSummaries>() : json.map((value) => ResGetContentSummaries.fromJson(value)).toList();
  }

  static Map<String, ResGetContentSummaries> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetContentSummaries>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetContentSummaries.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetContentSummaries-objects as value to a dart map
  static Map<String, List<ResGetContentSummaries>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetContentSummaries>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetContentSummaries.listFromJson(value);
       });
     }
     return map;
  }
}


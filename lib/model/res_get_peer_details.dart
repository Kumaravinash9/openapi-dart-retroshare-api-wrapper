part of openapi.api;

class ResGetPeerDetails {
  
  bool retval = null;
  
  RsPeerDetails det = null;
  ResGetPeerDetails();

  @override
  String toString() {
    return 'ResGetPeerDetails[retval=$retval, det=$det, ]';
  }

  ResGetPeerDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    det = (json['det'] == null) ?
      null :
      RsPeerDetails.fromJson(json['det']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (det != null)
      json['det'] = det;
    return json;
  }

  static List<ResGetPeerDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPeerDetails>() : json.map((value) => ResGetPeerDetails.fromJson(value)).toList();
  }

  static Map<String, ResGetPeerDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPeerDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPeerDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPeerDetails-objects as value to a dart map
  static Map<String, List<ResGetPeerDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPeerDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPeerDetails.listFromJson(value);
       });
     }
     return map;
  }
}


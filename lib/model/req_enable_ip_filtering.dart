part of openapi.api;

class ReqEnableIPFiltering {
  
  bool enable = null;
  ReqEnableIPFiltering();

  @override
  String toString() {
    return 'ReqEnableIPFiltering[enable=$enable, ]';
  }

  ReqEnableIPFiltering.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    enable = json['enable'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (enable != null)
      json['enable'] = enable;
    return json;
  }

  static List<ReqEnableIPFiltering> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqEnableIPFiltering>() : json.map((value) => ReqEnableIPFiltering.fromJson(value)).toList();
  }

  static Map<String, ReqEnableIPFiltering> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqEnableIPFiltering>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqEnableIPFiltering.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqEnableIPFiltering-objects as value to a dart map
  static Map<String, List<ReqEnableIPFiltering>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqEnableIPFiltering>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqEnableIPFiltering.listFromJson(value);
       });
     }
     return map;
  }
}


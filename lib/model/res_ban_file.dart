part of openapi.api;

class ResBanFile {
  
  int retval = null;
  ResBanFile();

  @override
  String toString() {
    return 'ResBanFile[retval=$retval, ]';
  }

  ResBanFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResBanFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ResBanFile>() : json.map((value) => ResBanFile.fromJson(value)).toList();
  }

  static Map<String, ResBanFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResBanFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResBanFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResBanFile-objects as value to a dart map
  static Map<String, List<ResBanFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResBanFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResBanFile.listFromJson(value);
       });
     }
     return map;
  }
}


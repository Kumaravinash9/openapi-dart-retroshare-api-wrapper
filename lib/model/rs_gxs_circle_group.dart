part of openapi.api;

class RsGxsCircleGroup {
  
  RsGroupMetaData mMeta = null;
  
  List<String> mLocalFriends = [];
  
  List<String> mInvitedMembers = [];
  
  List<String> mSubCircles = [];
  RsGxsCircleGroup();

  @override
  String toString() {
    return 'RsGxsCircleGroup[mMeta=$mMeta, mLocalFriends=$mLocalFriends, mInvitedMembers=$mInvitedMembers, mSubCircles=$mSubCircles, ]';
  }

  RsGxsCircleGroup.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsGroupMetaData.fromJson(json['mMeta']);
    mLocalFriends = (json['mLocalFriends'] == null) ?
      null :
      (json['mLocalFriends'] as List).cast<String>();
    mInvitedMembers = (json['mInvitedMembers'] == null) ?
      null :
      (json['mInvitedMembers'] as List).cast<String>();
    mSubCircles = (json['mSubCircles'] == null) ?
      null :
      (json['mSubCircles'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mLocalFriends != null)
      json['mLocalFriends'] = mLocalFriends;
    if (mInvitedMembers != null)
      json['mInvitedMembers'] = mInvitedMembers;
    if (mSubCircles != null)
      json['mSubCircles'] = mSubCircles;
    return json;
  }

  static List<RsGxsCircleGroup> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsCircleGroup>() : json.map((value) => RsGxsCircleGroup.fromJson(value)).toList();
  }

  static Map<String, RsGxsCircleGroup> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsCircleGroup>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsCircleGroup.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsCircleGroup-objects as value to a dart map
  static Map<String, List<RsGxsCircleGroup>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsCircleGroup>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsCircleGroup.listFromJson(value);
       });
     }
     return map;
  }
}


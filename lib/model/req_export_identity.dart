part of openapi.api;

class ReqExportIdentity {
  
  String filePath = null;
  
  String pgpId = null;
  ReqExportIdentity();

  @override
  String toString() {
    return 'ReqExportIdentity[filePath=$filePath, pgpId=$pgpId, ]';
  }

  ReqExportIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    filePath = json['filePath'];
    pgpId = json['pgpId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (filePath != null)
      json['filePath'] = filePath;
    if (pgpId != null)
      json['pgpId'] = pgpId;
    return json;
  }

  static List<ReqExportIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportIdentity>() : json.map((value) => ReqExportIdentity.fromJson(value)).toList();
  }

  static Map<String, ReqExportIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportIdentity-objects as value to a dart map
  static Map<String, List<ReqExportIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


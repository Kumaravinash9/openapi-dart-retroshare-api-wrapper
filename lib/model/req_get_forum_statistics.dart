part of openapi.api;

class ReqGetForumStatistics {
  
  String forumId = null;
  ReqGetForumStatistics();

  @override
  String toString() {
    return 'ReqGetForumStatistics[forumId=$forumId, ]';
  }

  ReqGetForumStatistics.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    forumId = json['forumId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (forumId != null)
      json['forumId'] = forumId;
    return json;
  }

  static List<ReqGetForumStatistics> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetForumStatistics>() : json.map((value) => ReqGetForumStatistics.fromJson(value)).toList();
  }

  static Map<String, ReqGetForumStatistics> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetForumStatistics>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetForumStatistics.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetForumStatistics-objects as value to a dart map
  static Map<String, List<ReqGetForumStatistics>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetForumStatistics>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetForumStatistics.listFromJson(value);
       });
     }
     return map;
  }
}


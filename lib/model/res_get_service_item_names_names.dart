part of openapi.api;

class ResGetServiceItemNamesNames {
  
  int key = null;
  
  String value = null;
  ResGetServiceItemNamesNames();

  @override
  String toString() {
    return 'ResGetServiceItemNamesNames[key=$key, value=$value, ]';
  }

  ResGetServiceItemNamesNames.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = json['value'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<ResGetServiceItemNamesNames> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServiceItemNamesNames>() : json.map((value) => ResGetServiceItemNamesNames.fromJson(value)).toList();
  }

  static Map<String, ResGetServiceItemNamesNames> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServiceItemNamesNames>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServiceItemNamesNames.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServiceItemNamesNames-objects as value to a dart map
  static Map<String, List<ResGetServiceItemNamesNames>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServiceItemNamesNames>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServiceItemNamesNames.listFromJson(value);
       });
     }
     return map;
  }
}


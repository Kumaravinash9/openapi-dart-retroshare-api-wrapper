part of openapi.api;

class ReqSetOperatingMode {
  
  int opMode = null;
  ReqSetOperatingMode();

  @override
  String toString() {
    return 'ReqSetOperatingMode[opMode=$opMode, ]';
  }

  ReqSetOperatingMode.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    opMode = json['opMode'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (opMode != null)
      json['opMode'] = opMode;
    return json;
  }

  static List<ReqSetOperatingMode> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetOperatingMode>() : json.map((value) => ReqSetOperatingMode.fromJson(value)).toList();
  }

  static Map<String, ReqSetOperatingMode> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetOperatingMode>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetOperatingMode.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetOperatingMode-objects as value to a dart map
  static Map<String, List<ReqSetOperatingMode>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetOperatingMode>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetOperatingMode.listFromJson(value);
       });
     }
     return map;
  }
}


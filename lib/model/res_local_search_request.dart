part of openapi.api;

class ResLocalSearchRequest {
  
  bool retval = null;
  ResLocalSearchRequest();

  @override
  String toString() {
    return 'ResLocalSearchRequest[retval=$retval, ]';
  }

  ResLocalSearchRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResLocalSearchRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ResLocalSearchRequest>() : json.map((value) => ResLocalSearchRequest.fromJson(value)).toList();
  }

  static Map<String, ResLocalSearchRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResLocalSearchRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResLocalSearchRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResLocalSearchRequest-objects as value to a dart map
  static Map<String, List<ResLocalSearchRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResLocalSearchRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResLocalSearchRequest.listFromJson(value);
       });
     }
     return map;
  }
}


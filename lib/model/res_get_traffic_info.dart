part of openapi.api;

class ResGetTrafficInfo {
  
  int retval = null;
  
  List<RSTrafficClue> outLst = [];
  
  List<RSTrafficClue> inLst = [];
  ResGetTrafficInfo();

  @override
  String toString() {
    return 'ResGetTrafficInfo[retval=$retval, outLst=$outLst, inLst=$inLst, ]';
  }

  ResGetTrafficInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    outLst = (json['out_lst'] == null) ?
      null :
      RSTrafficClue.listFromJson(json['out_lst']);
    inLst = (json['in_lst'] == null) ?
      null :
      RSTrafficClue.listFromJson(json['in_lst']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (outLst != null)
      json['out_lst'] = outLst;
    if (inLst != null)
      json['in_lst'] = inLst;
    return json;
  }

  static List<ResGetTrafficInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetTrafficInfo>() : json.map((value) => ResGetTrafficInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetTrafficInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetTrafficInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetTrafficInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetTrafficInfo-objects as value to a dart map
  static Map<String, List<ResGetTrafficInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetTrafficInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetTrafficInfo.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResRemoveSharedDirectory {
  
  bool retval = null;
  ResRemoveSharedDirectory();

  @override
  String toString() {
    return 'ResRemoveSharedDirectory[retval=$retval, ]';
  }

  ResRemoveSharedDirectory.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRemoveSharedDirectory> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRemoveSharedDirectory>() : json.map((value) => ResRemoveSharedDirectory.fromJson(value)).toList();
  }

  static Map<String, ResRemoveSharedDirectory> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRemoveSharedDirectory>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRemoveSharedDirectory.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRemoveSharedDirectory-objects as value to a dart map
  static Map<String, List<ResRemoveSharedDirectory>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRemoveSharedDirectory>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRemoveSharedDirectory.listFromJson(value);
       });
     }
     return map;
  }
}


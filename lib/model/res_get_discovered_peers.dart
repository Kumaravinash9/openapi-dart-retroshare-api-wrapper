part of openapi.api;

class ResGetDiscoveredPeers {
  
  List<RsBroadcastDiscoveryResult> retval = [];
  ResGetDiscoveredPeers();

  @override
  String toString() {
    return 'ResGetDiscoveredPeers[retval=$retval, ]';
  }

  ResGetDiscoveredPeers.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      RsBroadcastDiscoveryResult.listFromJson(json['retval']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetDiscoveredPeers> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDiscoveredPeers>() : json.map((value) => ResGetDiscoveredPeers.fromJson(value)).toList();
  }

  static Map<String, ResGetDiscoveredPeers> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDiscoveredPeers>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDiscoveredPeers.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDiscoveredPeers-objects as value to a dart map
  static Map<String, List<ResGetDiscoveredPeers>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDiscoveredPeers>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDiscoveredPeers.listFromJson(value);
       });
     }
     return map;
  }
}


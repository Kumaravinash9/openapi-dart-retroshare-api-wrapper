part of openapi.api;

class ReqIsFriend {
  
  String sslId = null;
  ReqIsFriend();

  @override
  String toString() {
    return 'ReqIsFriend[sslId=$sslId, ]';
  }

  ReqIsFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqIsFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsFriend>() : json.map((value) => ReqIsFriend.fromJson(value)).toList();
  }

  static Map<String, ReqIsFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsFriend-objects as value to a dart map
  static Map<String, List<ReqIsFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsFriend.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResImportIdentity {
  
  bool retval = null;
  
  String pgpId = null;
  
  String errorMsg = null;
  ResImportIdentity();

  @override
  String toString() {
    return 'ResImportIdentity[retval=$retval, pgpId=$pgpId, errorMsg=$errorMsg, ]';
  }

  ResImportIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    pgpId = json['pgpId'];
    errorMsg = json['errorMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (pgpId != null)
      json['pgpId'] = pgpId;
    if (errorMsg != null)
      json['errorMsg'] = errorMsg;
    return json;
  }

  static List<ResImportIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResImportIdentity>() : json.map((value) => ResImportIdentity.fromJson(value)).toList();
  }

  static Map<String, ResImportIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResImportIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResImportIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResImportIdentity-objects as value to a dart map
  static Map<String, List<ResImportIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResImportIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResImportIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


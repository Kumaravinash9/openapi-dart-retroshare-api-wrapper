part of openapi.api;

class ResGetServiceName {
  
  String retval = null;
  ResGetServiceName();

  @override
  String toString() {
    return 'ResGetServiceName[retval=$retval, ]';
  }

  ResGetServiceName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetServiceName> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetServiceName>() : json.map((value) => ResGetServiceName.fromJson(value)).toList();
  }

  static Map<String, ResGetServiceName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetServiceName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetServiceName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetServiceName-objects as value to a dart map
  static Map<String, List<ResGetServiceName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetServiceName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetServiceName.listFromJson(value);
       });
     }
     return map;
  }
}


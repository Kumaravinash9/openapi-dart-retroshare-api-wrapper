part of openapi.api;

class ResIsHashBanned {
  
  bool retval = null;
  ResIsHashBanned();

  @override
  String toString() {
    return 'ResIsHashBanned[retval=$retval, ]';
  }

  ResIsHashBanned.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIsHashBanned> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIsHashBanned>() : json.map((value) => ResIsHashBanned.fromJson(value)).toList();
  }

  static Map<String, ResIsHashBanned> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIsHashBanned>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIsHashBanned.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIsHashBanned-objects as value to a dart map
  static Map<String, List<ResIsHashBanned>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIsHashBanned>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIsHashBanned.listFromJson(value);
       });
     }
     return map;
  }
}


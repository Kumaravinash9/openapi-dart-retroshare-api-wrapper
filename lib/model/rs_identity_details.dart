part of openapi.api;

class RsIdentityDetails {
  
  String mId = null;
  
  String mNickname = null;
  
  int mFlags = null;
  
  String mPgpId = null;
  
  List<RsRecognTag> mRecognTags = [];
  
  RsReputationInfo mReputation = null;
  
  RsGxsImage mAvatar = null;
  
  RstimeT mPublishTS = null;
  
  RstimeT mLastUsageTS = null;
  
  List<RsIdentityDetailsMUseCases> mUseCases = [];
  RsIdentityDetails();

  @override
  String toString() {
    return 'RsIdentityDetails[mId=$mId, mNickname=$mNickname, mFlags=$mFlags, mPgpId=$mPgpId, mRecognTags=$mRecognTags, mReputation=$mReputation, mAvatar=$mAvatar, mPublishTS=$mPublishTS, mLastUsageTS=$mLastUsageTS, mUseCases=$mUseCases, ]';
  }

  RsIdentityDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mId = json['mId'];
    mNickname = json['mNickname'];
    mFlags = json['mFlags'];
    mPgpId = json['mPgpId'];
    mRecognTags = (json['mRecognTags'] == null) ?
      null :
      RsRecognTag.listFromJson(json['mRecognTags']);
    mReputation = (json['mReputation'] == null) ?
      null :
      RsReputationInfo.fromJson(json['mReputation']);
    mAvatar = (json['mAvatar'] == null) ?
      null :
      RsGxsImage.fromJson(json['mAvatar']);
    mPublishTS = (json['mPublishTS'] == null) ?
      null :
      RstimeT.fromJson(json['mPublishTS']);
    mLastUsageTS = (json['mLastUsageTS'] == null) ?
      null :
      RstimeT.fromJson(json['mLastUsageTS']);
    mUseCases = (json['mUseCases'] == null) ?
      null :
      RsIdentityDetailsMUseCases.listFromJson(json['mUseCases']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mId != null)
      json['mId'] = mId;
    if (mNickname != null)
      json['mNickname'] = mNickname;
    if (mFlags != null)
      json['mFlags'] = mFlags;
    if (mPgpId != null)
      json['mPgpId'] = mPgpId;
    if (mRecognTags != null)
      json['mRecognTags'] = mRecognTags;
    if (mReputation != null)
      json['mReputation'] = mReputation;
    if (mAvatar != null)
      json['mAvatar'] = mAvatar;
    if (mPublishTS != null)
      json['mPublishTS'] = mPublishTS;
    if (mLastUsageTS != null)
      json['mLastUsageTS'] = mLastUsageTS;
    if (mUseCases != null)
      json['mUseCases'] = mUseCases;
    return json;
  }

  static List<RsIdentityDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<RsIdentityDetails>() : json.map((value) => RsIdentityDetails.fromJson(value)).toList();
  }

  static Map<String, RsIdentityDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsIdentityDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsIdentityDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsIdentityDetails-objects as value to a dart map
  static Map<String, List<RsIdentityDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsIdentityDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsIdentityDetails.listFromJson(value);
       });
     }
     return map;
  }
}


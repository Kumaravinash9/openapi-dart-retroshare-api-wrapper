part of openapi.api;

class ResFreeDiskSpaceLimit {
  
  int retval = null;
  ResFreeDiskSpaceLimit();

  @override
  String toString() {
    return 'ResFreeDiskSpaceLimit[retval=$retval, ]';
  }

  ResFreeDiskSpaceLimit.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResFreeDiskSpaceLimit> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFreeDiskSpaceLimit>() : json.map((value) => ResFreeDiskSpaceLimit.fromJson(value)).toList();
  }

  static Map<String, ResFreeDiskSpaceLimit> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFreeDiskSpaceLimit>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFreeDiskSpaceLimit.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFreeDiskSpaceLimit-objects as value to a dart map
  static Map<String, List<ResFreeDiskSpaceLimit>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFreeDiskSpaceLimit>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFreeDiskSpaceLimit.listFromJson(value);
       });
     }
     return map;
  }
}


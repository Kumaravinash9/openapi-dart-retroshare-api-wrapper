part of openapi.api;

class ReqCollectEntropy {
  
  int bytes = null;
  ReqCollectEntropy();

  @override
  String toString() {
    return 'ReqCollectEntropy[bytes=$bytes, ]';
  }

  ReqCollectEntropy.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    bytes = json['bytes'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (bytes != null)
      json['bytes'] = bytes;
    return json;
  }

  static List<ReqCollectEntropy> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCollectEntropy>() : json.map((value) => ReqCollectEntropy.fromJson(value)).toList();
  }

  static Map<String, ReqCollectEntropy> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCollectEntropy>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCollectEntropy.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCollectEntropy-objects as value to a dart map
  static Map<String, List<ReqCollectEntropy>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCollectEntropy>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCollectEntropy.listFromJson(value);
       });
     }
     return map;
  }
}


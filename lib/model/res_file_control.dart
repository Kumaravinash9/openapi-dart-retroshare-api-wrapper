part of openapi.api;

class ResFileControl {
  
  bool retval = null;
  ResFileControl();

  @override
  String toString() {
    return 'ResFileControl[retval=$retval, ]';
  }

  ResFileControl.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResFileControl> listFromJson(List<dynamic> json) {
    return json == null ? List<ResFileControl>() : json.map((value) => ResFileControl.fromJson(value)).toList();
  }

  static Map<String, ResFileControl> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResFileControl>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResFileControl.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResFileControl-objects as value to a dart map
  static Map<String, List<ResFileControl>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResFileControl>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResFileControl.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqShareChannelKeys {
  
  String channelId = null;
  
  List<String> peers = [];
  ReqShareChannelKeys();

  @override
  String toString() {
    return 'ReqShareChannelKeys[channelId=$channelId, peers=$peers, ]';
  }

  ReqShareChannelKeys.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    peers = (json['peers'] == null) ?
      null :
      (json['peers'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (peers != null)
      json['peers'] = peers;
    return json;
  }

  static List<ReqShareChannelKeys> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqShareChannelKeys>() : json.map((value) => ReqShareChannelKeys.fromJson(value)).toList();
  }

  static Map<String, ReqShareChannelKeys> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqShareChannelKeys>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqShareChannelKeys.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqShareChannelKeys-objects as value to a dart map
  static Map<String, List<ReqShareChannelKeys>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqShareChannelKeys>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqShareChannelKeys.listFromJson(value);
       });
     }
     return map;
  }
}


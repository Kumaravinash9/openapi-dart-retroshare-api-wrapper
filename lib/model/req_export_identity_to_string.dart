part of openapi.api;

class ReqExportIdentityToString {
  
  String pgpId = null;
  
  bool includeSignatures = null;
  ReqExportIdentityToString();

  @override
  String toString() {
    return 'ReqExportIdentityToString[pgpId=$pgpId, includeSignatures=$includeSignatures, ]';
  }

  ReqExportIdentityToString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pgpId = json['pgpId'];
    includeSignatures = json['includeSignatures'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pgpId != null)
      json['pgpId'] = pgpId;
    if (includeSignatures != null)
      json['includeSignatures'] = includeSignatures;
    return json;
  }

  static List<ReqExportIdentityToString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExportIdentityToString>() : json.map((value) => ReqExportIdentityToString.fromJson(value)).toList();
  }

  static Map<String, ReqExportIdentityToString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExportIdentityToString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExportIdentityToString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExportIdentityToString-objects as value to a dart map
  static Map<String, List<ReqExportIdentityToString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExportIdentityToString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExportIdentityToString.listFromJson(value);
       });
     }
     return map;
  }
}


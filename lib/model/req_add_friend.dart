part of openapi.api;

class ReqAddFriend {
  
  String sslId = null;
  
  String gpgId = null;
  
  int flags = null;
  ReqAddFriend();

  @override
  String toString() {
    return 'ReqAddFriend[sslId=$sslId, gpgId=$gpgId, flags=$flags, ]';
  }

  ReqAddFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
    gpgId = json['gpgId'];
    flags = json['flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    if (gpgId != null)
      json['gpgId'] = gpgId;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqAddFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAddFriend>() : json.map((value) => ReqAddFriend.fromJson(value)).toList();
  }

  static Map<String, ReqAddFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAddFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAddFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAddFriend-objects as value to a dart map
  static Map<String, List<ReqAddFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAddFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAddFriend.listFromJson(value);
       });
     }
     return map;
  }
}


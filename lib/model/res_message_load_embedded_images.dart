part of openapi.api;

class ResMessageLoadEmbeddedImages {
  
  bool retval = null;
  ResMessageLoadEmbeddedImages();

  @override
  String toString() {
    return 'ResMessageLoadEmbeddedImages[retval=$retval, ]';
  }

  ResMessageLoadEmbeddedImages.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageLoadEmbeddedImages> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageLoadEmbeddedImages>() : json.map((value) => ResMessageLoadEmbeddedImages.fromJson(value)).toList();
  }

  static Map<String, ResMessageLoadEmbeddedImages> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageLoadEmbeddedImages>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageLoadEmbeddedImages.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageLoadEmbeddedImages-objects as value to a dart map
  static Map<String, List<ResMessageLoadEmbeddedImages>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageLoadEmbeddedImages>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageLoadEmbeddedImages.listFromJson(value);
       });
     }
     return map;
  }
}


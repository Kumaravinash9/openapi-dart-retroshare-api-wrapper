part of openapi.api;

class ReqMessageToTrash {
  
  String msgId = null;
  
  bool bTrash = null;
  ReqMessageToTrash();

  @override
  String toString() {
    return 'ReqMessageToTrash[msgId=$msgId, bTrash=$bTrash, ]';
  }

  ReqMessageToTrash.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
    bTrash = json['bTrash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    if (bTrash != null)
      json['bTrash'] = bTrash;
    return json;
  }

  static List<ReqMessageToTrash> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqMessageToTrash>() : json.map((value) => ReqMessageToTrash.fromJson(value)).toList();
  }

  static Map<String, ReqMessageToTrash> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqMessageToTrash>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqMessageToTrash.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqMessageToTrash-objects as value to a dart map
  static Map<String, List<ReqMessageToTrash>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqMessageToTrash>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqMessageToTrash.listFromJson(value);
       });
     }
     return map;
  }
}


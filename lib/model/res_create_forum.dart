part of openapi.api;

class ResCreateForum {
  
  bool retval = null;
  
  RsGxsForumGroup forum = null;
  ResCreateForum();

  @override
  String toString() {
    return 'ResCreateForum[retval=$retval, forum=$forum, ]';
  }

  ResCreateForum.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    forum = (json['forum'] == null) ?
      null :
      RsGxsForumGroup.fromJson(json['forum']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (forum != null)
      json['forum'] = forum;
    return json;
  }

  static List<ResCreateForum> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateForum>() : json.map((value) => ResCreateForum.fromJson(value)).toList();
  }

  static Map<String, ResCreateForum> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateForum>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateForum.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateForum-objects as value to a dart map
  static Map<String, List<ResCreateForum>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateForum>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateForum.listFromJson(value);
       });
     }
     return map;
  }
}


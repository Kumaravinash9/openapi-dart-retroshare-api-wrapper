part of openapi.api;

class RsGxsImage {
  
  int mSize = null;
  
  RsGxsImageMData mData = null;
  RsGxsImage();

  @override
  String toString() {
    return 'RsGxsImage[mSize=$mSize, mData=$mData, ]';
  }

  RsGxsImage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mSize = json['mSize'];
    mData = (json['mData'] == null) ?
      null :
      RsGxsImageMData.fromJson(json['mData']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mSize != null)
      json['mSize'] = mSize;
    if (mData != null)
      json['mData'] = mData;
    return json;
  }

  static List<RsGxsImage> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsImage>() : json.map((value) => RsGxsImage.fromJson(value)).toList();
  }

  static Map<String, RsGxsImage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsImage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsImage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsImage-objects as value to a dart map
  static Map<String, List<RsGxsImage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsImage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsImage.listFromJson(value);
       });
     }
     return map;
  }
}


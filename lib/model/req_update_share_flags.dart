part of openapi.api;

class ReqUpdateShareFlags {
  
  SharedDirInfo dir = null;
  ReqUpdateShareFlags();

  @override
  String toString() {
    return 'ReqUpdateShareFlags[dir=$dir, ]';
  }

  ReqUpdateShareFlags.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    dir = (json['dir'] == null) ?
      null :
      SharedDirInfo.fromJson(json['dir']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (dir != null)
      json['dir'] = dir;
    return json;
  }

  static List<ReqUpdateShareFlags> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqUpdateShareFlags>() : json.map((value) => ReqUpdateShareFlags.fromJson(value)).toList();
  }

  static Map<String, ReqUpdateShareFlags> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqUpdateShareFlags>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqUpdateShareFlags.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqUpdateShareFlags-objects as value to a dart map
  static Map<String, List<ReqUpdateShareFlags>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqUpdateShareFlags>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqUpdateShareFlags.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetServiceName {
  
  int serviceId = null;
  ReqGetServiceName();

  @override
  String toString() {
    return 'ReqGetServiceName[serviceId=$serviceId, ]';
  }

  ReqGetServiceName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    serviceId = json['serviceId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (serviceId != null)
      json['serviceId'] = serviceId;
    return json;
  }

  static List<ReqGetServiceName> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetServiceName>() : json.map((value) => ReqGetServiceName.fromJson(value)).toList();
  }

  static Map<String, ReqGetServiceName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetServiceName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetServiceName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetServiceName-objects as value to a dart map
  static Map<String, List<ReqGetServiceName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetServiceName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetServiceName.listFromJson(value);
       });
     }
     return map;
  }
}


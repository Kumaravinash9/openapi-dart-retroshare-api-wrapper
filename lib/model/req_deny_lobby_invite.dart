part of openapi.api;

class ReqDenyLobbyInvite {
  
  ChatLobbyId id = null;
  ReqDenyLobbyInvite();

  @override
  String toString() {
    return 'ReqDenyLobbyInvite[id=$id, ]';
  }

  ReqDenyLobbyInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqDenyLobbyInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqDenyLobbyInvite>() : json.map((value) => ReqDenyLobbyInvite.fromJson(value)).toList();
  }

  static Map<String, ReqDenyLobbyInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqDenyLobbyInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqDenyLobbyInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqDenyLobbyInvite-objects as value to a dart map
  static Map<String, List<ReqDenyLobbyInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqDenyLobbyInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqDenyLobbyInvite.listFromJson(value);
       });
     }
     return map;
  }
}


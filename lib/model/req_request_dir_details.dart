part of openapi.api;

class ReqRequestDirDetails {
  
  ReqBanFileFileSize handle = null;
  
  int flags = null;
  ReqRequestDirDetails();

  @override
  String toString() {
    return 'ReqRequestDirDetails[handle=$handle, flags=$flags, ]';
  }

  ReqRequestDirDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    handle = (json['handle'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['handle']);
    flags = json['flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (handle != null)
      json['handle'] = handle;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqRequestDirDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqRequestDirDetails>() : json.map((value) => ReqRequestDirDetails.fromJson(value)).toList();
  }

  static Map<String, ReqRequestDirDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqRequestDirDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqRequestDirDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqRequestDirDetails-objects as value to a dart map
  static Map<String, List<ReqRequestDirDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqRequestDirDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqRequestDirDetails.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqExtraFileHash {
  
  String localpath = null;
  
  RstimeT period = null;
  
  int flags = null;
  ReqExtraFileHash();

  @override
  String toString() {
    return 'ReqExtraFileHash[localpath=$localpath, period=$period, flags=$flags, ]';
  }

  ReqExtraFileHash.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    localpath = json['localpath'];
    period = (json['period'] == null) ?
      null :
      RstimeT.fromJson(json['period']);
    flags = json['flags'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (localpath != null)
      json['localpath'] = localpath;
    if (period != null)
      json['period'] = period;
    if (flags != null)
      json['flags'] = flags;
    return json;
  }

  static List<ReqExtraFileHash> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExtraFileHash>() : json.map((value) => ReqExtraFileHash.fromJson(value)).toList();
  }

  static Map<String, ReqExtraFileHash> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExtraFileHash>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExtraFileHash.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExtraFileHash-objects as value to a dart map
  static Map<String, List<ReqExtraFileHash>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExtraFileHash>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExtraFileHash.listFromJson(value);
       });
     }
     return map;
  }
}


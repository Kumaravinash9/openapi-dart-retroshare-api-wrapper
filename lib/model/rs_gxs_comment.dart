part of openapi.api;

class RsGxsComment {
  
  RsMsgMetaData mMeta = null;
  
  String mComment = null;
  
  int mUpVotes = null;
  
  int mDownVotes = null;
  
  num mScore = null;
  
  int mOwnVote = null;
  
  List<RsGxsVote> mVotes = [];
  RsGxsComment();

  @override
  String toString() {
    return 'RsGxsComment[mMeta=$mMeta, mComment=$mComment, mUpVotes=$mUpVotes, mDownVotes=$mDownVotes, mScore=$mScore, mOwnVote=$mOwnVote, mVotes=$mVotes, ]';
  }

  RsGxsComment.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMeta = (json['mMeta'] == null) ?
      null :
      RsMsgMetaData.fromJson(json['mMeta']);
    mComment = json['mComment'];
    mUpVotes = json['mUpVotes'];
    mDownVotes = json['mDownVotes'];
    mScore = json['mScore'];
    mOwnVote = json['mOwnVote'];
    mVotes = (json['mVotes'] == null) ?
      null :
      RsGxsVote.listFromJson(json['mVotes']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMeta != null)
      json['mMeta'] = mMeta;
    if (mComment != null)
      json['mComment'] = mComment;
    if (mUpVotes != null)
      json['mUpVotes'] = mUpVotes;
    if (mDownVotes != null)
      json['mDownVotes'] = mDownVotes;
    if (mScore != null)
      json['mScore'] = mScore;
    if (mOwnVote != null)
      json['mOwnVote'] = mOwnVote;
    if (mVotes != null)
      json['mVotes'] = mVotes;
    return json;
  }

  static List<RsGxsComment> listFromJson(List<dynamic> json) {
    return json == null ? List<RsGxsComment>() : json.map((value) => RsGxsComment.fromJson(value)).toList();
  }

  static Map<String, RsGxsComment> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsGxsComment>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsGxsComment.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsGxsComment-objects as value to a dart map
  static Map<String, List<RsGxsComment>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsGxsComment>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsGxsComment.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResIpFilteringEnabled {
  
  bool retval = null;
  ResIpFilteringEnabled();

  @override
  String toString() {
    return 'ResIpFilteringEnabled[retval=$retval, ]';
  }

  ResIpFilteringEnabled.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResIpFilteringEnabled> listFromJson(List<dynamic> json) {
    return json == null ? List<ResIpFilteringEnabled>() : json.map((value) => ResIpFilteringEnabled.fromJson(value)).toList();
  }

  static Map<String, ResIpFilteringEnabled> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResIpFilteringEnabled>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResIpFilteringEnabled.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResIpFilteringEnabled-objects as value to a dart map
  static Map<String, List<ResIpFilteringEnabled>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResIpFilteringEnabled>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResIpFilteringEnabled.listFromJson(value);
       });
     }
     return map;
  }
}


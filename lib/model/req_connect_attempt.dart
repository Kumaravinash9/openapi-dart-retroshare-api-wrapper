part of openapi.api;

class ReqConnectAttempt {
  
  String sslId = null;
  ReqConnectAttempt();

  @override
  String toString() {
    return 'ReqConnectAttempt[sslId=$sslId, ]';
  }

  ReqConnectAttempt.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqConnectAttempt> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqConnectAttempt>() : json.map((value) => ReqConnectAttempt.fromJson(value)).toList();
  }

  static Map<String, ReqConnectAttempt> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqConnectAttempt>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqConnectAttempt.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqConnectAttempt-objects as value to a dart map
  static Map<String, List<ReqConnectAttempt>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqConnectAttempt>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqConnectAttempt.listFromJson(value);
       });
     }
     return map;
  }
}


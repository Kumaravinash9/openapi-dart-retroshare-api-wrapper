part of openapi.api;

class ReqSendLobbyStatusPeerLeaving {
  
  ChatLobbyId lobbyId = null;
  ReqSendLobbyStatusPeerLeaving();

  @override
  String toString() {
    return 'ReqSendLobbyStatusPeerLeaving[lobbyId=$lobbyId, ]';
  }

  ReqSendLobbyStatusPeerLeaving.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    lobbyId = (json['lobby_id'] == null) ?
      null :
      ChatLobbyId.fromJson(json['lobby_id']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (lobbyId != null)
      json['lobby_id'] = lobbyId;
    return json;
  }

  static List<ReqSendLobbyStatusPeerLeaving> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSendLobbyStatusPeerLeaving>() : json.map((value) => ReqSendLobbyStatusPeerLeaving.fromJson(value)).toList();
  }

  static Map<String, ReqSendLobbyStatusPeerLeaving> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSendLobbyStatusPeerLeaving>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSendLobbyStatusPeerLeaving.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSendLobbyStatusPeerLeaving-objects as value to a dart map
  static Map<String, List<ReqSendLobbyStatusPeerLeaving>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSendLobbyStatusPeerLeaving>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSendLobbyStatusPeerLeaving.listFromJson(value);
       });
     }
     return map;
  }
}


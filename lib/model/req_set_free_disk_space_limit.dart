part of openapi.api;

class ReqSetFreeDiskSpaceLimit {
  
  int minimumFreeMB = null;
  ReqSetFreeDiskSpaceLimit();

  @override
  String toString() {
    return 'ReqSetFreeDiskSpaceLimit[minimumFreeMB=$minimumFreeMB, ]';
  }

  ReqSetFreeDiskSpaceLimit.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    minimumFreeMB = json['minimumFreeMB'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (minimumFreeMB != null)
      json['minimumFreeMB'] = minimumFreeMB;
    return json;
  }

  static List<ReqSetFreeDiskSpaceLimit> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetFreeDiskSpaceLimit>() : json.map((value) => ReqSetFreeDiskSpaceLimit.fromJson(value)).toList();
  }

  static Map<String, ReqSetFreeDiskSpaceLimit> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetFreeDiskSpaceLimit>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetFreeDiskSpaceLimit.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetFreeDiskSpaceLimit-objects as value to a dart map
  static Map<String, List<ReqSetFreeDiskSpaceLimit>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetFreeDiskSpaceLimit>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetFreeDiskSpaceLimit.listFromJson(value);
       });
     }
     return map;
  }
}


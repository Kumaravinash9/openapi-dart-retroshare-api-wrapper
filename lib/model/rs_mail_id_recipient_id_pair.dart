part of openapi.api;

class RsMailIdRecipientIdPair {
  
  String mMailId = null;
  
  String mRecipientId = null;
  RsMailIdRecipientIdPair();

  @override
  String toString() {
    return 'RsMailIdRecipientIdPair[mMailId=$mMailId, mRecipientId=$mRecipientId, ]';
  }

  RsMailIdRecipientIdPair.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mMailId = json['mMailId'];
    mRecipientId = json['mRecipientId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mMailId != null)
      json['mMailId'] = mMailId;
    if (mRecipientId != null)
      json['mRecipientId'] = mRecipientId;
    return json;
  }

  static List<RsMailIdRecipientIdPair> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMailIdRecipientIdPair>() : json.map((value) => RsMailIdRecipientIdPair.fromJson(value)).toList();
  }

  static Map<String, RsMailIdRecipientIdPair> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMailIdRecipientIdPair>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMailIdRecipientIdPair.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMailIdRecipientIdPair-objects as value to a dart map
  static Map<String, List<RsMailIdRecipientIdPair>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMailIdRecipientIdPair>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMailIdRecipientIdPair.listFromJson(value);
       });
     }
     return map;
  }
}


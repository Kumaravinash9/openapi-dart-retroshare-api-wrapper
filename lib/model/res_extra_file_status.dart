part of openapi.api;

class ResExtraFileStatus {
  
  bool retval = null;
  
  FileInfo info = null;
  ResExtraFileStatus();

  @override
  String toString() {
    return 'ResExtraFileStatus[retval=$retval, info=$info, ]';
  }

  ResExtraFileStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      FileInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResExtraFileStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExtraFileStatus>() : json.map((value) => ResExtraFileStatus.fromJson(value)).toList();
  }

  static Map<String, ResExtraFileStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExtraFileStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExtraFileStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExtraFileStatus-objects as value to a dart map
  static Map<String, List<ResExtraFileStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExtraFileStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExtraFileStatus.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResSetDefaultIdentityForChatLobby {
  
  bool retval = null;
  ResSetDefaultIdentityForChatLobby();

  @override
  String toString() {
    return 'ResSetDefaultIdentityForChatLobby[retval=$retval, ]';
  }

  ResSetDefaultIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetDefaultIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetDefaultIdentityForChatLobby>() : json.map((value) => ResSetDefaultIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResSetDefaultIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetDefaultIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetDefaultIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetDefaultIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ResSetDefaultIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetDefaultIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetDefaultIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqIsOwnId {
  
  String id = null;
  ReqIsOwnId();

  @override
  String toString() {
    return 'ReqIsOwnId[id=$id, ]';
  }

  ReqIsOwnId.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ReqIsOwnId> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsOwnId>() : json.map((value) => ReqIsOwnId.fromJson(value)).toList();
  }

  static Map<String, ReqIsOwnId> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsOwnId>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsOwnId.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsOwnId-objects as value to a dart map
  static Map<String, List<ReqIsOwnId>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsOwnId>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsOwnId.listFromJson(value);
       });
     }
     return map;
  }
}


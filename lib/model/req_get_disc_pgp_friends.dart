part of openapi.api;

class ReqGetDiscPgpFriends {
  
  String pgpid = null;
  ReqGetDiscPgpFriends();

  @override
  String toString() {
    return 'ReqGetDiscPgpFriends[pgpid=$pgpid, ]';
  }

  ReqGetDiscPgpFriends.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pgpid = json['pgpid'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pgpid != null)
      json['pgpid'] = pgpid;
    return json;
  }

  static List<ReqGetDiscPgpFriends> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetDiscPgpFriends>() : json.map((value) => ReqGetDiscPgpFriends.fromJson(value)).toList();
  }

  static Map<String, ReqGetDiscPgpFriends> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetDiscPgpFriends>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetDiscPgpFriends.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetDiscPgpFriends-objects as value to a dart map
  static Map<String, List<ReqGetDiscPgpFriends>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetDiscPgpFriends>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetDiscPgpFriends.listFromJson(value);
       });
     }
     return map;
  }
}


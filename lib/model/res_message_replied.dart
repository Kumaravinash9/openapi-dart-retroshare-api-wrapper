part of openapi.api;

class ResMessageReplied {
  
  bool retval = null;
  ResMessageReplied();

  @override
  String toString() {
    return 'ResMessageReplied[retval=$retval, ]';
  }

  ResMessageReplied.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageReplied> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageReplied>() : json.map((value) => ResMessageReplied.fromJson(value)).toList();
  }

  static Map<String, ResMessageReplied> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageReplied>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageReplied.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageReplied-objects as value to a dart map
  static Map<String, List<ResMessageReplied>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageReplied>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageReplied.listFromJson(value);
       });
     }
     return map;
  }
}


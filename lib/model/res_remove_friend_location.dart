part of openapi.api;

class ResRemoveFriendLocation {
  
  bool retval = null;
  ResRemoveFriendLocation();

  @override
  String toString() {
    return 'ResRemoveFriendLocation[retval=$retval, ]';
  }

  ResRemoveFriendLocation.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResRemoveFriendLocation> listFromJson(List<dynamic> json) {
    return json == null ? List<ResRemoveFriendLocation>() : json.map((value) => ResRemoveFriendLocation.fromJson(value)).toList();
  }

  static Map<String, ResRemoveFriendLocation> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResRemoveFriendLocation>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResRemoveFriendLocation.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResRemoveFriendLocation-objects as value to a dart map
  static Map<String, List<ResRemoveFriendLocation>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResRemoveFriendLocation>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResRemoveFriendLocation.listFromJson(value);
       });
     }
     return map;
  }
}


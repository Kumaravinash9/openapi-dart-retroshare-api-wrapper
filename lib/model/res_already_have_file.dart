part of openapi.api;

class ResAlreadyHaveFile {
  
  bool retval = null;
  
  FileInfo info = null;
  ResAlreadyHaveFile();

  @override
  String toString() {
    return 'ResAlreadyHaveFile[retval=$retval, info=$info, ]';
  }

  ResAlreadyHaveFile.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      FileInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResAlreadyHaveFile> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAlreadyHaveFile>() : json.map((value) => ResAlreadyHaveFile.fromJson(value)).toList();
  }

  static Map<String, ResAlreadyHaveFile> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAlreadyHaveFile>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAlreadyHaveFile.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAlreadyHaveFile-objects as value to a dart map
  static Map<String, List<ResAlreadyHaveFile>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAlreadyHaveFile>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAlreadyHaveFile.listFromJson(value);
       });
     }
     return map;
  }
}


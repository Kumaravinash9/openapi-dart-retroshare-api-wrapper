part of openapi.api;

class ResGetDistantChatStatus {
  
  bool retval = null;
  
  DistantChatPeerInfo info = null;
  ResGetDistantChatStatus();

  @override
  String toString() {
    return 'ResGetDistantChatStatus[retval=$retval, info=$info, ]';
  }

  ResGetDistantChatStatus.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    info = (json['info'] == null) ?
      null :
      DistantChatPeerInfo.fromJson(json['info']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (info != null)
      json['info'] = info;
    return json;
  }

  static List<ResGetDistantChatStatus> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDistantChatStatus>() : json.map((value) => ResGetDistantChatStatus.fromJson(value)).toList();
  }

  static Map<String, ResGetDistantChatStatus> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDistantChatStatus>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDistantChatStatus.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDistantChatStatus-objects as value to a dart map
  static Map<String, List<ResGetDistantChatStatus>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDistantChatStatus>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDistantChatStatus.listFromJson(value);
       });
     }
     return map;
  }
}


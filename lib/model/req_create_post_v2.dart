part of openapi.api;

class ReqCreatePostV2 {
  
  String channelId = null;
  
  String title = null;
  
  String mBody = null;
  
  List<RsGxsFile> files = [];
  
  RsGxsImage thumbnail = null;
  
  String origPostId = null;
  ReqCreatePostV2();

  @override
  String toString() {
    return 'ReqCreatePostV2[channelId=$channelId, title=$title, mBody=$mBody, files=$files, thumbnail=$thumbnail, origPostId=$origPostId, ]';
  }

  ReqCreatePostV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    channelId = json['channelId'];
    title = json['title'];
    mBody = json['mBody'];
    files = (json['files'] == null) ?
      null :
      RsGxsFile.listFromJson(json['files']);
    thumbnail = (json['thumbnail'] == null) ?
      null :
      RsGxsImage.fromJson(json['thumbnail']);
    origPostId = json['origPostId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (channelId != null)
      json['channelId'] = channelId;
    if (title != null)
      json['title'] = title;
    if (mBody != null)
      json['mBody'] = mBody;
    if (files != null)
      json['files'] = files;
    if (thumbnail != null)
      json['thumbnail'] = thumbnail;
    if (origPostId != null)
      json['origPostId'] = origPostId;
    return json;
  }

  static List<ReqCreatePostV2> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreatePostV2>() : json.map((value) => ReqCreatePostV2.fromJson(value)).toList();
  }

  static Map<String, ReqCreatePostV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreatePostV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreatePostV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreatePostV2-objects as value to a dart map
  static Map<String, List<ReqCreatePostV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreatePostV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreatePostV2.listFromJson(value);
       });
     }
     return map;
  }
}


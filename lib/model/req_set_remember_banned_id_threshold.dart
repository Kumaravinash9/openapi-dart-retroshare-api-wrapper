part of openapi.api;

class ReqSetRememberBannedIdThreshold {
  
  int days = null;
  ReqSetRememberBannedIdThreshold();

  @override
  String toString() {
    return 'ReqSetRememberBannedIdThreshold[days=$days, ]';
  }

  ReqSetRememberBannedIdThreshold.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    days = json['days'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (days != null)
      json['days'] = days;
    return json;
  }

  static List<ReqSetRememberBannedIdThreshold> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetRememberBannedIdThreshold>() : json.map((value) => ReqSetRememberBannedIdThreshold.fromJson(value)).toList();
  }

  static Map<String, ReqSetRememberBannedIdThreshold> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetRememberBannedIdThreshold>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetRememberBannedIdThreshold.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetRememberBannedIdThreshold-objects as value to a dart map
  static Map<String, List<ReqSetRememberBannedIdThreshold>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetRememberBannedIdThreshold>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetRememberBannedIdThreshold.listFromJson(value);
       });
     }
     return map;
  }
}


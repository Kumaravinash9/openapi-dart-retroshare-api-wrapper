part of openapi.api;

class ResSubscribeToChannel {
  
  bool retval = null;
  ResSubscribeToChannel();

  @override
  String toString() {
    return 'ResSubscribeToChannel[retval=$retval, ]';
  }

  ResSubscribeToChannel.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSubscribeToChannel> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSubscribeToChannel>() : json.map((value) => ResSubscribeToChannel.fromJson(value)).toList();
  }

  static Map<String, ResSubscribeToChannel> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSubscribeToChannel>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSubscribeToChannel.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSubscribeToChannel-objects as value to a dart map
  static Map<String, List<ResSubscribeToChannel>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSubscribeToChannel>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSubscribeToChannel.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class RsMsgsMsgTagTypeTypes {
  
  int key = null;
  
  RsMsgsMsgTagTypeValue value = null;
  RsMsgsMsgTagTypeTypes();

  @override
  String toString() {
    return 'RsMsgsMsgTagTypeTypes[key=$key, value=$value, ]';
  }

  RsMsgsMsgTagTypeTypes.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      RsMsgsMsgTagTypeValue.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<RsMsgsMsgTagTypeTypes> listFromJson(List<dynamic> json) {
    return json == null ? List<RsMsgsMsgTagTypeTypes>() : json.map((value) => RsMsgsMsgTagTypeTypes.fromJson(value)).toList();
  }

  static Map<String, RsMsgsMsgTagTypeTypes> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsMsgsMsgTagTypeTypes>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsMsgsMsgTagTypeTypes.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsMsgsMsgTagTypeTypes-objects as value to a dart map
  static Map<String, List<RsMsgsMsgTagTypeTypes>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsMsgsMsgTagTypeTypes>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsMsgsMsgTagTypeTypes.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSetMessageTagType {
  
  int tagId = null;
  
  String text = null;
  
  int rgbColor = null;
  ReqSetMessageTagType();

  @override
  String toString() {
    return 'ReqSetMessageTagType[tagId=$tagId, text=$text, rgbColor=$rgbColor, ]';
  }

  ReqSetMessageTagType.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    tagId = json['tagId'];
    text = json['text'];
    rgbColor = json['rgb_color'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (tagId != null)
      json['tagId'] = tagId;
    if (text != null)
      json['text'] = text;
    if (rgbColor != null)
      json['rgb_color'] = rgbColor;
    return json;
  }

  static List<ReqSetMessageTagType> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetMessageTagType>() : json.map((value) => ReqSetMessageTagType.fromJson(value)).toList();
  }

  static Map<String, ReqSetMessageTagType> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetMessageTagType>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetMessageTagType.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetMessageTagType-objects as value to a dart map
  static Map<String, List<ReqSetMessageTagType>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetMessageTagType>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetMessageTagType.listFromJson(value);
       });
     }
     return map;
  }
}


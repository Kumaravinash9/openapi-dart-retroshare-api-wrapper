part of openapi.api;

class ReqLoadCertificateFromString {
  
  String cert = null;
  ReqLoadCertificateFromString();

  @override
  String toString() {
    return 'ReqLoadCertificateFromString[cert=$cert, ]';
  }

  ReqLoadCertificateFromString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    cert = json['cert'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (cert != null)
      json['cert'] = cert;
    return json;
  }

  static List<ReqLoadCertificateFromString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqLoadCertificateFromString>() : json.map((value) => ReqLoadCertificateFromString.fromJson(value)).toList();
  }

  static Map<String, ReqLoadCertificateFromString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqLoadCertificateFromString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqLoadCertificateFromString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqLoadCertificateFromString-objects as value to a dart map
  static Map<String, List<ReqLoadCertificateFromString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqLoadCertificateFromString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqLoadCertificateFromString.listFromJson(value);
       });
     }
     return map;
  }
}


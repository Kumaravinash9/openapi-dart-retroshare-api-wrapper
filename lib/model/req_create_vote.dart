part of openapi.api;

class ReqCreateVote {
  
  RsGxsVote vote = null;
  ReqCreateVote();

  @override
  String toString() {
    return 'ReqCreateVote[vote=$vote, ]';
  }

  ReqCreateVote.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    vote = (json['vote'] == null) ?
      null :
      RsGxsVote.fromJson(json['vote']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (vote != null)
      json['vote'] = vote;
    return json;
  }

  static List<ReqCreateVote> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateVote>() : json.map((value) => ReqCreateVote.fromJson(value)).toList();
  }

  static Map<String, ReqCreateVote> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateVote>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateVote.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateVote-objects as value to a dart map
  static Map<String, List<ReqCreateVote>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateVote>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateVote.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetPeerDetails {
  
  String sslId = null;
  ReqGetPeerDetails();

  @override
  String toString() {
    return 'ReqGetPeerDetails[sslId=$sslId, ]';
  }

  ReqGetPeerDetails.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    sslId = json['sslId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (sslId != null)
      json['sslId'] = sslId;
    return json;
  }

  static List<ReqGetPeerDetails> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetPeerDetails>() : json.map((value) => ReqGetPeerDetails.fromJson(value)).toList();
  }

  static Map<String, ReqGetPeerDetails> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetPeerDetails>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetPeerDetails.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetPeerDetails-objects as value to a dart map
  static Map<String, List<ReqGetPeerDetails>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetPeerDetails>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetPeerDetails.listFromJson(value);
       });
     }
     return map;
  }
}


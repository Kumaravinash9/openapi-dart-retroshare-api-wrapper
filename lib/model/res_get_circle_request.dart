part of openapi.api;

class ResGetCircleRequest {
  
  bool retval = null;
  
  RsGxsCircleMsg msg = null;
  ResGetCircleRequest();

  @override
  String toString() {
    return 'ResGetCircleRequest[retval=$retval, msg=$msg, ]';
  }

  ResGetCircleRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msg = (json['msg'] == null) ?
      null :
      RsGxsCircleMsg.fromJson(json['msg']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msg != null)
      json['msg'] = msg;
    return json;
  }

  static List<ResGetCircleRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetCircleRequest>() : json.map((value) => ResGetCircleRequest.fromJson(value)).toList();
  }

  static Map<String, ResGetCircleRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetCircleRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetCircleRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetCircleRequest-objects as value to a dart map
  static Map<String, List<ResGetCircleRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetCircleRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetCircleRequest.listFromJson(value);
       });
     }
     return map;
  }
}


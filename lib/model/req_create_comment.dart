part of openapi.api;

class ReqCreateComment {
  
  RsGxsComment comment = null;
  ReqCreateComment();

  @override
  String toString() {
    return 'ReqCreateComment[comment=$comment, ]';
  }

  ReqCreateComment.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    comment = (json['comment'] == null) ?
      null :
      RsGxsComment.fromJson(json['comment']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (comment != null)
      json['comment'] = comment;
    return json;
  }

  static List<ReqCreateComment> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqCreateComment>() : json.map((value) => ReqCreateComment.fromJson(value)).toList();
  }

  static Map<String, ReqCreateComment> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqCreateComment>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqCreateComment.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqCreateComment-objects as value to a dart map
  static Map<String, List<ReqCreateComment>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqCreateComment>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqCreateComment.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResGetChannelContent {
  
  bool retval = null;
  
  List<RsGxsChannelPost> posts = [];
  
  List<RsGxsComment> comments = [];
  
  List<RsGxsVote> votes = [];
  ResGetChannelContent();

  @override
  String toString() {
    return 'ResGetChannelContent[retval=$retval, posts=$posts, comments=$comments, votes=$votes, ]';
  }

  ResGetChannelContent.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    posts = (json['posts'] == null) ?
      null :
      RsGxsChannelPost.listFromJson(json['posts']);
    comments = (json['comments'] == null) ?
      null :
      RsGxsComment.listFromJson(json['comments']);
    votes = (json['votes'] == null) ?
      null :
      RsGxsVote.listFromJson(json['votes']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (posts != null)
      json['posts'] = posts;
    if (comments != null)
      json['comments'] = comments;
    if (votes != null)
      json['votes'] = votes;
    return json;
  }

  static List<ResGetChannelContent> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetChannelContent>() : json.map((value) => ResGetChannelContent.fromJson(value)).toList();
  }

  static Map<String, ResGetChannelContent> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetChannelContent>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetChannelContent.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetChannelContent-objects as value to a dart map
  static Map<String, List<ResGetChannelContent>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetChannelContent>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetChannelContent.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResExportIdentity {
  
  bool retval = null;
  ResExportIdentity();

  @override
  String toString() {
    return 'ResExportIdentity[retval=$retval, ]';
  }

  ResExportIdentity.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResExportIdentity> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportIdentity>() : json.map((value) => ResExportIdentity.fromJson(value)).toList();
  }

  static Map<String, ResExportIdentity> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportIdentity>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportIdentity.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportIdentity-objects as value to a dart map
  static Map<String, List<ResExportIdentity>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportIdentity>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportIdentity.listFromJson(value);
       });
     }
     return map;
  }
}


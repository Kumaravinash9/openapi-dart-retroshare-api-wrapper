part of openapi.api;

class ResGetIdentitiesInfo {
  
  bool retval = null;
  
  List<RsGxsIdGroup> idsInfo = [];
  ResGetIdentitiesInfo();

  @override
  String toString() {
    return 'ResGetIdentitiesInfo[retval=$retval, idsInfo=$idsInfo, ]';
  }

  ResGetIdentitiesInfo.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    idsInfo = (json['idsInfo'] == null) ?
      null :
      RsGxsIdGroup.listFromJson(json['idsInfo']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (idsInfo != null)
      json['idsInfo'] = idsInfo;
    return json;
  }

  static List<ResGetIdentitiesInfo> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetIdentitiesInfo>() : json.map((value) => ResGetIdentitiesInfo.fromJson(value)).toList();
  }

  static Map<String, ResGetIdentitiesInfo> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetIdentitiesInfo>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetIdentitiesInfo.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetIdentitiesInfo-objects as value to a dart map
  static Map<String, List<ResGetIdentitiesInfo>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetIdentitiesInfo>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetIdentitiesInfo.listFromJson(value);
       });
     }
     return map;
  }
}


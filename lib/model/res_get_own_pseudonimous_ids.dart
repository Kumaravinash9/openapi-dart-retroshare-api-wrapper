part of openapi.api;

class ResGetOwnPseudonimousIds {
  
  bool retval = null;
  
  List<String> ids = [];
  ResGetOwnPseudonimousIds();

  @override
  String toString() {
    return 'ResGetOwnPseudonimousIds[retval=$retval, ids=$ids, ]';
  }

  ResGetOwnPseudonimousIds.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    ids = (json['ids'] == null) ?
      null :
      (json['ids'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (ids != null)
      json['ids'] = ids;
    return json;
  }

  static List<ResGetOwnPseudonimousIds> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetOwnPseudonimousIds>() : json.map((value) => ResGetOwnPseudonimousIds.fromJson(value)).toList();
  }

  static Map<String, ResGetOwnPseudonimousIds> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetOwnPseudonimousIds>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetOwnPseudonimousIds.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetOwnPseudonimousIds-objects as value to a dart map
  static Map<String, List<ResGetOwnPseudonimousIds>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetOwnPseudonimousIds>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetOwnPseudonimousIds.listFromJson(value);
       });
     }
     return map;
  }
}


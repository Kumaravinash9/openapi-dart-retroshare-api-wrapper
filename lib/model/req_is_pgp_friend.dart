part of openapi.api;

class ReqIsPgpFriend {
  
  String pgpId = null;
  ReqIsPgpFriend();

  @override
  String toString() {
    return 'ReqIsPgpFriend[pgpId=$pgpId, ]';
  }

  ReqIsPgpFriend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    pgpId = json['pgpId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (pgpId != null)
      json['pgpId'] = pgpId;
    return json;
  }

  static List<ReqIsPgpFriend> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqIsPgpFriend>() : json.map((value) => ReqIsPgpFriend.fromJson(value)).toList();
  }

  static Map<String, ReqIsPgpFriend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqIsPgpFriend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqIsPgpFriend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqIsPgpFriend-objects as value to a dart map
  static Map<String, List<ReqIsPgpFriend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqIsPgpFriend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqIsPgpFriend.listFromJson(value);
       });
     }
     return map;
  }
}


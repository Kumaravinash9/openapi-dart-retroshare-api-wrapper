part of openapi.api;

class ReqAuthorizeUser {
  
  String user = null;
  
  String password = null;
  ReqAuthorizeUser();

  @override
  String toString() {
    return 'ReqAuthorizeUser[user=$user, password=$password, ]';
  }

  ReqAuthorizeUser.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    user = json['user'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (user != null)
      json['user'] = user;
    if (password != null)
      json['password'] = password;
    return json;
  }

  static List<ReqAuthorizeUser> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqAuthorizeUser>() : json.map((value) => ReqAuthorizeUser.fromJson(value)).toList();
  }

  static Map<String, ReqAuthorizeUser> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqAuthorizeUser>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqAuthorizeUser.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqAuthorizeUser-objects as value to a dart map
  static Map<String, List<ReqAuthorizeUser>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqAuthorizeUser>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqAuthorizeUser.listFromJson(value);
       });
     }
     return map;
  }
}


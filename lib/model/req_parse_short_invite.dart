part of openapi.api;

class ReqParseShortInvite {
  
  String invite = null;
  ReqParseShortInvite();

  @override
  String toString() {
    return 'ReqParseShortInvite[invite=$invite, ]';
  }

  ReqParseShortInvite.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    invite = json['invite'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (invite != null)
      json['invite'] = invite;
    return json;
  }

  static List<ReqParseShortInvite> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqParseShortInvite>() : json.map((value) => ReqParseShortInvite.fromJson(value)).toList();
  }

  static Map<String, ReqParseShortInvite> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqParseShortInvite>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqParseShortInvite.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqParseShortInvite-objects as value to a dart map
  static Map<String, List<ReqParseShortInvite>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqParseShortInvite>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqParseShortInvite.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResMessageToDraft {
  
  bool retval = null;
  ResMessageToDraft();

  @override
  String toString() {
    return 'ResMessageToDraft[retval=$retval, ]';
  }

  ResMessageToDraft.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageToDraft> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageToDraft>() : json.map((value) => ResMessageToDraft.fromJson(value)).toList();
  }

  static Map<String, ResMessageToDraft> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageToDraft>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageToDraft.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageToDraft-objects as value to a dart map
  static Map<String, List<ResMessageToDraft>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageToDraft>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageToDraft.listFromJson(value);
       });
     }
     return map;
  }
}


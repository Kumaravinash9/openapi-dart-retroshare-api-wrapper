part of openapi.api;

class ResCreateComment {
  
  bool retval = null;
  
  RsGxsComment comment = null;
  ResCreateComment();

  @override
  String toString() {
    return 'ResCreateComment[retval=$retval, comment=$comment, ]';
  }

  ResCreateComment.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    comment = (json['comment'] == null) ?
      null :
      RsGxsComment.fromJson(json['comment']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (comment != null)
      json['comment'] = comment;
    return json;
  }

  static List<ResCreateComment> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateComment>() : json.map((value) => ResCreateComment.fromJson(value)).toList();
  }

  static Map<String, ResCreateComment> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateComment>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateComment.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateComment-objects as value to a dart map
  static Map<String, List<ResCreateComment>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateComment>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateComment.listFromJson(value);
       });
     }
     return map;
  }
}


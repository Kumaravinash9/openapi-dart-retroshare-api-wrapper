part of openapi.api;

class ResMessageSend {
  
  bool retval = null;
  ResMessageSend();

  @override
  String toString() {
    return 'ResMessageSend[retval=$retval, ]';
  }

  ResMessageSend.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageSend> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageSend>() : json.map((value) => ResMessageSend.fromJson(value)).toList();
  }

  static Map<String, ResMessageSend> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageSend>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageSend.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageSend-objects as value to a dart map
  static Map<String, List<ResMessageSend>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageSend>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageSend.listFromJson(value);
       });
     }
     return map;
  }
}


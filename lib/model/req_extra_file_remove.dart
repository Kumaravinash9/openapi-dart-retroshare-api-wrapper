part of openapi.api;

class ReqExtraFileRemove {
  
  String hash = null;
  ReqExtraFileRemove();

  @override
  String toString() {
    return 'ReqExtraFileRemove[hash=$hash, ]';
  }

  ReqExtraFileRemove.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    hash = json['hash'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (hash != null)
      json['hash'] = hash;
    return json;
  }

  static List<ReqExtraFileRemove> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqExtraFileRemove>() : json.map((value) => ReqExtraFileRemove.fromJson(value)).toList();
  }

  static Map<String, ReqExtraFileRemove> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqExtraFileRemove>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqExtraFileRemove.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqExtraFileRemove-objects as value to a dart map
  static Map<String, List<ReqExtraFileRemove>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqExtraFileRemove>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqExtraFileRemove.listFromJson(value);
       });
     }
     return map;
  }
}


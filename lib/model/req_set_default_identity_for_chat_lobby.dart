part of openapi.api;

class ReqSetDefaultIdentityForChatLobby {
  
  String nick = null;
  ReqSetDefaultIdentityForChatLobby();

  @override
  String toString() {
    return 'ReqSetDefaultIdentityForChatLobby[nick=$nick, ]';
  }

  ReqSetDefaultIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    nick = json['nick'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (nick != null)
      json['nick'] = nick;
    return json;
  }

  static List<ReqSetDefaultIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSetDefaultIdentityForChatLobby>() : json.map((value) => ReqSetDefaultIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ReqSetDefaultIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSetDefaultIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSetDefaultIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSetDefaultIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ReqSetDefaultIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSetDefaultIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSetDefaultIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class RsPeerServiceInfoMServiceList {
  
  int key = null;
  
  RsServiceInfo value = null;
  RsPeerServiceInfoMServiceList();

  @override
  String toString() {
    return 'RsPeerServiceInfoMServiceList[key=$key, value=$value, ]';
  }

  RsPeerServiceInfoMServiceList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    key = json['key'];
    value = (json['value'] == null) ?
      null :
      RsServiceInfo.fromJson(json['value']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (key != null)
      json['key'] = key;
    if (value != null)
      json['value'] = value;
    return json;
  }

  static List<RsPeerServiceInfoMServiceList> listFromJson(List<dynamic> json) {
    return json == null ? List<RsPeerServiceInfoMServiceList>() : json.map((value) => RsPeerServiceInfoMServiceList.fromJson(value)).toList();
  }

  static Map<String, RsPeerServiceInfoMServiceList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsPeerServiceInfoMServiceList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsPeerServiceInfoMServiceList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsPeerServiceInfoMServiceList-objects as value to a dart map
  static Map<String, List<RsPeerServiceInfoMServiceList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsPeerServiceInfoMServiceList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsPeerServiceInfoMServiceList.listFromJson(value);
       });
     }
     return map;
  }
}


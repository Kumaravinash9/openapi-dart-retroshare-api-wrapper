part of openapi.api;

class ResTurtleChannelRequest {
  
  bool retval = null;
  ResTurtleChannelRequest();

  @override
  String toString() {
    return 'ResTurtleChannelRequest[retval=$retval, ]';
  }

  ResTurtleChannelRequest.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResTurtleChannelRequest> listFromJson(List<dynamic> json) {
    return json == null ? List<ResTurtleChannelRequest>() : json.map((value) => ResTurtleChannelRequest.fromJson(value)).toList();
  }

  static Map<String, ResTurtleChannelRequest> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResTurtleChannelRequest>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResTurtleChannelRequest.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResTurtleChannelRequest-objects as value to a dart map
  static Map<String, List<ResTurtleChannelRequest>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResTurtleChannelRequest>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResTurtleChannelRequest.listFromJson(value);
       });
     }
     return map;
  }
}


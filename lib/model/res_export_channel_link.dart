part of openapi.api;

class ResExportChannelLink {
  
  bool retval = null;
  
  String link = null;
  
  String errMsg = null;
  ResExportChannelLink();

  @override
  String toString() {
    return 'ResExportChannelLink[retval=$retval, link=$link, errMsg=$errMsg, ]';
  }

  ResExportChannelLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    link = json['link'];
    errMsg = json['errMsg'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    if (errMsg != null)
      json['errMsg'] = errMsg;
    return json;
  }

  static List<ResExportChannelLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportChannelLink>() : json.map((value) => ResExportChannelLink.fromJson(value)).toList();
  }

  static Map<String, ResExportChannelLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportChannelLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportChannelLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportChannelLink-objects as value to a dart map
  static Map<String, List<ResExportChannelLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportChannelLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportChannelLink.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class TurtleFileInfoV2 {
  
  ReqBanFileFileSize fSize = null;
  
  String fHash = null;
  
  String fName = null;
  
  num fWeight = null;
  
  String fSnippet = null;
  TurtleFileInfoV2();

  @override
  String toString() {
    return 'TurtleFileInfoV2[fSize=$fSize, fHash=$fHash, fName=$fName, fWeight=$fWeight, fSnippet=$fSnippet, ]';
  }

  TurtleFileInfoV2.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    fSize = (json['fSize'] == null) ?
      null :
      ReqBanFileFileSize.fromJson(json['fSize']);
    fHash = json['fHash'];
    fName = json['fName'];
    fWeight = json['fWeight'];
    fSnippet = json['fSnippet'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (fSize != null)
      json['fSize'] = fSize;
    if (fHash != null)
      json['fHash'] = fHash;
    if (fName != null)
      json['fName'] = fName;
    if (fWeight != null)
      json['fWeight'] = fWeight;
    if (fSnippet != null)
      json['fSnippet'] = fSnippet;
    return json;
  }

  static List<TurtleFileInfoV2> listFromJson(List<dynamic> json) {
    return json == null ? List<TurtleFileInfoV2>() : json.map((value) => TurtleFileInfoV2.fromJson(value)).toList();
  }

  static Map<String, TurtleFileInfoV2> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, TurtleFileInfoV2>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = TurtleFileInfoV2.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of TurtleFileInfoV2-objects as value to a dart map
  static Map<String, List<TurtleFileInfoV2>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<TurtleFileInfoV2>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = TurtleFileInfoV2.listFromJson(value);
       });
     }
     return map;
  }
}


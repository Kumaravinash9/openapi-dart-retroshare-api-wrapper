part of openapi.api;

class ResAutoPositiveOpinionForContacts {
  
  bool retval = null;
  ResAutoPositiveOpinionForContacts();

  @override
  String toString() {
    return 'ResAutoPositiveOpinionForContacts[retval=$retval, ]';
  }

  ResAutoPositiveOpinionForContacts.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResAutoPositiveOpinionForContacts> listFromJson(List<dynamic> json) {
    return json == null ? List<ResAutoPositiveOpinionForContacts>() : json.map((value) => ResAutoPositiveOpinionForContacts.fromJson(value)).toList();
  }

  static Map<String, ResAutoPositiveOpinionForContacts> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResAutoPositiveOpinionForContacts>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResAutoPositiveOpinionForContacts.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResAutoPositiveOpinionForContacts-objects as value to a dart map
  static Map<String, List<ResAutoPositiveOpinionForContacts>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResAutoPositiveOpinionForContacts>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResAutoPositiveOpinionForContacts.listFromJson(value);
       });
     }
     return map;
  }
}


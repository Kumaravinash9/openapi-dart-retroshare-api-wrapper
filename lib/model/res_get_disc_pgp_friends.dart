part of openapi.api;

class ResGetDiscPgpFriends {
  
  bool retval = null;
  
  List<String> gpgFriends = [];
  ResGetDiscPgpFriends();

  @override
  String toString() {
    return 'ResGetDiscPgpFriends[retval=$retval, gpgFriends=$gpgFriends, ]';
  }

  ResGetDiscPgpFriends.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    gpgFriends = (json['gpg_friends'] == null) ?
      null :
      (json['gpg_friends'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (gpgFriends != null)
      json['gpg_friends'] = gpgFriends;
    return json;
  }

  static List<ResGetDiscPgpFriends> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDiscPgpFriends>() : json.map((value) => ResGetDiscPgpFriends.fromJson(value)).toList();
  }

  static Map<String, ResGetDiscPgpFriends> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDiscPgpFriends>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDiscPgpFriends.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDiscPgpFriends-objects as value to a dart map
  static Map<String, List<ResGetDiscPgpFriends>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDiscPgpFriends>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDiscPgpFriends.listFromJson(value);
       });
     }
     return map;
  }
}


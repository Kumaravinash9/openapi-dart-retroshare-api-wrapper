part of openapi.api;

class ResGetListOfNearbyChatLobbies {
  
  List<VisibleChatLobbyRecord> publicLobbies = [];
  ResGetListOfNearbyChatLobbies();

  @override
  String toString() {
    return 'ResGetListOfNearbyChatLobbies[publicLobbies=$publicLobbies, ]';
  }

  ResGetListOfNearbyChatLobbies.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    publicLobbies = (json['public_lobbies'] == null) ?
      null :
      VisibleChatLobbyRecord.listFromJson(json['public_lobbies']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (publicLobbies != null)
      json['public_lobbies'] = publicLobbies;
    return json;
  }

  static List<ResGetListOfNearbyChatLobbies> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetListOfNearbyChatLobbies>() : json.map((value) => ResGetListOfNearbyChatLobbies.fromJson(value)).toList();
  }

  static Map<String, ResGetListOfNearbyChatLobbies> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetListOfNearbyChatLobbies>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetListOfNearbyChatLobbies.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetListOfNearbyChatLobbies-objects as value to a dart map
  static Map<String, List<ResGetListOfNearbyChatLobbies>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetListOfNearbyChatLobbies>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetListOfNearbyChatLobbies.listFromJson(value);
       });
     }
     return map;
  }
}


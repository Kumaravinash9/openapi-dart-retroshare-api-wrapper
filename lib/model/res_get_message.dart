part of openapi.api;

class ResGetMessage {
  
  bool retval = null;
  
  RsMsgsMessageInfo msg = null;
  ResGetMessage();

  @override
  String toString() {
    return 'ResGetMessage[retval=$retval, msg=$msg, ]';
  }

  ResGetMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    msg = (json['msg'] == null) ?
      null :
      RsMsgsMessageInfo.fromJson(json['msg']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (msg != null)
      json['msg'] = msg;
    return json;
  }

  static List<ResGetMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetMessage>() : json.map((value) => ResGetMessage.fromJson(value)).toList();
  }

  static Map<String, ResGetMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetMessage-objects as value to a dart map
  static Map<String, List<ResGetMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetMessage.listFromJson(value);
       });
     }
     return map;
  }
}


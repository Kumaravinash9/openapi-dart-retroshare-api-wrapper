part of openapi.api;

class ResSetAsRegularContact {
  
  bool retval = null;
  ResSetAsRegularContact();

  @override
  String toString() {
    return 'ResSetAsRegularContact[retval=$retval, ]';
  }

  ResSetAsRegularContact.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetAsRegularContact> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetAsRegularContact>() : json.map((value) => ResSetAsRegularContact.fromJson(value)).toList();
  }

  static Map<String, ResSetAsRegularContact> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetAsRegularContact>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetAsRegularContact.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetAsRegularContact-objects as value to a dart map
  static Map<String, List<ResSetAsRegularContact>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetAsRegularContact>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetAsRegularContact.listFromJson(value);
       });
     }
     return map;
  }
}


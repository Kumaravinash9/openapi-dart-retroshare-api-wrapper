part of openapi.api;

class RsServicePermissions {
  
  int mServiceId = null;
  
  String mServiceName = null;
  
  bool mDefaultAllowed = null;
  
  List<String> mPeersAllowed = [];
  
  List<String> mPeersDenied = [];
  RsServicePermissions();

  @override
  String toString() {
    return 'RsServicePermissions[mServiceId=$mServiceId, mServiceName=$mServiceName, mDefaultAllowed=$mDefaultAllowed, mPeersAllowed=$mPeersAllowed, mPeersDenied=$mPeersDenied, ]';
  }

  RsServicePermissions.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    mServiceId = json['mServiceId'];
    mServiceName = json['mServiceName'];
    mDefaultAllowed = json['mDefaultAllowed'];
    mPeersAllowed = (json['mPeersAllowed'] == null) ?
      null :
      (json['mPeersAllowed'] as List).cast<String>();
    mPeersDenied = (json['mPeersDenied'] == null) ?
      null :
      (json['mPeersDenied'] as List).cast<String>();
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (mServiceId != null)
      json['mServiceId'] = mServiceId;
    if (mServiceName != null)
      json['mServiceName'] = mServiceName;
    if (mDefaultAllowed != null)
      json['mDefaultAllowed'] = mDefaultAllowed;
    if (mPeersAllowed != null)
      json['mPeersAllowed'] = mPeersAllowed;
    if (mPeersDenied != null)
      json['mPeersDenied'] = mPeersDenied;
    return json;
  }

  static List<RsServicePermissions> listFromJson(List<dynamic> json) {
    return json == null ? List<RsServicePermissions>() : json.map((value) => RsServicePermissions.fromJson(value)).toList();
  }

  static Map<String, RsServicePermissions> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, RsServicePermissions>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = RsServicePermissions.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of RsServicePermissions-objects as value to a dart map
  static Map<String, List<RsServicePermissions>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<RsServicePermissions>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = RsServicePermissions.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResSetSharedDirectories {
  
  bool retval = null;
  ResSetSharedDirectories();

  @override
  String toString() {
    return 'ResSetSharedDirectories[retval=$retval, ]';
  }

  ResSetSharedDirectories.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetSharedDirectories> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetSharedDirectories>() : json.map((value) => ResSetSharedDirectories.fromJson(value)).toList();
  }

  static Map<String, ResSetSharedDirectories> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetSharedDirectories>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetSharedDirectories.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetSharedDirectories-objects as value to a dart map
  static Map<String, List<ResSetSharedDirectories>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetSharedDirectories>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetSharedDirectories.listFromJson(value);
       });
     }
     return map;
  }
}


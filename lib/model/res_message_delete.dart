part of openapi.api;

class ResMessageDelete {
  
  bool retval = null;
  ResMessageDelete();

  @override
  String toString() {
    return 'ResMessageDelete[retval=$retval, ]';
  }

  ResMessageDelete.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResMessageDelete> listFromJson(List<dynamic> json) {
    return json == null ? List<ResMessageDelete>() : json.map((value) => ResMessageDelete.fromJson(value)).toList();
  }

  static Map<String, ResMessageDelete> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResMessageDelete>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResMessageDelete.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResMessageDelete-objects as value to a dart map
  static Map<String, List<ResMessageDelete>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResMessageDelete>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResMessageDelete.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ResSetDestinationName {
  
  bool retval = null;
  ResSetDestinationName();

  @override
  String toString() {
    return 'ResSetDestinationName[retval=$retval, ]';
  }

  ResSetDestinationName.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSetDestinationName> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSetDestinationName>() : json.map((value) => ResSetDestinationName.fromJson(value)).toList();
  }

  static Map<String, ResSetDestinationName> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSetDestinationName>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSetDestinationName.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSetDestinationName-objects as value to a dart map
  static Map<String, List<ResSetDestinationName>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSetDestinationName>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSetDestinationName.listFromJson(value);
       });
     }
     return map;
  }
}


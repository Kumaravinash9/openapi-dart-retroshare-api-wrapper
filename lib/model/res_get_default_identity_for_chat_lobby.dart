part of openapi.api;

class ResGetDefaultIdentityForChatLobby {
  
  String id = null;
  ResGetDefaultIdentityForChatLobby();

  @override
  String toString() {
    return 'ResGetDefaultIdentityForChatLobby[id=$id, ]';
  }

  ResGetDefaultIdentityForChatLobby.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    return json;
  }

  static List<ResGetDefaultIdentityForChatLobby> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetDefaultIdentityForChatLobby>() : json.map((value) => ResGetDefaultIdentityForChatLobby.fromJson(value)).toList();
  }

  static Map<String, ResGetDefaultIdentityForChatLobby> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetDefaultIdentityForChatLobby>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetDefaultIdentityForChatLobby.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetDefaultIdentityForChatLobby-objects as value to a dart map
  static Map<String, List<ResGetDefaultIdentityForChatLobby>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetDefaultIdentityForChatLobby>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetDefaultIdentityForChatLobby.listFromJson(value);
       });
     }
     return map;
  }
}


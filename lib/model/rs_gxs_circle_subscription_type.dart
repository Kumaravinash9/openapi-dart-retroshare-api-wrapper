part of openapi.api;

class RsGxsCircleSubscriptionType {
  /// The underlying value of this enum member.
  final int value;

  const RsGxsCircleSubscriptionType._internal(this.value);

  static const RsGxsCircleSubscriptionType number0_ = const RsGxsCircleSubscriptionType._internal(0);
  static const RsGxsCircleSubscriptionType number1_ = const RsGxsCircleSubscriptionType._internal(1);
  static const RsGxsCircleSubscriptionType number2_ = const RsGxsCircleSubscriptionType._internal(2);
  
  int toJson (){
    return this.value;
  }

  static RsGxsCircleSubscriptionType fromJson(int value) {
    return new RsGxsCircleSubscriptionTypeTypeTransformer().decode(value);
  }
  
  static List<RsGxsCircleSubscriptionType> listFromJson(List<dynamic> json) {
    return json == null ? new List<RsGxsCircleSubscriptionType>() : json.map((value) => RsGxsCircleSubscriptionType.fromJson(value)).toList();
  }
}

class RsGxsCircleSubscriptionTypeTypeTransformer {

  dynamic encode(RsGxsCircleSubscriptionType data) {
    return data.value;
  }

  RsGxsCircleSubscriptionType decode(dynamic data) {
    switch (data) {
      case 0: return RsGxsCircleSubscriptionType.number0_;
      case 1: return RsGxsCircleSubscriptionType.number1_;
      case 2: return RsGxsCircleSubscriptionType.number2_;
      default: throw('Unknown enum value to decode: $data');
    }
  }
}


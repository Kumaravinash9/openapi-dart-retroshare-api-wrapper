part of openapi.api;

class ResSendChat {
  
  bool retval = null;
  ResSendChat();

  @override
  String toString() {
    return 'ResSendChat[retval=$retval, ]';
  }

  ResSendChat.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResSendChat> listFromJson(List<dynamic> json) {
    return json == null ? List<ResSendChat>() : json.map((value) => ResSendChat.fromJson(value)).toList();
  }

  static Map<String, ResSendChat> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResSendChat>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResSendChat.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResSendChat-objects as value to a dart map
  static Map<String, List<ResSendChat>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResSendChat>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResSendChat.listFromJson(value);
       });
     }
     return map;
  }
}


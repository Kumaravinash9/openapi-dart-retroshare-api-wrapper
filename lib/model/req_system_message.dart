part of openapi.api;

class ReqSystemMessage {
  
  String title = null;
  
  String message = null;
  
  int systemFlag = null;
  ReqSystemMessage();

  @override
  String toString() {
    return 'ReqSystemMessage[title=$title, message=$message, systemFlag=$systemFlag, ]';
  }

  ReqSystemMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    title = json['title'];
    message = json['message'];
    systemFlag = json['systemFlag'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (title != null)
      json['title'] = title;
    if (message != null)
      json['message'] = message;
    if (systemFlag != null)
      json['systemFlag'] = systemFlag;
    return json;
  }

  static List<ReqSystemMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSystemMessage>() : json.map((value) => ReqSystemMessage.fromJson(value)).toList();
  }

  static Map<String, ReqSystemMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSystemMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSystemMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSystemMessage-objects as value to a dart map
  static Map<String, List<ReqSystemMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSystemMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSystemMessage.listFromJson(value);
       });
     }
     return map;
  }
}


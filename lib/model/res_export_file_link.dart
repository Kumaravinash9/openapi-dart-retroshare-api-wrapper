part of openapi.api;

class ResExportFileLink {
  
  ResExportCollectionLinkRetval retval = null;
  
  String link = null;
  ResExportFileLink();

  @override
  String toString() {
    return 'ResExportFileLink[retval=$retval, link=$link, ]';
  }

  ResExportFileLink.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = (json['retval'] == null) ?
      null :
      ResExportCollectionLinkRetval.fromJson(json['retval']);
    link = json['link'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (link != null)
      json['link'] = link;
    return json;
  }

  static List<ResExportFileLink> listFromJson(List<dynamic> json) {
    return json == null ? List<ResExportFileLink>() : json.map((value) => ResExportFileLink.fromJson(value)).toList();
  }

  static Map<String, ResExportFileLink> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResExportFileLink>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResExportFileLink.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResExportFileLink-objects as value to a dart map
  static Map<String, List<ResExportFileLink>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResExportFileLink>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResExportFileLink.listFromJson(value);
       });
     }
     return map;
  }
}


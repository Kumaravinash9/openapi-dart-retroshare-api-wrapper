part of openapi.api;

class ResCreateVote {
  
  bool retval = null;
  
  RsGxsVote vote = null;
  ResCreateVote();

  @override
  String toString() {
    return 'ResCreateVote[retval=$retval, vote=$vote, ]';
  }

  ResCreateVote.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    vote = (json['vote'] == null) ?
      null :
      RsGxsVote.fromJson(json['vote']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (vote != null)
      json['vote'] = vote;
    return json;
  }

  static List<ResCreateVote> listFromJson(List<dynamic> json) {
    return json == null ? List<ResCreateVote>() : json.map((value) => ResCreateVote.fromJson(value)).toList();
  }

  static Map<String, ResCreateVote> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResCreateVote>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResCreateVote.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResCreateVote-objects as value to a dart map
  static Map<String, List<ResCreateVote>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResCreateVote>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResCreateVote.listFromJson(value);
       });
     }
     return map;
  }
}


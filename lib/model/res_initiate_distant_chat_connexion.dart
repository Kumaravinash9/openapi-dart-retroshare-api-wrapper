part of openapi.api;

class ResInitiateDistantChatConnexion {
  
  bool retval = null;
  
  String pid = null;
  
  int errorCode = null;
  ResInitiateDistantChatConnexion();

  @override
  String toString() {
    return 'ResInitiateDistantChatConnexion[retval=$retval, pid=$pid, errorCode=$errorCode, ]';
  }

  ResInitiateDistantChatConnexion.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    pid = json['pid'];
    errorCode = json['error_code'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (pid != null)
      json['pid'] = pid;
    if (errorCode != null)
      json['error_code'] = errorCode;
    return json;
  }

  static List<ResInitiateDistantChatConnexion> listFromJson(List<dynamic> json) {
    return json == null ? List<ResInitiateDistantChatConnexion>() : json.map((value) => ResInitiateDistantChatConnexion.fromJson(value)).toList();
  }

  static Map<String, ResInitiateDistantChatConnexion> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResInitiateDistantChatConnexion>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResInitiateDistantChatConnexion.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResInitiateDistantChatConnexion-objects as value to a dart map
  static Map<String, List<ResInitiateDistantChatConnexion>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResInitiateDistantChatConnexion>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResInitiateDistantChatConnexion.listFromJson(value);
       });
     }
     return map;
  }
}


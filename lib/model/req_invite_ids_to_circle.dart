part of openapi.api;

class ReqInviteIdsToCircle {
  
  List<String> identities = [];
  
  String circleId = null;
  ReqInviteIdsToCircle();

  @override
  String toString() {
    return 'ReqInviteIdsToCircle[identities=$identities, circleId=$circleId, ]';
  }

  ReqInviteIdsToCircle.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    identities = (json['identities'] == null) ?
      null :
      (json['identities'] as List).cast<String>();
    circleId = json['circleId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (identities != null)
      json['identities'] = identities;
    if (circleId != null)
      json['circleId'] = circleId;
    return json;
  }

  static List<ReqInviteIdsToCircle> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqInviteIdsToCircle>() : json.map((value) => ReqInviteIdsToCircle.fromJson(value)).toList();
  }

  static Map<String, ReqInviteIdsToCircle> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqInviteIdsToCircle>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqInviteIdsToCircle.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqInviteIdsToCircle-objects as value to a dart map
  static Map<String, List<ReqInviteIdsToCircle>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqInviteIdsToCircle>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqInviteIdsToCircle.listFromJson(value);
       });
     }
     return map;
  }
}


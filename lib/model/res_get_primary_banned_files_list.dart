part of openapi.api;

class ResGetPrimaryBannedFilesList {
  
  bool retval = null;
  
  List<ResGetPrimaryBannedFilesListBannedFiles> bannedFiles = [];
  ResGetPrimaryBannedFilesList();

  @override
  String toString() {
    return 'ResGetPrimaryBannedFilesList[retval=$retval, bannedFiles=$bannedFiles, ]';
  }

  ResGetPrimaryBannedFilesList.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
    bannedFiles = (json['bannedFiles'] == null) ?
      null :
      ResGetPrimaryBannedFilesListBannedFiles.listFromJson(json['bannedFiles']);
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    if (bannedFiles != null)
      json['bannedFiles'] = bannedFiles;
    return json;
  }

  static List<ResGetPrimaryBannedFilesList> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetPrimaryBannedFilesList>() : json.map((value) => ResGetPrimaryBannedFilesList.fromJson(value)).toList();
  }

  static Map<String, ResGetPrimaryBannedFilesList> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetPrimaryBannedFilesList>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetPrimaryBannedFilesList.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetPrimaryBannedFilesList-objects as value to a dart map
  static Map<String, List<ResGetPrimaryBannedFilesList>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetPrimaryBannedFilesList>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetPrimaryBannedFilesList.listFromJson(value);
       });
     }
     return map;
  }
}


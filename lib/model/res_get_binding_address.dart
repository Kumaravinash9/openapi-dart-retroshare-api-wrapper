part of openapi.api;

class ResGetBindingAddress {
  
  String retval = null;
  ResGetBindingAddress();

  @override
  String toString() {
    return 'ResGetBindingAddress[retval=$retval, ]';
  }

  ResGetBindingAddress.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    retval = json['retval'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (retval != null)
      json['retval'] = retval;
    return json;
  }

  static List<ResGetBindingAddress> listFromJson(List<dynamic> json) {
    return json == null ? List<ResGetBindingAddress>() : json.map((value) => ResGetBindingAddress.fromJson(value)).toList();
  }

  static Map<String, ResGetBindingAddress> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ResGetBindingAddress>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ResGetBindingAddress.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ResGetBindingAddress-objects as value to a dart map
  static Map<String, List<ResGetBindingAddress>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ResGetBindingAddress>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ResGetBindingAddress.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqSendStatusString {
  
  ChatId id = null;
  
  String statusString = null;
  ReqSendStatusString();

  @override
  String toString() {
    return 'ReqSendStatusString[id=$id, statusString=$statusString, ]';
  }

  ReqSendStatusString.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    id = (json['id'] == null) ?
      null :
      ChatId.fromJson(json['id']);
    statusString = json['status_string'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (id != null)
      json['id'] = id;
    if (statusString != null)
      json['status_string'] = statusString;
    return json;
  }

  static List<ReqSendStatusString> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqSendStatusString>() : json.map((value) => ReqSendStatusString.fromJson(value)).toList();
  }

  static Map<String, ReqSendStatusString> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqSendStatusString>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqSendStatusString.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqSendStatusString-objects as value to a dart map
  static Map<String, List<ReqSendStatusString>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqSendStatusString>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqSendStatusString.listFromJson(value);
       });
     }
     return map;
  }
}


part of openapi.api;

class ReqGetMessage {
  
  String msgId = null;
  ReqGetMessage();

  @override
  String toString() {
    return 'ReqGetMessage[msgId=$msgId, ]';
  }

  ReqGetMessage.fromJson(Map<String, dynamic> json) {
    if (json == null) return;
    msgId = json['msgId'];
  }

  Map<String, dynamic> toJson() {
    Map <String, dynamic> json = {};
    if (msgId != null)
      json['msgId'] = msgId;
    return json;
  }

  static List<ReqGetMessage> listFromJson(List<dynamic> json) {
    return json == null ? List<ReqGetMessage>() : json.map((value) => ReqGetMessage.fromJson(value)).toList();
  }

  static Map<String, ReqGetMessage> mapFromJson(Map<String, dynamic> json) {
    var map = Map<String, ReqGetMessage>();
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic value) => map[key] = ReqGetMessage.fromJson(value));
    }
    return map;
  }

  // maps a json object with a list of ReqGetMessage-objects as value to a dart map
  static Map<String, List<ReqGetMessage>> mapListFromJson(Map<String, dynamic> json) {
    var map = Map<String, List<ReqGetMessage>>();
     if (json != null && json.isNotEmpty) {
       json.forEach((String key, dynamic value) {
         map[key] = ReqGetMessage.listFromJson(value);
       });
     }
     return map;
  }
}

